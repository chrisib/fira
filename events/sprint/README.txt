SPRINT
Author (of this readme): Josh Jung

The bulk of this code was written for FIRA 2012.  At that time, the target used had 2 colours, one 25 cm behind the other, with position in the lane being estimated by the parallax between them.

In 2013, the code was modified to make use of a new target which, in addition to the 2 colours, had a third laid horizontally between them to give the stopping distance.  The steering and gait were also heavily modified to prioritize reliability.

At FIRA 2013, the SNOBOTS finished fourth overall with a time of ~38 seconds.  The world record that year was ~25 seconds.  The forward gait was very fast and performed well, but the backwards gait was slow for the sake of reliability.  This is the most important thing to improve.  It is important to note that, however, that every sprint during the competition was performed correctly on the first try.

Vision is processed continuously in a separate thread from the main state machine.

No command line arguments are necessary for a competitive run.

While the program is running, pushing the left button will return the robot to the idle state.  From here, pushing the middle button will restart the forward leg.

There are some important things to calibrate before running:
Vision - Standard calibration of the target using DarwinViewer.  The colour assigned to each panel is set in config.ini.  Be sure to calibrate from multiple distances and angles.
Walking Config - Configuration is split into Forward and Backward legs.  Do each separately.  The main things to change are starting_hip_pitch_offset and max_hip_pitch_offset, though they are set equal within both legs as of this writing.
Under the Sprint header:
    Forward - X-amplitude for the forward leg
    Backward - X-amplitude for the backward leg
    ForwardDefaultTurn - A-amplitude for the forward leg intended to counter wandering in the gait
    BackwardDefaultTurn - A-amplitude for the backward leg intended to counter wandering in the gait
    HeadTilt - Initial angle the head will be tilted at (this is adjusted dynamically)
    TurnP - A-amplitude PID value
    TurnD - A-amplitude PID value
    SlideP - Y-amplitude PID value
    SlideD - Y-amplitude PID value
    MaxTurn - Maximum A-amplitude allowed (not including default offset)
    MaxSlide - Maximum Y-amplitude allowed
    MidHeightForReverse - Height (along Y-axis) in pixels of the middle panel that the robot must see before it starts slowing down, then backing up
    MidHeightForLookDown - Height in pixels of the midel panel that the robot must see before it halves its speed and looks down
    MaxLookDownSteps - If the required number of pixels have not been seen in the look down state by the time this number of steps have been taken, start backing up regardless of vision
    LookDownHeadTilt - Head tilt angle when looking down near target
    ForwardSlideYFactor - Factor determining how much the parallax determines Y-amplitude for the forward leg
    ForwardSlideAFactor - Factor determining how much the parallax determines A-amplitude for the forward leg
    ForwardTurnAFactor - Factor determining how much the target's average X-axis position in the frame determines A-amplitude for the forward leg
    BackSlideYFactor - Factor determining how much the parallax determines Y-amplitude for the backward leg
    BackSlideAFactor - Factor determining how much the parallax determines A-amplitude for the backward leg
    BackTurnAFactor - Factor determining how much the target's average X-axis position in the frame determines A-amplitude for the backward leg

The state machine has 8 states:
IDLE - Stand there.
INIT_FORWARD - Prepare the robot to begin the forward leg
INIT_BACKWARD - Prepare the robot to begin the backward leg
MOVING_FORWARD - Advance along the forward leg
MOVING_BACKWARD - Advance along the backward leg
LOOKING_DOWN - When close to the target, angle the head down and reduce speed to get a better look at the middle panel
SLOWING_DOWN - Once the enough of the middle panel has been seen in the looking down state, slow forward speed until it is safe to stop
STOPPING - Stop forward motion before beginning the backward leg

For next year, I reccomend speeding up the backwards gait.  As the gait is adjusted, the various slide and turn factors will have to be adjusted as well.  It would be best if reliability were not sacrificed.  The world record setting team ended up finishing behind us in 2013 because, when it counted, they wandered out of their lane.
