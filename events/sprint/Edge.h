/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Diana Carrier
 *
 * This class is used to represent the edge in a graph. Developed for use 
 * with the sprint event.
 ************************************************************************/

#ifndef EDGE_H
#define EDGE_H

#include "BoundingBox.h"

namespace Robot
{
	class Edge
	{
	public:
		Edge();
		Edge(BoundingBox *a, BoundingBox *b, int dist);
		~Edge();
	
		int distance;
		BoundingBox *box1;
		BoundingBox *box2;

		// comparison for sorting
		// sorts into ascending order by distance
		static bool Compare(Edge *a, Edge *b);
	};

	class EdgeCompare
	{
	public:
		EdgeCompare();
		~EdgeCompare();
	
		bool operator() (const Edge& lhs, const Edge& rhs) const;
	};
}

#endif // EDGE_H