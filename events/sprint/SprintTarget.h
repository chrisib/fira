/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton <chrisib@cs.umanitoba.ca, ve4cib@gmail.com>
 * Diana Carrier
 *
 * This class is used to locate the sprint target in a frame.
 * The sprint target is a two-panel target that looks like this (in ascii art):
 *
 *       FRONT
 * +----------------+
 * |  ############  |
 * |  ############  |
 * |  ############  |
 * |                |
 * |  ############  |
 * |  ############  |
 * |  ############  |
 * +----------------+
 *
 *    SIDE
 *  |
 *  #
 *  #
 *  #
 *  +-------#
 *  |       #
 *  +-------#
 *
 * The # symbols indicate configurable coloured pan	Loading configs for target panels...
els (may be -- and likely
 * should be -- distinct colours).  The entire target must fit within a
 * 30x30x30cm cube.
 *
 * The target is detected by finding the two panels of the appropriate colours
 * that are the closest together and in the correct orientation.
 *
 * The difference between the centre points of each box is used to calculate
 * angular error towards the face of the target.  Due to parallax the top
 * panel will shift less than the front one when offset to one side.
 *
 * If no objects of the appropriate size and orientation are found then
 * the target is assumed to not be visible.
 *
 ************************************************************************/

 #ifndef SPRINTTARGET_H
 #define SPRINTTARGET_H 

#include "SingleBlob.h"
#include "MultiBlob.h"
#include <string>
#include <iostream>
#include <vector>

#define BOTTOM_OF_TOP_PANEL_HEIGHT 126
#define A_LARGE_NUMBER 999999
 
namespace Robot
{
    class SprintTarget : public SingleBlob
	{
		public:
            SprintTarget();
            SprintTarget(const char* name);
	        ~SprintTarget(); 

            // load config settings
            // same as SingleTarget with additions:
            // - TopColour : the name of the config section with the YUV settings & proportions for the top panel
            // - BottomColour : the name of the config section with the YUV settings & proporions for the bottom panel
            // - DistanceThreshold: see DISTANCE_THRESHOLD, below
	        virtual void LoadIniSettings(minIni &ini, const std::string &section);

	        // print the name, current position, and pixel sizes of the target in the last frame examined
	        virtual void Print();

	        // draw the target's bounding boxes on the provided image
	        virtual void Draw(cv::Mat &image);

	        // locate this target on the image;
            virtual int FindInFrame(CameraPosition *cameraPosition, cv::Mat &image, cv::Mat *dest=NULL);

            virtual void ProcessCandidates();

            // get the the error to approximate how far off-center we are
            // these are the last-known values if the target was not found in the latest frame
            double GetSlopeError();
            double GetPositionError();
            double GetRange();

            //return the height (in pixels) of the middle panel
            int GetMidPanelHeight();

            //reset the target, as if nothing has been found
            void Reset();

            //return true if the target was found in the last frame
            bool GetFoundInLastFrame();

      	private:
            void Init(const char *name);
            double CalculateSlope(Point2D &top, Point2D &bottom);
            double CalculateCentre();

            MultiBlob *topPanelCandidates;
            MultiBlob *midPanelCandidates;
            MultiBlob *bottomPanelCandidates;
            std::vector<BlobTarget*> candidates;

            BoundingBox *topPanel;
            BoundingBox *midPanel;
            BoundingBox *bottomPanel;

            // the slope between the top and bottom panels
            // zero slope indicates vertical; horizontal slope is undefined
            // (i.e. we calculate cartesean slope sideways)
            double slope;

            // the x-position of the centre of the target
            double position;

            //distance from the robot to the back of the target
            double range;

            // the maximum distance two candidate panels can have from each other
            // before we consider them to be too far apart
            double DISTANCE_THRESHOLD;
	};
}

 #endif
