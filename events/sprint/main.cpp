#include <LinuxDARwIn.h>
#include <iostream>
#include <darwin/framework/Voice.h>
#include <darwin/framework/Target.h>
#include <darwin/linux/CvCamera.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/LeftArm.h>
#include <cstring>
#include <vector>
#include <ctime>
#include <cmath>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include "MultiBlob.h"
#include "SprintTarget.h"
#include <darwin/framework/MotionStatus.h>
#include <pthread.h>

using namespace Robot;
using namespace std;
using namespace cv;

minIni *ini = new minIni("config.ini");

// for turning
double forwardDefaultTurn;
double backwardDefaultTurn;
double turnP = 0.0;
double turnD = 0.0;
double slideP = 0.0;
double slideD = 0.0;
double MAX_TURN = 20.0;
double MAX_SLIDE = 10.0;
double previousTurnError = 0.0;
double currentTurnError = 0.0;
double previousSlideError = 0.0;
double currentSlideError = 0.0;
int MID_HEIGHT_FOR_REVERSE = 200;
int MID_HEIGHT_FOR_LOOK_DOWN = 40;
int MAX_LOOK_DOWN_STEPS = 5;
double LOOK_DOWN_HEAD_TILT = -10;
double FORWARD_SLIDE_Y_FACTOR = 1;
double FORWARD_SLIDE_A_FACTOR = 0.5;
double FORWARD_TURN_A_FACTOR = 1;
double BACK_SLIDE_Y_FACTOR = 1;
double BACK_SLIDE_A_FACTOR = 0.25;
double BACK_TURN_A_FACTOR = 0;
double LOOK_DOWN_DISTANCE = 600;
double midPanelLength = 0;

//toggle between looking for the whole target or only the blue part
bool lookForMiddleOnly = false;

// how far and how fast to run
int stepsRemaining;
int targetSpeed;
int startSteps;

// body settings
int headTilt;
double maxHipPitch;

// vision
SprintTarget sprintTarget("Sprint Target");
SingleBlob middleOnly("Middle Only");
int targetOffset = 0;
int imgCenter = Camera::WIDTH/2;
bool frameUsed = false; // false when the a_amplitude has not been adjusted based on the current frame

// for recording video
VideoWriter *rawStream = NULL;
VideoWriter *processedStream = NULL;

enum
{
	IDLE,
	INIT_FORWARD,
	MOVING_FORWARD,
    LOOKING_DOWN,
	SLOWING_DOWN,
	STOPPING,
	INIT_BACKWARD,
	MOVING_BACKWARD
};
int state = IDLE; 

// command line parameters
bool moving_forward = true;
bool noDisplay = true;
bool visTest = false;
bool record = false;
bool startBackwards = false;
bool enableLog = false;
bool debugPrint = false;

// to run the video in another thread
pthread_t videoThreadId;

// for steering with gyro data
int totalGyro = 0;
int lastZValue = 1000;
int turnSteps, currentSteps;

// controls acceleration
double speedUp = 5;

const char* state2txt(int state)
{
    switch(state)
    {
    case IDLE:
        return "Idle";
    case INIT_FORWARD:
        return "Init Forward";
    case INIT_BACKWARD:
        return "Init Backward";
    case MOVING_FORWARD:
        return "Moving Forward";
    case MOVING_BACKWARD:
        return "Moving Backward";
    case STOPPING:
        return "Stopping";
    case SLOWING_DOWN:
        return "Slowing Down";
    case LOOKING_DOWN:
        return "Looking Down";
    default:
        return "Unknown State";
    }
}

void waitAMoment()
{
    volatile int j;
    for(int i=0;i<30000000;i++){j++;}
}

void handleVideo()
{
    CameraPosition cameraPosition = CameraPosition();
    CvCamera::GetInstance()->RGB_ENABLE = !noDisplay || visTest;
    CvCamera::GetInstance()->CaptureFrame();
    static Mat raw = Mat::zeros(CvCamera::GetInstance()->yuvFrame.rows,CvCamera::GetInstance()->yuvFrame.cols,CvCamera::GetInstance()->yuvFrame.type());
    static Mat processed = Mat::zeros(CvCamera::GetInstance()->yuvFrame.rows,CvCamera::GetInstance()->yuvFrame.cols,CvCamera::GetInstance()->yuvFrame.type());
    raw = CvCamera::GetInstance()->yuvFrame;

    if(!lookForMiddleOnly)
    {
        midPanelLength = 0;

        sprintTarget.FindInFrame(&cameraPosition, raw, &processed);

        if (!noDisplay)
        {
            //sprintTarget.Print();
            sprintTarget.Draw(CvCamera::GetInstance()->rgbFrame);
            sprintTarget.Draw(processed);

            char buffer[255];
            sprintf(buffer,"%d of %d (state: %s)", Walking::GetInstance()->GetStepCount(), stepsRemaining, state2txt(state));
            cv::Scalar colour = cv::Scalar(255,255,255);
            cv::Point pt = cv::Point(0,12);
            cv::putText(CvCamera::GetInstance()->rgbFrame, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(processed, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

            pt = cv::Point(0,Camera::HEIGHT-16);
            sprintf(buffer,"X=%0.2f A=%0.2f Y=%0.2f", Walking::GetInstance()->X_MOVE_AMPLITUDE, Walking::GetInstance()->A_MOVE_AMPLITUDE, Walking::GetInstance()->Y_MOVE_AMPLITUDE);
            cv::putText(CvCamera::GetInstance()->rgbFrame,buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(processed,buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

            sprintf(buffer,"(eA=%0.2f eY=%0.2f)", sprintTarget.GetPositionError(), sprintTarget.GetSlopeError());
            pt = cv::Point(0,Camera::HEIGHT-4);
            cv::putText(CvCamera::GetInstance()->rgbFrame, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(processed, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        }
    }
    else
    {
        middleOnly.FindInFrame(raw, &processed);
        if(middleOnly.WasFound())
        {
            midPanelLength = middleOnly.GetBoundingBox()->center.Y + middleOnly.GetBoundingBox()->height/2.0;
        }
    }

    frameUsed = false;

	if (!noDisplay)
	{
        cv::imshow("Raw",CvCamera::GetInstance()->rgbFrame);
		cv::imshow("Processed",processed);
		cv::waitKey(1);
	}

	if(record)
    {
        (*rawStream) << CvCamera::GetInstance()->rgbFrame;
        (*processedStream) << processed;
	}
}

void pauseThread()
{
	pthread_yield();
}

void* videoLoop(void* arg)
{
    (void)arg;
    for(;;)
    {
		handleVideo();
		
		// sleep 5 ms
        //MotionManager::GetInstance()->msleep(5,pauseThread);
	}

    pthread_exit(NULL);
    return NULL; // Unreachable
}

void handleFall()
{
	if(MotionStatus::FALLEN != STANDUP)
    {
        Walking::GetInstance()->Stop();
        while(Walking::GetInstance()->IsRunning())
			usleep(1000);

        Action::GetInstance()->m_Joint.SetEnableBody(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

        Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
        Action::GetInstance()->Finish();

        if(MotionStatus::FALLEN == FORWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_F_UP);   // FORWARD GETUP
        else if(MotionStatus::FALLEN == BACKWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_B_UP);   // BACKWARD GETUP

        while(Action::GetInstance()->IsRunning())
			usleep(1000);
			
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Walking::GetInstance()->X_MOVE_AMPLITUDE = 2;
        Walking::GetInstance()->A_MOVE_AMPLITUDE = forwardDefaultTurn;
        speedUp = 5;
        /*stepsRemaining = stepsRemaining - Walking::GetInstance()->GetStepCount();
        if(stepsRemaining <= 0) {
			state = INIT_BACKWARD;
		} else {
			Walking::GetInstance()->Start(stepsRemaining);
        }*/
        if(state == INIT_FORWARD || state == MOVING_FORWARD)
            state = INIT_FORWARD;
        else if(state == INIT_BACKWARD || state == MOVING_BACKWARD || state == SLOWING_DOWN || state == STOPPING)
            state = INIT_BACKWARD;
        else
            state = IDLE;
        turnSteps = 0;
    }
}

void handleButton()
{
	static int lastButton = MotionStatus::BUTTON;
	
	if(MotionStatus::BUTTON==lastButton)
		return;
	else
	{
		lastButton = MotionStatus::BUTTON;
		
		if(lastButton == CM730::MIDDLE_BUTTON)
		{
			cout << "Loading INI settings..." << endl;
			sprintTarget.LoadIniSettings(*ini,"Target");
			stepsRemaining = ini->geti("Sprint", "StepsRemaining", -2);
			if (stepsRemaining < -1) {
				cout << "Wrong number of steps to take" << endl;
				exit(EXIT_FAILURE);
			}
			forwardDefaultTurn = ini->getd("Sprint","ForwardDefaultTurn",0);
			backwardDefaultTurn = ini->getd("Sprint","BackwardDefaultTurn",0);
			turnP = ini->getd("Sprint","TurnP",0.0);
			turnD = ini->getd("Sprint","TurnD",0.0);
			slideP = ini->getd("Sprint","SlideP",0.0);
			slideD = ini->getd("Sprint","SlideD",0.0);
			MAX_TURN = ini->getd("Sprint","MaxTurn",20.0);
			MAX_SLIDE = ini->getd("Sprint","MaxSlide",10.0);
            MID_HEIGHT_FOR_REVERSE = ini->getd("Sprint","MidHeightForReverse",10);
			cout << "Done" << endl;
			speedUp = 5;
			state = INIT_FORWARD; // change back to INIT_FORWARD for proper functioning
		}
		else if(state!=IDLE && lastButton == CM730::LEFT_BUTTON)
		{
            sprintTarget.Reset();
			Walking::GetInstance()->Stop();
			Walking::GetInstance()->Finish();
			state = IDLE;
		}
	}
}

void initialize()
{
	cout << "Initializing Camera..." << endl;
    CvCamera::GetInstance()->Initialize(0);
    CvCamera::GetInstance()->LoadINISettings(ini);
    CameraSettings cs = CvCamera::GetInstance()->GetCameraSettings();
    cs.autoExposure = false;
    CvCamera::GetInstance()->SetCameraSettings(cs);
	if(!noDisplay)
	{
		cv::namedWindow("Raw",1);
		cv::namedWindow("Processed",1);
		cvMoveWindow("Raw",0,20);
        cvMoveWindow("Processed",0,290);
	}
	cout << "Done" << endl;

	cout << "Loading INI settings..." << endl;
    sprintTarget.LoadIniSettings(*ini,"Target");
    middleOnly.LoadIniSettings(*ini,"Green");
    stepsRemaining = ini->geti("Sprint", "StepsRemaining", -2);
	if (stepsRemaining < -1) {
		cout << "Wrong number of steps to take" << endl;
		exit(EXIT_FAILURE);
	}
	forwardDefaultTurn = ini->getd("Sprint","ForwardDefaultTurn",0);
	backwardDefaultTurn = ini->getd("Sprint","BackwardDefaultTurn",0);
	turnP = ini->getd("Sprint","TurnP",0.0);
	turnD = ini->getd("Sprint","TurnD",0.0);
	slideP = ini->getd("Sprint","SlideP",0.0);
	slideD = ini->getd("Sprint","SlideD",0.0);
	MAX_TURN = ini->getd("Sprint","MaxTurn",20.0);
	MAX_SLIDE = ini->getd("Sprint","MaxSlide",10.0);
    MID_HEIGHT_FOR_REVERSE = ini->getd("Sprint","MidHeightForReverse",200);
    MID_HEIGHT_FOR_LOOK_DOWN = ini->getd("Sprint","MidHeightForLookDown",40);
    MAX_LOOK_DOWN_STEPS = ini->getd("Sprint","MaxLookDownSteps",5);
    LOOK_DOWN_HEAD_TILT = ini->getd("Sprint","LookDownHeadTilt", -10);
    FORWARD_SLIDE_Y_FACTOR = ini->getd("Sprint","ForwardSlideYFactor",1);
    FORWARD_SLIDE_A_FACTOR = ini->getd("Sprint","ForwardSlideAFactor",0.5);
    FORWARD_TURN_A_FACTOR = ini->getd("Sprint","ForwardTurnAFactor",1);
    BACK_SLIDE_Y_FACTOR = ini->getd("Sprint","BackSlideYFactor",1);
    BACK_SLIDE_A_FACTOR = ini->getd("Sprint","BackSlideAFactor",0.25);
    BACK_TURN_A_FACTOR = ini->getd("Sprint","BackTurnAFactor",0);
    cout << "Done" << endl;
	
	if(!visTest)
	{
		cout << "Initializing Framework..." << endl;
		LinuxDARwIn::ChangeCurrentDir();
		LinuxDARwIn::Initialize("motion_4096.bin",Action::DEFAULT_MOTION_SIT_DOWN);
	    //LinuxDARwIn::InitializeSignals();
	    Voice::Initialize("default");//"en-us+f3");
		cout << "done" << endl << endl;
	    
	    cout << "Standing up..." << endl;
	    Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
		while(Action::GetInstance()->IsRunning())
			usleep(8000);
	    cout << "done" << endl << endl;

		cout << "Setting up Walking module..." << endl;
		Walking::GetInstance()->m_Joint.SetEnableBody(false);
		Walking::GetInstance()->m_Joint.SetEnableLowerBody(true,true);
		RightArm::GetInstance()->m_Joint.SetEnableBody(false);
		RightArm::GetInstance()->m_Joint.SetEnableRightHandOnly(true,true);
		LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
		LeftArm::GetInstance()->m_Joint.SetEnableLeftHandOnly(true,true);
		Head::GetInstance()->m_Joint.SetEnableBody(false);
		Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
		MotionManager::GetInstance()->AddModule(Walking::GetInstance());
		MotionManager::GetInstance()->AddModule(Head::GetInstance());
		MotionManager::GetInstance()->AddModule(RightArm::GetInstance());
		MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
		headTilt = ini->geti("Sprint","HeadTilt",30);
		Head::GetInstance()->MoveByAngle(0,headTilt);
		RightArm::GetInstance()->RelaxHand();
		LeftArm::GetInstance()->RelaxHand();
        Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Forward","starting_hip_pitch_offset");
		cout << "done" << endl << endl;

        if(enableLog)
            MotionManager::GetInstance()->StartLogging();
	}
	
	if (record)
	{
		cout << "creating video streams" << endl;
        rawStream = new VideoWriter (ini->gets("Files","Raw","raw.avi"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        processedStream = new VideoWriter (ini->gets("Files","Processed","processed.avi"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
    }
}

inline void accelerate()
{
	static int stepCounter = 0;
	int currentStepCount;
	double nextAmplitude;
	
	currentStepCount = Walking::GetInstance()->GetStepCount();
	if(currentStepCount != stepCounter)
	{
		stepCounter = currentStepCount;
	
		if( (targetSpeed < 0 && Walking::GetInstance()->X_MOVE_AMPLITUDE > targetSpeed) ||
			(targetSpeed > 0 && Walking::GetInstance()->X_MOVE_AMPLITUDE < targetSpeed)
		)
		{		
            nextAmplitude = Walking::GetInstance()->X_MOVE_AMPLITUDE * 2;	// TODO: configurable gain
			if( (nextAmplitude > targetSpeed && targetSpeed > 0) || (nextAmplitude < targetSpeed && targetSpeed < 0) )
			{
				nextAmplitude = targetSpeed;
			}
			Walking::GetInstance()->X_MOVE_AMPLITUDE = nextAmplitude;
		}
		
		if(Walking::GetInstance()->HIP_PITCH_OFFSET > maxHipPitch && state == MOVING_FORWARD)
		{
            Walking::GetInstance()->HIP_PITCH_OFFSET += 0.15;
		}
		else if(Walking::GetInstance()->HIP_PITCH_OFFSET < maxHipPitch && state == MOVING_BACKWARD)
		{
            Walking::GetInstance()->HIP_PITCH_OFFSET -= 0.15;
		}
	}
}

void tiltHead()
{
    if(sprintTarget.GetFoundInLastFrame() && sprintTarget.GetBoundingBox()->center.Y > Camera::HEIGHT*0.6)
    {
        headTilt -= 2;
        Head::GetInstance()->MoveByAngle(0,headTilt);
        //cout<<"TILTING DOWN"<<endl;
    }
    else if(sprintTarget.GetFoundInLastFrame() && sprintTarget.GetBoundingBox()->center.Y < Camera::HEIGHT*0.4)
    {
        headTilt += 2;
        Head::GetInstance()->MoveByAngle(0,headTilt);
        //cout<<"TILTING UP"<<endl;
    }
}

inline void steerWithVision_forward()
{
    // use PD-controller to adjust A and Y amplitudes to compensate for error
    previousSlideError = currentSlideError;
    currentSlideError = sprintTarget.GetSlopeError();

    previousTurnError = currentTurnError;
    currentTurnError = sprintTarget.GetPositionError();

    //cout<<"FROM TURN "<<MAX_TURN * currentTurnError * turnP + MAX_TURN * previousTurnError * turnD<<endl;
    //cout<<"FROM SLIDE "<<0.5*(MAX_TURN * currentSlideError * slideP + MAX_TURN * previousSlideError * slideD)<<endl;

    double aAmplitude = FORWARD_TURN_A_FACTOR * (MAX_TURN * currentTurnError * turnP + MAX_TURN * previousTurnError * turnD) + FORWARD_SLIDE_A_FACTOR *(MAX_TURN * currentSlideError * slideP + MAX_TURN * previousSlideError * slideD);
    double yAmplitude = FORWARD_SLIDE_Y_FACTOR * (MAX_SLIDE * currentSlideError * slideP + MAX_SLIDE * previousSlideError * slideD);

    if(aAmplitude > MAX_TURN)
        aAmplitude = MAX_TURN;
    else if(aAmplitude < -MAX_TURN)
        aAmplitude = -MAX_TURN;

    if(yAmplitude > MAX_SLIDE)
        yAmplitude = MAX_SLIDE;
    else if(yAmplitude < -MAX_SLIDE)
        yAmplitude = -MAX_SLIDE;

    Walking::GetInstance()->A_MOVE_AMPLITUDE = aAmplitude + forwardDefaultTurn; // add the default turn
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = yAmplitude;
    tiltHead();
}

inline void steerWithVision_backward()
{
    // use PD-controller to adjust A amplitude to compensate for error
    previousSlideError = currentSlideError;
    currentSlideError = sprintTarget.GetSlopeError();
    
    previousTurnError = currentTurnError;
    currentTurnError = sprintTarget.GetPositionError();

    //double aAmplitude = -MAX_TURN * currentTurnError * turnP + MAX_TURN * previousTurnError * turnD;
    double aAmplitude = BACK_TURN_A_FACTOR * (MAX_TURN * currentTurnError * turnP + MAX_TURN * previousTurnError * turnD) - BACK_SLIDE_A_FACTOR*(MAX_TURN * currentSlideError * slideP + MAX_TURN * previousSlideError * slideD);
    double yAmplitude = BACK_SLIDE_Y_FACTOR * (MAX_SLIDE * currentSlideError * slideP + MAX_SLIDE * previousSlideError * slideD);

    if(aAmplitude > MAX_TURN)
        aAmplitude = MAX_TURN;
    else if(aAmplitude < -MAX_TURN)
        aAmplitude = -MAX_TURN;

    if(yAmplitude > MAX_SLIDE)
        yAmplitude = MAX_SLIDE;
    else if(yAmplitude < -MAX_SLIDE)
        yAmplitude = -MAX_SLIDE;

    Walking::GetInstance()->A_MOVE_AMPLITUDE = aAmplitude + backwardDefaultTurn; // add the default turn
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = yAmplitude;
    tiltHead();

    //cout<<"A AMPLITUDE: "<<Walking::GetInstance()->A_MOVE_AMPLITUDE<<endl;
}

inline void process_initForward()
{
    lookForMiddleOnly = false;

	Walking::GetInstance()->LoadINISettings(ini,"Forward");
	targetSpeed = ini->geti("Sprint","Forward");
	Walking::GetInstance()->X_MOVE_AMPLITUDE = 2;
    Walking::GetInstance()->Z_MOVE_AMPLITUDE = ini->getd("Forward","z_amplitude");
    Walking::GetInstance()->A_MOVE_AMPLITUDE = forwardDefaultTurn;
	maxHipPitch = ini->getd("Forward","max_hip_pitch_offset");
	Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Forward","starting_hip_pitch_offset");
    //Walking::GetInstance()->Start(stepsRemaining);
    headTilt = ini->geti("Sprint","HeadTilt",30);
    Head::GetInstance()->MoveByAngle(0,headTilt);
    Walking::GetInstance()->Start();
	state = MOVING_FORWARD;
	//startTime = time(NULL);
	
    //Voice::Speak("Sprinting forward");
	cout << "Sprinting forward" << endl;
}

inline void process_initBackward()
{
    lookForMiddleOnly = false;

	Walking::GetInstance()->LoadINISettings(ini,"Backward");
	targetSpeed = ini->geti("Sprint","Backward");
	Walking::GetInstance()->X_MOVE_AMPLITUDE = -2;
    Walking::GetInstance()->Z_MOVE_AMPLITUDE = ini->getd("Backward","z_amplitude");
    Walking::GetInstance()->A_MOVE_AMPLITUDE = backwardDefaultTurn;
	maxHipPitch = ini->getd("Backward","max_hip_pitch_offset");
	Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Backward","starting_hip_pitch_offset");
    headTilt = ini->geti("Sprint","HeadTilt",30);
    Head::GetInstance()->MoveByAngle(0,headTilt);
	Walking::GetInstance()->Start();
	state = MOVING_BACKWARD;
	//startTime = time(NULL);
	
    //Voice::Speak("Sprinting backwards");
	cout << "Sprinting backward" << endl;
	stepsRemaining = -1;
	speedUp = -5;
}

inline void process_movingForward()
{
    lookForMiddleOnly = false;

    //cout<<sprintTarget.GetMidPanelHeight()<<endl;
    //if (Walking::GetInstance()->GetStepCount() >= stepsRemaining || stepsRemaining <= 0)
    if(debugPrint)
        cout << "RANGE TO TARGET: " << sprintTarget.GetMidPanelHeight() << endl;

    //if(sprintTarget.GetRange() <= LOOK_DOWN_DISTANCE)
    if(debugPrint)
        cout << "MID PANEL HEIGHT: " << sprintTarget.GetMidPanelHeight() << endl;
    if(sprintTarget.GetMidPanelHeight() >= MID_HEIGHT_FOR_LOOK_DOWN)
    {
        headTilt = LOOK_DOWN_HEAD_TILT;
        Walking::GetInstance()->X_MOVE_AMPLITUDE /= 2.0;
        Walking::GetInstance()->HIP_PITCH_OFFSET -= 5;
        Head::GetInstance()->MoveByAngle(0,headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();
        waitAMoment();
        state = LOOKING_DOWN;
        startSteps = Walking::GetInstance()->GetStepCount();
    }
	else
	{ 
		accelerate();
        if (/*sprintTarget.WasFound() &&*/ !frameUsed)
		{
			steerWithVision_forward();
            frameUsed = true;
		}
	}
}

inline void process_movingBackward()
{
    lookForMiddleOnly = false;

	accelerate();
	
    if (/*sprintTarget.WasFound() &&*/ !frameUsed)
	{
		steerWithVision_backward();
        frameUsed = true;
	}
}

inline void process_lookingDown()
{
    int currSteps = Walking::GetInstance()->GetStepCount();

    lookForMiddleOnly = true;

    if(debugPrint)
        cout << "MID PANEL HEIGHT: " << midPanelLength << endl;

    if(midPanelLength >= MID_HEIGHT_FOR_REVERSE || currSteps-startSteps >= MAX_LOOK_DOWN_STEPS)
    {
        cout << "Slowing down" << endl;
        //Voice::Speak("Slowing down");
        state = SLOWING_DOWN;
    }
}

inline void process_slowingDown()
{
	if(Walking::GetInstance()->PERIOD_TIME > 200)
        Walking::GetInstance()->PERIOD_TIME-= (int) (Walking::GetInstance()->PERIOD_TIME*0.05); //*0.1);
	
	if(Walking::GetInstance()->X_MOVE_AMPLITUDE > 4) //10
            Walking::GetInstance()->X_MOVE_AMPLITUDE-= Walking::GetInstance()->X_MOVE_AMPLITUDE*0.05;    //-= 2;
	else
	{
		cout << "Stopping" << endl;
        //Voice::Speak("Stopping");
		state = STOPPING;
		Walking::GetInstance()->Stop();
	}
}

inline void process_stopping()
{
	if(!Walking::GetInstance()->IsRunning())
	{
		MotionManager::msleep(1000, NULL); // wait for motion to stop
		state = INIT_BACKWARD;
	}
}

void process()
{
    //cout << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl;
	//cout << "\t\t\tState: " << state << endl;
	switch(state)
	{
		case INIT_FORWARD:
			process_initForward();
		break;
		
		case INIT_BACKWARD:
			process_initBackward();
		break;
		
		case MOVING_FORWARD:
			process_movingForward();
		break;
			
		case MOVING_BACKWARD:
			process_movingBackward();
		break;

        case LOOKING_DOWN:
            process_lookingDown();
        break;
			
		case SLOWING_DOWN:
			process_slowingDown();
		break;
			
		case STOPPING:
			process_stopping();
		break;
		
		case IDLE:
		default:
			// do nothing
		break;
	}//switch
	
	if(state!=IDLE)
	{
		//handleVideo();
		handleFall();
	}
	handleButton();
	MotionManager::Sync();
}// process


int main(int argc, char** argv)
{	
	for(int i=1; i<argc; i++)
	{
		if(!strcmp(argv[i],"--show-video"))
			noDisplay = false;
		else if (!strcmp(argv[i],"--vision-test"))
        {
			visTest = true;
            noDisplay = false;
        }
		else if (!strcmp(argv[i],"--record-video"))
			record = true;
        else if(!strcmp(argv[i],"--backwards"))
            startBackwards = true;
        else if(!strcmp(argv[i],"--log"))
            enableLog = true;
        else if(!strcmp(argv[i],"--debug"))
            debugPrint = true;
		else if (!strcmp(argv[i],"--help"))
		{
            printf("Usage:\n\tsprint [--show-video] [--vision-test] [--record-video] [--backwards] [--log] [--help]\n\n");
			cout << "--show-video runs sprint with video for debugging." << endl;
			cout << "--vision-test runs only the vision portion of the sprint." << endl;
            cout << "--backwards starts the program in the moving backwards state." << endl;
            cout << "--log enables MotionManager logging" << endl;
            cout << "--debug enables extra console debug printing" << endl;
			cout << "--record-video records video." << endl;
			exit(EXIT_SUCCESS);
		}
		else
		{
			printf("Usage:\n\tsprint [--no-video] [--vision-test] [--record-video] [--help]\n\n");
			exit(EXIT_FAILURE);
		}
	}
	initialize();
    if(startBackwards)
        state = INIT_BACKWARD;

	if (visTest)
	{
        if(startBackwards)
            state = MOVING_BACKWARD;
        else
            state = MOVING_FORWARD;

		for (;;)
		{
			handleVideo();
            if(sprintTarget.WasFound() && !frameUsed)
            {
                if(startBackwards)
                {
                    steerWithVision_backward();
                }
                else
                {
                    steerWithVision_forward();
                }
                frameUsed = true;
            }
		}
	}
	else
	{
		// start vision processing in new thread
		pthread_create(&videoThreadId, NULL, &videoLoop, NULL);
		
	    Voice::Speak("Press the middle button");
	    cout << "Press the middle button" << endl;
	    
	    for(;;)
	    {
			process();
		}
	}
	return 0;
} //main
