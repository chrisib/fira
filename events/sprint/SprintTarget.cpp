/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Diana Carrier
 *
 * This class is used to locate the sprint target in a frame.
 ************************************************************************/

#include "SprintTarget.h"
#include <darwin/linux/YuvCamera.h>
#include "Point.h"

using namespace Robot;
using namespace std;
using namespace cv;

SprintTarget::SprintTarget() : SingleBlob()
{
    Init("Unnamed Sprint Target");
}

SprintTarget::SprintTarget(const char* name) : SingleBlob()
{
    Init(name);
}

SprintTarget::~SprintTarget()
{
    delete topPanelCandidates;
    delete midPanelCandidates;
    delete bottomPanelCandidates;
}

void SprintTarget::Init(const char* name)
{
    SingleBlob::Init(name);
    topPanelCandidates = new MultiBlob("Top");
    midPanelCandidates = new MultiBlob("Middle");
    bottomPanelCandidates = new MultiBlob("Bottom");
    candidates.push_back(topPanelCandidates);
    candidates.push_back(midPanelCandidates);
    candidates.push_back(bottomPanelCandidates);

    topPanel = NULL;
    midPanel = NULL;
    bottomPanel = NULL;

    range = A_LARGE_NUMBER;
}

void SprintTarget::LoadIniSettings(minIni &ini, const std::string &section)
{
    SingleBlob::LoadIniSettings(ini, section);
    
    string topColour = ini.gets(section,"TopColour","Yellow");
    string midColour = ini.gets(section,"MidColour","Green");
    string bottomColour = ini.gets(section,"BottomColour","Blue");
    
    cout << "\tLoading configs for target panels..." << endl <<
            "\t\tTop: " << topColour << endl <<
            "\t\tMid: " << midColour << endl <<
            "\t\tBtm: " << bottomColour << endl;
    
    topPanelCandidates->LoadIniSettings(ini,topColour);
    midPanelCandidates->LoadIniSettings(ini,midColour);
    bottomPanelCandidates->LoadIniSettings(ini,bottomColour);
    DISTANCE_THRESHOLD = ini.getd(section,"DistanceThreshold",50.0);
}

void SprintTarget::Print()
{
    cout << "+++++ BEGIN SPRINT TARGET +++++" << endl;
    cout << "Top" << endl;
    topPanelCandidates->Print();
    cout << "Middle" << endl;
    midPanelCandidates->Print();
    cout << "Bottom" << endl;
    bottomPanelCandidates->Print();

	cout << "Sprint Target" << endl;
    cout << "\tcenter: (" << this->boundingBox.center.X << ", " << this->boundingBox.center.Y << ")" << endl <<
            "\theight: " << this->boundingBox.height << endl <<
            "\twidth : " << this->boundingBox.width << endl <<
            "\tslope : " << this->slope << endl;

    // TODO: may be more, indicating angles etc. ??

    cout << "+++++ END SPRINT TARGET +++++" << endl;
}

// draw the target's bounding boxes on the provided image
void SprintTarget::Draw(cv::Mat &image)
{
    this->boundingBox.Draw(image, *(this->GetMarkColour()),2);
    topPanelCandidates->Draw(image);
    midPanelCandidates->Draw(image);
    bottomPanelCandidates->Draw(image);
    
    for(vector<BoundingBox*>::iterator i=midPanelCandidates->GetBoundingBoxes()->begin(); i!=midPanelCandidates->GetBoundingBoxes()->end(); i++)
    {
        BoundingBox *bb = *i;
        char buffer[255];
        sprintf(buffer,"%i x %i",bb->width, bb->height);
        cv::putText(image,buffer,Point(bb->center.X, bb->center.Y),FONT_HERSHEY_PLAIN, 0.75, Scalar(255,255,255), 1);
    }

    // draw a line between the centres of the two panels
    if(foundInLastFrame)
    {
        cv::Point pt1(topPanel->center.X, topPanel->center.Y);
        cv::Point pt2(bottomPanel->center.X, bottomPanel->center.Y);
        cv::line(image,pt1, pt2, this->markColour, 1);

        pt1 = cv::Point(topPanel->center.X-topPanel->width/2, topPanel->center.Y);
        pt2 = cv::Point(bottomPanel->center.X-bottomPanel->width/2, bottomPanel->center.Y);
        cv::line(image,pt1, pt2, this->markColour, 1);

        pt1 = cv::Point(topPanel->center.X+topPanel->width/2, topPanel->center.Y);
        pt2 = cv::Point(bottomPanel->center.X+bottomPanel->width/2, bottomPanel->center.Y);
        cv::line(image,pt1, pt2, this->markColour, 1);
    }
}

// locate the sprint target in the frame
int SprintTarget::FindInFrame(CameraPosition *cameraPosition, cv::Mat &image, cv::Mat *dest)
{
    this->foundInLastFrame = false;

    // find the candidate panels and then decide which pair is the correct one
    BlobTarget::FindTargets(candidates,image, dest);
    ProcessCandidates();

    // set up a bounding box around both targets and calculate the slope (if necessary)
    if(foundInLastFrame && topPanel != NULL && bottomPanel != NULL)
    {
        int top = topPanel->center.Y - topPanel->height/2;
        int bottom = bottomPanel->center.Y + bottomPanel->height/2;
        int left = min(topPanel->center.X - topPanel->width/2, bottomPanel->center.X - bottomPanel->width/2);
        int right = max(topPanel->center.X + topPanel->width/2, bottomPanel->center.X + bottomPanel->width/2);

        boundingBox.center.Y = (top+bottom)/2;
        boundingBox.center.X = (left+right)/2;
        boundingBox.height = bottom-top;
        boundingBox.width = right-left;

        // average the left-to-left, right-to-right, and center-to-center slopes
        Point2D p1 = topPanel->center;
        Point2D p2 = bottomPanel->center;
        slope = CalculateSlope(p1,p2);

        p1.X = topPanel->center.X - topPanel->width/2;
        p2.X = bottomPanel->center.X - bottomPanel->width/2;
        slope += CalculateSlope(p1,p2);

        p1.X = topPanel->center.X + topPanel->width/2;
        p2.X = bottomPanel->center.X + bottomPanel->width/2;
        slope += CalculateSlope(p1,p2);

        this->slope = slope/3.0;

        this->position = CalculateCentre();

        Point2D topPanelBL = Point2D();
        Point2D topPanelBR = Point2D();
        topPanelBL.X = topPanel->center.X - topPanel->width/2;
        topPanelBL.Y = topPanel->center.Y + topPanel->height/2;
        topPanelBR.X = topPanel->center.X + topPanel->width/2;
        topPanelBR.Y = topPanel->center.Y + topPanel->height/2;
        this->range = (cameraPosition->CalculateRange(topPanelBL, BOTTOM_OF_TOP_PANEL_HEIGHT) + cameraPosition->CalculateRange(topPanelBR, BOTTOM_OF_TOP_PANEL_HEIGHT))/2.0;
    }
    else
    {
        boundingBox.center.X = -1;
        boundingBox.center.Y = -1;
        boundingBox.height = 0;
        boundingBox.width = 0;
    }

    return foundInLastFrame;
}

double SprintTarget::CalculateSlope(Point2D &top, Point2D &bottom)
{
    double slope = (top.X - bottom.X) / (top.Y - bottom.Y);
    return slope;
}

double SprintTarget::CalculateCentre()
{
    double position =  (double)(Camera::WIDTH/2 - boundingBox.center.X)/Camera::WIDTH;
    return position;
}

void SprintTarget::ProcessCandidates()
{
    // use a brute-force approach to find the two candidate panels that are the closest together
    // and in the correct orientation (i.e. top is above bottom and they overlap L/R at least a little)
    vector<BoundingBox*> *topPanels = topPanelCandidates->GetBoundingBoxes();
    vector<BoundingBox*> *midPanels = midPanelCandidates->GetBoundingBoxes();
    vector<BoundingBox*> *bottomPanels = bottomPanelCandidates->GetBoundingBoxes();

    double minDistance = DISTANCE_THRESHOLD;
//    topPanel = NULL;
//    midPanel = NULL;
//    bottomPanel = NULL;
    bool foundMidPanel = false;
    midPanel = NULL;

    BoundingBox *top, *mid, *bottom;
    unsigned int topPanelsSize = topPanels->size();
    unsigned int midPanelsSize = midPanels->size();
    unsigned int bottomPanelsSize = bottomPanels->size();
    foundInLastFrame = false;
    for(unsigned int i=0; i<topPanelsSize; i++)
    {
        top = (*topPanels)[i];
        for(unsigned int j=0; j<bottomPanelsSize; j++)
        {
            bottom = (*bottomPanels)[j];

            // left/right edges for the candidate panels we are considering
            int tl = top->center.X - top->width/2;
            int tr = top->center.X + top->width/2;
            int bl = bottom->center.X - bottom->width/2;
            int br = bottom->center.X + bottom->width/2;

            // distance between the panels
            double distance = sqrt(pow(top->center.X - bottom->center.X, 2.0) + pow(top->center.Y - bottom->center.Y, 2.0));

            // determine if these two targets are a valid pair
            if(top->center.Y < bottom->center.Y && distance <= minDistance &&(
                        (tl >= bl && tl <= br) ||   // top's left edge is between bottom's left and right edges
                        (tr >= bl && tr <= br) ||   // top's right edge is between bottom's left and right edges
                        (bl >= tl && bl <= tr) ||   // bottom's left edge is between top's left and right edges
                        (br >= bl && br <= tr)      // bottom's right edge is between top's left and right edges
              ))
            {
                minDistance = distance;
                topPanel = top;
                bottomPanel = bottom;
                foundInLastFrame = true;
            }
        }
    }

    //if the top and bottom panels have been found, find the best middle panel candidate
    if(foundInLastFrame)
    {
        for(unsigned int i=0; i<midPanelsSize; i++)
        {
            mid = (*midPanels)[i];
            //if the center of the middle panel is between the top and bottom panels, and the its x-value is within the bounds of the bottom panel
            if(mid->center.Y<bottomPanel->center.Y && mid->center.Y>topPanel->center.Y && mid->center.X>(bottomPanel->center.X-bottomPanel->width/2) && mid->center.X<(bottomPanel->center.X+bottomPanel->width/2))
            {
                //set the middle panel if this one has a larger width than the last one
                if(!foundMidPanel || midPanel->height<mid->height) {
                    midPanel = mid;
                    foundMidPanel = true;
                }
            }
        }
    }
    else //if top and bottom panels were not found
    {
        minDistance = DISTANCE_THRESHOLD;
        for(unsigned int i=0; i<topPanelsSize; i++)
        {
            top = (*topPanels)[i];
            for(unsigned int j=0; j<midPanelsSize; j++)
            {
                mid = (*midPanels)[j];

                // distance between the panels
                double distance = sqrt(pow(top->center.X - mid->center.X, 2.0) + pow(top->center.Y - mid->center.Y, 2.0));

                // determine if these two targets are a valid pair
                if(mid->center.Y>top->center.Y && mid->center.X>(top->center.X-top->width/2) && mid->center.X<(top->center.X+top->width/2) && distance<minDistance)
                {
                    minDistance = distance;
                    topPanel = top;
                    midPanel = mid;
                    foundMidPanel = true;
                    foundInLastFrame = true;
                    bottomPanel = mid;
                }
            }
        }
        //This was commented out, I added it back in. BP
        if(!foundMidPanel) 
        {
        //    topPanel = NULL;
            midPanel = NULL;
        }
        //bottomPanel = NULL;
    }
}

double SprintTarget::GetSlopeError()
{
    return -slope;
}

double SprintTarget::GetPositionError()
{
     return position;
}

double SprintTarget::GetRange()
{
    return range;
}

int SprintTarget::GetMidPanelHeight()
{
    if(midPanel != NULL)
    {
        double h = midPanel->height;
        
        // dirty hack to deal with some garbage numbers we've been getting
        // CIB FIRA 2014
        if(h <= 0)
            h = 0;
        else if(h >= Camera::HEIGHT)
            h = 0;
            
        return h;
    }
    else
    {
        return 0;
    }
}

void SprintTarget::Reset()
{
    topPanel = NULL;
    midPanel = NULL;
    bottomPanel = NULL;
    foundInLastFrame = false;
}

bool SprintTarget::GetFoundInLastFrame()
{
    return foundInLastFrame;
}
