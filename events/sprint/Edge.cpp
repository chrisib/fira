/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Diana Carrier
 *
 * This class is used to represent the edge in a graph. Developed for use 
 * with the sprint event.
 ************************************************************************/

#include "Edge.h"

using namespace Robot;
using namespace std;
using namespace cv;

Edge::Edge()
{
	box1 = NULL;
	box2 = NULL;
	distance = -1;
}

Edge::Edge(BoundingBox *a, BoundingBox *b, int dist)
{
	box1 = a;
	box2 = b;
	distance = dist;
}

Edge::~Edge()
{}

EdgeCompare::EdgeCompare()
{}

EdgeCompare::~EdgeCompare()
{}

bool EdgeCompare::operator() (const Edge& lhs, const Edge& rhs) const
{
	return (lhs.distance) < (rhs.distance);
}