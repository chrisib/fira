#ifndef WALLCLIMBER_H
#define WALLCLIMBER_H

#include <LeftArm.h>
#include <LeftLeg.h>
#include <RightArm.h>
#include <RightLeg.h>
#include <LadderTarget.h>
#include <string>
#include <Application.h>
#include <opencv2/core/core.hpp>
#include <pthread.h>
#include <darwin/framework/MultiLine.h>

class WallClimber : public Robot::Application
{
public:
    WallClimber();

    virtual bool Initialize(const char *iniFilePath, const char *motionFilePath=NULL, int safePosition=Robot::Action::DEFAULT_MOTION_SIT_DOWN);
    virtual void LoadIniSettings();

    virtual void HandleVideo();

    virtual void Execute();
    virtual void Process();

    enum ROBOT_STATE
    {
        STATE_IDLE = 0,
        STATE_WALKING_TO_LADDER,
        STATE_MOUNTING_LADDER,
        STATE_MOVING_RIGHT_ARM,
        STATE_MOVING_LEFT_ARM,
        STATE_MOVING_LEFT_LEG,
        STATE_MOVING_RIGHT_LEG,
        STATE_PAUSED,
        STATE_FINISHING,

        STATE_DEBUG
    };


private:
    LadderTarget ladder;
    //Robot::MultiLine ladder;

    cv::Mat dbgImage;

    int myState;

    double approachStepSize;
    double approachDistance;
    double approachMinDistance;
    double defaultTurn;
    double turnGain;
    double approachMinSlope;
    double approachMaxSlope;

    double ladderInclination;
    double ladderWidth;
    int numRungs;

    bool pauseOnRung;
    bool recordVideo;
    bool noApproach;
    bool enableLog;
    bool useStaticMotions;
    bool stepByStep;
    bool enableDebugPrinting;

    // motion.bin indices
    /*
    StandStraight = 100
    ExtendArms = 101
    ReachForward = 102
    LiftLeftLeg = 104
    LowerLeftLeg = 105
    LiftRightLeg = 106
    LowerRightLeg = 107
    SquatRung = 108
    RungRightArm = 109
    RungLeftArm = 115
    StandRung = 120
    Finish = 121
    */
    int STAND_STRAIGHT_MOTION;
    int EXTEND_ARMS_MOTION;
    int REACH_FORWARD_MOTION;
    int RAISE_LEFT_LEG_MOTION;
    int LOWER_LEFT_LEG_MOTION;
    int RAISE_RIGHT_LEG_MOTION;
    int LOWER_RIGHT_LEG_MOTION;
    int SQUAT_RUNG_MOTION;
    int SQUAT_RUNG_CLOSE_MOTION;
    int STAND_RUNG_MOTION;
    int STAND_RUNG_CLOSE_MOTION;
    int FINISH_MOTION;
    int RIGHT_ARM_BACK_MOTION;
    int LEFT_ARM_BACK_MOTION;
    int RIGHT_ARM_FORWARD_MOTION;
    int LEFT_ARM_FORWARD_MOTION;

    int AIM_NUM_FRAMES;
    int DISCARD_RUNG_OUTLIERS;

    double MIN_SQUAT_SHOULDER_PITCH;
    double MAX_SQUAT_SHOULDER_PITCH;
    double CLOSE_SQUAT_SEPARATION;

    double SEMI_OPEN_HAND_ANGLE;

    double WIGGLE_KNEE_ANGLE;
    double WIGGLE_SHOULDER_ANGLE;

    double EXTEND_ANKLE_PITCH_OFFSET;
    static double STAND_HAND_OPEN_SPEED;

    enum{
        FILTER_AVERAGE,
        FILTER_MEDIAN
    };
    int FILTER_METHOD;

    double TILT_UP;
    double TILT_DOWN;
    double PAN_LEFT;
    double PAN_RIGHT;

    double LEG_SPEED;
    double AIM_OFFSET;
    double SHOULDER_ROLL_ANGLE;
    double MIN_DISTANCE_BETWEEN_RUNGS;

    // direct access to each limb
    Robot::LeftArm *leftArm;
    Robot::RightArm *rightArm;
    Robot::LeftLeg *leftLeg;
    Robot::RightLeg *rightLeg;

    volatile bool visionSync; // false while waiting for vision, true when vision has been handled

    Robot::CameraPosition cameraPosition;

    const std::string state2txt();

    cv::VideoWriter rgbStream;
    cv::VideoWriter debugStream;

    void Process_WalkToLadder();
    void Process_MountLadder();
    void Process_MoveRightArm();
    void Process_MoveLeftArm();
    void Process_MoveRightLeg();
    void Process_MoveLeftLeg();
    void Process_Finish();

    void FinishMovingLimbs(void (*func)(void) = 0 );

    void Pause3seconds();
    bool FinishedClimbing();

    void CopyPosition(Robot::JointData &dst, Robot::JointData &src);
    void PlayAction(int id, bool leftArm=true, bool rightArm=true, bool leftLeg=true, bool rightLeg=true, void (*func)(void) = 0);
    void ExtendLeg(Robot::Leg* leg);
    void SetArmAngles(Robot::Arm* arm, double shoulderPitch, double shoulderRoll, double elbow);

    double average(double values[], int numValues);
    double median(double values[], int numValues);

    double FindDistanceToNextRung();

    // keep track of the separation between the rungs at each step
    std::vector<double> rungSeparations;

    double CalculateShoulderPitchForSquat(const double RUNG_SEPARATION);
    double CalculateShoulderPitchForLegLift();
    void SquatOnRung();
    void Wiggle();

    static char WaitForKeyboard();
    static void OpenHandWhileStanding();
};

#endif // WALLCLIMBER_H
