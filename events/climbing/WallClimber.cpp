#include "WallClimber.h"
#include <LinuxDARwIn.h>
#include <Walking.h>
#include <CvCamera.h>
#include <cstring>
#include <opencv2/highgui/highgui.hpp>
#include <Math.h>
#include <Voice.h>
#include <Kinematics.h>
#include <ctime>
#include <termio.h>

using namespace Robot;
using namespace std;
using namespace cv;

double WallClimber::STAND_HAND_OPEN_SPEED;

WallClimber::WallClimber() : Application()
{
    dbgImage = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);

    leftArm = LeftArm::GetInstance();
    rightArm = RightArm::GetInstance();
    leftLeg = LeftLeg::GetInstance();
    rightLeg = RightLeg::GetInstance();

    RegisterSwitch("--pause",&pauseOnRung,false,"Pause for 3 seconds on each rung");
    RegisterSwitch("--record",&recordVideo,false,"Record video streams to a file");
    RegisterSwitch("--no-approach",&noApproach,false,"Skip the approach phase and mount the ladder immediately");
    RegisterSwitch("--log",&enableLog,false,"Enable MotionManager logging");
    RegisterSwitch("--static",&useStaticMotions,false,"Use static motions for climbing instead of doing everything dynamically");
    RegisterSwitch("--step",&stepByStep,false,"Wait for user input after every step");
    RegisterSwitch("--debug",&enableDebugPrinting,false,"Enable additional command-line output for debugging");

    visionSync = false;

    STAND_HAND_OPEN_SPEED = 0.0;
}

bool WallClimber::Initialize(const char *iniFilePath, const char *motionFilePath, int safePosition)
{
    ini = new minIni(iniFilePath);

    if(motionFilePath == NULL)
        motionFilePath = ini->gets("Files","MotionFile","motion.bin").c_str();

    //Application::Initialize(iniFilePath, motionFilePath, safePosition);
    InitIni(iniFilePath);
    if(!visionOnly)
        InitCM730(motionFilePath, safePosition);
    InitCamera();
    LoadIniSettings();

    cout << "[init] Application initialization complete" << endl;
    CvCamera::GetInstance()->RGB_ENABLE = true;

    Voice::Initialize("default");
    cout << "[init] Voice module initialized" << endl;

    // add additional modules
    leftArm->m_Joint.SetEnableBody(false);
    rightArm->m_Joint.SetEnableBody(false);
    leftLeg->m_Joint.SetEnableBody(false);
    rightLeg->m_Joint.SetEnableBody(false);
    Walking::GetInstance()->m_Joint.SetEnableBody(false);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
    cout << "[init] Additional motion modules set up" << endl;

    MotionManager::GetInstance()->AddModule(leftArm);
    MotionManager::GetInstance()->AddModule(rightArm);
    MotionManager::GetInstance()->AddModule(leftLeg);
    MotionManager::GetInstance()->AddModule(rightLeg);
    MotionManager::GetInstance()->AddModule(Head::GetInstance());
    MotionManager::GetInstance()->AddModule(Walking::GetInstance());
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    leftLeg->SetMoveMethod(Limb::ApproachByOne);
    rightLeg->SetMoveMethod(Limb::ApproachByOne);
    cout << "[init] Motion modules registered" << endl;

    if(showVideo)
    {
        namedWindow("Webcam");
        namedWindow("Processed");

        cvMoveWindow("Webcam",0,0);
        cvMoveWindow("Processed",0,290);
        cvWaitKey(1);
        cout << "[init] GUI elements created" << endl;
    }

    if(recordVideo)
    {
        rgbStream = cv::VideoWriter(ini->gets("Files","RgbStream","rgb.avi"),CV_FOURCC('D', 'I', 'V', 'X'), 15, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        debugStream = cv::VideoWriter(ini->gets("Files","DebugStream","debug.avi"),CV_FOURCC('D', 'I', 'V', 'X'), 15, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
    }

    cout << "[init] Initialization complete" << endl;

    if(!visionOnly)
        Voice::Speak("Initialization Complete");

    CvCamera::GetInstance()->RGB_ENABLE = showVideo || recordVideo;

    StartVisionThread();

    return true;
}

void WallClimber::LoadIniSettings()
{
    ladder.LoadIniSettings(*ini,"Ladder");
    Walking::GetInstance()->LoadINISettings(ini);
    CvCamera::GetInstance()->LoadINISettings(ini);
    leftLeg->LoadIniSettings(ini,"Leg");
    rightLeg->LoadIniSettings(ini,"Leg");

    ladderInclination = ini->getd("Ladder","Inclination");
    ladderWidth = ini->getd("Ladder","Width");
    numRungs = ini->geti("Ladder","NumRungs");

    approachStepSize = ini->getd("Approach","StepSize");
    approachDistance = ini->getd("Approach","TargetDistance");
    approachMinDistance = ini->getd("Approach","MinDistance");
    defaultTurn = ini->getd("Approach","DefaultTurn");
    turnGain = ini->getd("Approach","TurnGain");
    approachMinSlope = ini->getd("Approach","MinSlope");
    approachMaxSlope = ini->getd("Approach","MaxSlope");
    //criticalSlope = ini->getd("Approach","CriticalSlope");

    rightArm->NEUTRAL_ANGLE = ini->getd("Hand","NeutralHandAngle");
    leftArm->NEUTRAL_ANGLE = ini->getd("Hand","NeutralHandAngle");
    rightArm->TORQUE_LIMIT = ini->geti("Hand","MaxHandTorque",RightArm::GetInstance()->TORQUE_LIMIT);
    leftArm->TORQUE_LIMIT = ini->geti("Hand","MaxHandTorque",LeftArm::GetInstance()->TORQUE_LIMIT);
    rightArm->OPEN_ANGLE = ini->getd("Hand","OpenHandAngle",90);
    leftArm->OPEN_ANGLE = ini->getd("Hand","OpenHandAngle",90);
    rightArm->CLOSE_ANGLE = ini->getd("Hand","ClosedHandAngle",rightArm->CLOSE_ANGLE);
    leftArm->CLOSE_ANGLE = ini->getd("Hand","ClosedHandAngle",leftArm->CLOSE_ANGLE);

    SEMI_OPEN_HAND_ANGLE = ini->getd("Hand","SemiOpenAngle",60);

    WIGGLE_SHOULDER_ANGLE = ini->getd("Wiggle","Shoulder",20);
    WIGGLE_KNEE_ANGLE = ini->getd("Wiggle","Knee",20);

    EXTEND_ANKLE_PITCH_OFFSET = ini->getd("Leg","AnklePitchOffset",0);

    // set the hands' PID gains
    std::list<MotionModule*> modules;
    modules.push_back(LeftArm::GetInstance());
    modules.push_back(RightArm::GetInstance());
    modules.push_back(LeftLeg::GetInstance());
    modules.push_back(RightLeg::GetInstance());
    modules.push_back(Action::GetInstance());
    modules.push_back(Head::GetInstance());
    modules.push_back(Walking::GetInstance());
    for(std::list<MotionModule*>::iterator i=modules.begin(); i!=modules.end(); i++)
    {
        MotionModule *m = *i;

        m->m_Joint.SetPGain(JointData::ID_R_HAND,ini->geti("Hand","P",rightArm->m_Joint.GetPGain(JointData::ID_R_HAND)));
        m->m_Joint.SetPGain(JointData::ID_L_HAND,ini->geti("Hand","P",rightArm->m_Joint.GetPGain(JointData::ID_L_HAND)));
        m->m_Joint.SetIGain(JointData::ID_R_HAND,ini->geti("Hand","I",rightArm->m_Joint.GetIGain(JointData::ID_R_HAND)));
        m->m_Joint.SetIGain(JointData::ID_L_HAND,ini->geti("Hand","I",rightArm->m_Joint.GetIGain(JointData::ID_L_HAND)));
        m->m_Joint.SetDGain(JointData::ID_R_HAND,ini->geti("Hand","D",rightArm->m_Joint.GetDGain(JointData::ID_R_HAND)));
        m->m_Joint.SetDGain(JointData::ID_L_HAND,ini->geti("Hand","D",rightArm->m_Joint.GetDGain(JointData::ID_L_HAND)));
    }

    cout << "Customized arm settings:" << endl <<
            "\tNeutral Angle: " << LeftArm::GetInstance()->NEUTRAL_ANGLE << endl <<
            "\tMax Torque:    " << LeftArm::GetInstance()->TORQUE_LIMIT << endl <<
            endl;

    /*
    [Motions]
    StandStraight = 100
    ExtendArms = 101
    ReachForward = 102
    LiftLeftLeg = 104
    LowerLeftLeg = 105
    LiftRightLeg = 106
    LowerRightLeg = 107
    SquatRung = 108
    RungRightArm = 109
    RungLeftArm = 115
    StandRung = 120
    Finish = 121
    RightArmBack = 109
    RightArmAhead = = 112
    LeftArmBack = 115
    LeftArmAhead = 118
    */
    STAND_STRAIGHT_MOTION = ini->geti("Motions","StandStraight",100);
    EXTEND_ARMS_MOTION = ini->geti("Motions","ExtendArms",101);
    REACH_FORWARD_MOTION = ini->geti("Motions","ReachForward",102);
    RAISE_LEFT_LEG_MOTION = ini->geti("Motions","LiftLeftLeg",104);
    LOWER_LEFT_LEG_MOTION = ini->geti("Motions","LowerLeftLeg",105);
    RAISE_RIGHT_LEG_MOTION = ini->geti("Motions","LiftRightLeg",106);
    LOWER_RIGHT_LEG_MOTION = ini->geti("Motions","LowerRightLeg",107);
    SQUAT_RUNG_MOTION = ini->geti("Motions","SquatRung",108);
    SQUAT_RUNG_CLOSE_MOTION = ini->geti("Motions","SquatCloseRungs",122);
    STAND_RUNG_MOTION = ini->geti("Motions","StandRung",113);
    STAND_RUNG_CLOSE_MOTION = ini->geti("Motions","StandCloseRungs",123);
    FINISH_MOTION = ini->geti("Motions","Finish",121);
    RIGHT_ARM_BACK_MOTION = ini->geti("Motions","RightArmBack",109);
    RIGHT_ARM_FORWARD_MOTION = ini->geti("Motions","RightArmAhead",112);
    LEFT_ARM_BACK_MOTION = ini->geti("Motions","LeftArmBack",115);
    LEFT_ARM_FORWARD_MOTION = ini->geti("Motions","LeftArmAhead",118);

    TILT_UP = ini->getd("Climbing","TiltUp",15);
    TILT_DOWN = ini->getd("Climbing","TiltDown",-45);
    PAN_LEFT = ini->getd("Climbing","PanLeft",20);
    PAN_RIGHT = ini->getd("Climbing","PanRight",-PAN_LEFT);

    AIM_NUM_FRAMES = ini->geti("Climbing","AimNumFrames",5);
    DISCARD_RUNG_OUTLIERS = ini->geti("Climbing","DiscardOutliers",1);
    FILTER_METHOD = ini->gets("Climbing","FilterMethod","average") == "median" ? FILTER_MEDIAN : FILTER_AVERAGE;
    LEG_SPEED = ini->getd("Climbing","LegSpeed",0.1);
    AIM_OFFSET = ini->getd("Climbing","AimHandOffset",0.0);

    MIN_SQUAT_SHOULDER_PITCH = ini->getd("Climbing","MinSquatPitch",45);
    MAX_SQUAT_SHOULDER_PITCH = ini->getd("Climbing","MaxSquatPitch",65);

    SHOULDER_ROLL_ANGLE = ini->getd("Climbing","ShoulderRollOffset",0.0);

    CLOSE_SQUAT_SEPARATION = ini->getd("Climbing","SquatCloseSeparation",130.0);
    MIN_DISTANCE_BETWEEN_RUNGS = ini->getd("Climbing","MinDistanceForAim",80.0);

    // add the separations between the first two rungs
    rungSeparations.push_back(ladder.FirstRungHeight());
    rungSeparations.push_back(ladder.SecondRungHeight()-ladder.FirstRungHeight());

    STAND_HAND_OPEN_SPEED = ini->getd("Climbing","StandOpenHandSpeed",0.0);
}

void WallClimber::HandleVideo()
{
    CvCamera::GetInstance()->CaptureFrame();

    ladder.FindInFrame(CvCamera::GetInstance()->yuvFrame,&dbgImage);

    if(showVideo || recordVideo)
    {
        ladder.Draw(CvCamera::GetInstance()->rgbFrame);
        ladder.Draw(dbgImage);

        ladder.DRAW_BUCKETS = true;

        Point p(0,Camera::HEIGHT-12);
        const Scalar white(255,255,255);
        char s[255];
        sprintf(s,"State: %s",state2txt().c_str());
        putText(CvCamera::GetInstance()->rgbFrame,s,p,FONT_HERSHEY_PLAIN,1.0,white,1);
        putText(dbgImage,s,p,FONT_HERSHEY_PLAIN,1.0,white,1);

        Point3D ll = LeftLeg::GetInstance()->GetPosition(MotionStatus::m_CurrentJoints);
        Point3D rl = RightLeg::GetInstance()->GetPosition(MotionStatus::m_CurrentJoints);
        Point3D la = LeftArm::GetInstance()->GetPosition(MotionStatus::m_CurrentJoints);
        Point3D ra = RightArm::GetInstance()->GetPosition(MotionStatus::m_CurrentJoints);
        Point3D h = Head::GetInstance()->GetPosition(MotionStatus::m_CurrentJoints);

        p = Point(0,12);
        sprintf(s,"Arm: (%0.2f, %0.2f, %0.2f) (%0.2f, %0.2f, %0.2f)",la.X,la.Y,la.Z,ra.X,ra.Y,ra.Z);
        putText(CvCamera::GetInstance()->rgbFrame,s,p,FONT_HERSHEY_PLAIN,0.75,white,1);
        putText(dbgImage,s,p,FONT_HERSHEY_PLAIN,0.75,white,1);

        p = Point(0,24);
        sprintf(s,"Leg: (%0.2f, %0.2f, %0.2f) (%0.2f, %0.2f, %0.2f)",ll.X,ll.Y,ll.Z,rl.X,rl.Y,rl.Z);
        putText(CvCamera::GetInstance()->rgbFrame,s,p,FONT_HERSHEY_PLAIN,0.75,white,1);
        putText(dbgImage,s,p,FONT_HERSHEY_PLAIN,0.75,white,1);

        p = Point(0,36);
        sprintf(s, "Head: (%0.2f, %0.2f, %0.2f)", h.X, h.Y, h.Z);
        putText(CvCamera::GetInstance()->rgbFrame,s,p,FONT_HERSHEY_PLAIN,0.75,white,1);
        putText(dbgImage,s,p,FONT_HERSHEY_PLAIN,0.75,white,1);


        if(recordVideo)
        {
            rgbStream.write(CvCamera::GetInstance()->rgbFrame);
            debugStream.write(dbgImage);
        }

        if(showVideo)
        {
            imshow("Webcam",CvCamera::GetInstance()->rgbFrame);
            imshow("Processed",dbgImage);
            cvWaitKey(1);
        }
    }

    visionSync = true;
}

void WallClimber::Execute()
{
    if(visionOnly)
    {
        myState = STATE_DEBUG;
    }
    else
    {
        PlayAction(Action::DEFAULT_MOTION_WALKREADY);
        Head::GetInstance()->MoveByAngle(0,TILT_DOWN);
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);

        Voice::Speak("press the middle button");
        cout << "Press the middle button" << endl;
        MotionManager::GetInstance()->WaitButton(CM730::MIDDLE_BUTTON);
        if(enableLog)
            MotionManager::GetInstance()->StartLogging();

        // skip the approach phase if necessary
        if(noApproach)
            myState = STATE_MOUNTING_LADDER;
        else
            myState = STATE_WALKING_TO_LADDER;
    }

    for(;;)
    {
        if(visionSync)
        {
            visionSync = false;
            Process();
        }
    }
}

void WallClimber::Process()
{
	if(stepByStep && myState != STATE_IDLE && myState != STATE_DEBUG)
    {
        cout << "Press any key to continue" << endl;
        WaitForKeyboard();
    }

    switch(myState)
    {
    case STATE_WALKING_TO_LADDER:
        Process_WalkToLadder();
        break;

    case STATE_MOUNTING_LADDER:
        Process_MountLadder();
        break;

    case STATE_MOVING_LEFT_ARM:
        Process_MoveLeftArm();
        break;

    case STATE_MOVING_RIGHT_ARM:
        Process_MoveRightArm();
        break;

    case STATE_MOVING_LEFT_LEG:
        Process_MoveLeftLeg();
        break;

    case STATE_MOVING_RIGHT_LEG:
        Process_MoveRightLeg();
        break;

    case STATE_PAUSED:
        PlayAction(FINISH_MOTION);
        Pause3seconds();
        SquatOnRung();
        myState = STATE_MOVING_RIGHT_ARM;
        break;

    case STATE_FINISHING:
        Process_Finish();
        break;

    case STATE_DEBUG:
    case STATE_IDLE:
    default:
        break;
    }

#if 0
    
    leftArm->m_Joint.SetPGain(JointData::ID_L_HAND,ini->geti("Hand","P",16));
	rightArm->m_Joint.SetPGain(JointData::ID_R_HAND,ini->geti("Hand","P",16));
	MotionStatus::m_CurrentJoints.SetPGain(JointData::ID_L_HAND,ini->geti("Hand","P",16));
	MotionStatus::m_CurrentJoints.SetPGain(JointData::ID_R_HAND,ini->geti("Hand","P",16));
	
	leftArm->m_Joint.SetIGain(JointData::ID_L_HAND,ini->geti("Hand","I",0));
	rightArm->m_Joint.SetIGain(JointData::ID_R_HAND,ini->geti("Hand","I",0));
	MotionStatus::m_CurrentJoints.SetIGain(JointData::ID_L_HAND,ini->geti("Hand","I",0));
	MotionStatus::m_CurrentJoints.SetIGain(JointData::ID_R_HAND,ini->geti("Hand","I",0));
	
	leftArm->m_Joint.SetDGain(JointData::ID_L_HAND,ini->geti("Hand","D",0));
	rightArm->m_Joint.SetDGain(JointData::ID_R_HAND,ini->geti("Hand","D",0));
	MotionStatus::m_CurrentJoints.SetDGain(JointData::ID_L_HAND,ini->geti("Hand","D",0));
	MotionStatus::m_CurrentJoints.SetDGain(JointData::ID_R_HAND,ini->geti("Hand","D",0));
#endif
}

void WallClimber::FinishMovingLimbs(void (*waitFcn)(void))
{
    while(leftArm->IsRunning() || rightArm->IsRunning() || leftLeg->IsRunning() || rightLeg->IsRunning() || Action::GetInstance()->IsRunning())
    {
#ifdef DEBUG
        cout << "Waiting for ";

        if(leftArm->IsRunning())
            cout << "Left Arm ";
        if(rightArm->IsRunning())
            cout << "Right Arm ";
        if(leftLeg->IsRunning())
            cout << "Left Leg ";
        if(rightLeg->IsRunning())
            cout << "Right Leg ";
        if(Action::GetInstance()->IsRunning())
            cout << "Action ";
        cout << endl;
#endif
        while(!visionSync);

        if(waitFcn != NULL)
            waitFcn();
    }
}

void WallClimber::Process_WalkToLadder()
{
    Head::GetInstance()->MoveByAngle(0,TILT_DOWN);

    // real-world height of the lowest rung from the ground
    //const double RUNG_HEIGHT = rungSeparation * sin(deg2rad(ladderInclination));
    const double RUNG_HEIGHT = 0.0; // assume the rung is on the ground and we'll try looking straight down at it as we approach

    if(!Walking::GetInstance()->IsRunning())
    {
        Voice::Speak("Approaching ladder");
        cout << "Approaching ladder" << endl;
        cout << "X amplitude: " << approachStepSize << endl;
        cout << "Target distance: " << approachDistance << endl;
        //cout << "Critical slope: " << criticalSlope << endl;
        cout << "Target slope: [" << approachMinSlope << " , " << approachMaxSlope << "]" << endl;

        Walking::GetInstance()->Start();
        Walking::GetInstance()->X_MOVE_AMPLITUDE = approachStepSize;
    }

    // check the position of the bottom rung
    vector<Line2D*> *rungs = ladder.GetRungs();
    if(rungs->size() > 0)
    {
        Line2D *bottomRung = (*rungs)[0];

        cameraPosition.RecalculatePosition();
        double distance = (cameraPosition.CalculateRange(bottomRung->start,RUNG_HEIGHT) + cameraPosition.CalculateRange(bottomRung->end,RUNG_HEIGHT))/2.0;

        // check the slope of the line
        double slope = (bottomRung->start.Y - bottomRung->end.Y) / (bottomRung->start.X - bottomRung->end.X);

        if(slope < approachMinSlope || slope > approachMaxSlope) // need to do MAJOR corrections
        {
            if (distance > this->approachDistance + 20)
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 0.0;
            else
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -approachStepSize;

        }
        else
        {
            if(distance < this->approachMinDistance)
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -approachStepSize;
            else
                Walking::GetInstance()->X_MOVE_AMPLITUDE = approachStepSize;
        }

        Walking::GetInstance()->A_MOVE_AMPLITUDE = defaultTurn + slope * turnGain;

        cout << "Ladder at range " << distance << "mm; slope: " << slope << endl;

        if(distance <= this->approachDistance && distance >= this->approachMinDistance && slope >= approachMinSlope && slope <= approachMaxSlope)
        {
            cout << "May have reached the ladder" << endl;

            Walking::GetInstance()->Stop();
            Walking::GetInstance()->Finish();

            // re-sync and grab a fresh frame
            while(!visionSync);
            distance = (cameraPosition.CalculateRange(bottomRung->start,RUNG_HEIGHT) + cameraPosition.CalculateRange(bottomRung->end,RUNG_HEIGHT))/2.0;

            if(distance < this->approachDistance)
            {
                Voice::Speak("Reached the ladder");
                cout << "Reached the ladder" << endl;
                myState = STATE_MOUNTING_LADDER;
            }
        }
    }
}

void WallClimber::Process_MountLadder()
{
    cout << "Preparing to mount the ladder" << endl;

    // give the action module control of everything but the hands
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
    leftArm->m_Joint.SetEnableLeftHandOnly(true,true);
    rightArm->m_Joint.SetEnableRightHandOnly(true,true);

    // stand with the legs straight
    PlayAction(STAND_STRAIGHT_MOTION);

    // extend the arms and grab the rung
    cout << "Extending arms" << endl;
    PlayAction(EXTEND_ARMS_MOTION);
    cout << "Finished extending arms" << endl;

    // open the hands
    cout << "Opening Hands" << endl;
    leftArm->OpenHand();
    rightArm->OpenHand();
    FinishMovingLimbs();
    cout << "Finished opening hands" << endl;

    if(useStaticMotions)
    {
        cout << "Reaching Forward" << endl;
        PlayAction(REACH_FORWARD_MOTION);
        cout << "Finished reaching forward" << endl;
    }
    else
    {
        // we know how high the second rung is, so reach out and grab it
        double shoulderHeight = Kinematics::ANKLE_LENGTH+Kinematics::CALF_LENGTH+Kinematics::THIGH_LENGTH+Kinematics::TORSO_LENGTH;
        double armLength = Kinematics::SHOULDER_BOTTOM_OFFSET+Kinematics::UPPER_ARM_LENGTH+Kinematics::LOWER_ARM_LENGTH;
        double ladderLength = ladder.SecondRungHeight();// + AIM_OFFSET;

        // cosine law to get the shoulder pitch
        double shoulderPitch = Math::GetInternalAngle(shoulderHeight,armLength,ladderLength);

        CopyPosition(leftArm->m_Joint,MotionStatus::m_CurrentJoints);
        CopyPosition(rightArm->m_Joint,MotionStatus::m_CurrentJoints);

        leftArm->m_Joint.SetEnableLeftArmOnly(true,true);
        rightArm->m_Joint.SetEnableRightArmOnly(true,true);

        leftArm->SetAngles(shoulderPitch,90,0);
        rightArm->SetAngles(shoulderPitch,90,0);
        FinishMovingLimbs();

        leftArm->SetAngles(shoulderPitch,SHOULDER_ROLL_ANGLE,0);
        rightArm->SetAngles(shoulderPitch,SHOULDER_ROLL_ANGLE,0);
        FinishMovingLimbs();
    }

    cout << "Relaxing hands around first rung" << endl;
    leftArm->RelaxHand();
    rightArm->RelaxHand();
    FinishMovingLimbs();
    cout << "Finished relaxing hands" << endl;

    Head::GetInstance()->MoveByAngle(PAN_LEFT,TILT_DOWN);

    if(useStaticMotions)
    {
        PlayAction(RAISE_LEFT_LEG_MOTION);
        PlayAction(LOWER_LEFT_LEG_MOTION);
    }
    else
    {
        PlayAction(RAISE_LEFT_LEG_MOTION,false,false,true,true);
        ExtendLeg(LeftLeg::GetInstance());
    }

    Head::GetInstance()->MoveByAngle(PAN_LEFT,TILT_DOWN);

    if(useStaticMotions)
    {
        PlayAction(RAISE_RIGHT_LEG_MOTION);
        PlayAction(LOWER_RIGHT_LEG_MOTION);
    }
    else
    {
        PlayAction(RAISE_RIGHT_LEG_MOTION,false,false,true,true);
        ExtendLeg(RightLeg::GetInstance());
    }

    Head::GetInstance()->MoveByAngle(0,0);

    Action::GetInstance()->m_Joint = MotionStatus::m_CurrentJoints;
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    SquatOnRung();

    numRungs--;

    cout << "Mounted ladder. " << numRungs << " rungs remaining" << endl;

    if(pauseOnRung)
    {
        PlayAction(FINISH_MOTION);
        Pause3seconds();
        SquatOnRung();
    }

    if(FinishedClimbing())
        myState = STATE_FINISHING;
    else
        myState = STATE_MOVING_RIGHT_ARM;
}

void WallClimber::Process_MoveRightArm()
{
    cout << "Moving Right Arm" << endl;

    Head::GetInstance()->MoveByAngle(PAN_RIGHT,TILT_UP);

    rightArm->OpenHand();
    rightArm->Finish();
    cout << "Finished opening hand" << endl;

    if(useStaticMotions)
    {
        PlayAction(RIGHT_ARM_BACK_MOTION);
        PlayAction(RIGHT_ARM_FORWARD_MOTION);
    }
    else
    {
        PlayAction(RIGHT_ARM_BACK_MOTION,false,true,false,false);

        // guesstimate how far away the next rung is
        double distanceToNextRung = FindDistanceToNextRung();

        // the distance between the rung we're holding right now and the rung our feet are on
        // it's going to be slightly off because the toe is on the rung, not the centre of the foot
        // but it should be a close approximation
        //double rungSeparation = (leftArm->GetAbsolutePosition().XZ() - leftLeg->GetAbsolutePosition().XZ()).Magnitude();
        double rungSeparation = rungSeparations.back(); // use the previous rung separation instead of using the FK

        // if the rungs are too close together then we use a different stand motion
        double legLengthStanding;
        if(rungSeparation < CLOSE_SQUAT_SEPARATION)
            legLengthStanding = 160.0;
        else
            legLengthStanding = 200.0;

        double armLength = Kinematics::SHOULDER_BOTTOM_OFFSET+Kinematics::UPPER_ARM_LENGTH+Kinematics::LOWER_ARM_LENGTH;
        double torsoLength = Kinematics::TORSO_LENGTH-Kinematics::HIP_DROP_LENGTH;

        if(enableDebugPrinting)
            cout << "Support Rung Separation: " << rungSeparation << endl <<
                    "Torso Length: " << torsoLength << endl <<
                    "Leg Length: " << legLengthStanding << endl <<
                    "Arm Length: " << armLength << endl;

        // pitch we need for the left shoulder to keep our grip on the lower rung
        double leftShoulderPitch = Math::GetInternalAngle(legLengthStanding+torsoLength, armLength,rungSeparation);

        // pitch we need for the left shoulder to grab the upper rung
        // TODO: this is consistently slightly too low
        double rightShoulderPitch = Math::GetInternalAngle(legLengthStanding + torsoLength, armLength, rungSeparation+distanceToNextRung+AIM_OFFSET);

        if(enableDebugPrinting)
            cout << "L Arm Pitch: " << leftShoulderPitch << endl <<
                    "R Arm Pitch: " << rightShoulderPitch << endl;

        if(stepByStep)
            WaitForKeyboard();

        // raise the right arm to our desired angle
        SetArmAngles(rightArm,rightShoulderPitch,SHOULDER_ROLL_ANGLE,0);

        // extend the legs while moving the left arm toward the desired final position
        CopyPosition(leftArm->m_Joint,MotionStatus::m_CurrentJoints);
        leftArm->m_Joint.SetEnableLeftArmOnly(true,true);
        leftArm->SetAngles(leftShoulderPitch,SHOULDER_ROLL_ANGLE,0);

        rightArm->MoveHand(SEMI_OPEN_HAND_ANGLE);
        FinishMovingLimbs();

        if(rungSeparation < CLOSE_SQUAT_SEPARATION)
            PlayAction(STAND_RUNG_CLOSE_MOTION,false,false,true,true,WallClimber::OpenHandWhileStanding);
        else
            PlayAction(STAND_RUNG_MOTION,false,false,true,true,WallClimber::OpenHandWhileStanding);

        // we're securely on the next rung, so push that distance onto the list of separations
        rungSeparations.push_back(distanceToNextRung);
    }

    cout << "Closing right hand" << endl;
    rightArm->RelaxHand();
    FinishMovingLimbs();
    cout << "Done" << endl;

    myState = STATE_MOVING_LEFT_ARM;
}

double WallClimber::FindDistanceToNextRung()
{
    double distanceToNextRung = 0.0;

    double d[AIM_NUM_FRAMES];
    for(int i=0; i<AIM_NUM_FRAMES; i++)
    {//i=0; // shortcut for vision debugging
        if(ladder.GetAllLines()->size() == 0)
        {
            cout << i << ". Didn't see anything.  Trying again" << endl;
            i--;
        }
        else
        {
            double r;
            unsigned int j=0;
            do
            {
                if(enableDebugPrinting)
                    cout << "Rung #" << (j+1) << " in frame" << endl;
                r = ladder.GetDistanceToRung(ladder.GetAllLines()->at(j),enableDebugPrinting);
                j++;
            }while(r < MIN_DISTANCE_BETWEEN_RUNGS && j < ladder.GetAllLines()->size());

            d[i] = r;
            cout << (i+1) << ". Next rung is " << d[i] << "mm away" << endl;
        }
        while(!visionSync);
        visionSync = false;
    }

    switch(FILTER_METHOD)
    {
    case FILTER_MEDIAN:
        distanceToNextRung = median(d,AIM_NUM_FRAMES);
    case FILTER_AVERAGE:
    default:
        distanceToNextRung = average(d,AIM_NUM_FRAMES);
    }
    cout << "Next rung is " << distanceToNextRung << "mm away" << endl;
    return distanceToNextRung;
}

void WallClimber::Process_MoveLeftArm()
{
    cout << "Moving Left Arm" << endl;

    Head::GetInstance()->MoveByAngle(PAN_LEFT,0);

    leftArm->OpenHand();
    leftArm->Finish();
    cout << "Finished opening hand" << endl;

    if(useStaticMotions)
    {
        PlayAction(LEFT_ARM_BACK_MOTION);
        PlayAction(LEFT_ARM_FORWARD_MOTION);
    }
    else
    {
        PlayAction(LEFT_ARM_BACK_MOTION,true,false,false,false);

        leftArm->MoveHand(SEMI_OPEN_HAND_ANGLE);
        FinishMovingLimbs();

        // copy the right arm's position to the left arm
        SetArmAngles(leftArm, rightArm->GetShoulderPitch(), rightArm->GetShoulderRoll(), rightArm->GetElbowAngle());
    }

    leftArm->RelaxHand();
    rightArm->RelaxHand();
    FinishMovingLimbs();

    myState = STATE_MOVING_LEFT_LEG;
}

void WallClimber::Process_MoveLeftLeg()
{
    cout << "Moving Left Leg" << endl;

    Head::GetInstance()->MoveByAngle(PAN_LEFT,TILT_DOWN);

    if(useStaticMotions)
    {
        PlayAction(RAISE_LEFT_LEG_MOTION);
        PlayAction(LOWER_LEFT_LEG_MOTION);
    }
    else
    {
        CopyPosition(leftArm->m_Joint,MotionStatus::m_CurrentJoints);
        CopyPosition(rightArm->m_Joint,MotionStatus::m_CurrentJoints);
        leftArm->m_Joint.SetEnableLeftArmOnly(true,true);
        rightArm->m_Joint.SetEnableRightArmOnly(true,true);

        double angle = CalculateShoulderPitchForLegLift();
        leftArm->SetAngles(angle,SHOULDER_ROLL_ANGLE,0);
        rightArm->SetAngles(angle,SHOULDER_ROLL_ANGLE,0);

        // use the fixed motion to extend the support leg and raise the left leg
        PlayAction(RAISE_LEFT_LEG_MOTION,false,false,true,true);

        // extend the left leg until we hit the rung
        ExtendLeg(LeftLeg::GetInstance());
    }

    myState = STATE_MOVING_RIGHT_LEG;
}

void WallClimber::Process_MoveRightLeg()
{
    cout << "Moving Right Leg" << endl;

    Head::GetInstance()->MoveByAngle(PAN_RIGHT,TILT_DOWN);

    if(useStaticMotions)
    {
        PlayAction(RAISE_RIGHT_LEG_MOTION);
        PlayAction(LOWER_RIGHT_LEG_MOTION);

        // squat stably on the rung
        SquatOnRung();
    }
    else
    {
        CopyPosition(leftArm->m_Joint,MotionStatus::m_CurrentJoints);
        CopyPosition(rightArm->m_Joint,MotionStatus::m_CurrentJoints);
        leftArm->m_Joint.SetEnableLeftArmOnly(true,true);
        rightArm->m_Joint.SetEnableRightArmOnly(true,true);

        double angle = CalculateShoulderPitchForSquat(rungSeparations.back());
        leftArm->SetAngles(angle,SHOULDER_ROLL_ANGLE,0);
        rightArm->SetAngles(angle,SHOULDER_ROLL_ANGLE,0);

        // use the fixed motion to extend the support leg and raise the left leg
        PlayAction(RAISE_RIGHT_LEG_MOTION,false,false,true,true);

        // extend the right leg until we hit the rung
        ExtendLeg(RightLeg::GetInstance());
        SquatOnRung();
    }

    numRungs--;
    cout << "Climbed another rung. " << numRungs << " remaining." << endl;

    if(FinishedClimbing())
    {
        myState = STATE_FINISHING;
    }
    else
    {
        if(pauseOnRung)
            myState = STATE_PAUSED;
        else
            myState = STATE_MOVING_RIGHT_ARM;
    }
}

void WallClimber::Process_Finish()
{
    Voice::Speak("Finished Climbing");
    cout << "[info] Entering finishing stance" << endl;

    PlayAction(FINISH_MOTION);
    leftArm->CloseHand();
    rightArm->CloseHand();
    myState = STATE_IDLE;
}

const string WallClimber::state2txt()
{
    switch(myState)
    {
    case STATE_IDLE:
        return "Idle";
    case STATE_WALKING_TO_LADDER:
        return "Walking";
    case STATE_MOUNTING_LADDER:
        return "Mounting ladder";
    case STATE_MOVING_LEFT_ARM:
        return "Moving left arm";
    case STATE_MOVING_RIGHT_ARM:
        return "Moving right arm";
    case STATE_MOVING_LEFT_LEG:
        return "Moving left leg";
    case STATE_MOVING_RIGHT_LEG:
        return "Moving right leg";
    case STATE_PAUSED:
        return "Paused";
    case STATE_FINISHING:
        return "Finishing";
    case STATE_DEBUG:
        return "[debug]";
    default:
        return "[unknown]";
    }
}

void WallClimber::Pause3seconds()
{
    time_t start,end;
    Voice::Speak("Pausing");
    cout << "[info] Pausing for 3 seconds" << endl;

    //leftArm->CloseHand();
    //rightArm->CloseHand();
    //FinishMovingLimbs();

    bool waiting = true;
    start = time(NULL);
    while(waiting)
    {
        end = time(NULL);

        if(difftime(end,start) >= 3.0)
            waiting = false;
    }

    Voice::Speak("Resuming");
    cout << "[info] Resuming climb" << endl;
}

bool WallClimber::FinishedClimbing()
{
    return numRungs <= 1;
}

void WallClimber::CopyPosition(JointData &dst, JointData &src)
{
    // set the positions, but DO NOT touch anything else
    for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
        dst.SetValue(i,src.GetValue(i));
}

void WallClimber::PlayAction(int id, bool leftArm, bool rightArm, bool leftLeg, bool rightLeg, void (*waitFunc)(void))
{
    CopyPosition(Action::GetInstance()->m_Joint, MotionStatus::m_CurrentJoints);

    JointData &joints= Action::GetInstance()->m_Joint;

    joints.SetEnableBody(false);
    if(leftArm)
    {
        joints.SetEnable(JointData::ID_L_SHOULDER_PITCH,true,true);
        joints.SetEnable(JointData::ID_L_SHOULDER_ROLL,true,true);
        joints.SetEnable(JointData::ID_L_ELBOW,true,true);
    }

    if(rightArm)
    {
        joints.SetEnable(JointData::ID_R_SHOULDER_PITCH,true,true);
        joints.SetEnable(JointData::ID_R_SHOULDER_ROLL,true,true);
        joints.SetEnable(JointData::ID_R_ELBOW,true,true);
    }

    if(leftLeg)
    {
        joints.SetEnable(JointData::ID_L_HIP_YAW,true,true);
        joints.SetEnable(JointData::ID_L_HIP_ROLL,true,true);
        joints.SetEnable(JointData::ID_L_HIP_PITCH,true,true);
        joints.SetEnable(JointData::ID_L_KNEE,true,true);
        joints.SetEnable(JointData::ID_L_ANKLE_ROLL,true,true);
        joints.SetEnable(JointData::ID_L_ANKLE_PITCH,true,true);
    }

    if(rightLeg)
    {
        joints.SetEnable(JointData::ID_R_HIP_YAW,true,true);
        joints.SetEnable(JointData::ID_R_HIP_ROLL,true,true);
        joints.SetEnable(JointData::ID_R_HIP_PITCH,true,true);
        joints.SetEnable(JointData::ID_R_KNEE,true,true);
        joints.SetEnable(JointData::ID_R_ANKLE_ROLL,true,true);
        joints.SetEnable(JointData::ID_R_ANKLE_PITCH,true,true);
    }

    LeftArm::GetInstance()->m_Joint.SetEnableLeftHandOnly(true,true);
    RightArm::GetInstance()->m_Joint.SetEnableRightHandOnly(true,true);
    Action::GetInstance()->Start(id);
    Action::GetInstance()->Finish(waitFunc);
}

void WallClimber::ExtendLeg(Leg *leg)
{
    CopyPosition(leg->m_Joint, MotionStatus::m_CurrentJoints);
    leg->SetEnable(true,true);
    // make the legs point forward
    // this is a little hackish, since we don't know which leg we're dealing with
    // but we trust the Leg module to not mess up the other leg
    leg->m_Joint.SetBodyAngle(JointData::ID_L_HIP_YAW,0);
    leg->m_Joint.SetBodyAngle(JointData::ID_R_HIP_YAW,0);

    leg->ANKLE_PITCH_OFFSET = EXTEND_ANKLE_PITCH_OFFSET;

    leg->Extend(LEG_SPEED);
    FinishMovingLimbs();
}

void WallClimber::SetArmAngles(Arm *arm, double shoulderPitch, double shoulderRoll, double elbow)
{
    CopyPosition(arm->m_Joint, MotionStatus::m_CurrentJoints);
    arm->SetEnable(true,true);
    arm->SetAngles(shoulderPitch, shoulderRoll, elbow);
    FinishMovingLimbs();
}

double WallClimber::average(double values[], int numValues)
{
    sort(values,values+numValues);

    double total = 0.0;
    for(int i=DISCARD_RUNG_OUTLIERS; i<numValues-DISCARD_RUNG_OUTLIERS; i++)
        total += values[i];
    return total/(numValues-2*DISCARD_RUNG_OUTLIERS);
}

double WallClimber::median(double values[], int numValues)
{
    sort(values,values+numValues);

    return values[numValues/2];
}

void WallClimber::SquatOnRung()
{
    if(useStaticMotions)
    {
        PlayAction(SQUAT_RUNG_MOTION);
    }
    else
    {
        if(enableDebugPrinting)
            cout << "Squatting on rung..." << endl;

        double separation = rungSeparations.back();
        double shoulderAngle = CalculateShoulderPitchForSquat(separation);
        double elbowAngle = 0.0;

        if(enableDebugPrinting)
            cout << "\tSeparation between feet and support rung: " << separation << "mm" << endl <<
                    "\tShoulder angle: " << shoulderAngle << " deg" << endl;

        CopyPosition(leftArm->m_Joint, MotionStatus::m_CurrentJoints);
        CopyPosition(rightArm->m_Joint, MotionStatus::m_CurrentJoints);

        leftArm->m_Joint.SetEnableLeftArmOnly(true,true);
        rightArm->m_Joint.SetEnableRightArmOnly(true,true);

        leftArm->SetAngles(shoulderAngle,SHOULDER_ROLL_ANGLE,elbowAngle);
        rightArm->SetAngles(shoulderAngle,SHOULDER_ROLL_ANGLE,elbowAngle);

        if(separation < CLOSE_SQUAT_SEPARATION)
            PlayAction(SQUAT_RUNG_CLOSE_MOTION,false,false,true,true);
        else
            PlayAction(SQUAT_RUNG_MOTION,false,false,true,true);
        FinishMovingLimbs();

        if(enableDebugPrinting)
            cout << "Finished squat" << endl;

        Wiggle();
    }
}

void WallClimber::Wiggle()
{
    // wiggle around to try to force the robot into a backwards-leaning pose with the arms in tension
    if(enableDebugPrinting)
        cout << "Wiggling..." << endl <<
                "\tShoulder: " << (WIGGLE_SHOULDER_ANGLE>0 ? "+" : "") << WIGGLE_SHOULDER_ANGLE << " deg" << endl <<
                "\tKnee: " << (WIGGLE_KNEE_ANGLE>0 ? "+" : "") << WIGGLE_KNEE_ANGLE << " deg" << endl;

    JointData newPose;
    JointData oldPose;

    CopyPosition(oldPose,MotionStatus::m_CurrentJoints);
    CopyPosition(newPose,MotionStatus::m_CurrentJoints);
    CopyPosition(leftArm->m_Joint,MotionStatus::m_CurrentJoints);
    CopyPosition(rightArm->m_Joint,MotionStatus::m_CurrentJoints);
    CopyPosition(leftLeg->m_Joint,MotionStatus::m_CurrentJoints);
    CopyPosition(rightLeg->m_Joint,MotionStatus::m_CurrentJoints);
    leftArm->m_Joint.SetEnableLeftArmOnly(true,true);
    rightArm->m_Joint.SetEnableRightArmOnly(true,true);
    leftLeg->m_Joint.SetEnableLeftLegOnly(true,true);
    rightLeg->m_Joint.SetEnableRightLegOnly(true,true);

    newPose.SetBodyAngle(JointData::ID_R_SHOULDER_PITCH,oldPose.GetBodyAngle(JointData::ID_R_SHOULDER_PITCH)+WIGGLE_SHOULDER_ANGLE);
    newPose.SetBodyAngle(JointData::ID_L_SHOULDER_PITCH,oldPose.GetBodyAngle(JointData::ID_L_SHOULDER_PITCH)+WIGGLE_SHOULDER_ANGLE);
    newPose.SetBodyAngle(JointData::ID_R_KNEE,oldPose.GetBodyAngle(JointData::ID_R_KNEE)+WIGGLE_KNEE_ANGLE);
    newPose.SetBodyAngle(JointData::ID_L_KNEE,oldPose.GetBodyAngle(JointData::ID_L_KNEE)+WIGGLE_KNEE_ANGLE);

    leftArm->SetAngles(newPose);
    rightArm->SetAngles(newPose);
    leftLeg->SetAngles(newPose);
    rightLeg->SetAngles(newPose);
    FinishMovingLimbs();

    if(enableDebugPrinting)
        cout << "\tSettling back" << endl;

    leftArm->SetAngles(oldPose);
    rightArm->SetAngles(oldPose);
    leftLeg->SetAngles(oldPose);
    rightLeg->SetAngles(oldPose);
    FinishMovingLimbs();

    if(enableDebugPrinting)
        cout << "Finished wiggling" << endl;
}

double WallClimber::CalculateShoulderPitchForSquat(const double RUNG_SEPARATION)
{
    // calculate the shoulder pitch angle needed so that we can squat stably on this rung
    // this is determined by the distance between the rung our hands are on
    // and the rung our feet are on
    double FOOT_TO_SHOULDER = 250; //mm, measured by hand when the robot is sitting in the close squat position
    if(RUNG_SEPARATION >= CLOSE_SQUAT_SEPARATION)
        FOOT_TO_SHOULDER = 235; //mm, measured by hand when the robot is sitting in the standard squat position

    const double SHOULDER_TO_RUNG = Kinematics::SHOULDER_BOTTOM_OFFSET + Kinematics::UPPER_ARM_LENGTH + Kinematics::LOWER_ARM_LENGTH;

    double shoulderAngle = Math::GetInternalAngle(FOOT_TO_SHOULDER, SHOULDER_TO_RUNG, RUNG_SEPARATION);

    cout << "Pitch for " << RUNG_SEPARATION << " = " << shoulderAngle << endl;

    if(shoulderAngle < MIN_SQUAT_SHOULDER_PITCH)
        shoulderAngle = MIN_SQUAT_SHOULDER_PITCH;
    else if(shoulderAngle > MAX_SQUAT_SHOULDER_PITCH)
        shoulderAngle = MAX_SQUAT_SHOULDER_PITCH;

    return shoulderAngle;
}

double WallClimber::CalculateShoulderPitchForLegLift()
{
    // calculate the shoulder pitch angle needed when lifting the left leg from a lower rung to a higher one

    // support foot position using standard stand
    Point3D SUPPORT_FOOT_POSITION = Point3D(0,0,-Kinematics::LEG_LENGTH); //rightLeg->GetAbsolutePosition(MotionStatus::m_CurrentJoints);

    Point3D HAND_POSITION = rightArm->GetAbsolutePosition(MotionStatus::m_CurrentJoints);
    Point3D SHOULDER_ORIGIN(Kinematics::SHOULDER_FRONT_OFFSET,Kinematics::SHOULDER_WIDTH/2.0,Kinematics::TORSO_LENGTH-Kinematics::HIP_DROP_LENGTH);

    double SHOULDER_TO_HAND = leftArm->GetPosition().XZ().Magnitude();
    double SUPPORT_FOOT_TO_HAND = (SUPPORT_FOOT_POSITION.XZ() - HAND_POSITION.XZ()).Magnitude();
    //double SUPPORT_FOOT_TO_GOAL_FOOT = rungSeparations.back() + rungSeparations.at(rungSeparations.size()-2);
    double SUPPORT_FOOT_TO_SHOULDER = (SHOULDER_ORIGIN.XZ() - SUPPORT_FOOT_POSITION.XZ()).Magnitude();

    cout << "Shoulder to hand: " << SHOULDER_TO_HAND << endl <<
            "Foot to hand: " << SUPPORT_FOOT_TO_HAND << endl <<
            "Foot to shoulder: " << SUPPORT_FOOT_TO_SHOULDER << endl;

    //rightArm->OpenHand();
    //leftArm->OpenHand();
    //for(;;){}

    double shoulderAngle = Math::GetInternalAngle(SHOULDER_TO_HAND, SUPPORT_FOOT_TO_SHOULDER, SUPPORT_FOOT_TO_HAND);

    return shoulderAngle;
}

char WallClimber::WaitForKeyboard()
{
    struct termios oldt, newt;
    int ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    return ch;
}

void WallClimber::OpenHandWhileStanding()
{
	if(Action::GetInstance()->IsRunning() && STAND_HAND_OPEN_SPEED != 0.0)
	{
		double handAngle = LeftArm::GetInstance()->GetHandAngle();
		handAngle += STAND_HAND_OPEN_SPEED;
		
		if(handAngle > LeftArm::GetInstance()->OPEN_ANGLE)
			handAngle = LeftArm::GetInstance()->OPEN_ANGLE;
		else if(handAngle < LeftArm::GetInstance()->CLOSE_ANGLE)
			handAngle = LeftArm::GetInstance()->CLOSE_ANGLE;
		
		if(handAngle < LeftArm::GetInstance()->OPEN_ANGLE && handAngle > LeftArm::GetInstance()->CLOSE_ANGLE)
			LeftArm::GetInstance()->MoveHand(handAngle);
	}
}
