#include <WallClimber.h>

using namespace std;
using namespace Robot;
using namespace cv;

int main(int argc, char** argv)
{
    WallClimber *climber = new WallClimber();
    climber->ParseArguments(argc, argv);
    climber->Initialize("config.ini");
    climber->Execute();

    return 0;
}
