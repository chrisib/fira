#include "LadderTarget.h"
#include <iostream>
#include <opencv2/core/core.hpp>
#include "Util.h"
#include <cstdio>
#include <darwin/framework/Math.h>
#include <darwin/framework/Head.h>
#include <darwin/framework/CameraPosition.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/LeftLeg.h>
#include <darwin/framework/RightLeg.h>
#include <darwin/framework/Kinematics.h>

using namespace Robot;
using namespace std;
using namespace cv;

// acceleration due to gravity -- used to calculate the torso's inclination
#define G 9.81

LadderTarget::LadderTarget() : MultiLine()
{
}

LadderTarget::~LadderTarget()
{
}

void LadderTarget::Draw(Mat &canvas)
{
    // draw buckets, raw lines
    LineTarget::Draw(canvas);

    // draw the bottom rung in a contrasting colour
    Line2D *l;
    for(vector<Line2D*>::iterator it=lines.begin(); it!=lines.end(); it++)
    {
        l = *it;

        if(it == lines.begin())
            cv::line(canvas,Point(l->start.X,l->start.Y),Point(l->end.X,l->end.Y),Scalar(0,255,0),2);
        else
            cv::line(canvas,Point(l->start.X,l->start.Y),Point(l->end.X,l->end.Y),markColour,2);

        double d = GetDistanceToRung(l);
        char buffer[255];
        sprintf(buffer,"%0.2fcm",d/10.0);
        Point2D ctr = (l->start+l->end)/2.0;
        cv::putText(canvas,buffer,Point(ctr.X,ctr.Y),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,255));
    }
}

void LadderTarget::Print()
{
    cout << "Ladder:" << endl <<
            "  Rungs:" << endl;
    for(vector<Line2D*>::iterator it = lines.begin(); it!=lines.end(); it++)
    {
        Line2D* l = (Line2D*)(*it);
        cout << "    (" << l->start.X << " , " << l->start.Y << ") -- (" << l->end.X << " , " << l->end.Y << ")" << endl;
    }
}

void LadderTarget::LoadIniSettings(minIni &ini, const std::string &section)
{
    cout << "Loading rung colour profile from section " << ini.gets(section,"RungColour") << endl;

    MultiLine::LoadIniSettings(ini,ini.gets(section,"RungColour","Red"));

    numRungs = ini.geti(section,"NumRungs",8);
    ladderWidth = ini.getd(section,"Width",50);
    minSeparation = ini.getd(section,"MinSeparation",10);
    maxSeparation = ini.getd(section,"MaxSeparation",15);
    inclination = ini.getd(section,"Inclination",45.0);

    // TODO: these should probably be either dynamically calculated OR passed as command-line parameters
    firstRung = ini.getd(section,"FirstRung",150);
    secondRung = ini.getd(section,"SecondRung",300);

    visionBias = ini.getd(section,"VisionBias",0.0);

    rungThickness = ini.getd(section,"RungThickness",16);
}

// given a rung on the ladder figure out how far away this rung is, moving along the ladder
// should be between 10 and 15cm, according to the FIRA 2014 rules
double LadderTarget::GetDistanceToRung(Line2D *rung, bool debugPrint)
{
#ifndef USE_INCLINATION
    double fbAccel = ((MotionStatus::FB_ACCEL/128.0) - 4.0) * G;
    double zAccel = ((MotionStatus::Z_ACCEL/128.0) - 4.0) * G;
    double torsoInclination = -atan2(fbAccel, zAccel);

    Point2D footPosition = LeftLeg::GetInstance()->GetAbsolutePosition(MotionStatus::m_CurrentJoints).XZ();
    Point2D handPosition = LeftArm::GetInstance()->GetAbsolutePosition(MotionStatus::m_CurrentJoints).XZ();
    Point2D cameraPosition = Head::GetInstance()->GetAbsolutePosition(MotionStatus::m_CurrentJoints).XZ();

    if(debugPrint)
        cout << "Foot: " << footPosition.X << " " << footPosition.Y << endl <<
                "Hand: " << handPosition.X << " " << handPosition.Y << endl <<
                "Head: " << cameraPosition.X << " " << cameraPosition.Y << endl;

    // calculate the inclination of the ladder relative to the torso
    // everything is in radians
    double ladderInclination = atan2(handPosition.X-footPosition.X,handPosition.Y-footPosition.Y) - torsoInclination;

    // angle from the face to the hand measured from horizontal
    double armAngle = atan2(fabs(handPosition.Y-cameraPosition.Y),fabs(handPosition.X-cameraPosition.X));

    // angle of the object in the frame
    double angleInFrame = (AngleInFrame(rung->start) + AngleInFrame(rung->end))/2.0; // average the angle to the start and end points of the line
    double angleToObject = deg2rad(Head::GetInstance()->GetTiltAngle()) + angleInFrame - deg2rad(Kinematics::EYE_TILT_OFFSET_ANGLE);

    // determine the angles of the triangle formed by the support hand, face, and the rung we're looking at
    double thetaHandFaceRung = angleToObject + armAngle;
    double thetaFaceHandRung = deg2rad(90)-armAngle + ladderInclination;
    double thetaFaceRungHand = PI - thetaHandFaceRung - thetaFaceHandRung;

    // get the distance between the hand and the face
    double distanceHandFace = (handPosition-cameraPosition).Magnitude();

    // use the sine law to determine the distance from the hand to the next rung
    double distanceHandRung = sin(thetaHandFaceRung) * distanceHandFace / sin(thetaFaceRungHand);

    if(debugPrint)
        cout << "Ladder Inclination: " << rad2deg(ladderInclination) << " deg (relative to torso)" << endl <<
                "Arm angle: " << rad2deg(armAngle) << " deg" << endl <<
                "Head tilt: " << Head::GetInstance()->GetTiltAngle() << " deg" << endl <<
                "Object in frame: " << rad2deg(angleInFrame) << " deg" << endl <<
                "Angle to object: " << rad2deg(angleToObject) << " deg" << endl <<
                "Face to hand distance: " << distanceHandFace << "mm" << endl <<
                "Hand-Face-Rung angle: " << rad2deg(thetaHandFaceRung) << " deg" << endl <<
                "Face-hand-rung angle: " << rad2deg(thetaFaceHandRung) << " deg" << endl <<
                "Face-rung-hand angle: " << rad2deg(thetaFaceRungHand) << " deg" << endl <<
                "Hand to rung distance: " << distanceHandRung << "mm (before applying bias of " << visionBias << "mm)" << endl;

    return distanceHandRung + visionBias + rungThickness;
#else
    // figure out how many Gs are acting along each axis
    double fbAccel = ((MotionStatus::FB_ACCEL/128.0) - 4.0) * G;
    double zAccel = ((MotionStatus::Z_ACCEL/128.0) - 4.0) * G;
    double torsoInclination = -rad2deg(atan2(fbAccel, zAccel));

    // figure out the absolute inclination of the camera in the front-back direction
    // this works best if the pan angle is zero (otherwise it gets noisy)
    double cameraInclination = torsoInclination + Head::GetInstance()->GetTiltAngle() - Kinematics::EYE_TILT_OFFSET_ANGLE;

    // determine the angle from the camera to the object
    Point2D ctr = (rung->start+rung->end)/2.0;
    double angleInFrame = AngleInFrame(ctr);
    double angleToObject = cameraInclination - angleInFrame;

    // work from the shoulder as the origin in the X-Z plane
    Point3D handPosition3D = LeftArm::GetInstance()->GetPosition(MotionStatus::m_CurrentJoints);
    Point3D facePosition3D = Head::GetInstance()->GetPosition(MotionStatus::m_CurrentJoints);
    Point2D handPosition = handPosition3D.XZ();
    Point2D facePosition = facePosition3D.XZ();

    // the angle from the face to the hand in the body's coordinate system
    double thetaFaceToHand = rad2deg(atan2(fabs(handPosition.Y-facePosition.Y),fabs(handPosition.X-facePosition.X)));

    // offset the face-hand angle to be relative to ground
    thetaFaceToHand += torsoInclination;

    // we have 3 points forming a triangle: the face, the hand, and the rung we can see
    // call those A, B, and C
    // we know AB, and angles BAC and ABC
    // therefore we can construct the complete triangle
    double handFaceDistance = (handPosition - facePosition).Magnitude(); // distance from face to hand
    double handFaceRungAngle = thetaFaceToHand + angleToObject; // angle at face between hand and rung
    double faceHandRungAngle = (90 - inclination) + (90 - thetaFaceToHand);

    // the last angle is easy, since there are 180 degrees in the triangle
    double faceRungHandAngle = 180 - faceHandRungAngle - handFaceRungAngle;

    // finally use the sine law to find the distance between the hand and the next rung
    /* sinA/A = sinB/B = sinC/C */
    double lenBC = handFaceDistance * sin(deg2rad(handFaceRungAngle)) / sin(deg2rad(faceRungHandAngle));

    if(debugPrint)
        cout << "Torso inclination: " << torsoInclination << endl <<
                "Camera Inclination: " << cameraInclination << endl <<
                "Object in frame: " << angleInFrame << endl <<
                "Angle to object: " << angleToObject << endl <<
                "Face to hand angle: " << thetaFaceToHand << endl <<
                "Face to hand distance: " << handFaceDistance << endl <<
                "Hand-Face-Rung angle: " << handFaceRungAngle << endl <<
                "Face-hand-rung angle: " << faceHandRungAngle << endl <<
                "Face-rung-hand angle: " << faceRungHandAngle << endl <<
                "Hand to rung distance: " << lenBC << endl;

#ifdef CLAMP_RANGE
    if(lenBC < minSeparation)
        lenBC = minSeparation;
    if(lenBC > maxSeparation)
        lenBC = maxSeparation;
#endif
    return lenBC;
#endif
}

double LadderTarget::AngleInFrame(Point2D xy)
{
    Point2D frameCenter = Point2D(Camera::WIDTH/2, Camera::HEIGHT/2);
    Point2D anglesInFrame = xy - frameCenter;
    anglesInFrame *= -1; // Inverse X-axis, Y-axis
    anglesInFrame.X *= (Camera::VIEW_H_ANGLE / (double)Camera::WIDTH); // pixel per angle
    anglesInFrame.Y *= (Camera::VIEW_V_ANGLE / (double)Camera::HEIGHT); // pixel per angle

    return deg2rad(anglesInFrame.Y);
}
