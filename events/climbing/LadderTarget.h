#ifndef LADDERRUNG_H
#define LADDERRUNG_H

#include <MultiLine.h>
#include <vector>

class LadderTarget : public Robot::MultiLine
{
public:
    LadderTarget();
    virtual ~LadderTarget();

    virtual void LoadIniSettings(minIni &ini, const std::string &section);
    virtual void Print();

    virtual void Draw(cv::Mat &canvas);

    virtual std::vector<Robot::Line2D*> *GetRungs(){return &(this->lines);}

    double GetDistanceToRung(Robot::Line2D *rung, bool debugPrint=false);

    int NumRungs(){return numRungs;}
    int Width(){return ladderWidth;}
    double Inclination(){return inclination;}
    double FirstRungHeight(){return firstRung;}
    double SecondRungHeight(){return secondRung;}
    double RungThickness(){return rungThickness;}

    double MaxSeparation(){return maxSeparation;}
    double MinSeparation(){return minSeparation;}

protected:

    int numRungs;
    double ladderWidth;
    double minSeparation,maxSeparation;
    double inclination;
    double firstRung, secondRung;
    double visionBias;
    double rungThickness;

    double AngleInFrame(Robot::Point2D xy);
};

#endif // LADDERRUNG_H
