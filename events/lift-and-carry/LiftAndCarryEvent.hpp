#ifndef LIFT_AND_CARRY_EVENT
#define LIFT_AND_CARRY_EVENT

#include <string>

#include "Settings.hpp"

namespace Robot
{
    class CM730;
    class LinuxSerialPort;
}

using std::string;
using Robot::CM730;
using Robot::LinuxSerialPort;

class LiftAndCarryEvent
{
public:
    LiftAndCarryEvent();
    ~LiftAndCarryEvent();

    bool initialize();
    void destroy();
    void execute();

    bool isInitialized() const;
    string getErrorString() const;

private:
    void setSlowWalkingParams();
    void waitForStopWalking();
    void sleep(int ms) const;

private:
    bool initialized;
    CM730 *cm730;
    LinuxSerialPort *linuxCM730;
    string errorString;
    Settings settings;
};

#endif
