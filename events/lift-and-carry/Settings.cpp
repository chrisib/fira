#include "Settings.hpp"

#include "minIni.h"

bool Settings::load(const string &fileName)
{
    minIni *config = new minIni(fileName.c_str());
    bool success = true;

    if (-1.0 == (x_move_amplitude = config->getd("SLOW_WALK", "XMoveAmplitude", -1.0)))
        success = false;

    if (-1.0 == (y_move_amplitude = config->getd("SLOW_WALK", "YMoveAmplitude", -1.0)))
        success = false;

    if (-1.0 == (z_move_amplitude = config->getd("SLOW_WALK", "ZMoveAmplitude", -1.0)))
        success = false;

    if (-1.0 == (z_offset = config->getd("SLOW_WALK", "ZOffset", -1.0)))
        success = false;

    if (-1.0 == (period_time = config->getd("SLOW_WALK", "PeriodTime", -1.0)))
        success = false;

    if (-1 == (hip_pitch_offset = config->geti("SLOW_WALK", "HipPitchOffset", -1)))
        success = false;

    delete config;
    return success;
}
