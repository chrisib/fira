#include "LiftAndCarryEvent.hpp"

#include <QTimer>
#include <QTime>

#include <CM730.h>
#include "LinuxCM730.h"
#include "MotionManager.h"
#include "Walking.h"
#include "LinuxCamera.h"
#include "LinuxMotionTimer.h"

#include <iostream>
using namespace std;

using Robot::LinuxCM730;
using Robot::MotionManager;
using Robot::Walking;
using Robot::LinuxMotionTimer;
using Robot::LinuxCamera;

LiftAndCarryEvent::LiftAndCarryEvent()
{
    cm730 = NULL;
    linuxCM730 = NULL;
    initialized = false;
}

LiftAndCarryEvent::~LiftAndCarryEvent()
{
    destroy();
}

bool LiftAndCarryEvent::initialize()
{
    if (initialized)
        destroy();

    // Load INI settings.
    if (!settings.load("./config.ini"))
    {
        errorString = "could not load settings";
        destroy();
        return false;
    }

    // Initialize the CM730.
    linuxCM730 = new LinuxCM730("/dev/ttyUSB0");
    cm730 = new CM730(linuxCM730);

    // Initialize the motion manager.
    if (!MotionManager::GetInstance()->Initialize(cm730))
    {
        destroy();
        errorString = "could not initialize motion manager";
        return false;
    }

    MotionManager::GetInstance()->AddModule(Walking::GetInstance());

    // Initialize the linux camera.
    if (1 != LinuxCamera::GetInstance()->Initialize(0))
    {
        destroy();
        errorString = "could not initialize linux camera";
        return false;
    }

    LinuxMotionTimer::Initialize(MotionManager::GetInstance());
    MotionManager::GetInstance()->SetEnable(true);
    LinuxMotionTimer::Start();
    Walking::GetInstance()->m_Joint.SetEnableBody(true);

    return (initialized = true);
}

void LiftAndCarryEvent::execute()
{
    if (!initialized)
        return;

    while (true)
    {
        setSlowWalkingParams();
        Walking::GetInstance()->Start();
        waitForStopWalking();
        /*
        setSlowWalkingParams();
        Walking::GetInstance()->Start();

        sleep(8000);
        Walking::GetInstance()->Stop();

        waitForStopWalking();
    
        Walking::GetInstance()->Z_MOVE_AMPLITUDE = 60;
        Walking::GetInstance()->X_MOVE_AMPLITUDE = 20;
        Walking::GetInstance()->PERIOD_TIME = 1000;
        Walking::GetInstance()->HIP_PITCH_OFFSET = 95;

        Walking::GetInstance()->Start();
        sleep(100);
        Walking::GetInstance()->Stop();
        waitForStopWalking();
        Walking::GetInstance()->Start();
        sleep(100);
        Walking::GetInstance()->Stop();
        waitForStopWalking();

        sleep(2000);
        */
    }
}

string LiftAndCarryEvent::getErrorString() const
{
    return errorString;
}

void LiftAndCarryEvent::destroy()
{
    LinuxMotionTimer::Start();

    if (NULL != cm730)
    {
        delete cm730;
        cm730 = NULL;
    }

    if (NULL != linuxCM730)
    {
        delete linuxCM730;
        linuxCM730 = NULL;
    }

    initialized = false;
}

void LiftAndCarryEvent::setSlowWalkingParams()
{
    Walking * w = Walking::GetInstance();
    w->X_MOVE_AMPLITUDE = settings.x_move_amplitude;
    w->Y_MOVE_AMPLITUDE = settings.y_move_amplitude;
    w->Z_MOVE_AMPLITUDE = settings.z_move_amplitude;
    w->Z_OFFSET = settings.z_offset;
    w->PERIOD_TIME = settings.period_time;
    w->HIP_PITCH_OFFSET = settings.hip_pitch_offset;
    cout << "default knee gain = " << w->BALANCE_KNEE_GAIN << endl;
    w->BALANCE_KNEE_GAIN = .75;
}

void LiftAndCarryEvent::waitForStopWalking()
{
    while (Walking::GetInstance()->IsRunning())
    {
        LinuxMotionTimer::msleep(20);
    }
}

void LiftAndCarryEvent::sleep(int ms) const
{
    if (ms <= 0)
        return;

    QTime time;
    time.start();

    while (time.elapsed() < ms)
    {
    }
}
