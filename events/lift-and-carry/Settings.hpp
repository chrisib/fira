#ifndef LIFT_AND_CARRY_HPP
#define LIFT_AND_CARRY_HPP

#include <string>

using std::string;

struct Settings
{
    bool load(const string &fileName);

    double x_move_amplitude;
    double y_move_amplitude;
    double z_move_amplitude;
    double z_offset;
    double period_time;
    int hip_pitch_offset;
};

#endif
