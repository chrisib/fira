#include <cstdlib>
#include <iostream>
#include <libgen.h>
#include <unistd.h>

#include "LiftAndCarryEvent.hpp"

using std::cout;
using std::endl;

//--------------------------------//
//       Function Prototypes      //
//--------------------------------//
bool changeToExeDir();

int main(int argc, char *argv[])
{
    if (!changeToExeDir())
    {
        cout << "error: could not change directory to that of the executable." << endl;
        return EXIT_FAILURE;
    }

    LiftAndCarryEvent event;

    if (!event.initialize())
    {
        cout << "error: " << event.getErrorString() << "." << endl;
        return EXIT_FAILURE;
    }

    cout << "lift and carry initialized" << endl;
    event.execute();
    cout << "finished execution" << endl;

    event.destroy();

    return EXIT_SUCCESS;
}

//------------------------------------------------------------
// changeToExeDir
//
// Change the program's current directory to the directory
// of this executable.
//
// Return: bool, true if successful.
//------------------------------------------------------------
bool changeToExeDir()
{
    char path[1024] = {0};

    if (-1 != readlink("/proc/self/exe", path, sizeof(path)))
    {
        if (-1 != chdir(dirname(path)))
        {
            return true;
        }
    }

    return false;
}
