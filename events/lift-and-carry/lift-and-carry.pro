#QT -= core gui

TARGET = lift-and-carry
LIBS += -lcv -lhighgui -lmlibrary -lespeak -L../../darwin/lib ../../darwin/lib/darwin.a
INCLUDEPATH += /usr/local/include/darwin \
               /usr/local/include/darwin/linux \
               /usr/local/include/darwin/framework

QMAKE_LFLAGS+=

SOURCES = main.cpp \
    LiftAndCarryEvent.cpp \
    Settings.cpp

HEADERS = LiftAndCarryEvent.hpp \
    Settings.hpp

OBJECTS_DIR = build
