#ifndef FEATURE_H
#define FEATURE_H

#include <vector>
#include "Point.h"
#include "Target.h"

#define POSN_ERR_FACTOR 1
//#define SIZE_ERR_FACTOR 1
#define PI 3.14159

namespace Robot
{
    class Feature
    {
        public:
            //static virtual std::vector<Feature*>* makeFeature(const cv::Scalar* drawColour, double height, Target* target, Point3D* robotPosn, CameraPosition* cameraPosn) = 0;
            virtual ~Feature();
            virtual void updateFeature(BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn);
            virtual void draw(cv::Mat* image, cv::Point* origin);
            void setPosition(Point2D* newLeft, Point2D* newRight);
            //bool updateFeature(MultiTarget* candidates);
            //Point2D* getLastPosnDifference();
            //void updateMapAsAnchor(Map* map);
            Point2D* getLeftCorner();
            Point2D* getRightCorner();

        protected:
            const cv::Scalar* drawColour;
            double height;
            Point2D *leftCorner, *rightCorner;

            Feature(const cv::Scalar* drawColour, double height);
            Feature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
            virtual Point2D* findAbsPosn(Point2D* p, Point3D* robotPosn, CameraPosition* cameraPosn);
            //void findBoxCorners(Point2D** pointStorageArray, bool* boolStorageArray, BoundingBox* box);
            //double findError(BoundingBox* candidate);
            //BoundingBox* findBestCandidate (MultiTarget* candidates);
    };
}

#endif // FEATURE_H
