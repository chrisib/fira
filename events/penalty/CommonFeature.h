#ifndef COMMON_FEATURE_H
#define COMMON_FEATURE_H

#include "Feature.h"
#include "MultiTarget.h"

namespace Robot
{
    class CommonFeature : public Robot::Feature
    {
        public:
            static std::vector<CommonFeature*>* makeFeature(const cv::Scalar* drawColour, double height, MultiTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            static std::vector<CommonFeature*>* makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);

        protected:
            CommonFeature(const cv::Scalar* drawColour, double height, BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn);
            CommonFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
    };
}

#endif // COMMON_FEATURE_H
