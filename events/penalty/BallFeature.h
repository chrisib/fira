#ifndef BALL_FEATURE_H
#define BALL_FEATURE_H

#include "UniqueFeature.h"

namespace Robot
{
    class BallFeature : public Robot::UniqueFeature
    {
        public:
            static BallFeature* makeFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            static BallFeature* makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
            virtual void updateFeature(BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn);
            virtual void draw(cv::Mat* image, cv::Point* origin);
            Point2D* getCenter();

        protected:
            Point2D *center;

            BallFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            BallFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
    };
}

#endif // BALL_FEATURE_H
