TEMPLATE = app
Qt -= core gui

QMAKE_CFLAGS_RELEASE -= -O2
QMAKE_CFLAGS_RELEASE += -O3 -g
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3 -g

QMAKE_LFLAGS_RELEASE -= -Wl,-O1
QMAKE_LFLAGS_RELEASE += -Wl,-O3 -g

INCLUDEPATH +=  /usr/local/include/ \
                /usr/local/include/darwin/framework/ \
                /usr/local/include/darwin/linux/ \

TARGET = penalty

SOURCES +=  main.cpp \
    Feature.cpp \
    GoalTarget.cpp \
    util.cpp \
    Map.cpp \
    Colour.cpp \
    UniqueFeature.cpp \
    GoalFeature.cpp \
    CommonFeature.cpp \
    BinderFeature.cpp \
    BallFeature.cpp

HEADERS += \
    Feature.h \
    GoalTarget.h \
    util.h \
    Map.h \
    Colour.h \
    UniqueFeature.h \
    GoalFeature.h \
    CommonFeature.h \
    BinderFeature.h \
    main.h \
    BallFeature.h

LIBS += -ldl \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lespeak \
        -ljpeg \
        -lpthread \
        -ldarwin \
        # ANY ADDITIONAL LIBRARIES GO HERE

QMAKE_CFLAGS_RELEASE += -g
QMAKE_CXXFLAGS_RELEASE += -g

OTHER_FILES += config.ini \


