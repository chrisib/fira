#pragma once

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include "Point.h"

class Util
{
		
    public:
	
		template<typename T>
		static T min(T x, T y) { return (x > y)? y : x; }
		template<typename T>
		static T max(T x, T y) { return (x < y)? y : x; }
		
		static cv::Point2f average(cv::Point2f, cv::Point2f);
		static cv::Point2f average(cv::Point2f, cv::Point2f, cv::Point2f);
		static cv::Point2f average(const std::vector<cv::Point2f>&);
		
		static float length(cv::Point2f);
		static float length(cv::Vec4i);
		static float dist(cv::Point2f, cv::Point2f);
		static float dist(cv::Vec4i);
		static float angle(cv::Point2f);
		static cv::Point2f normalize(cv::Point2f);
		static cv::Point2f vecAtAngle(float);
        static double angle90to90(double degrees);
			
		static void orderLinePointsTogether(cv::Vec4i&, cv::Vec4i&);
		static void orderLineLeftToRight(cv::Vec4i&);
		static void orderLineTopToBottom(cv::Vec4i&);
		static bool linePointsNearby(cv::Vec4i, cv::Vec4i, int);
		static cv::Vec4i verticalMerge(cv::Vec4i line1, cv::Vec4i line2);
		static cv::Vec4i averageLines(cv::Vec4i, cv::Vec4i);
		static cv::Vec4i averageLinesToLongestVertical(cv::Vec4i, cv::Vec4i);
		static cv::Vec4i averageLinesToLongestVertical(std::vector<cv::Vec4i>&);
		static float computeSlope(cv::Vec4i);
		static cv::Vec4i getLongestLine(const std::vector<cv::Vec4i>&);
		static cv::Vec4i getClosestLongestLine(const std::vector<cv::Vec4i>&);
		static int endPointDistance(cv::Vec4i, cv::Vec4i, cv::Vec2i&); 
			
		static float radToDeg(float);
		static float degToRad(float);
		static float wrapvalue(float);
			
		static cv::Vec3b matToVec3b(cv::Mat);
        static cv::Mat vec3bToMat(cv::Vec3b);
		static cv::Point2f keyPointToPoint(cv::KeyPoint);
		static cv::Vec2i point2fToVec2i(cv::Point2f);
		static cv::Point2f vec2iToPoint2f(cv::Vec2i);
			
		static int getFurthestFromAverage(const std::vector<cv::Point2f>&);
			
		static float areaToRadius(float);
			
		static void sortKeyPointsDownBySize(std::vector<cv::KeyPoint>&);
	
        static Robot::Point2D* findLowYCircleIntersection(Robot::Point2D* center1, double radius1, Robot::Point2D* center2, double radius2);
        static Robot::Point2D* findHighYCircleIntersection(Robot::Point2D* center1, double radius1, Robot::Point2D* center2, double radius2);
};
