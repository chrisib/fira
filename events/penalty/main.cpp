#include <iostream>
#include <LinuxDARwIn.h>
#include <darwin/linux/YuvCamera.h>
#include <darwin/framework/Voice.h>
#include <opencv/cv.h>
#include "main.h"
#include "Feature.h"
#include "UniqueFeature.h"
#include "GoalFeature.h"
#include "MultiTarget.h"
#include "GoalTarget.h"
#include "Map.h"
#include "Colour.h"

using namespace Robot;
using namespace std;
using namespace cv;

bool noDisplay;
bool testingGait;
bool enableLog;

PenaltyState stateOrder[] = {IDLE, SIDE_KICK, SIDESTEPPING, APPROACHING_BALL, REALIGNING, ALIGNING_FOOT, LIGHT_KICK, REALIGNING, PASSING_OBSTACLES, REALIGNING, SIDESTEPPING, APPROACHING_BALL, ALIGNING_FOOT, STRONG_KICK};

minIni *ini;
Map* absMap;
MultiTarget* binders;
GoalTarget* goal;
SingleTarget* ball;
SingleTarget* goalie;
BallTracker tracker;
BallFollower follower;

int state, kickChosen;
//pthread_t videoThreadId;
bool frameUsed, goingLeft, leftKickChosen;
double headTilt, headPan;
int lastButton;
bool lookForGoal, lookForBall, lookForBinders, lookForGoalie;

void initialize()
{
    noDisplay = false;
    testingGait = false;

    ini = new minIni("config.ini");
    absMap = new Map(*ini);
    binders = new MultiTarget("Binders");
    goal = new GoalTarget();
    goal->setCrossbarOnly(true);
    ball = new SingleTarget();
    goalie = new SingleTarget();

    state = 0;
    kickChosen = SOFT_KICK_RIGHT;
    frameUsed = true;
    goingLeft = false;
    leftKickChosen = false;
    headTilt = -10;
    headPan = 0;
    lastButton = MotionStatus::BUTTON;
    lookForGoal = true;
    lookForBall = true;
    lookForBinders = true;
    lookForGoalie = true;

    cout << "Initializing Camera..." << endl;
    YuvCamera::GetInstance()->Initialize(0);
    YuvCamera::GetInstance()->LoadINISettings(ini);
    if(!noDisplay)
    {
        cv::namedWindow("Raw",1);
        cv::namedWindow("Processed",1);
        cvMoveWindow("Raw",0,20);
        cvMoveWindow("Processed",0,290);
    }
    cout << "Done" << endl;

    cout << "Loading INI settings..." << endl;
    binders->LoadIniSettings(*ini, "Blue");
    goal->LoadIniSettings(*ini, "Goal");
    ball->LoadIniSettings(*ini, "Red");
    goalie->LoadIniSettings(*ini, "Green");
    //forwardDefaultTurn = ini->getd("Sprint","ForwardDefaultTurn",0);
    cout << "Done" << endl;

    cout << "Initializing Framework..." << endl;
    LinuxDARwIn::ChangeCurrentDir();
    LinuxDARwIn::Initialize("motion.bin",Action::DEFAULT_MOTION_SIT_DOWN);
    //LinuxDARwIn::InitializeSignals();
    Voice::Initialize("default");
    cout << "done" << endl << endl;

    cout << "Standing up..." << endl;
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
    while(Action::GetInstance()->IsRunning())
        usleep(8000);
    cout << "done" << endl << endl;

    cout << "Initializing modules..." << endl;
    MotionManager::GetInstance()->AddModule(Head::GetInstance());
    MotionManager::GetInstance()->AddModule(Walking::GetInstance());
    MotionManager::GetInstance()->AddModule(RightArm::GetInstance());
    MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
    Head::GetInstance()->m_Joint.SetEnableBody(false);
    Walking::GetInstance()->m_Joint.SetEnableBody(false);
    RightArm::GetInstance()->m_Joint.SetEnableBody(false);
    LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
    Action::GetInstance()->m_Joint.SetEnableBody(true, true);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);

    Walking::GetInstance()->LoadINISettings(ini);

    Head::GetInstance()->MoveByAngle(headPan,headTilt);
    Head::GetInstance()->Finish();
    cout << "done" << endl << endl;
}

void handleCommandLineArgs(int argc, char** argv)
{
    for(int i=1; i<argc; i++)
    {
        if(!strcmp(argv[i],"--no-video"))
            noDisplay = true;
        else if(!strcmp(argv[i],"--log"))
            enableLog = true;
        else if (!strcmp(argv[i],"--help"))
        {
            cout << "Usage:"<<endl;
            cout << "\tsprint [--no-video] [--log] [--help]" << endl;
            cout << "--no-video runs penalty kick without video output to screen" << endl;
            cout << "--log      enables MotionManager logging" << endl;
            exit(EXIT_SUCCESS);
        }
        else
        {
            printf("Usage:\n\tsprint [--no-video] [--vision-test] [--record-video] [--help]\n\n");
            exit(EXIT_FAILURE);
        }
    }
}

void handleVideo()
{
    CameraPosition cameraPosition = CameraPosition();

    YuvCamera::GetInstance()->CaptureFrame();
    static Mat raw = Mat::zeros(YuvCamera::GetInstance()->img.rows,YuvCamera::GetInstance()->img.cols,YuvCamera::GetInstance()->img.type());
    static Mat processed = Mat::zeros(YuvCamera::GetInstance()->img.rows,YuvCamera::GetInstance()->img.cols,YuvCamera::GetInstance()->img.type());
    raw = YuvCamera::GetInstance()->img;

    if(lookForGoal)
    {
        goal->FindInFrame(raw, &processed);
        if(!noDisplay)
        {
            goal->Draw(raw);
            goal->Draw(processed);
        }
    }

    if(lookForBall)
    {
        ball->FindInFrame(raw, &processed);
        absMap->updateBall(ball, &cameraPosition);
        if(!noDisplay)
        {
            ball->Draw(raw);
            ball->Draw(processed);
        }
    }

    if(lookForBinders)
    {
        binders->FindInFrame(raw, &processed);
        absMap->updateBinders(binders, &cameraPosition);
        if(!noDisplay)
        {
            binders->Draw(raw);
            binders->Draw(processed);
        }
    }

    if(lookForGoalie)
    {
        goalie->FindInFrame(raw, &processed);
        if(!noDisplay)
        {
            goalie->Draw(raw);
            goalie->Draw(processed);
        }
    }

    frameUsed = false;

    if (!noDisplay)
    {
        cv::imshow("Raw",raw);
        cv::imshow("Processed",processed);
        cv::imshow("Map", absMap->fieldImage);
        cv::waitKey(1);
        absMap->Draw();
    }
}

void handleButton()
{
    if(MotionStatus::BUTTON==lastButton)
        return;
    else
    {
        lastButton = MotionStatus::BUTTON;

        if(stateOrder[state] == IDLE)
        {
            state++;
            if(enableLog)
                MotionManager::GetInstance()->StartLogging();
        }

        if(lastButton == CM730::MIDDLE_BUTTON)
        {
            goingLeft = false;
            cout<<"Middle button pressed"<<endl;
        }
        else if(lastButton == CM730::LEFT_BUTTON)
        {
            goingLeft = true;
            cout<<"Left button pressed"<<endl;
        }
    }
}

void handleFall()
{
    if(MotionStatus::FALLEN != STANDUP)
    {
        Walking::GetInstance()->Stop();
        while(Walking::GetInstance()->IsRunning())
            usleep(1000);

        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

        Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
        Action::GetInstance()->Finish();

        if(MotionStatus::FALLEN == FORWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_F_UP);   // FORWARD GETUP
        else if(MotionStatus::FALLEN == BACKWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_B_UP);   // BACKWARD GETUP

        while(Action::GetInstance()->IsRunning())
            usleep(1000);
    }
}

void locateBall()
{
    int tilt;
    int scanDirection = 1;
    int framesFound = 0;

    lookForGoal = false;
    lookForBall = true;
    lookForBinders = false;
    lookForGoalie = false;

    while(framesFound < 5)
    {
        clearOldFrames();
        handleVideo();

        if(ball->WasFound())
        {
            framesFound++;
            Head::GetInstance()->LookAt(*ball, LOOK_AT_DAMPING_FACTOR);
        }
        else
        {
            framesFound = 0;

            if(scanDirection == 1)
                tilt = HIGH_SCAN_TILT;
            else
                tilt = LOW_SCAN_TILT;

            if((scanDirection<0 || headPan+SCAN_AMPLITUDE*scanDirection<=MAX_SCAN_PAN) && (scanDirection>0 || headPan+SCAN_AMPLITUDE*scanDirection>=MIN_SCAN_PAN))
            {
                headPan+=SCAN_AMPLITUDE*scanDirection;
                headTilt = tilt;
                Head::GetInstance()->MoveByAngle(headPan,headTilt);
            }
            else
            {
                scanDirection *= -1;
            }
        }
    }
}

void waitAMoment()
{
    volatile int j;
    for(int i=0;i<30000000;i++){j++;}
}

void shuffle(int shuffleDirection)
{
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    if(shuffleDirection == 2)
        Action::GetInstance()->Start(SIDESTEP_RIGHT);
    else if(shuffleDirection == 1)
        Action::GetInstance()->Start(SHORT_SIDESTEP_RIGHT);
    else if(shuffleDirection == -1)
        Action::GetInstance()->Start(SHORT_SIDESTEP_LEFT);
    else if(shuffleDirection == -2)
        Action::GetInstance()->Start(SIDESTEP_LEFT);
    Action::GetInstance()->Finish();
    //absMap->moveLaterally(-SHUFFLE_DISTANCE);
}

void shuffleLeft()
{
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    Action::GetInstance()->Start(SIDESTEP_LEFT);
    Action::GetInstance()->Finish();
    //absMap->moveLaterally(SHUFFLE_DISTANCE);
}

void shuffleRight()
{
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    Action::GetInstance()->Start(SIDESTEP_RIGHT);
    Action::GetInstance()->Finish();
    //absMap->moveLaterally(SHUFFLE_DISTANCE);
}

void clearOldFrames()
{
    YuvCamera::GetInstance()->CaptureFrame();
    YuvCamera::GetInstance()->CaptureFrame();
    YuvCamera::GetInstance()->CaptureFrame();
    YuvCamera::GetInstance()->CaptureFrame();
    YuvCamera::GetInstance()->CaptureFrame();
    YuvCamera::GetInstance()->CaptureFrame();
}

int ballInFrontOfLeftFoot(double angle)
{
    int ans;

    if(angle > LEFT_FOOT_RIGHT_THRESHOLD && angle < LEFT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(abs(angle-LEFT_FOOT_RIGHT_THRESHOLD) < 5)
        ans = 1;
    else if(abs(angle-LEFT_FOOT_LEFT_THRESHOLD) < 5)
        ans = -1;
    else if(angle <= LEFT_FOOT_RIGHT_THRESHOLD)
        ans = 2;
    else
        ans = -2;

    return ans;
}

int ballInFrontOfRightFoot(double angle)
{
    int ans;

    if(angle > RIGHT_FOOT_RIGHT_THRESHOLD && angle < RIGHT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(abs(angle-RIGHT_FOOT_RIGHT_THRESHOLD) < 5)
        ans = 1;
    else if(abs(angle-RIGHT_FOOT_LEFT_THRESHOLD) < 5)
        ans = -1;
    else if(angle <= RIGHT_FOOT_RIGHT_THRESHOLD)
        ans = 2;
    else
        ans = -2;

    return ans;
}

void processSideKick()
{
    if(goingLeft)
    {
        cout<<"Kicking the ball to the left"<<endl;
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        Action::GetInstance()->Start(SIDE_KICK_LEFT);
        Action::GetInstance()->Finish();

        headTilt = -10;
        headPan = 60;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(headPan,headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();

        clearOldFrames();
        handleVideo();
    }
    else
    {
        cout<<"Kicking the ball to the right"<<endl;
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        Action::GetInstance()->Start(SIDE_KICK_RIGHT);
        Action::GetInstance()->Finish();

        headTilt = -10;
        headPan = -60;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(headPan,headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();

        clearOldFrames();
        handleVideo();
    }

    state++;
}

void processSidestepping()
{
    double ballDist;

    lookForGoal = false;
    lookForBall = true;
    lookForBinders = false;
    lookForGoalie = false;

    Point2D* ballCenter;

    clearOldFrames();
    handleVideo();

    if(ball->WasFound())
    {
        Head::GetInstance()->LookAt(*ball, LOOK_AT_DAMPING_FACTOR*2);
    }
    else
    {
        locateBall();
        clearOldFrames();
        handleVideo();
    }

    ballCenter = absMap->getBallCenter();

    if(abs(ballCenter->X - absMap->presentPosition.X) < SHUFFLE_NEAR_BALL_THRESHOLD*4)
    {
        headTilt = Head::GetInstance()->GetTiltAngle();
        headPan = 0;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(headPan, headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();
    }

    ballDist = ballCenter->X - absMap->presentPosition.X;

    cout << "ANGLE: " << absMap->angleToBall()<<" RANGE: "<<absMap->rangeToBall()<<endl;

    if(goingLeft)
    {
        if(ball->WasFound() && abs(absMap->angleToBall()) > 65 && absMap->rangeToBall() < 300)
        {
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = -10;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->Start(2);
            Walking::GetInstance()->Finish();
        }
        else if(!ball->WasFound() || absMap->angleToBall() > 35 || (ballDist < -SHUFFLE_NEAR_BALL_THRESHOLD && absMap->rangeToBall() < 450))
        {
            shuffleLeft();
        }
        else if(absMap->angleToBall() < -35 || (ballDist > SHUFFLE_NEAR_BALL_THRESHOLD && absMap->rangeToBall() < 450))
        {
            shuffleRight();
        }
        else
        {
            state++;
        }
    }
    else
    {
        if(ball->WasFound() && abs(absMap->angleToBall()) > 65 && absMap->rangeToBall() < 300)
        {
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = -10;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->Start(2);
            Walking::GetInstance()->Finish();
        }
        else if(!ball->WasFound() || absMap->angleToBall() < -35 || (ballDist > SHUFFLE_NEAR_BALL_THRESHOLD && absMap->rangeToBall() < 450))
        {
            shuffleRight();
        }
        else if(absMap->angleToBall() > 35 || (ballDist < -SHUFFLE_NEAR_BALL_THRESHOLD && absMap->rangeToBall() < 450))
        {
            shuffleLeft();
        }
        else
        {
            state++;
        }
    }
}

void processApproachingBall()
{
    static int framesClose = 0;
    static int REQUIRED_FRAMES_CLOSE = 3;

    lookForGoal = false;
    lookForBall = true;
    lookForBinders = false;
    lookForGoalie = false;

    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
    Head::GetInstance()->LookAt(*ball, LOOK_AT_DAMPING_FACTOR);
    //clearOldFrames();
    handleVideo();

    if(ball->WasFound() && !Walking::GetInstance()->IsRunning() && framesClose == 0)
    {
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
        Walking::GetInstance()->X_MOVE_AMPLITUDE = 5;
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
        Walking::GetInstance()->Start();
    }

    if(ball->WasFound())
    {
        //cout<<absMap->rangeToBall()<<endl;
        //cout<<"RANGE: "<<absMap->rangeToBall()<<" ANGLE: "<<absMap->angleToBall()<<endl;
        if(abs(absMap->angleToBall()) >= 40)
        {
            Walking::GetInstance()->Stop();
            Walking::GetInstance()->Finish();
            state--;
            framesClose = 0;
        }
        else if(absMap->rangeToBall() > WALKING_KICK_RANGE) //|| absMap->angleToBall() > LEFT_FOOT_LEFT_THRESHOLD || absMap->angleToBall() < RIGHT_FOOT_RIGHT_THRESHOLD)
        {
            //tracker.Process(ball->GetBoundingBox()->center);
            //follower.Process(tracker.ball_position);

            framesClose = 0;
        }
        else //if(absMap->rangeToBall() <= WALKING_KICK_RANGE && absMap->angleToBall() >= RIGHT_FOOT_RIGHT_THRESHOLD && absMap->angleToBall() <= LEFT_FOOT_LEFT_THRESHOLD)
        {
            Walking::GetInstance()->Stop();
            Walking::GetInstance()->Finish();
            clearOldFrames();
            handleVideo();
            if(absMap->rangeToBall() <= MAX_KICK_RANGE)
                framesClose++;
            else
            {
                framesClose = 0;
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 2;
                Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->Start(2);
                Walking::GetInstance()->Finish();
                waitAMoment();
            }
            if(framesClose >= REQUIRED_FRAMES_CLOSE)
            {
                framesClose = 0;
                Walking::GetInstance()->Stop();
                Walking::GetInstance()->Finish();
                state++;
            }
        }
    }
    else
    {
        cout<<"DIDN'T FIND BALL"<<endl;
        Walking::GetInstance()->Stop();
        Walking::GetInstance()->Finish();
        locateBall();
        clearOldFrames();
        handleVideo();
    }
}

void processRealigning()
{
    bool done = false;
    double originalOffset;
    bool foundAtLeastOnce;
    double sum;
    int numSamples;

    lookForGoal = true;
    lookForBall = false;
    lookForBinders = false;
    lookForGoalie = false;

    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);

    if(headTilt != 60 || headPan != 0)
    {
        headTilt = 60;
        headPan = 0;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(headPan,headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();
    }

    while(!done)
    {
        handleFall();
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);

        originalOffset = Walking::GetInstance()->HIP_PITCH_OFFSET;
        Walking::GetInstance()->HIP_PITCH_OFFSET = 10;

        foundAtLeastOnce = false;
        sum = 0;
        numSamples = 0;

        waitAMoment();
        clearOldFrames();
        for(int i=0;i<FRAMES_TO_AVERAGE;i++)
        {
            //clearOldFrames();
            handleVideo();

            if(goal->crossbarWasFound())
            {
                cout<<"CROSSBAR ANGLE: "<<goal->getLastCrossbarAngle()<<endl;
                sum += goal->getLastCrossbarAngle();
                numSamples++;
                foundAtLeastOnce = true;
            }
        }

        Walking::GetInstance()->HIP_PITCH_OFFSET = originalOffset;
        waitAMoment();

        if(foundAtLeastOnce)
        {
            if(sum/numSamples > REALIGNMENT_THRESHOLD_ANGLE)
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 22;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -3;
                Walking::GetInstance()->Start(2);
                Walking::GetInstance()->Finish();
            }
            else if(sum/numSamples < -REALIGNMENT_THRESHOLD_ANGLE)
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = -25;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -3;
                Walking::GetInstance()->Start(2);
                Walking::GetInstance()->Finish();
            }
            else
            {
                done = true;
            }
        }
        else //guessing a direction is better than nothing
        {
            cout<<"LOST THE GOAL"<<endl;
            if(goingLeft)
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = -25;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -3;
                Walking::GetInstance()->Start(2);
                Walking::GetInstance()->Finish();
            }
            else
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 22;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -3;
                Walking::GetInstance()->Start(2);
                Walking::GetInstance()->Finish();
            }
        }
    }

    state++;
}

void processAligningFoot()
{
    int shuffleDirection = 1;
    double sum = 0;
    double average = 0;

    lookForGoal = false;
    lookForBall = true;
    lookForBinders = false;
    lookForGoalie = false;

    if(headTilt != -10 || headPan != 0)
    {
        headTilt = -10;
        headPan = 0;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(headPan,headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();
    }

    Head::GetInstance()->LookAt(*ball, LOOK_AT_DAMPING_FACTOR);

    for(int i=0;i<FRAMES_TO_AVERAGE;i++)
    {
        clearOldFrames();
        handleVideo();
        sum += absMap->angleToBall();
    }
    average = sum/3.0;

    if(ball->WasFound())
    {
        if(average > DEAD_CENTER_ANGLE)
        {
            leftKickChosen = true;
            shuffleDirection = ballInFrontOfLeftFoot(average);
        }
        else
        {
            leftKickChosen = false;
            shuffleDirection = ballInFrontOfRightFoot(average);
        }

        if(shuffleDirection != 0)
        {
            shuffle(shuffleDirection);
        }
        {
            state++;
        }
    }
    else
    {
        locateBall();
    }
}

void processForwardKick()
{
    if(kickChosen == STRONG_KICK_LEFT || kickChosen == STRONG_KICK_RIGHT)
    {
        lookForGoal = false;
        lookForBall = false;
        lookForBinders = false;
        lookForGoalie = true;

        if(headTilt != 20 || headPan != 0)
        {
            headTilt = 20;
            headPan = 0;
            Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
            Head::GetInstance()->MoveByAngle(headPan,headTilt);
            Head::GetInstance()->Finish();
            MotionManager::Sync();
        }

        waitAMoment();
        waitAMoment();
        clearOldFrames();
        handleVideo();

        //cout<<abs(goalie->GetBoundingBox()->center.X-Camera::WIDTH/2.0)/Camera::WIDTH<<endl;

        if(goalie->WasFound() && abs(goalie->GetBoundingBox()->center.X-Camera::WIDTH/2.0)/Camera::WIDTH < 0.15)
        {
            if(kickChosen == STRONG_KICK_LEFT)
            {
                if(goingLeft)
                    kickChosen = LEFT_KICK_LEFT;
                else
                    kickChosen = LEFT_KICK_RIGHT;
            }
            else
            {
                if(goingLeft)
                    kickChosen = RIGHT_KICK_LEFT;
                else
                    kickChosen = RIGHT_KICK_RIGHT;
            }
        }
        else if(!goalie->WasFound() || (goalie->WasFound() && abs(goalie->GetBoundingBox()->center.X-Camera::WIDTH/2.0)/Camera::WIDTH > 0.4))
        {
            if(kickChosen == STRONG_KICK_LEFT)
            {
                if(goingLeft)
                    kickChosen = LEFT_KICK_RIGHT;
                else
                    kickChosen = LEFT_KICK_LEFT;
            }
            else
            {
                if(goingLeft)
                    kickChosen = RIGHT_KICK_RIGHT;
                else
                    kickChosen = RIGHT_KICK_LEFT;
            }
        }
    }

    cout<<"KICK CHOSEN: "<<kickChosen<<endl;

    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    Action::GetInstance()->Start(kickChosen);
    Action::GetInstance()->Finish();

    if(headTilt != -20 || headPan != 0)
    {
        headTilt = -20;
        headPan = 0;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(headPan,headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();
        waitAMoment();
        waitAMoment();
        waitAMoment();
    }

    lookForGoal = false;
    lookForBall = true;
    lookForBinders = false;
    lookForGoalie = false;

    clearOldFrames();
    handleVideo();
    if(ball->WasFound())
    {
        cout<<"KICK FAILED"<<endl;
        //ball was not kicked, return to approaching state
        //this is a bit of a hack to get back to the appropriate state
        if(kickChosen == SOFT_KICK_RIGHT || kickChosen == SOFT_KICK_LEFT)
            state -= 3;
        else
            state -= 2;
    }
    else
    {
        cout<<"KICK SUCCESSFUL"<<endl;
        if(kickChosen == SOFT_KICK_RIGHT || kickChosen == SOFT_KICK_LEFT)
            state++;
        else
        {
            //state -= 3;
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
            Action::GetInstance()->Start(THRUST);
            Action::GetInstance()->Finish();
            state = 0;
        }
    }
}

void processPassingObstacles()
{
    bool throughBinders = false;
    bool stopAndRealign = false;
    double curr;
    double closestBinder = 0;

    lookForGoal = false;
    lookForBall = false;
    lookForBinders = true;
    lookForGoalie = false;

    if(headTilt != -5 || abs(headPan) != 50)
    {
        headTilt = -5;
        if(goingLeft)
            headPan = 50;
        else
            headPan = -50;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(headPan,headTilt);
        Head::GetInstance()->Finish();
        MotionManager::Sync();
    }

    while(!stopAndRealign && !throughBinders)
    {
        handleFall();

        closestBinder = A_LARGE_NUMBER;

        clearOldFrames();
        clearOldFrames();
        clearOldFrames();
        for(int i=0;i<FRAMES_TO_AVERAGE;i++)
        {
            handleVideo();
            if(goingLeft)
                curr = absMap->nearestBinderLeft();
            else
                curr = absMap->nearestBinderRight();
            cout<<"CURR BINDER: "<<curr<<endl;
            if(curr >= 0 && curr < closestBinder)
                closestBinder = curr;
        }

        cout<<"CLOSEST BINDER: "<<closestBinder<<endl;

        if(abs(closestBinder - A_LARGE_NUMBER) > TOLERANCE && closestBinder < BINDER_TOO_CLOSE_THRESHOLD)
        {
            if(goingLeft)
                shuffleRight();
            else
                shuffleLeft();
        }
        else if(abs(closestBinder - A_LARGE_NUMBER) > TOLERANCE && closestBinder > BINDER_TOO_FAR_THRESHOLD)
        {
            if(goingLeft)
                shuffleLeft();
            else
                shuffleRight();
        }
        else if(abs(closestBinder - A_LARGE_NUMBER) > TOLERANCE)
        {
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 0; //FOUR_STEP_A/4.0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 10; //FOUR_STEP_X;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0; //FOUR_STEP_Y;
            Walking::GetInstance()->Start(STEPS_BETWEEN_BINDERS);
            Walking::GetInstance()->Finish();

            stopAndRealign = true;
        }
        else
        {
            throughBinders = true;
        }
    }

    if(stopAndRealign)
    {
        state--;
    }
    else
    {
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0; //FOUR_STEP_A/4.0;
        Walking::GetInstance()->X_MOVE_AMPLITUDE = 10; //FOUR_STEP_X;
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0; //FOUR_STEP_Y;
        Walking::GetInstance()->Start(STEPS_EMERGING_FROM_BINDERS);
        Walking::GetInstance()->Finish();
        state++;
    }
}

void process()
{
    static int counter = 0;
    cout<<counter<<endl;
    counter++;
    //cout << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl;
    //cout << "\t\t\tState: " << state << endl;
    switch(stateOrder[state])
    {
        case SIDE_KICK:
            processSideKick();
            break;

        case SIDESTEPPING:
            processSidestepping();
            break;

        case APPROACHING_BALL:
            processApproachingBall();
            break;

        case REALIGNING:
            processRealigning();
            break;

         case ALIGNING_FOOT:
            processAligningFoot();
            break;

        case LIGHT_KICK:
            if(leftKickChosen)
            {
                kickChosen = SOFT_KICK_LEFT;
            }
            else
            {
                kickChosen = SOFT_KICK_RIGHT;
            }
            processForwardKick();
            break;

        case PASSING_OBSTACLES:
            processPassingObstacles();
            break;

        case STRONG_KICK:
            if(leftKickChosen)
            {
                kickChosen = STRONG_KICK_LEFT;
            }
            else
            {
                kickChosen = STRONG_KICK_RIGHT;
            }
            processForwardKick();
            break;

        case IDLE:
        default:
            //lookForGoal = true;
            //lookForBall = true;
            //lookForBinders = true;
            //lookForGoalie = true;
            //clearOldFrames();
            handleVideo();
            break;
    }//switch

    if(state!=IDLE)
    {
        //handleVideo();
        handleFall();
    }

    handleButton();
}// process


int main(int argc, char** argv)
{
    initialize();
    handleCommandLineArgs(argc, argv);

    /*headTilt = -5;
    headPan = 0;
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
    Head::GetInstance()->MoveByAngle(headPan,headTilt);
    Head::GetInstance()->Finish();*/

    /*double sum = 0;
    int count = 0;

    while(true)
    {
        handleVideo();
        cout<<absMap->angleToBall()<<endl;
        sum += absMap->angleToBall();
        count++;
        if(count == 3)
        {
            cout<<"AVERAGE: "<<sum/3.0<<endl;
            count = 0;
            sum = 0;
        }
    }*/

    /*Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    while(true)
    {
        handleVideo();
        lookForGoal = false;
        lookForBall = true;
        lookForBinders = false;
        if(ball->WasFound())
        {
            Head::GetInstance()->LookAt(*ball, LOOK_AT_DAMPING_FACTOR);
            tracker.Process(ball->GetBoundingBox()->center);
            follower.Process(tracker.ball_position);
        }
        else
            locateBall();
    }*/

    /*Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    Walking::GetInstance()->Start();
    while(true)
    {
        clearOldFrames();
        handleVideo();
        Head::GetInstance()->LookAt(*ball, LOOK_AT_DAMPING_FACTOR);
        tracker.Process(ball->GetBoundingBox()->center);
        follower.Process(tracker.ball_position);
    }*/

    //state = 7;

    if(testingGait)
    {
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        Walking::GetInstance()->A_MOVE_AMPLITUDE = FOUR_STEP_A ;
        Walking::GetInstance()->X_MOVE_AMPLITUDE = FOUR_STEP_X;
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = FOUR_STEP_Y;

        while(true)
        {
            Walking::GetInstance()->A_MOVE_AMPLITUDE = FOUR_STEP_A;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = FOUR_STEP_X;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = FOUR_STEP_Y;
            Walking::GetInstance()->Start(STEPS_BETWEEN_BINDERS);
            Walking::GetInstance()->Finish();
            handleVideo();
        }
    }

    Voice::Speak("Press button");
    cout << "Press button" << endl;

    while(true)
    {
        process();
    }

    return 0;
} //main

