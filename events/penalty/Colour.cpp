#include "Colour.h"

using namespace Robot;
using namespace cv;

const Scalar Colour::WHITE = Scalar(255,255,255);
const Scalar Colour::BLACK = Scalar(0,0,0);
const Scalar Colour::BLUE = Scalar(255,0,0);
const Scalar Colour::RED = Scalar(0,0,255);
const Scalar Colour::YELLOW = Scalar(0,255,255);
const Scalar Colour::GREEN = Scalar(0,255,0);
const Scalar Colour::ORANGE = Scalar(0,128,255);
const Scalar Colour::CYAN = Scalar(255,255,0);
const Scalar Colour::MAGENTA = Scalar(255,0,255);
const Scalar Colour::DK_GREEN = Scalar(0,128,0);
