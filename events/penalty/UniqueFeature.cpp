#include <iostream>
#include "UniqueFeature.h"
#include "SingleTarget.h"

using namespace std;
using namespace Robot;
using namespace cv;

UniqueFeature::UniqueFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn) : Feature(drawColour, height)
{
    if(target==NULL || robotPosn==NULL || cameraPosn==NULL)
    {
        cerr<<"NULL parameter passed to UniqueFeature constructor"<<endl;
    }
    else
    {
        updateFeature(target->GetBoundingBox(), robotPosn, cameraPosn);
    }
}

UniqueFeature::UniqueFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right) : Feature(drawColour, height, left, right)
{
}

UniqueFeature* UniqueFeature::makeFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    return new UniqueFeature(drawColour, height, target, robotPosn, cameraPosn);
}

UniqueFeature* UniqueFeature::makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right)
{
    return new UniqueFeature(drawColour, height, left, right);
}
