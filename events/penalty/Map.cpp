#include <iostream>
#include <darwin/framework/Math.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include "Map.h"
#include "Colour.h"
#include "util.h"

using namespace Robot;
using namespace cv;
using namespace std;

Map::Map(minIni &ini)
{
    LoadIniSettings(ini,"Field");
    fieldImage = Mat::zeros(MAP_Y_LENGTH, MAP_X_LENGTH, CV_8UC3);
    presentPosition = Point3D(0,500,0);
    binders = vector<BinderFeature*>();
    Point2D leftPost = Point2D(-850,1900);
    Point2D rightPost = Point2D(850,1900);
    Point2D origin = Point2D(0,0);
    goal = GoalFeature::makeFeature(&Colour::YELLOW, 900, &leftPost, &rightPost);
    ball = BallFeature::makeFeature(&Colour::ORANGE, 65, &origin, &origin);
}

Map::~Map()
{
    if(goal != NULL)
    {
        delete goal;
    }

    if(ball != NULL)
    {
        delete ball;
    }

    for(unsigned int i;i<binders.size();i++)
    {
        delete binders[i];
    }
}

void Map::Draw()
{
    Point origin = Point(MAP_X_LENGTH/2, MAP_Y_LENGTH);

    Point me, pt;

    // clear the entire field
    rectangle(fieldImage,Point(0,0),Point(fieldImage.cols, fieldImage.rows),Colour::BLACK,-1);

    //draw all features
    /*if(goal != NULL)
    {
        goal->draw(&fieldImage, &origin);
    }*/
    if(ball != NULL)
    {
        ball->draw(&fieldImage, &origin);
    }
    for(unsigned int i=0;i<binders.size();i++)
    {
        binders[i]->draw(&fieldImage, &origin);
    }

    //rectangle(fieldImage,tl,br,WHITE,5);            // touch lines
    //line(fieldImage,ctrLeft,ctrRight,WHITE,5);      // centre line
    //circle(fieldImage,ctr,centreRadius, WHITE,5);   // centre circle

    // write our position in the corner
    char buffer[255];
    sprintf(buffer,"(%0.2f %0.2f) @ %0.2f",presentPosition.X, presentPosition.Y, presentPosition.Z);
    putText(fieldImage, buffer, Point(0, fieldImage.rows-12),FONT_HERSHEY_PLAIN, 1.0, Colour::WHITE);

    // our position on the field
    me = Point(origin.x+presentPosition.X*MAP_SCALE_FACTOR, origin.y-presentPosition.Y*MAP_SCALE_FACTOR);

    // draw us with a line indicating orientation
    pt = Point(me.x - sin(deg2rad(presentPosition.Z)) * 20, me.y - cos(deg2rad(presentPosition.Z))*20);
    circle(fieldImage, me, 15, Colour::GREEN, -1);
    line(fieldImage, me, pt, Colour::GREEN, 4);

    // mark our position with a dark ring to indicate the uncertainty
    if(!positionKnown)
    {
        //circle(fieldImage, me, 15, DK_GREEN, 2);
    }

    // draw the ball
    /*if(polarMap->ballPosition.X > 0)
    {
        pt.x = me.x - sin(deg2rad(presentPosition.Z + polarMap->ballPosition.Y)) * polarMap->ballPosition.X/10;
        pt.y = me.y - cos(deg2rad(presentPosition.Z + polarMap->ballPosition.Y)) * polarMap->ballPosition.X/10;

        circle(fieldImage, pt, 8, ORANGE, -1);
    }*/
}

void Map::LoadIniSettings(minIni &ini, const char* section)
{
    /*fieldLength = ini.geti(section,"FieldLength");*/
}

double Map::normalizeAngle(double degrees)
{
    if(-180 <= degrees && 180 >= degrees)
        return degrees;

    while(degrees < -180)
        degrees += 360;
    while(degrees > 180)
        degrees -= 360;

    return degrees;
}

bool Map::useFrameWithGoal(GoalTarget* goalTarget)
{
    bool ans;
    Point3D* newRobotPosn;

    goal->addFrameToAverage(goalTarget);
    ans = goal->getAverageReady();
    if(ans)
    {
        newRobotPosn = goal->performAverage();
        presentPosition.X = newRobotPosn->X;
        presentPosition.Y = newRobotPosn->Y;
        presentPosition.Z = newRobotPosn->Z;
        delete newRobotPosn;
    }

    return ans;
}

void Map::updateBinders(MultiTarget* binderTarget, CameraPosition* cameraPosn)
{
    vector<BinderFeature*>* features = BinderFeature::makeFeature(&Colour::BLUE, 300, binderTarget, &presentPosition, cameraPosn);

    binders.clear();
    for(unsigned int i=0;i<features->size();i++)
    {
        binders.push_back((*features)[i]);
    }
}

void Map::updateBall(SingleTarget* ballTarget, CameraPosition* cameraPosn)
{
    if(ballTarget != NULL)
    {
        if(ballTarget->WasFound())
        {
            ball->updateFeature(ballTarget->GetBoundingBox(), &presentPosition, cameraPosn);
        }
    }
    else
    {
        cerr<<"ERROR: NULL ball target passed to map"<<endl;
    }
}

Point2D* Map::getBallCenter()
{
    return ball->getCenter();
}

void Map::moveLaterally(double dist)
{
    presentPosition.X += dist*cos(Util::degToRad(presentPosition.Z));
    presentPosition.Y += dist*sin(Util::degToRad(presentPosition.Z));
}

double Map::rangeToBall()
{
    Point2f robotPosn = Point2f(presentPosition.X, presentPosition.Y);
    Point2f ballPosn = Point2f(ball->getCenter()->X, ball->getCenter()->Y);

    return Util::dist(robotPosn, ballPosn);
}

double Map::angleToBall()
{
    return presentPosition.Z + Util::radToDeg(atan((presentPosition.X-ball->getCenter()->X)/(ball->getCenter()->Y-presentPosition.Y)));
}

double Map::nearestBinderRight()
{
    BinderFeature* curr;
    double closestDist = -1;
    double currDist;

    for(unsigned int i=0;i<binders.size();i++)
    {
        curr = binders[i];
        currDist = curr->getLeftCorner()->X - presentPosition.X;
        if(closestDist == -1 || (currDist >= 0 && currDist < closestDist))
            closestDist = currDist;
    }

    return closestDist;
}

double Map::nearestBinderLeft()
{
    BinderFeature* curr;
    double closestDist = -1;
    double currDist;

    for(unsigned int i=0;i<binders.size();i++)
    {
        curr = binders[i];
        currDist = presentPosition.X - curr->getRightCorner()->X;
        if(closestDist == -1 || (currDist >= 0 && currDist < closestDist))
            closestDist = currDist;
    }

    return closestDist;
}
