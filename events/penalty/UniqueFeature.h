#ifndef UNIQUE_FEATURE_H
#define UNIQUE_FEATURE_H

#include "Feature.h"
#include "SingleTarget.h"

namespace Robot
{
    class UniqueFeature : public Robot::Feature
    {
        public:
            static UniqueFeature* makeFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            static UniqueFeature* makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);

        protected:
            UniqueFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            UniqueFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
    };
}

#endif // UNIQUE_FEATURE_H
