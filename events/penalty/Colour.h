#ifndef COLOUR_H
#define COLOUR_H

#include <opencv2/core/core.hpp>

namespace Robot
{
    class Colour
    {
        public:
            static const cv::Scalar WHITE;
            static const cv::Scalar BLACK;
            static const cv::Scalar BLUE;
            static const cv::Scalar RED;
            static const cv::Scalar YELLOW;
            static const cv::Scalar GREEN;
            static const cv::Scalar ORANGE;
            static const cv::Scalar CYAN;
            static const cv::Scalar MAGENTA;
            static const cv::Scalar DK_GREEN;
    };
}

#endif // COLOUR_H
