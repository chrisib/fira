#ifndef BINDER_FEATURE_H
#define BINDER_FEATURE_H

#include "CommonFeature.h"

#define BINDER_DEPTH 300

namespace Robot
{
    class BinderFeature : public Robot::CommonFeature
    {
        public:
            static std::vector<BinderFeature*>* makeFeature(const cv::Scalar* drawColour, double height, MultiTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            static std::vector<BinderFeature*>* makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
            virtual Point2D* findAbsPosn(Point2D *p, Point3D *robotPosn, CameraPosition *cameraPosn);
            virtual void updateFeature(BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn);
            virtual void draw(cv::Mat* image, cv::Point* origin);

        protected:
            Point2D *leftBackCorner, *rightBackCorner;

            BinderFeature(const cv::Scalar* drawColour, double height, BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn);
            BinderFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
    };
}

#endif // BINDER_FEATURE_H
