#ifndef GOAL_FEATURE_H
#define GOAL_FEATURE_H

#include <vector>
#include "UniqueFeature.h"
#include "GoalTarget.h"

#define DEFAULT_FRAMES_FOR_AVERAGE 10
#define DEFAULT_OUTLIER_PROPORTION 0.5

namespace Robot
{
    class GoalFeature : public Robot::UniqueFeature
    {
        public:
            static GoalFeature* makeFeature(const cv::Scalar* drawColour, double height, GoalTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            static GoalFeature* makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
            virtual Point2D* findAbsPosn(Point2D *p, Point3D *robotPosn, CameraPosition *cameraPosn);
            virtual void updateFeature(GoalTarget* goalTarget, Point3D* robotPosn, CameraPosition* cameraPosn);
            Point3D* findRobotPosn(GoalTarget* target);
            void setFramesForAverage(int numFrames);
            void setOutlierProportion(double newProportion);
            void addFrameToAverage(GoalTarget* target);
            bool getAverageReady();
            Point3D* performAverage();

        private:
            int framesForAverage, framesSoFar;
            double outlierProportion;
            cv::vector<Point3D*> pointsToAverage;
            bool averageReady;

            GoalFeature(const cv::Scalar* drawColour, double height, GoalTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn);
            GoalFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right);
            void trashOutliers();
            void averageExcluding(Point3D* storagePoint, int indexToExclude);
    };
}

#endif // GOALFEATURE_H
