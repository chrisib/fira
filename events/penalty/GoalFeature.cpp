#include <iostream>
#include "GoalFeature.h"
#include "UniqueFeature.h"
#include "GoalTarget.h"
#include "util.h"

using namespace std;
using namespace Robot;
using namespace cv;

GoalFeature::GoalFeature(const cv::Scalar* drawColour, double height, GoalTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn) : UniqueFeature(drawColour, height, target, robotPosn, cameraPosn)
{
    updateFeature(target, robotPosn, cameraPosn);
    framesForAverage = DEFAULT_FRAMES_FOR_AVERAGE;
    outlierProportion = DEFAULT_OUTLIER_PROPORTION;
    framesSoFar = 0;
    averageReady = false;
    pointsToAverage = vector<Point3D*>();
}

GoalFeature::GoalFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right) : UniqueFeature(drawColour, height, left, right)
{
    framesForAverage = DEFAULT_FRAMES_FOR_AVERAGE;
    outlierProportion = DEFAULT_OUTLIER_PROPORTION;
    framesSoFar = 0;
    averageReady = false;
    pointsToAverage = vector<Point3D*>();
}

GoalFeature* GoalFeature::makeFeature(const cv::Scalar* drawColour, double height, GoalTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    return new GoalFeature(drawColour, height, target, robotPosn, cameraPosn);
}

GoalFeature* GoalFeature::makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right)
{
    return new GoalFeature(drawColour, height, left, right);
}

void GoalFeature::updateFeature(GoalTarget* goalTarget, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    delete leftCorner;
    delete rightCorner;

    Point2D leftPost = Point2D(goalTarget->GetLeftPostRange(), goalTarget->GetLeftPostAngle());
    Point2D rightPost = Point2D(goalTarget->GetRightPostRange(), goalTarget->GetRightPostAngle());
    leftCorner = findAbsPosn(&leftPost, robotPosn, cameraPosn);
    rightCorner = findAbsPosn(&rightPost, robotPosn, cameraPosn);
}

Point2D* GoalFeature::findAbsPosn(Point2D *p, Point3D *robotPosn, CameraPosition *cameraPosn)
{
    double x, y, phi, angle, range;

    range = p->X;
    angle = p->Y;
    phi = robotPosn->Z + angle;
    x = robotPosn->X - range*sin(phi*PI/180);
    y = robotPosn->Y + range*cos(phi*PI/180);
    //cout<<"RANGE: "<<range<<" ANGLE: "<<angle<<" X: "<<x<<" Y: "<<y<<endl;
    return new Point2D(x,y);
}

Point3D* GoalFeature::findRobotPosn(GoalTarget* target)
{
    Point2D leftPost = Point2D(target->GetLeftPostRange(), target->GetLeftPostAngle());
    Point2D rightPost = Point2D(target->GetRightPostRange(), target->GetRightPostAngle());
    Point2D* robotPosn = Util::findLowYCircleIntersection(leftCorner,leftPost.X,rightCorner,rightPost.X);
    double angle = -(Util::radToDeg(asin((leftCorner->X-robotPosn->X)/leftPost.X)) + leftPost.Y);
    //cout<<"ROBOT X: "<<robotPosn->X<<" ROBOT Y: "<<robotPosn->Y<<" ROBOT ANGLE: "<<angle<<endl;

    return  new Point3D(robotPosn->X, robotPosn->Y, angle);

}

void GoalFeature::setFramesForAverage(int numFrames)
{
    framesForAverage = numFrames;
}

void GoalFeature::setOutlierProportion(double newProportion)
{
    outlierProportion = newProportion;
}

void GoalFeature::addFrameToAverage(GoalTarget* target)
{
    pointsToAverage.push_back(findRobotPosn(target));
    framesSoFar++;
    if(framesSoFar >= framesForAverage)
        averageReady = true;
}

bool GoalFeature::getAverageReady()
{
    return averageReady;
}

Point3D* GoalFeature::performAverage()
{
    Point3D* result = NULL;
    Point3D* curr;
    int numPoints;

    if(averageReady)
    {
        trashOutliers();

        result = new Point3D();
        numPoints = pointsToAverage.size();
        for(int i=0;i<numPoints;i++)
        {
            curr = pointsToAverage.back();
            pointsToAverage.pop_back();
            result->X += curr->X;
            result->Y += curr->Y;
            result->Z += curr->Z;
            delete curr;
        }

        result->X = (double)result->X / numPoints;
        result->Y = (double)result->Y / numPoints;
        result->Z = (double)result->Z / numPoints;

        averageReady = false;
        framesSoFar = 0;
    }

    return result;
}

void GoalFeature::trashOutliers()
{
    int numOutliers = (int)floor(framesSoFar*outlierProportion);
    int worstIndex;
    Point3D *worstPoint, *currPoint;
    Point3D averageExcludingCurr = Point3D();
    double worstValue, currValue;

    if(numOutliers >= (int)pointsToAverage.size())
        numOutliers = pointsToAverage.size()-1;

    for(int i=0;i<numOutliers;i++)
    {
        worstIndex = 0;
        worstPoint = pointsToAverage.front();
        averageExcluding(&averageExcludingCurr, 0);
        worstValue = pow((worstPoint->X - averageExcludingCurr.X), 2) + pow((worstPoint->Y - averageExcludingCurr.Y), 2) + pow((worstPoint->Z - averageExcludingCurr.Z), 2);

        for(unsigned int j=1;j<pointsToAverage.size();j++)
        {
            currPoint = pointsToAverage[j];
            averageExcluding(&averageExcludingCurr, j);
            currValue = pow((currPoint->X - averageExcludingCurr.X), 2) + pow((currPoint->Y - averageExcludingCurr.Y), 2) + pow((currPoint->Z - averageExcludingCurr.Z), 2);

            if(currValue > worstValue)
            {
                worstValue = currValue;
                worstPoint = currPoint;
                worstIndex = j;
            }
        }

        //cout<<"TRASHED "<<i<<" AT INDEX "<<worstIndex<<" WITH X "<<worstPoint->X<<endl;
        delete worstPoint;
        pointsToAverage.erase(pointsToAverage.begin() + worstIndex);
    }
}

void GoalFeature::averageExcluding(Point3D* storagePoint, int indexToExclude)
{
    Point3D* curr;
    int numPoints;
    int pointsUsed = 0;

    storagePoint->X = 0;
    storagePoint->Y = 0;
    storagePoint->Z = 0;
    numPoints = pointsToAverage.size();

    for(int i=0;i<numPoints;i++)
    {
        if(i != indexToExclude)
        {
            curr = pointsToAverage[i];
            storagePoint->X += curr->X;
            storagePoint->Y += curr->Y;
            storagePoint->Z += curr->Z;
            pointsUsed++;
        }
    }

    storagePoint->X = (double)storagePoint->X / pointsUsed;
    storagePoint->Y = (double)storagePoint->Y / pointsUsed;
    storagePoint->Z = (double)storagePoint->Z / pointsUsed;
}
