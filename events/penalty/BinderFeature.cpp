#include <iostream>
#include "BinderFeature.h"
#include "CommonFeature.h"
#include "MultiTarget.h"
#include "Map.h"

using namespace std;
using namespace Robot;
using namespace cv;

BinderFeature::BinderFeature(const cv::Scalar* drawColour, double height, BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn) : CommonFeature(drawColour, height, featureBox, robotPosn, cameraPosn)
{
    leftBackCorner = new Point2D();
    rightBackCorner = new Point2D();
    updateFeature(featureBox,robotPosn,cameraPosn);
}

BinderFeature::BinderFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right) : CommonFeature(drawColour, height, left, right)
{
    leftBackCorner = new Point2D();
    rightBackCorner = new Point2D();
}

vector<BinderFeature*>* BinderFeature::makeFeature(const cv::Scalar* drawColour, double height, MultiTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    vector<BinderFeature*>* returnList = new vector<BinderFeature*>();
    vector<BoundingBox*>* boxes = target->GetBoundingBoxes();
    BoundingBox* currBox;
    BinderFeature* currFeature;

    for(unsigned int i=0;i<boxes->size();i++)
    {
        currBox = (*boxes)[i];
        currFeature = new BinderFeature(drawColour, height, currBox, robotPosn, cameraPosn);
        returnList->push_back(currFeature);
    }

    return returnList;
}

vector<BinderFeature*>* BinderFeature::makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right)
{
    vector<BinderFeature*>* returnList = new vector<BinderFeature*>();
    BinderFeature* onlyOne = new BinderFeature(drawColour, height, left, right);
    returnList->push_back(onlyOne);

    return returnList;
}

//uses the bottom of the BoundingBox instead of the top because the binder has a very similar height to the robot
void BinderFeature::updateFeature(BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    delete leftCorner;
    delete rightCorner;
    delete leftBackCorner;
    delete rightBackCorner;

    Point2D topLeft = Point2D(featureBox->center.X-featureBox->width/2.0, featureBox->center.Y+featureBox->height/2.0);
    Point2D topRight = Point2D(featureBox->center.X+featureBox->width/2.0, featureBox->center.Y+featureBox->height/2.0);
    leftCorner = findAbsPosn(&topLeft, robotPosn, cameraPosn);
    rightCorner = findAbsPosn(&topRight, robotPosn, cameraPosn);

    if(leftCorner->Y < rightCorner->Y)
    {
        rightCorner->Y = leftCorner->Y;
    }
    else
    {
        leftCorner->Y = rightCorner->Y;
    }

    leftBackCorner = new Point2D(leftCorner->X, leftCorner->Y+BINDER_DEPTH);
    rightBackCorner = new Point2D(rightCorner->X, rightCorner->Y+BINDER_DEPTH);
}

Point2D* BinderFeature::findAbsPosn(Point2D *p, Point3D *robotPosn, CameraPosition *cameraPosn)
{
    double x, y, phi, angle, range;

    angle = cameraPosn->CalculateAngle(*p, 0);
    range = cameraPosn->CalculateRange(*p, 0);
    phi = robotPosn->Z + angle;
    x = robotPosn->X - range*sin(phi*PI/180);
    y = robotPosn->Y + range*cos(phi*PI/180);

    return new Point2D(x,y);
}

void BinderFeature::draw(cv::Mat* image, cv::Point* origin)
{
    cv::Point lc = cv::Point(leftCorner->X*MAP_SCALE_FACTOR+origin->x, origin->y-leftCorner->Y*MAP_SCALE_FACTOR);
    cv::Point rc = cv::Point(rightCorner->X*MAP_SCALE_FACTOR+origin->x, origin->y-rightCorner->Y*MAP_SCALE_FACTOR);
    cv::Point lbc = cv::Point(leftBackCorner->X*MAP_SCALE_FACTOR+origin->x, origin->y-leftBackCorner->Y*MAP_SCALE_FACTOR);
    cv::Point rbc = cv::Point(rightBackCorner->X*MAP_SCALE_FACTOR+origin->x, origin->y-rightBackCorner->Y*MAP_SCALE_FACTOR);
    circle(*image,lc,5, *drawColour,4);
    circle(*image,rc,5, *drawColour,4);
    circle(*image,lbc,5, *drawColour,4);
    circle(*image,rbc,5, *drawColour,4);
    line(*image,lc,rc,*drawColour,4);
    line(*image,rc,rbc,*drawColour,4);
    line(*image,rbc,lbc,*drawColour,4);
    line(*image,lbc,lc,*drawColour,4);
}
