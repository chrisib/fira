#ifndef MAP_H
#define MAP_H

#include <opencv2/core/core.hpp>
#include "GoalFeature.h"
#include "CommonFeature.h"
#include "BinderFeature.h"
#include "BallFeature.h"

#define MAP_X_LENGTH 400
#define MAP_Y_LENGTH 200
#define MAP_SCALE_FACTOR 0.1

namespace Robot{
    class Map
    {
        public:
            Map(minIni &ini);
            virtual ~Map();

            // the robot's current position on the field relative to our back left corner (in cm) (Z = orientation in degrees)
            // NOTE: confidence is not currently used
            Robot::Point3D presentPosition;
            bool positionKnown;

            // draw an OpenCV image of the field and objects' positions on it
            void Draw();
            cv::Mat fieldImage;

            // convert an angle so that it lies within a standard [-180,180] range
            static double normalizeAngle(double degrees);

            bool useFrameWithGoal(GoalTarget* goalTarget);
            void updateBinders(MultiTarget* binderTarget, CameraPosition* cameraPosn);
            void updateBall(SingleTarget* ballTarget, CameraPosition* cameraPosn);
            void moveLaterally(double dist);
            double rangeToBall();
            double angleToBall();
            double nearestBinderRight();
            double nearestBinderLeft();

            Point2D* getBallCenter();

        private:
            GoalFeature* goal;
            BallFeature* ball;
            std::vector<BinderFeature*> binders;

            void LoadIniSettings(minIni &ini, const char* section);
    };
}

#endif // MAP_H
