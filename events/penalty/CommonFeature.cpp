#include <iostream>
#include "CommonFeature.h"
#include "MultiTarget.h"

using namespace std;
using namespace Robot;
using namespace cv;

CommonFeature::CommonFeature(const cv::Scalar* drawColour, double height, BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn) : Feature(drawColour, height)
{
    if(featureBox==NULL || robotPosn==NULL || cameraPosn==NULL)
    {
        cerr<<"NULL parameter passed to CommonFeature constructor"<<endl;
    }
    else
    {
        updateFeature(featureBox, robotPosn, cameraPosn);
    }
}

CommonFeature::CommonFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right) : Feature(drawColour, height, left, right)
{
}

vector<CommonFeature*>* CommonFeature::makeFeature(const cv::Scalar* drawColour, double height, MultiTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    vector<CommonFeature*>* returnList = new vector<CommonFeature*>();
    vector<BoundingBox*>* boxes = target->GetBoundingBoxes();
    BoundingBox* currBox;
    CommonFeature* currFeature;

    for(unsigned int i=0;i<boxes->size();i++)
    {
        currBox = (*boxes)[i];
        currFeature = new CommonFeature(drawColour, height, currBox, robotPosn, cameraPosn);
        returnList->push_back(currFeature);
    }

    return returnList;
}

vector<CommonFeature*>* CommonFeature::makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right)
{
    vector<CommonFeature*>* returnList = new vector<CommonFeature*>();
    CommonFeature* onlyOne = new CommonFeature(drawColour, height, left, right);
    returnList->push_back(onlyOne);

    return returnList;
}
