#ifndef MAIN_H
#define MAIN_H

#define SIDE_KICK_RIGHT 139
#define SIDE_KICK_LEFT 107
#define SIDESTEP_RIGHT 169
#define SIDESTEP_LEFT 159
#define SHORT_SIDESTEP_RIGHT 247
#define SHORT_SIDESTEP_LEFT 237
#define SOFT_KICK_RIGHT 149
#define STRONG_KICK_RIGHT 179
#define SOFT_KICK_LEFT 81
#define STRONG_KICK_LEFT 73
#define LEFT_KICK_RIGHT 61
#define RIGHT_KICK_RIGHT 209
#define LEFT_KICK_LEFT 124
#define RIGHT_KICK_LEFT 199
#define THRUST 32

#define SHUFFLE_NEAR_BALL_THRESHOLD 30
#define SHUFFLE_DISTANCE 53

#define LOOK_AT_DAMPING_FACTOR 0.3

#define WALKING_KICK_RANGE 150
#define MAX_KICK_RANGE 130

#define LEFT_FOOT_RIGHT_THRESHOLD 7
#define LEFT_FOOT_LEFT_THRESHOLD 12.5
#define RIGHT_FOOT_RIGHT_THRESHOLD -11
#define RIGHT_FOOT_LEFT_THRESHOLD -5.5
#define DEAD_CENTER_ANGLE 2

#define BINDER_TOO_CLOSE_THRESHOLD 130
#define BINDER_TOO_FAR_THRESHOLD 175

#define REALIGNMENT_THRESHOLD_ANGLE 4.5

#define FOUR_STEP_X 10
#define FOUR_STEP_Y -5
#define FOUR_STEP_A 14

#define A_LARGE_NUMBER 999999
#define TOLERANCE 0.000001

#define SCAN_AMPLITUDE 25
#define MIN_SCAN_PAN -80
#define MAX_SCAN_PAN 80
#define LOW_SCAN_TILT -10
#define HIGH_SCAN_TILT 30

#define STEPS_BETWEEN_BINDERS 7
#define STEPS_EMERGING_FROM_BINDERS 4

#define FRAMES_TO_AVERAGE 3

typedef enum PENALTY_STATE
{
    SIDE_KICK,
    SIDESTEPPING,
    REALIGNING,
    APPROACHING_BALL,
    ALIGNING_FOOT,
    LIGHT_KICK,
    PASSING_OBSTACLES,
    STRONG_KICK,
    IDLE
} PenaltyState;

void initialize();
void handleCommandLineArgs(int argc, char** argv);
void handleVideo();
void handleButton();
void handleFall();
void locateBall();
void waitAMoment();
void shuffle(int shuffleDirection);
void shuffleLeft();
void shuffleRight();
void clearOldFrames();
int ballInFrontOfLeftFoot(double angle);
int ballInFrontOfRightFoot(double angle);
void processSideKick();
void processSidestepping();
void processRealigning();
void processApproachingBall();
void processAligningFoot();
void processForwardKick();
void processPassingObstacles();
void process();
int main(int argc, char** argv);

#endif // MAIN_H
