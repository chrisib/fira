#include <iostream>
#include "BallFeature.h"
#include "UniqueFeature.h"
#include "SingleTarget.h"
#include "Map.h"

using namespace std;
using namespace Robot;
using namespace cv;

BallFeature::BallFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn) : UniqueFeature(drawColour, height, target, robotPosn, cameraPosn)
{
    center = new Point2D();
    updateFeature(target->GetBoundingBox(),robotPosn,cameraPosn);
}

BallFeature::BallFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right) : UniqueFeature(drawColour, height, left, right)
{
    center = new Point2D();
    center->X = (leftCorner->X + rightCorner->X)/2.0;
    center->Y = (leftCorner->Y + rightCorner->Y)/2.0;
}

BallFeature* BallFeature::makeFeature(const cv::Scalar* drawColour, double height, SingleTarget* target, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    return new BallFeature(drawColour, height, target, robotPosn, cameraPosn);
}

BallFeature* BallFeature::makeFeature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right)
{
    return new BallFeature(drawColour, height, left, right);
}

void BallFeature::draw(cv::Mat* image, cv::Point* origin)
{
    cv::Point c = cv::Point(center->X*MAP_SCALE_FACTOR+origin->x, origin->y-center->Y*MAP_SCALE_FACTOR);

    circle(*image,c,5, *drawColour,4);
}

void BallFeature::updateFeature(BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    UniqueFeature::updateFeature(featureBox,robotPosn,cameraPosn);
    center->X = (leftCorner->X + rightCorner->X)/2.0;
    center->Y = (leftCorner->Y + rightCorner->Y)/2.0;
}

Point2D* BallFeature::getCenter()
{
    return center;
}
