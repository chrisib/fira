#include <iostream>
#include <vector>
#include <darwin/framework/CameraPosition.h>
#include "Feature.h"
#include "Map.h"

using namespace std;
using namespace Robot;
using namespace cv;

Feature::Feature(const cv::Scalar* drawColour, double height)
{
    this->drawColour = drawColour;
    this->height = height;
    leftCorner = new Point2D(-1,-1);
    rightCorner = new Point2D(-1,-1);
}

Feature::Feature(const cv::Scalar* drawColour, double height, Point2D* left, Point2D* right)
{
    this->drawColour = drawColour;
    this->height = height;
    leftCorner = new Point2D(-1,-1);
    rightCorner = new Point2D(-1,-1);

    if(left==NULL || right==NULL)
    {
        cerr<<"NULL parameter passed to Feature constructor"<<endl;
    }
    else
    {
        setPosition(left, right);
    }
}

Feature::~Feature()
{
    delete leftCorner;
    delete rightCorner;
}

Point2D* Feature::findAbsPosn(Point2D *p, Point3D *robotPosn, CameraPosition *cameraPosn)
{
    double x, y, phi, angle, range;

    angle = cameraPosn->CalculateAngle(*p, height);
    range = cameraPosn->CalculateRange(*p, height);
    phi = robotPosn->Z + angle;
    x = robotPosn->X - range*sin(phi*PI/180);
    y = robotPosn->Y + range*cos(phi*PI/180);

    return new Point2D(x,y);
}

void Feature::draw(cv::Mat* image, cv::Point* origin)
{
    cv::Point lc = cv::Point(leftCorner->X*MAP_SCALE_FACTOR+origin->x, origin->y-leftCorner->Y*MAP_SCALE_FACTOR);
    cv::Point rc = cv::Point(rightCorner->X*MAP_SCALE_FACTOR+origin->x, origin->y-rightCorner->Y*MAP_SCALE_FACTOR);
    //cout<<"LEFT: "<<leftCorner->X<<" "<<leftCorner->Y<<endl;
    //cout<<"RIGHT: "<<rightCorner->X<<" "<<rightCorner->Y<<endl;
    circle(*image,lc,5, *drawColour,4);
    circle(*image,rc,5, *drawColour,4);
    line(*image,lc,rc,*drawColour,4);
}

void Feature::setPosition(Point2D* newLeft, Point2D* newRight)
{
    leftCorner->X = newLeft->X;
    leftCorner->Y = newLeft->Y;
    rightCorner->X = newRight->X;
    rightCorner->Y = newRight->Y;
}

void Feature::updateFeature(BoundingBox* featureBox, Point3D* robotPosn, CameraPosition* cameraPosn)
{
    delete leftCorner;
    delete rightCorner;

    Point2D topLeft = Point2D(featureBox->center.X-featureBox->width/2.0, featureBox->center.Y-featureBox->height/2.0);
    Point2D topRight = Point2D(featureBox->center.X+featureBox->width/2.0, featureBox->center.Y-featureBox->height/2.0);
    leftCorner = findAbsPosn(&topLeft, robotPosn, cameraPosn);
    rightCorner = findAbsPosn(&topRight, robotPosn, cameraPosn);
}

Point2D* Feature::getLeftCorner()
{
    return leftCorner;
}

Point2D* Feature::getRightCorner()
{
    return rightCorner;
}

/*double Feature::findError(BoundingBox* candidate)
{
    double errSum = 0;
    int attributesCompared = 0;
    Point2D* candidateCorners[4];
    bool candidateFeatureCorners[4];

    findBoxCorners(candidateCorners, candidateFeatureCorners, candidate);
    for(int i=0;i<4;i++)
    {
        if(candidateFeatureCorners[i] && featureCorners[i])
        {
            errSum += POSN_ERR_FACTOR*Point2D::Distance(*candidateCorners[i], *corners[i]);
            attributesCompared++;
        }
    }

    return errSum/attributesCompared;
}

BoundingBox* Feature::findBestCandidate (MultiTarget* candidates)
{
    BoundingBox* bestCandidate = NULL;
    double bestError = DBL_MAX;
    double currError;
    vector<BoundingBox*>* boxes = candidates->GetBoundingBoxes();

    for(unsigned int i=0;i<boxes->size();i++)
    {
        currError = findError((*boxes)[i]);
        if(currError < bestError)
        {
            bestError = currError;
            bestCandidate = (*boxes)[i];
        }
    }

    return bestCandidate;
}*/

/*bool Feature::updateFeature(MultiTarget* candidates)
{
    bool successful = false;
    BoundingBox* best = findBestCandidate(candidates);

    if(best != NULL)
    {
        successful = true;

        for(int i=0;i<4;i++)
        {
            lastCorners[i] = corners[i];
            lastFeatureCorners[i] = featureCorners[i];
        }

        findBoxCorners(corners, featureCorners, best);
    }

    return successful;
}*/

/*Point2D* Feature::getLastPosnDifference()
{
    double
}*/
