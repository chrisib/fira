#include "LineFinder.h"
#include <darwin/framework/MultiBlob.h>
#include <iostream>
#include <cstdlib>
#include <opencv/cv.h>

using namespace std;
using namespace cv;
using namespace Robot;

LineFinder::LineFinder(std::vector<Robot::MultiBlob*> *segments, double maxAllowedDeviation, bool zeroSlopeIsVertical)
{
	this->segments = segments;
	this->zeroSlopeIsVertical = zeroSlopeIsVertical;
	this->maxAllowedDeviation = maxAllowedDeviation;
	points = (Point2D*)malloc(sizeof(Point2D) * segments->size());
	
    cout << "Created LineFinder with maximum allowed deviation of " << maxAllowedDeviation << endl;

    numPoints = 0;
    minDeviation = 999999;
    lineParts.clear();
    slope = 0;
    center.X = Camera::WIDTH/2;
    center.Y = Camera::HEIGHT/2;
}

LineFinder::~LineFinder()
{

}

void LineFinder::LoadIniSettings(minIni &ini, const char *section)
{
    (void)ini;
    (void)section;
}

void LineFinder::Update()
{
	numPoints = 0;
    minDeviation = 999999;
    lineParts.clear();
    BuildLine(0);
    ProcessSegments();
}

void LineFinder::Draw(cv::Mat &image)
{
	for(int i=segments->size()-1; i>=0; i--)
    {
		(*segments)[i]->Draw(image);
        //(*segments)[i]->DrawScanRange(image);
        //(*segments)[i]->DrawScanRange(debugImage);
    }
    
    // connect the dots to show where the line we've actually found goes
    //cout << "drawing line with " << numPoints << " points" << endl;
    cv::Point pt = cv::Point((int)points[0].X, (int)points[0].Y);
    cv::Point pt2;
    cv::Scalar colour = *(*segments)[0]->GetMarkColour(); //cv::Scalar(0,255,255);
    for(int i=1; i<numPoints; i++)
    {
        pt2 = cv::Point((int)points[i].X, (int)points[i].Y);
        cv::line(image, pt, pt2, colour, 2);

        pt = pt2;
    }
    
    // put a point on the line center
    colour = cv::Scalar(255,0,255);
    pt = cv::Point((int)center.X, (int)center.Y);
    cv::circle(image, pt, 5, colour, -1);
}


void LineFinder::BuildLine(int nextPart)
{
    int numCandidatesUsed = 0;
	
    if((unsigned int)nextPart < segments->size() && nextPart >= 0)
	{
		// recursively build the line one segment at a time
		// try every bounding box in every segment + skipping a segment
		// to find the line with the smallest slope error
        vector<BoundingBox*>* candidates = ((MultiBlob*)((*segments)[nextPart]))->GetBoundingBoxes();
		if(candidates != NULL && candidates->size() > 0)
		{
			//if(lineParts.size()>0)
			//{
			//	sort(candidates->begin(), candidates->end(), boundingBoxCompare);
			//}
			
			for(unsigned int i=0; i<candidates->size(); i++)
			{
				// anything with an aspect ratio of greater than 2.0 is going to be noise
				double aspectRatio = (double)((*candidates)[i]->width) / (*candidates)[i]->height;
				if(aspectRatio <= 3.0 || (aspectRatio > 3.0 && aspectRatio <= 6.0 && (*candidates)[i]->fill < 0.6))
				{
					lineParts.push_back((*candidates)[i]);
                    BuildLine(nextPart+1);
					lineParts.pop_back();
					numCandidatesUsed++;
				}
			}
			
			// if nothing was suitable then simply move to the next segment
			if(numCandidatesUsed == 0)
			{
                BuildLine(nextPart+1);
			}
		}
		else
		{
            BuildLine(nextPart+1);
		}
	}
	else
	{
		// we've processed all segments; calculate final stastics
		double deviation = 0;
		double slope;
		
		//cout << "Built a line with " << lineParts.size() << " parts" << endl;
		
		if(lineParts.size()>=3) // we need at least 2 points to make a line
		{
			// calculate the average
			double averageSlope = 0;
			for(unsigned int i=0; i<lineParts.size()-1; i++)
			{
				if(zeroSlopeIsVertical)
					slope = (lineParts[i]->center.X - lineParts[i+1]->center.X) / (lineParts[i]->center.Y - lineParts[i+1]->center.Y);
				else
					slope = (lineParts[i]->center.Y - lineParts[i+1]->center.Y) / (lineParts[i]->center.X - lineParts[i+1]->center.X);
				averageSlope += slope;
			}
			averageSlope = averageSlope / (lineParts.size()-1);
			
			// calculate the sum of squared errors
			for(unsigned int i=0; i<lineParts.size()-1; i++)
			{
				if(zeroSlopeIsVertical)
					slope = (lineParts[i]->center.X - lineParts[i+1]->center.X) / (lineParts[i]->center.Y - lineParts[i+1]->center.Y);
				else
					slope = (lineParts[i]->center.Y - lineParts[i+1]->center.Y) / (lineParts[i]->center.X - lineParts[i+1]->center.X);
				deviation += pow(slope - averageSlope, 2.0);
			}
			deviation = deviation / (lineParts.size()-1);
			
			//cout << deviation << " ?< " << minDeviation << endl;
			
			if(deviation < minDeviation && deviation < maxAllowedDeviation)
			{
				minDeviation = deviation;
				numPoints = lineParts.size();
				for(unsigned int i=0; i<lineParts.size(); i++)
					points[i] = lineParts[i]->center;
			}
		}
	}
}

void LineFinder::ProcessSegments()
{
    numPoints = 0;
    minDeviation = 999999;
    lineParts.clear();
    BuildLine(0);
    slopeError = minDeviation;

    // find the center of the point cloud
    double sumX = 0;
    double sumY = 0;

    for(int i=0; i<numPoints; i++)
    {
        sumX += points[i].X;
        sumY += points[i].Y;
    }

    double avgX = sumX/numPoints;
    double avgY = sumY/numPoints;

    center.X = avgX;
    center.Y = avgY;

    // find the average slope between consecutive segments
    // note that slope is calculated upside-down such that zero = vertical = straight forward
    // positive slope is to the left, negative slope to the right
    double sumSlopes = 0;
    for(int i=0; i<numPoints-1; i++)
    {
        /*
        for(int j=i+1; j<numPoints; j++)
        {
            sumSlopes += (points[i].X - points[j].X) / (points[i].Y - points[j].Y);
        }
        */
        
        if((points[i].Y-points[i+1].Y) != 0)
        {
            // calculate the slope normally
            sumSlopes +=  (points[i].X - points[i+1].X) / (points[i].Y - points[i+1].Y);
        }
        else
        {
            // effectively a horizontal line
            // if it grows to the left give it a very extreme positive slope
            // otherwise give it a very extreme negative slope
            // to avoid NaN issues
            if((points[i].X - points[i+1].X) < 0)
            {
                sumSlopes += INT_MAX;
            }
            else
            {
                sumSlopes += INT_MIN;
            }
        }
    }

    //slope = sumSlopes / ((numPoints-1) * (numPoints-1));
    if(numPoints > 1)
        slope = sumSlopes / (numPoints-1);
    else
        slope = 0.0;
}
