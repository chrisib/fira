#ifndef LINE_H_INCLUDED
#define LINE_H_INCLUDED

#include <vector>
#include <opencv/cv.h>
#include <darwin/framework/minIni.h>
#include <darwin/framework/MultiBlob.h>

// used by MarathonLine to find mainly horizontal or mainly vertical lines in the field of view
class LineFinder
{
public:
    LineFinder(std::vector<Robot::MultiBlob*> *segments, double maxAllowedDeviation, bool zeroSlopeIsVertical=true);
    ~LineFinder();

    void Update();
    void Draw(cv::Mat &image);

    void LoadIniSettings(minIni &ini, const char* section);

	double slope;
	Robot::Point2D center;
	double slopeError;
	int numPoints;
	
	

private:
    void BuildLine(int nextPart);
    void ProcessSegments();
	
	bool zeroSlopeIsVertical;
    std::vector<Robot::MultiBlob*> *segments;
	Robot::Point2D *points;
	int ignoreTopSegments;
	std::vector<Robot::BoundingBox*> lineParts;
	double minDeviation;
	double maxAllowedDeviation;
};

#endif
