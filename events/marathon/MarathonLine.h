#ifndef MARATHONLINE_H
#define MARATHONLINE_H

#include <darwin/framework/Target.h>
#include <darwin/framework/MultiBlob.h>
#include <LineFinder.h>

class MarathonLine : public Robot::Target
{
public:
    MarathonLine();
    ~MarathonLine();

    virtual void LoadIniSettings(minIni &ini, const char *section);
    virtual void Print();
    virtual void Draw(cv::Mat &canvas);

    virtual int FindInFrame(cv::Mat &image, cv::Mat *dest);
    virtual void ProcessCandidateImage(cv::Mat &binaryImages);

    void SetSubsample(int s){subsample=s;}

    Robot::Point2D Position(){return lineCtr;}
    double Angle(){return lineAngle;}
    bool IsLost(){return missedFrames >= maxMissedFrames;}

private:
    // line tracking
    Robot::Point2D lineCtr;
    double lineAngle;
    double maxAllowedDeviation;
    int missedFrames;
    int maxMissedFrames;
    bool lineAlreadyLost;
    std::vector<Robot::MultiBlob*> bottomSegments;
    std::vector<Robot::MultiBlob*> leftSegments;
    std::vector<Robot::MultiBlob*> rightSegments;

    int subsample;

    LineFinder *bottomLine, *leftLine, *rightLine;

    // assemble the three lines into a single line
    void BuildLine();
};

#endif // MARATHONLINE_H
