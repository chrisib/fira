#include "MarathonRunner.h"
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/MotionStatus.h>
#include <darwin/framework/Walking.h>
#include <darwin/framework/Head.h>
#include <darwin/framework/MotionManager.h>
#include <LinuxDARwIn.h>
#include <darwin/framework/Voice.h>
#include <darwin/framework/Math.h>
#include <iostream>

using namespace Robot;
using namespace std;
using namespace cv;

MarathonRunner::MarathonRunner()
{
    RegisterSwitch("--record",&recordVideo,false,"Record webcam input to a video file");
    RegisterArgument("--subsample",&subsample,2,"Set the image processing subsample (default: 2)");
    RegisterArgument("--config",&iniFilePath,"config.ini","Set the path to the configuration file");
    RegisterSwitch("--debug",&debug,false,"Enable debugging command-line output");
    RegisterSwitch("--log",&enableLog,false,"Enable MotionManager logging");

    motionFilePath = "motion.bin";

    isStandingUp = false;

    dbgImage = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);
    rgbImage = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);

    newFrame = false;
    lastButton = 0;

    state = STATE_IDLE;
}

MarathonRunner::~MarathonRunner()
{
}

void MarathonRunner::Execute()
{
    if(visionOnly)
    {
        state = STATE_DEBUG;
        for(;;)
        {
            if(newFrame)
            {
                newFrame = false;
                FollowLine();
            }
        }
    }
    else
    {
        state = STATE_DEBUG;
        Voice::Speak("Press the middle button");
        cout << "Press the middle button" << endl;
        WaitButton(CM730::MIDDLE_BUTTON);

        cout << "=== Starting the Marathon ===" << endl;
        if(enableLog)
            MotionManager::GetInstance()->StartLogging();

        state = STATE_WALKING;

        Walking::GetInstance()->X_MOVE_AMPLITUDE = minSpeedX;
        Walking::GetInstance()->Start();
        for(;;)
        {
            Process();
        }
    }
}

void MarathonRunner::Process()
{
    HandleButton();

    if(state!=STATE_IDLE)
    {
        if(indicateSigns && !LeftArm::GetInstance()->IsRunning() && MotionManager::GetInstance()->GetTorqueEnable(JointData::ID_L_SHOULDER_ROLL))
        {
            MotionManager::GetInstance()->SetTorqueEnable(JointData::ID_L_SHOULDER_ROLL,false);
        }
        if(indicateSigns && !RightArm::GetInstance()->IsRunning() && MotionManager::GetInstance()->GetTorqueEnable(JointData::ID_R_SHOULDER_ROLL))
        {
            MotionManager::GetInstance()->SetTorqueEnable(JointData::ID_R_SHOULDER_ROLL,false);
        }

        HandleFall();

        AdjustGait();
    }

    if(newFrame)
    {
        newFrame = false;
        FollowLine();
    }
}

bool MarathonRunner::Initialize()
{
    ini = new minIni(iniFilePath);
    motionFilePath = ini->gets("Files","MotionFile",motionFilePath);

    if(showVideo)
    {
        namedWindow("Webcam", CV_WINDOW_AUTOSIZE);
        namedWindow("Scanline", CV_WINDOW_AUTOSIZE);
        cvMoveWindow("Webcam",0,0);
        cvMoveWindow("Scanline",0,280);
    }

    if(!Application::Initialize(iniFilePath.c_str(), motionFilePath.c_str(), STAND_UP_MOTION))
        exit(1);

    if(!this->visionOnly)
    {
        //const char *motionFile = ini->gets("Files","MotionFile").c_str();
        //cout << "Loading motion file " << motionFile << endl;
        //LinuxDARwIn::Initialize(motionFile, STAND_UP_MOTION);
        //LinuxDARwIn::Initialize();

        // disable modules before adding them
        Walking::GetInstance()->m_Joint.SetEnableBody(false);
        Head::GetInstance()->m_Joint.SetEnableBody(false);
        LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
        RightArm::GetInstance()->m_Joint.SetEnableBody(false);

        // insert the new modules
        MotionManager::GetInstance()->AddModule(Walking::GetInstance());
        MotionManager::GetInstance()->AddModule(Head::GetInstance());
        MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
        MotionManager::GetInstance()->AddModule(RightArm::GetInstance());

        // insert the walking and head modules, zero-out the head position
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        Action::GetInstance()->m_Joint.SetEnableBody(false);

        int headAngle = ini->geti("Vision","HeadAngle",0);
        cout << "Setting head tilt to " << headAngle << " degrees" << endl;
        Head::GetInstance()->MoveByAngle(0,headAngle);  // center the head with a slight downward tilt

        // set the hands to the relaxed position
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();

        ARM_MOTORS.push_back(JointData::ID_R_SHOULDER_PITCH);
        ARM_MOTORS.push_back(JointData::ID_R_SHOULDER_ROLL);
        ARM_MOTORS.push_back(JointData::ID_R_ELBOW);
        ARM_MOTORS.push_back(JointData::ID_R_HAND);
        ARM_MOTORS.push_back(JointData::ID_L_SHOULDER_PITCH);
        ARM_MOTORS.push_back(JointData::ID_L_SHOULDER_ROLL);
        ARM_MOTORS.push_back(JointData::ID_L_ELBOW);
        ARM_MOTORS.push_back(JointData::ID_L_HAND);

        // torque off the arms
        MotionManager::GetInstance()->SetTorqueEnable(ARM_MOTORS,armTorqueOn);
    }
    else
    {
        MotionManager::GetInstance()->SetEnable(false);
    }

    Voice::Speak("Initialization Complete");
    cerr << "Finished initializing" << endl;

    return true;
}

void MarathonRunner::LoadIniSettings()
{
    CvCamera::GetInstance()->LoadINISettings(ini);
    CvCamera::GetInstance()->v4l2SetControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_AUTO);
    Walking::GetInstance()->LoadINISettings(ini);

    if(ini->geti("Voice","Enable",1))
        Voice::Initialize();

    indicateLED = !!ini->geti("LEDs","Enable",0);

    maxSpeedX = ini->getd("Marathon", "MaxStep");
    minSpeedX = ini->getd("Marathon", "MinStep");
    maxTurn = ini->getd("Marathon", "MaxTurn");
    maxSlide = ini->getd("Marathon", "MaxSlide");
    turnP = ini->getd("Marathon", "TurnP", 1.0);
    turnI = ini->getd("Marathon","TurnI", 0.0);
    turnD = ini->getd("Marathon", "TurnD", 0.1);
    slideP = ini->getd("Marathon", "SlideP", 1.0);
    slideI = ini->getd("Marathon", "SlideI",0.0);
    slideD = ini->getd("Marathon", "SlideD", 0.1);
    stepP = ini->getd("Marathon","StepP",1.0);
    stepI = ini->getd("Marathon","StepI",0.0);
    stepD = ini->getd("Marathon","StepD",0.1);
    turnPointGain = ini->getd("Marathon","TurnPointGain",1.0);
    slidePointGain = ini->getd("Marathon","SlidePointGain",1.0);
    turnSlopeGain = ini->getd("Marathon","TurnSlopeGain",1.0);
    slideSlopeGain = ini->getd("Marathon","SlideSlopeGain",1.0);
    stepSizeGain = ini->getd("Marathon","StepSizeGain",1.0);

    armTorqueOn = !!ini->geti("Arms","Torque",0);
    indicateSigns = !!ini->geti("Arms","IndicateSign",0);

    // print out config settings so we can make sure they look right
    cout << "Controller Settings:" << endl <<
            "\tStep:  [" << minSpeedX << " , " << maxSpeedX << "] (" << stepP << " , " << stepI << " , " << stepD << ") Gain: " << stepSizeGain << endl <<
            "\tTurn:  [" << -maxTurn << " , " << maxTurn << "] (" << turnP << " , " << turnI << " , " << turnD << ") Point: " << turnPointGain << " Slope: " << turnSlopeGain << endl <<
            "\tSlide: [" << -maxSlide << " , " << maxSlide << "] (" << slideP << " , " << slideI << " , " << slideD << ") Point: " << slidePointGain << " Slope: " << slideSlopeGain << endl <<
            endl;

    maxVoltage = ini->getd("Marathon","MaxVoltage",12.5);
    voltageHipPitchGain = ini->getd("Balance","VoltageHipPitchGain",0.0);

    deltaHip = ini->getd("Balance","Delta");
    fsrFwdLimit = ini->geti("Balance","FwdLimit");
    fsrBackLimit = ini->geti("Balance","BackLimit");
    gyroLimit = ini->geti("Balance","GyroLimit");
    balanceLimit = ini->geti("Balance","BalanceLimit");
    balanceCount = 0;
    balanceOffset = 0.0;
    voltageOffset = 0.0;
    
    cout << "Balance Settings:" << endl <<
            "\tDelta: " << deltaHip << endl <<
            "\tFSR: [" << fsrFwdLimit << ", " << fsrBackLimit << "]" << endl <<
            "\tGyro: [" << -gyroLimit << ", " << gyroLimit << "]" << endl <<
            "\tVoltage: " << voltageHipPitchGain << endl <<
            endl;

    turnControl = new DoublePidController(&currentTurn,&goalTurn, turnP, turnI, turnD);
    slideControl = new DoublePidController(&currentSlide, &goalSlide, slideP, slideI, slideD);
    stepControl = new DoublePidController(&currentStep, &goalStep, stepP, stepI, stepD);
    //slideControl->DEBUG_PRINT = true;

    sign.LoadIniSettings(*ini,"Sign");
    line.LoadIniSettings(*ini,"Line");
    line.SetSubsample(subsample);
    sign.DEBUG_PRINT = debug;

    if(recordVideo)
    {
        webcamStream = VideoWriter (ini->gets("Files","WebcamStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        debugStream = VideoWriter (ini->gets("Files","ScanlineStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
    }
}

void MarathonRunner::HandleVideo()
{
    CvCamera::GetInstance()->CaptureFrame();

    cv::cvtColor(CvCamera::GetInstance()->yuvFrame,rgbImage,CV_YCrCb2RGB);
    dbgImage = Mat::zeros(dbgImage.rows,dbgImage.cols,dbgImage.type());   // clear the debug image so that the text overlay is legible

    // calculate 1st x and y derivatives with kernel of 3 (-1 = same channels as src)
    //Sobel(rgbImage, edges, -1, 1, 1, 3);
    //Laplacian(CvCamera::GetInstance()->img, edges, -1);
    //imshow("Edges",edges);

    line.FindInFrame(CvCamera::GetInstance()->yuvFrame,&dbgImage);
    sign.SetLinePosition(line.Position(),line.Angle());
    sign.FindInFrame(CvCamera::GetInstance()->yuvFrame,&dbgImage);

    newFrame = true;


    if(showVideo || recordVideo)
    {
        line.Draw(rgbImage);
        line.Draw(dbgImage);
        sign.Draw(rgbImage);
        sign.Draw(dbgImage);


        char buffer[512];
        Point pt;
        Scalar colour(255,255,255);

        sprintf(buffer,"X: %0.2f  Y: %0.2f  A: %0.2f", currentStep, currentSlide, currentTurn);
        pt = cv::Point(0,12);
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 0.75, colour, 1);

        pt = cv::Point(0,24);
        sprintf(buffer,"Goal Turn: %0.2f (e=%0.2f)", goalTurn, (goalTurn - currentTurn));
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 0.75, colour, 1);

        pt = cv::Point(0,36);
        sprintf(buffer,"Goal Slide: %0.2f (e=%0.2f)", goalSlide, (goalSlide - currentSlide));
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 0.75, colour, 1);

        pt = cv::Point(0,48);
        sprintf(buffer,"Goal Step: %0.2f (e=%0.2f)", goalStep, (goalStep - currentStep));
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 0.75, colour, 1);

        pt = cv::Point(0,Camera::HEIGHT-12);
        sprintf(buffer,"Bat. %0.2fV (%0.2f%%)",MotionStatus::CM730_VOLTAGE,MotionStatus::CM730_VOLTAGE/maxVoltage*100);
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 0.75, colour, 1);

        if(showVideo)
        {
            imshow("Webcam", rgbImage);
            imshow("Scanline", dbgImage);
            cvWaitKey(1);
        }
        if(recordVideo)
        {
            webcamStream << (rgbImage);
            debugStream << dbgImage;
        }
    }
}

void MarathonRunner::HandleFall()
{
    if(MotionStatus::FALLEN != STANDUP && !isStandingUp)
    {
        int direction = MotionStatus::FALLEN;

        isStandingUp = true;
        if(debug)
            cout << "[fall] Fallen over! Stopping walking module" << endl;
        Voice::Speak("Fallen over");

		if(debug)
			cout << "[fall] Re-enabling arm motors" << endl;
        MotionManager::GetInstance()->SetTorqueEnable(ARM_MOTORS,true);
        if(debug)
			cout << "[fall] Done" << endl;

        Walking::GetInstance()->Stop();
        Walking::GetInstance()->Finish();

        //MotionManager::GetInstance()->AddModule(Action::GetInstance());
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();

        if(direction == FORWARD)
        {
            if(debug)
                cout << "[fall] Standing up from front" << endl;
            Action::GetInstance()->Start(F_UP_MOTION);//Action::DEFAULT_MOTION_F_UP);   // FORWARD GETUP
        }
        else if(direction == BACKWARD)
        {
            if(debug)
                cout << "[fall] Standing up from back" << endl;
            Action::GetInstance()->Start(B_UP_MOTION);//Action::DEFAULT_MOTION_B_UP);   // BACKWARD GETUP
        }

        if(debug)
            cout << "[fall] Watiting for action to finish" << endl;
        while(Action::GetInstance()->IsRunning())
        {
            MotionManager::msleep(10);
        }
        if(debug)
            cout << "[fall] Done" << endl;

        if(debug)
            cout << "[fall] Re-enabling walking module" << endl;
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();

        Walking::GetInstance()->X_MOVE_AMPLITUDE = minSpeedX;
        Walking::GetInstance()->Start();

		if(debug)
			cout << "[fall] Disabling arm torque" << endl;
        MotionManager::GetInstance()->SetTorqueEnable(ARM_MOTORS,armTorqueOn);
        if(debug)
			cout << "[fall] Done" << endl;

        isStandingUp = false;
    }
}

void MarathonRunner::HandleButton()
{
    if(MotionStatus::BUTTON==lastButton)
        return;
    else
    {
        lastButton = MotionStatus::BUTTON;

        if(lastButton == CM730::MIDDLE_BUTTON)
        {
            Walking::GetInstance()->Start();
            state = STATE_WALKING;
        }
        else if(state!=STATE_IDLE && lastButton == CM730::LEFT_BUTTON)
        {
            Walking::GetInstance()->Stop();
            Walking::GetInstance()->Finish();
            LoadIniSettings();
            state = STATE_IDLE;
        }
    }
}

void MarathonRunner::AdjustGait()
{
    double currentVoltage = MotionStatus::CM730_VOLTAGE;
    double voltagePercent = currentVoltage/maxVoltage;

    double defaultHipPitch = ini->getd("Walking Config","hip_pitch_offset");
    voltageOffset = defaultHipPitch - defaultHipPitch * voltagePercent * voltageHipPitchGain;
    
//    if(debug)
//		cout << "[fall] Disabling arm torque" << endl;
    MotionManager::GetInstance()->SetTorqueEnable(ARM_MOTORS,armTorqueOn);
//	if(debug)
//		cout << "[fall] Done" << endl;

    // read FSR and Gyro data
    int lfsr = MotionStatus::L_FSR_X;
    int rfsr = MotionStatus::R_FSR_X;
    int gyro = MotionStatus::FB_GYRO;

    // use fsr data from whatever foot is on the ground (or average if both are on the ground)
    int fsr;
    if(lfsr<0)
        fsr = rfsr;
    else if(rfsr<0)
        fsr = lfsr;
    else
        fsr = (lfsr+rfsr)/2;

    if(debug)
        cout << "[balance] FSR: " << fsr << " (" << lfsr << ", " << rfsr << ") Gyro: " << gyro << " Voltage: " << voltagePercent*100 << endl;

    if((fsr >= 0 && fsr >= fsrBackLimit) || gyro > gyroLimit)
    {
        balanceCount++;
        if(debug)
            cout << "[balance] leaning too far back (" << balanceCount << ")" << endl;
        if (balanceCount > balanceLimit)
        {
            balanceOffset += deltaHip;
            balanceCount = 0;
        }
    }
    else if ((fsr >= 0 && fsr <= fsrFwdLimit) || gyro < -gyroLimit)
    {
        balanceCount--;
        if(debug)
            cout << "[balance] leaning too far forward (" << balanceCount << ")" << endl;
        if (balanceCount < -balanceLimit)
        {
            balanceOffset -= deltaHip;
            balanceCount = 0;
        }
    }
    
    double goalHipPitch = defaultHipPitch + voltageOffset + balanceOffset;
    
    if(debug)
        cout << "[balance] Final hip pitch: " << goalHipPitch << endl;

    // apply the adjusted hip pitch
    Walking::GetInstance()->HIP_PITCH_OFFSET = goalHipPitch;
}

void MarathonRunner::FollowLine()
{
    Point2D target = line.Position();
    double angle = line.Angle();
    bool lineLost = line.IsLost();
    bool followingSign = false;

    if(sign.Direction() != MarathonSign::NOT_FOUND)
    {
        followingSign = true;
        FollowSign(target,angle);

        // set the forehead colour to yellow
        if(indicateLED && MotionManager::GetInstance()->GetEnable())
            MotionManager::GetInstance()->SetForeheadColour(255,255,0);
    }
    else if(line.IsLost())
    {
        RecoverLostLine(target,angle);

        // set forehead colour to red
        if(indicateLED && MotionManager::GetInstance()->GetEnable())
            MotionManager::GetInstance()->SetForeheadColour(255,0,0);
    }
    else
    {
        if(indicateLED && MotionManager::GetInstance()->GetEnable())
        {
            // set forehead colour to green
            MotionManager::GetInstance()->SetForeheadColour(0,255,0);

            // set eye colour to blue
            MotionManager::GetInstance()->SetEyeColour(0,0,255);
        }
    }

    currentPointError = (Camera::WIDTH/2 - target.X)/(Camera::WIDTH/2);

    goalTurn = maxTurn * currentPointError * turnPointGain + maxTurn * angle/90.0 * turnSlopeGain;
    if(goalTurn > maxTurn)
        goalTurn = maxTurn;
    else if(goalTurn < -maxTurn)
        goalTurn = -maxTurn;

    turnControl->Update();
    if(currentTurn > maxTurn)
        currentTurn = maxTurn;
    else if(currentTurn <= -maxTurn)
        currentTurn = -maxTurn;

    // if we're recovering from a lost line DO NOT SLIDE
    // likewise, if we're following the sign don't worry about sliding
    if(!lineLost && !followingSign)
        goalSlide = maxSlide * currentPointError * slidePointGain + maxSlide * angle/90.0 * slideSlopeGain;
    else
        goalSlide = 0.0;
    if(goalSlide > maxSlide)
        goalSlide = maxSlide;
    else if(goalSlide < -maxSlide)
        goalSlide = -maxSlide;

    slideControl->Update();
    if(currentSlide < -maxSlide)
        currentSlide = -maxSlide;
    else if(currentSlide > maxSlide)
        currentSlide = maxSlide;

    // adjust the X amplitude based on how far ahead we can see and how fast we're turning
    goalStep = maxSpeedX * (Camera::HEIGHT - target.Y)/Camera::HEIGHT * stepSizeGain;
    if(goalStep < minSpeedX)
        goalStep = minSpeedX;
    else if(goalStep > maxSpeedX)
        goalStep = maxSpeedX;

    stepControl->Update();
    if(currentStep < minSpeedX)
        currentStep = minSpeedX;
    else if(currentStep > maxSpeedX)
        currentStep = maxSpeedX;

    Walking::GetInstance()->A_MOVE_AMPLITUDE = currentTurn;
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = currentSlide;
    Walking::GetInstance()->X_MOVE_AMPLITUDE = currentStep;
}

void MarathonRunner::FollowSign(Point2D &pt, double &angle)
{
    // we can see the line and the sign we need to follow
    int direction = sign.Direction();
    double lineAngle = line.Angle();

    switch(direction)
    {
    case MarathonSign::DIR_LEFT:
        pt = Point2D(0,Camera::HEIGHT/2); // wide arcing turn left
        angle = 90-lineAngle;

        // turning left; eye colour to red
        if(indicateLED && MotionManager::GetInstance()->GetEnable())
            MotionManager::GetInstance()->SetEyeColour(255,0,0);

        if(indicateSigns)
        {
            LeftArm::GetInstance()->m_Joint.SetEnableLeftArmOnly(true,true);
            LeftArm::GetInstance()->m_Joint.SetValue(JointData::ID_L_SHOULDER_ROLL,MotionStatus::m_CurrentJoints.GetValue(JointData::ID_L_SHOULDER_ROLL));
            MotionManager::GetInstance()->SetTorqueEnable(JointData::ID_L_SHOULDER_ROLL,true);
            LeftArm::GetInstance()->SetMoveMethod(Limb::ApproachByOne);
            LeftArm::GetInstance()->SetAngles(0,30,0);
        }
        break;

    case MarathonSign::DIR_RIGHT:
        pt = Point2D(Camera::WIDTH,Camera::HEIGHT/2); // wide arcing turn right
        angle = -90-lineAngle;

        // turning right; eye colour to green
        if(indicateLED && MotionManager::GetInstance()->GetEnable())
            MotionManager::GetInstance()->SetEyeColour(0,255,0);

        if(indicateSigns)
        {
            RightArm::GetInstance()->m_Joint.SetEnableRightArmOnly(true,true);
            RightArm::GetInstance()->m_Joint.SetValue(JointData::ID_R_SHOULDER_ROLL,MotionStatus::m_CurrentJoints.GetValue(JointData::ID_R_SHOULDER_ROLL));
            MotionManager::GetInstance()->SetTorqueEnable(JointData::ID_R_SHOULDER_ROLL,true);
            RightArm::GetInstance()->SetMoveMethod(Limb::ApproachByOne);
            RightArm::GetInstance()->SetAngles(0,90,0);
        }
        break;

    case MarathonSign::DIR_FORWARD:
        // don't change the point; keep going the same direction we are now
        angle = lineAngle;

        // going forward; eye colour to blue
        if(indicateLED && MotionManager::GetInstance()->GetEnable())
            MotionManager::GetInstance()->SetEyeColour(0,0,255);

        if(indicateSigns)
        {
            LeftArm::GetInstance()->m_Joint.SetEnableLeftArmOnly(true,true);
            LeftArm::GetInstance()->m_Joint.SetValue(JointData::ID_L_SHOULDER_ROLL,MotionStatus::m_CurrentJoints.GetValue(JointData::ID_L_SHOULDER_ROLL));
            MotionManager::GetInstance()->SetTorqueEnable(JointData::ID_L_SHOULDER_ROLL,true);
            LeftArm::GetInstance()->SetMoveMethod(Limb::ApproachByOne);
            LeftArm::GetInstance()->SetAngles(0,30,0);

            RightArm::GetInstance()->m_Joint.SetEnableRightArmOnly(true,true);
            RightArm::GetInstance()->m_Joint.SetValue(JointData::ID_R_SHOULDER_ROLL,MotionStatus::m_CurrentJoints.GetValue(JointData::ID_R_SHOULDER_ROLL));
            MotionManager::GetInstance()->SetTorqueEnable(JointData::ID_R_SHOULDER_ROLL,true);
            RightArm::GetInstance()->SetMoveMethod(Limb::ApproachByOne);
            RightArm::GetInstance()->SetAngles(0,90,0);
        }
        break;

    default:
        break;
    }
}

void MarathonRunner::RecoverLostLine(Point2D &pt, double &angle)
{
    // if we recently saw a sign then follow its instructions
    // otherwise just go for the last point we saw the line
    int direction = sign.Direction();
    if(direction == MarathonSign::NOT_FOUND)
    {
        pt = line.Position();
        angle = line.Angle();

        // we're lost; set eye colour to yellow
        if(indicateLED && MotionManager::GetInstance()->GetEnable())
            MotionManager::GetInstance()->SetEyeColour(255,255,0);
    }
    else
    {
        // we recently saw a sign; do whatever it told us to do
        FollowSign(pt, angle);
    }
}
