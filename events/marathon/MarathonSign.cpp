#include "MarathonSign.h"
#include <iostream>
#include <darwin/framework/Math.h>
#include <cstdio>

using namespace std;
using namespace Robot;
using namespace cv;

MarathonSign::MarathonSign() : Robot::Target()
{
    spectrum.push_back(Scalar(0,0,255));
    spectrum.push_back(Scalar(0,128,255));
    spectrum.push_back(Scalar(0,255,255));
    spectrum.push_back(Scalar(0,255,128));
    spectrum.push_back(Scalar(0,255,0));
    spectrum.push_back(Scalar(128,255,0));
    spectrum.push_back(Scalar(255,255,0));
    spectrum.push_back(Scalar(255,128,0));
    spectrum.push_back(Scalar(255,0,0));
    spectrum.push_back(Scalar(255,0,128));
    spectrum.push_back(Scalar(255,0,255));
    spectrum.push_back(Scalar(128,0,255));

    lineAngle = 0;
    DEBUG_PRINT = false;
    prevDirections = NULL;
}

MarathonSign::~MarathonSign()
{
    if(prevDirections != NULL)
        free(prevDirections);
}

void MarathonSign::LoadIniSettings(minIni &ini, const char *section)
{
    string fgColour = ini.gets(section,"FgColour","White");
    string bgColour = ini.gets(section,"BgColour","Black");

    cout << "Loading sign background colour from section " << bgColour << endl <<
            "Loading sign foreground colour from section " << fgColour << endl;

    bg.LoadIniSettings(ini,bgColour);
    fg.LoadIniSettings(ini,fgColour);

    numSegments = ini.geti(section,"NumSegments",3);
    minSize = ini.geti(section,"MinSize",50);
    angleThreshold = ini.getd(section,"AngleThreshold",20);
    segmentFillThreshold = ini.getd(section,"FillThreshold",0.5);

    maxMissedFrames = ini.geti(section,"MaxMissedFrames",10);
    directionQueueIn = 0;
    prevDirections = (int*)malloc(sizeof(int)*maxMissedFrames);

    for(int i=0; i<maxMissedFrames; i++)
        prevDirections[i] = NOT_FOUND;
}

int MarathonSign::FindInFrame(Mat &image, Mat *dest)
{
    // find thresholded images for the foreground and background colours
    Mat fgBinary(image.rows,image.cols,CV_8UC1);
    inRange(image,Scalar(fg.GetYRange()->min, fg.GetURange()->min, fg.GetVRange()->min),
            Scalar(fg.GetYRange()->max, fg.GetURange()->max, fg.GetVRange()->max),
            fgBinary);
    Mat bgBinary(image.rows,image.cols,CV_8UC1);
    inRange(image,Scalar(bg.GetYRange()->min, bg.GetURange()->min, bg.GetVRange()->min),
            Scalar(bg.GetYRange()->max, bg.GetURange()->max, bg.GetVRange()->max),
            bgBinary);

    dilate(fgBinary,fgBinary,Mat());
    dilate(bgBinary,bgBinary,Mat());

    // blob detection modifies the original image, so keep a copy around
    Mat fgBinaryCopy;
    fgBinary.copyTo(fgBinaryCopy);
    Mat bgBinaryCopy;
    bgBinary.copyTo(bgBinaryCopy);

    // for there to be a sign the foreground-coloured blob must fit within the background blob
    // find the bounding boxes and we'll go from there
    std::vector<BoundingBox> fgBoundingBoxes, bgBoundingBoxes;
    FindBoundingBoxes(fgBinaryCopy,fgBoundingBoxes);
    FindBoundingBoxes(bgBinaryCopy,bgBoundingBoxes);

    candidateForegrounds.clear();
    candidateBackgrounds.clear();

    for(unsigned int i=0; i<fgBoundingBoxes.size(); i++)
    {
        BoundingBox &inner = fgBoundingBoxes.at(i);
        double angleToSign = -rad2deg(atan2(inner.center.X-linePosition.X,-inner.center.Y+linePosition.Y));
        if(inner.width >= minSize && inner.height >= minSize && fabs(angleToSign-lineAngle) <= angleThreshold)
        {
            for(unsigned int j=0; j<bgBoundingBoxes.size(); j++)
            {
                BoundingBox &outer = bgBoundingBoxes.at(j);

                // make sure that inner is completely surrounded by outer
                if(inner.top() >= outer.top() && inner.bottom() <= outer.bottom() &&
                   inner.left() >= outer.left() && inner.right() <= outer.right())
                {
                    candidateForegrounds.push_back(inner);
                    candidateBackgrounds.push_back(outer);
                }
            }
        }
    }

    // go through all of the candidate foregrounds and find the one that is closest to the end of the line in the direction of the line's slope
    double rangeToSign;
    double minDistance = INT_MAX;
    BoundingBox closest;
    for(unsigned int i=0; i<candidateForegrounds.size(); i++)
    {
        BoundingBox &bbox = candidateForegrounds.at(i);
        rangeToSign = (bbox.center - linePosition).Magnitude();

        if(rangeToSign < minDistance)
        {
            minDistance = rangeToSign;
            closest = bbox;
        }
    }

    if(dest!=NULL)
    {
        int imgSize = fgBinary.rows * fgBinary.cols;

        Scalar bgColour = *(bg.GetMarkColour());
        Scalar fgColour = *(fg.GetMarkColour());

        for(int i=0; i<imgSize; i++)
        {
            if(fgBinary.data[i] == 0xff)
            {
                for(int j=0; j<dest->channels(); j++)
                {
                    dest->data[i*dest->channels()+j] = fgColour[j];
                }
            }
            else if(bgBinary.data[i] == 0xff)
            {
                for(int j=0; j<dest->channels(); j++)
                {
                    dest->data[i*dest->channels()+j] = bgColour[j];
                }
            }
        }
    }

    prevDirections[directionQueueIn] = signDirection;
    directionQueueIn = (directionQueueIn+1) % maxMissedFrames;
    signDirection = ReadSign(closest,lineAngle,fgBinary,dest);

    if(signDirection == NOT_FOUND)
        noSignFrames++;
    else
        noSignFrames = 0;

    return signDirection;
}

void MarathonSign::FindBoundingBoxes(Mat &binary, std::vector<BoundingBox> &boxes)
{
    std::vector<std::vector<Point> > contours;
    cv::findContours(binary, contours,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);

    unsigned int numContours = contours.size();
    for(unsigned int c=0; c<numContours; c++)
    {
        unsigned int numPoints = contours[c].size();
        int maxX = -1;
        int minX = 99999;
        int maxY = -1;
        int minY = 99999;
        for(unsigned int p=0; p<numPoints; p++)
        {
            Point pt = contours[c][p];

            if(pt.x < minX)
                minX = pt.x;
            if(pt.x > maxX)
                maxX = pt.x;
            if(pt.y < minY)
                minY = pt.y;
            if(pt.y > maxY)
                maxY = pt.y;
        }

        if(minX >= ignoreLeft && maxX <= (binary.cols - ignoreRight) && minY >= ignoreTop && maxY <= (binary.rows - ignoreBottom))
        {

            BoundingBox bbox;
            bbox.center.X = (maxX + minX)/2;
            bbox.center.Y = (maxY + minY)/2;

            bbox.height = maxY - minY + 1;
            bbox.width = maxX - minX + 1;

            // just hard-set all the other parameters for the bounding box since we have no idea what they really are
            bbox.fill = 1.0;   // hack because we really have no idea
            bbox.avgY = (yRange.max + yRange.min)/2;
            bbox.avgU = (uRange.max + uRange.min)/2;
            bbox.avgV = (vRange.max + vRange.min)/2;

            //cout << minX << " " << maxX << " :: " << minY << " " << maxY << endl;

            boxes.push_back(bbox);
        }
    }
}

int MarathonSign::Direction()
{
    // based on the last several frames worth of signs decide which direction is the best to go
    int votes[NUM_DIRECTIONS];
    for(int i=0; i<NUM_DIRECTIONS; i++)
    {
        votes[i] = 0;
    }

    votes[signDirection]++;

    for(int i=0; i<maxMissedFrames; i++)
    {
        if(prevDirections[i] < 0 || prevDirections[i] >= NUM_DIRECTIONS)
            votes[NOT_FOUND]++;
        else
            votes[prevDirections[i]]++;
    }

    if(votes[DIR_FORWARD] > votes[DIR_LEFT] && votes[DIR_FORWARD] > votes[DIR_RIGHT])
        return DIR_FORWARD;
    else if(votes[DIR_LEFT] > votes[DIR_FORWARD] && votes[DIR_LEFT] > votes[DIR_RIGHT])
        return DIR_LEFT;
    else if(votes[DIR_RIGHT] > votes[DIR_FORWARD] && votes[DIR_RIGHT] > votes[DIR_LEFT])
        return DIR_RIGHT;
    else
        return NOT_FOUND;
}

void MarathonSign::Print()
{
    cout << "MarathonSign::Print() not implemented yet" << endl;
}

void MarathonSign::Draw(Mat &canvas)
{

    for(unsigned int i=0; i<candidateForegrounds.size(); i++)
    {
        candidateForegrounds.at(i).Draw(canvas,spectrum.at(i%spectrum.size()),1);
        //candidateBackgrounds.at(i).Draw(canvas,spectrum.at(i%spectrum.size()),1);
    }


    signPosition.Draw(canvas,numSegments);

    char buffer[8];
    if(signDirection == DIR_LEFT)
        sprintf(buffer,"[ <- ]");
    else if(signDirection == DIR_FORWARD)
        sprintf(buffer,"[ ^ ]");
    else if(signDirection == DIR_RIGHT)
        sprintf(buffer,"[ -> ]");
    else
        sprintf(buffer,"[ X ]");
    cv::putText(canvas,buffer,Point(signPosition.center.X,signPosition.center.Y),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,0),2);
}

void MarathonSign::ProcessCandidateImage(Mat &image)
{
    (void)image;
    cout << "MarathonSign::ProcessCandidateImage(Mat) not implemented yet" << endl;
}

int MarathonSign::ReadSign(BoundingBox &region, double rotationAngle, Mat binaryImage, Mat *dbg)
{
#ifndef DEBUG
    (void)dbg;
#endif
    signPosition.Update(region,rotationAngle);
    rotationAngle = -deg2rad(rotationAngle);

    int pixelCounts[numSegments*numSegments];
    for(int i=0; i<numSegments*numSegments; i++)
    {
        pixelCounts[i] = 0;
    }

    Point2D p,q;
    const int REGION_WIDTH = (int)(signPosition.width/numSegments);
    const int REGION_HEIGHT = (int)(signPosition.height/numSegments);
    Point2D rotationOrigin = Point2D(signPosition.width/2,signPosition.height/2);
    for(q.Y=0; q.Y<signPosition.height; q.Y++)
    {
        for(q.X=0; q.X<signPosition.width; q.X++)
        {
            p = RotatePoint(q,rotationOrigin,rotationAngle) - rotationOrigin + signPosition.center;
            p.X = (int)p.X;
            p.Y = (int)p.Y;

            uint8_t px = binaryImage.at<uint8_t>(Point(p.X,p.Y));
            int u = (int)q.X/REGION_WIDTH;
            int v = (int)q.Y/REGION_HEIGHT;

            if(u>=numSegments)
                u = numSegments-1;
            if(v>=numSegments)
                v = numSegments-1;

            if(px == 0xff)
            {
                pixelCounts[v*numSegments+u]++;
            }
#ifdef DEBUG
            if(dbg!=NULL && px == 0xff)
                cv::circle(*dbg,Point(p.X,p.Y),1,Scalar(255/numSegments*u,255/numSegments*v,255/numSegments*(numSegments-u)));
#endif
        }
    }

    const int REGION_AREA = REGION_HEIGHT * REGION_WIDTH;
    return ReadHistogram(pixelCounts, REGION_AREA);
}

int MarathonSign::ReadHistogram(int pixelCounts[], int regionArea)
{
    const int LEFT_COL = numSegments/2-1;
    const int MIDDLE_COL = numSegments/2;
    const int RIGHT_COL = numSegments/2+1;
    const int TOP_ROW = numSegments/2-1;
    const int MIDDLE_ROW = numSegments/2;
    const int BOTTOM_ROW = numSegments/2+1;

    // hot/cold plot indicating if each region is full or empty
    bool regions[3][3];
    for(int i=0; i<numSegments; i++)
    {
        if(i>=TOP_ROW && i<=BOTTOM_ROW)
        {
            for(int j=0; j<numSegments; j++)
            {
                if(j>=LEFT_COL && j<=RIGHT_COL)
                {
                    double fill = (double)pixelCounts[i*numSegments+j]/regionArea;

                    if(fill >= segmentFillThreshold)
                    {
                        regions[i-TOP_ROW][j-LEFT_COL] = true;
                    }
                    else
                    {
                        regions[i-TOP_ROW][j-LEFT_COL] = false;
                    }
                }
            }
        }
    }

    if(DEBUG_PRINT)
    {
        // print out the sign so we can check that it looks good
        cout << "+---+" << endl;
        for(int i=TOP_ROW; i<=BOTTOM_ROW; i++)
        {
            cout << "|";
            for(int j=LEFT_COL; j<=RIGHT_COL; j++)
            {
                if(regions[i-TOP_ROW][j-LEFT_COL])
                {
                    cout << "*";
                }
                else
                {
                    cout << " ";
                }
            }
            cout << "|" << endl;
        }
        cout << "+---+" << endl;
    }

    int vHistogram[3] = {0,0,0};
    int hHistogram[3] = {0,0,0};

    for(int i=TOP_ROW; i<=BOTTOM_ROW; i++)
    {
        if(regions[i][LEFT_COL])
        {
            vHistogram[0]++;
        }

        if(regions[i][MIDDLE_COL])
        {
            hHistogram[1]++;
        }

        if(regions[i][LEFT_COL])
        {
            hHistogram[2]++;
        }
    }

    for(int i=LEFT_COL; i<=RIGHT_COL; i++)
    {
        if(regions[TOP_ROW][i])
        {
            vHistogram[0]++;
        }

        if(regions[MIDDLE_ROW][i])
        {
            vHistogram[1]++;
        }

        if(regions[BOTTOM_ROW][i])
        {
            vHistogram[2]++;
        }
    }


    return Histogram2Direction(hHistogram, vHistogram, regions);
}

int MarathonSign::Histogram2Direction(int rowCounts[3], int colCounts[3], bool regions[3][3])
{
    const int TOP = 0;
    const int MID = 1;
    const int BTM = 2;
    const int LFT = 0;
    const int RT = 2;

    int fwdVotes = 0;
    int leftVotes = 0;
    int rightVotes = 0;

    // first off check the lower corners
    if(!regions[BTM][LFT])
    {
        fwdVotes++;
        leftVotes++;
    }
    else
    {
        rightVotes++;
    }
    if(!regions[BTM][RT])
    {
        fwdVotes++;
        rightVotes++;
    }
    else
    {
        leftVotes++;
    }

    // check the middle of the bottom row
    if(regions[BTM][MID])
    {
        fwdVotes++;
    }
    else
    {
        rightVotes++;
        leftVotes++;
    }

    // check for strong horizontal lines in the top and/or middle
    if(rowCounts[TOP] >= 2 || rowCounts[MID] >= 2)
    {
        leftVotes++;
        rightVotes++;
    }
    if(rowCounts[TOP] == 1 || rowCounts[MID] == 1)
    {
        fwdVotes++;
    }

    // check for strong vertical lines on the sides
    // they could either be a forward arrow, or the vertical portion of a turn
    if(colCounts[LFT] >= 2)
    {
        fwdVotes++;
        rightVotes++;
    }
    else
    {
        leftVotes++;
    }

    if(colCounts[RT] >= 2)
    {
        fwdVotes++;
        leftVotes++;
    }
    else
    {
        rightVotes++;
    }

    if(colCounts[MID] >= 2)
    {
        fwdVotes++;
    }
    else
    {
        rightVotes++;
        leftVotes++;
    }

    if(fwdVotes > rightVotes && fwdVotes > leftVotes)
        return DIR_FORWARD;
    else if(rightVotes > fwdVotes && rightVotes > leftVotes)
        return DIR_RIGHT;
    else if(leftVotes > fwdVotes && leftVotes > rightVotes)
        return DIR_LEFT;
    else
    {
        // we have at least a two-way tie
        if(fwdVotes == leftVotes && leftVotes == rightVotes)
        {
            //3-way tie
            // TODO: some tie-breakers
            return NOT_FOUND;
        }
        else if(fwdVotes == leftVotes)
        {
            // use the presence/absence of a complete row and the first decider
            if(rowCounts[TOP] == 3 || rowCounts[MID] == 3)
                return DIR_LEFT;
            else if(rowCounts[TOP] == 1 || rowCounts[MID] == 1)
                return DIR_FORWARD;

            // use the right and middle columns as the decider
            if(colCounts[MID] > colCounts[RT])
                return DIR_FORWARD;
            else if(colCounts[MID] < colCounts[RT])
                return DIR_LEFT;

            // use the corner/center as the decider
            if(regions[BTM][MID] && !regions[BTM][RT])
                return DIR_FORWARD;
            else if(!regions[BTM][MID] && regions[BTM][RT])
                return DIR_LEFT;

            // still guessing; we just don't know
            return NOT_FOUND;
        }
        else if(fwdVotes == rightVotes)
        {
            // use the presence/absence of a complete row and the first decider
            if(rowCounts[TOP] == 3 || rowCounts[MID] == 3)
                return DIR_RIGHT;
            else if(rowCounts[TOP] == 1 || rowCounts[MID] == 1)
                return DIR_FORWARD;

            // use the right and middle columns as the decider
            if(colCounts[MID] > colCounts[LFT])
                return DIR_FORWARD;
            else if(colCounts[MID] < colCounts[LFT])
                return DIR_RIGHT;

            // use the corner/center as the decider
            if(regions[BTM][MID] && !regions[BTM][LFT])
                return DIR_FORWARD;
            else if(!regions[BTM][MID] && regions[BTM][LFT])
                return DIR_RIGHT;

            // still guessing; we just don't know
            return NOT_FOUND;
        }
        else if(leftVotes == rightVotes)
        {
            // use the lower corners as the tie breaker
            if(regions[BTM][LFT] && !regions[BTM][RT])
                return DIR_RIGHT;
            else if(!regions[BTM][LFT] && regions[BTM][RT])
                return DIR_LEFT;

            // if we're still here then use the left/right columns as the deciding factor
            if(colCounts[LFT] > colCounts[RT])
                return DIR_RIGHT;
            else if(colCounts[LFT] < colCounts[RT])
                return DIR_LEFT;

            // if we're still guessing than we really don't know
            return NOT_FOUND;
        }
        else
        {
            //?? this should never happen
            return NOT_FOUND;
        }

    }
}

Point2D MarathonSign::RotatePoint(Point2D p, Point2D center, double radians)
{
    Point2D q = p-center;

    double c = cos(radians);
    double s = sin(radians);

    q = Point2D(
                q.X * c - q.Y * s,
                q.Y * c + q.X * s
    );

    q = q+center;

    return q;
}


void MarathonSign::SignPosition::Update(BoundingBox &srcRegion, double rotationAngle)
{
    // enlarge the bounding box so that it is square with a width and height
    // equal to the diagonal of the original bounding  box
    double diagonal = (Point2D(srcRegion.left(),srcRegion.top())-Point2D(srcRegion.right(),srcRegion.bottom())).Magnitude();
    BoundingBox region;
    region.center = srcRegion.center;
    region.height = diagonal;
    region.width = diagonal;

    rotationAngle = deg2rad(-rotationAngle);

    Point2D tl(region.left(),region.top());
    Point2D tr(region.right(),region.top());
    Point2D bl(region.left(),region.bottom());
    Point2D br(region.right(),region.bottom());

    tl = MarathonSign::RotatePoint(tl,region.center,rotationAngle);
    tr = MarathonSign::RotatePoint(tr,region.center,rotationAngle);
    bl = MarathonSign::RotatePoint(bl,region.center,rotationAngle);
    br = MarathonSign::RotatePoint(br,region.center,rotationAngle);

    topLeft = tl;
    topRight = tr;
    bottomLeft = bl;
    bottomRight = br;

    height = region.height;
    width = region.width;
    center = region.center;
}

void MarathonSign::SignPosition::Draw(Mat &canvas, int numSegments)
{
    cv::line(canvas,cv::Point(topLeft.X,topLeft.Y),cv::Point(topRight.X,topRight.Y),Scalar(255,0,0));
    cv::line(canvas,cv::Point(bottomRight.X,bottomRight.Y),cv::Point(topRight.X,topRight.Y),Scalar(0,255,0));
    cv::line(canvas,cv::Point(bottomRight.X,bottomRight.Y),cv::Point(bottomLeft.X,bottomLeft.Y),Scalar(0,255,255));
    cv::line(canvas,cv::Point(topLeft.X,topLeft.Y),cv::Point(bottomLeft.X,bottomLeft.Y),Scalar(0,0,255));

    // draw the grid
    Point2D rows[numSegments-1][2];
    for(int i=0; i<numSegments-1; i++)
    {
        rows[i][0] = (bottomLeft-topLeft)/numSegments*(i+1) + topLeft;
        rows[i][1] = (bottomRight-topRight)/numSegments*(i+1) + topRight;
    }
    Point2D cols[numSegments-1][2];
    for(int i=0; i<numSegments-1; i++)
    {
        cols[i][0] = (topRight-topLeft)/numSegments*(i+1) + topLeft;
        cols[i][1] = (bottomRight-bottomLeft)/numSegments*(i+1) + bottomLeft;
    }

    for(int i=0; i<numSegments-1; i++)
    {
        cv::line(canvas, Point(rows[i][0].X,rows[i][0].Y),Point(rows[i][1].X,rows[i][1].Y),Scalar(255,0,255));
        cv::line(canvas, Point(cols[i][0].X,cols[i][0].Y),Point(cols[i][1].X,cols[i][1].Y),Scalar(255,255,0));
    }
}
