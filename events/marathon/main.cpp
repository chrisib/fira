#include <darwin/framework/Target.h>
#include <darwin/linux/CvCamera.h>
#include <LinuxDARwIn.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/MultiBlob.h>
#include <darwin/framework/BallFollower.h>
#include <darwin/framework/BallTracker.h>
#include "MarathonLine.h"
#include <list>
#include <darwin/framework/Voice.h>
#include <darwin/framework/DoublePidController.h>
#include <darwin/framework/Math.h>

using namespace std;
using namespace Robot;
using namespace cv;

#if 1
#include <MarathonRunner.h>

int main(int argc, char** argv)
{
    MarathonRunner runner;
    runner.ParseArguments(argc, argv);

    runner.Initialize();
    runner.LoadIniSettings();

    runner.Execute();

    return 0;
}

#else

#define STAND_UP_MOTION 100
#define F_UP_MOTION 101
#define B_UP_MOTION 102

// forward definitions
void followLine();
void handleFall();
void handleVideo();
void initialize();
void loadIniSettings();
void parseArguments(int argc, char** argv);
void pauseThread();
void* videoLoop(void* arg);

bool showVideo = true;
bool visionTest = false;
bool recordVideo = false;
int subsample = 4;

minIni *ini = new minIni("config.ini");
Mat dbgImage = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);
Mat rgbImage = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);

// line tracking
Point2D avgPt;
double maxAllowedDeviation;
int missedFrames = 0;
int maxMissedFrames = 20;

// movement parameters
double maxSpeedX;
double minSpeedX;
double maxTurn;
double maxSlide;
double turnP = 0.0;
double turnI = 0.0;
double turnD = 0.0;
double slideP = 0.0;
double slideI = 0.0;
double slideD = 0.0;
double stepP = 0.0;
double stepI = 0.0;
double stepD = 0.0;
double currentTurn = 0.0;
double goalTurn = 0.0;
double currentSlide = 0.0;
double goalSlide = 0.0;
double currentStep = 0.0;
double goalStep = 0.0;
double currentPointError = 0.0;
double slidePointGain = 0.0;
double slideSlopeGain = 0.0;
double turnPointGain = 0.0;
double turnSlopeGain = 0.0;
double stepSizeGain = 0.0;

DoublePidController *slideControl;
DoublePidController *turnControl;
DoublePidController *stepControl;

std::list<int> ARM_MOTORS;
std::list<bool> ENABLE_TRUE;
std::list<bool> ENABLE_FALSE;

// is the get-up motion presently running?
bool isStandingUp = false;

VideoWriter webcamStream;
VideoWriter debugStream;

pthread_t videoThreadId;
pthread_mutex_t videoSync;

vector<Target*> bottomSegments;
#ifdef USE_TOP_REGIONS
vector<AbstractTarget*> middleSegments;
vector<AbstractTarget*> topSegments;
#endif
vector<Target*> leftSegments;
vector<Target*> rightSegments;
MarathonLine *bottomLine;
#ifdef USE_TOP_REGIONS
MarathonLine *middleLine;
MarathonLine *topLine;
#endif
MarathonLine *leftLine;
MarathonLine *rightLine;

int main(int argc, char** argv)
{
    parseArguments(argc, argv);
    initialize();
    loadIniSettings();

    cerr << "[info] Starting async video processing thread" << endl;
    pthread_mutex_init(&videoSync,0);
    pthread_mutex_lock(&videoSync);
    pthread_create(&videoThreadId, NULL, &videoLoop, NULL);

    if(visionTest)
    {
        cerr << "[info] In vision test mode" << endl;
        for(;;)
        {
            pthread_mutex_lock(&videoSync);
            followLine();
        }

    }
    else
    {
        // start the video thread
        time_t t = time(NULL);
        while(time(NULL) - t < 3)
        {
            // do nothing
        }
    }

    Voice::Speak("Press the middle button");
    cout << "Press the middle button to start" << endl;
    MotionManager::WaitButton(CM730::MIDDLE_BUTTON);
    cout << "=== Starting the Marathon ===" << endl;

    //handleVideo();

    // run the marathon
    Walking::GetInstance()->X_MOVE_AMPLITUDE = minSpeedX;
    Walking::GetInstance()->Start();
    for(;;)
    {
        handleFall();

        pthread_mutex_lock(&videoSync);
        followLine();

        // sleep 10ms
        //usleep(10000);
    }

    return 0;
}

void parseArguments(int argc, char** argv)
{
    for(int i=1; i<argc; i++)
    {
        if(!strcmp(argv[i], "--no-video"))
            showVideo = false;
        else if(!strcmp(argv[i], "--vision-test"))
            visionTest = true;
        else if(!strcmp(argv[i], "--record-video"))
            recordVideo = true;
        else if(!strcmp(argv[i], "--subsample"))
        {
            i++;
            subsample = atoi(argv[i]);

            if(subsample < 1)
            {
                cerr << "ERROR: subsample cannot be " << subsample << endl;
                exit(1);
            }
        }
        else
        {
            cerr << endl <<
                    "Usage:" << endl <<
                    "\tmarathon [--no-video] [--record-video] [--subsample INT] [--vision-test][--help]" << endl << endl <<
                    "Argument summaries:" << endl <<
                    "\t--no-video     suppress all X window output" << endl <<
                    "\t--record-video record the video streams to .avi files for debugging" << endl <<
                    "\t--subsample    specify the scaline subsample (default 2)" << endl <<
                    "\t--vision-test  do not power the motors; simply process the vision" << endl <<
                    "\t--help         show this message" << endl;
            exit(0);
        }
    }
}

void initialize()
{
    CvCamera::GetInstance()->Initialize(0);
//	CvCamera::GetInstance()->SetAutoWhiteBalance( 0 );
//    cout << "AUTO_WHITE_BALANCE: " << Robot::CvCamera::GetInstance()->v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, 0 ) << endl;

    if(showVideo)
    {
        namedWindow("Webcam", CV_WINDOW_AUTOSIZE);
        namedWindow("Scanline", CV_WINDOW_AUTOSIZE);
        cvMoveWindow("Webcam",0,0);
        cvMoveWindow("Scanline",0,280);
        imshow("Webcam", dbgImage);
        imshow("Scanline", dbgImage);
    }

    int numVSegments = ini->geti("Vision","NumVSegments",11);
    int numHSegments = ini->geti("Vision","NumHSegments",21);
    int top = 0;
    int bottom;
    int height = Camera::HEIGHT/numVSegments;
    std::string lineColour = ini->gets("Vision","LineColour");
    cerr << "Configuration Line Colour: " << lineColour << endl;

    MultiTarget *newTarget;
#ifdef USE_TOP_REGIONS
    // set up the vertical line segments
    for(int i=0; i<numVSegments/4; i++)
    {
        newTarget = new MultiTarget();
        newTarget->LoadIniSettings(*ini, lineColour);

        bottom = Camera::HEIGHT-(height+top)+1;

        //cout << "V Segment " << i << ": " << top << " to " << bottom << endl;

        newTarget->SetIgnoreTop(top);
        newTarget->SetIgnoreBottom(bottom);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(255,0,0);

        top += height;

        topSegments.push_back(newTarget);
    }
    for(int i=numVSegments/4; i<numVSegments/2; i++)
    {
        newTarget = new MultiTarget();
        newTarget->LoadIniSettings(*ini, lineColour);

        bottom = Camera::HEIGHT-(height+top)+1;

        //cout << "V Segment " << i << ": " << top << " to " << bottom << endl;

        newTarget->SetIgnoreTop(top);
        newTarget->SetIgnoreBottom(bottom);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(0,255,0);

        top += height;

        middleSegments.push_back(newTarget);
    }
    for(int i=numVSegments/2; i<numVSegments; i++)
#else
    top = Camera::HEIGHT/3;
    bottom = Camera::HEIGHT/3-height;
    for(int i=numVSegments/3; i<numVSegments; i++)
#endif
    {
        newTarget = new MultiTarget();
        newTarget->LoadIniSettings(*ini, lineColour);

        bottom = Camera::HEIGHT-(height+top)+1;

        cout << "V Segment " << i << ": " << top << " to " << bottom << endl;

        newTarget->SetIgnoreTop(top);
        newTarget->SetIgnoreBottom(bottom);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(255,255,0);

        top += height;

        bottomSegments.push_back(newTarget);
    }

    // set up the horizontal segments
    int left = 0;
    int right;
    int width = Camera::WIDTH/numHSegments;

    for(int i=0; i<numHSegments/2; i++)
    {
        newTarget = new MultiTarget();
        newTarget->LoadIniSettings(*ini, lineColour);

        right = Camera::WIDTH-(width+left)+1;

        //cout << "H Segment " << i << ": " << left << " to " << right << endl;

        newTarget->SetIgnoreLeft(left);
        newTarget->SetIgnoreRight(right);
        newTarget->SetIgnoreTop(Camera::HEIGHT/6);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(255,0,0);

        left += width;

        leftSegments.push_back(newTarget);
    }

    for(int i=numHSegments/2; i<numHSegments; i++)
    {
        newTarget = new MultiTarget();
        newTarget->LoadIniSettings(*ini, lineColour);

        right = Camera::WIDTH-(width+left)+1;

        //cout << "H Segment " << i << ": " << left << " to " << right << endl;

        newTarget->SetIgnoreLeft(left);
        newTarget->SetIgnoreRight(right);
        newTarget->SetIgnoreTop(Camera::HEIGHT/6);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(0,255,0);

        left += width;

        rightSegments.push_back(newTarget);
    }

    maxAllowedDeviation = ini->getd("Strategy","MaxError",10.0);
    bottomLine = new MarathonLine(&bottomSegments, maxAllowedDeviation);
#ifdef USE_TOP_REGIONS
    middleLine = new MarathonLine(&middleSegments, maxAllowedDeviation);
    topLine = new MarathonLine(&topSegments, maxAllowedDeviation);
#endif
    leftLine = new MarathonLine(&leftSegments, maxAllowedDeviation, false);
    rightLine = new MarathonLine(&rightSegments, maxAllowedDeviation, false);

    if(!visionTest)
    {
        const char *motionFile = ini->gets("Files","MotionFile").c_str();
        cout << "Loading motion file " << motionFile << endl;
        LinuxDARwIn::Initialize(motionFile, STAND_UP_MOTION);
        //LinuxDARwIn::Initialize();

        // disable modules before adding them
        Walking::GetInstance()->m_Joint.SetEnableBody(false);
        Head::GetInstance()->m_Joint.SetEnableBody(false);
        LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
        RightArm::GetInstance()->m_Joint.SetEnableBody(false);

        // insert the new modules
        MotionManager::GetInstance()->AddModule(Walking::GetInstance());
        MotionManager::GetInstance()->AddModule(Head::GetInstance());
        MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
        MotionManager::GetInstance()->AddModule(RightArm::GetInstance());

        // insert the walking and head modules, zero-out the head position
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        Action::GetInstance()->m_Joint.SetEnableBody(false);

        int headAngle = ini->geti("Vision","HeadAngle",0);
        cout << "Setting head tilt to " << headAngle << " degrees" << endl;
        Head::GetInstance()->MoveByAngle(0,headAngle);  // center the head with a slight downward tilt

        // set the hands to the relaxed position
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();

        ARM_MOTORS.push_back(JointData::ID_R_SHOULDER_PITCH);
        ARM_MOTORS.push_back(JointData::ID_R_SHOULDER_ROLL);
        ARM_MOTORS.push_back(JointData::ID_R_ELBOW);
        ARM_MOTORS.push_back(JointData::ID_R_HAND);
        ARM_MOTORS.push_back(JointData::ID_L_SHOULDER_PITCH);
        ARM_MOTORS.push_back(JointData::ID_L_SHOULDER_ROLL);
        ARM_MOTORS.push_back(JointData::ID_L_ELBOW);
        ARM_MOTORS.push_back(JointData::ID_L_HAND);

        for(int i=0; i<ARM_MOTORS.size(); i++)
        {
            ENABLE_TRUE.push_back(true);
            ENABLE_FALSE.push_back(false);
        }

        // torque off the arms
        MotionManager::GetInstance()->SetTorqueEnable(ARM_MOTORS,ENABLE_FALSE);
    }

    Voice::Initialize();
    Voice::Speak("Initialization Complete");
    cerr << "Finished initializing" << endl;
}

void loadIniSettings()
{
    CvCamera::GetInstance()->LoadINISettings(ini);
    bottomLine->loadIniSettings(*ini,"Vision");
    leftLine->loadIniSettings(*ini,"Vision");
    rightLine->loadIniSettings(*ini,"Vision");
    maxMissedFrames = ini->geti("Vision","MaxMissedFrames",20);

    cout << "Vision Settings:" << endl <<
            "\tLine Colour: " << ini->gets("Vision","LineColour") << endl <<
            "\tMax Missed Frames: " << maxMissedFrames << endl <<
            endl;

    Walking::GetInstance()->LoadINISettings(ini);

    maxSpeedX = ini->getd("Strategy", "MaxStep");
    minSpeedX = ini->getd("Strategy", "MinStep");
    maxTurn = ini->getd("Strategy", "MaxTurn");
    maxSlide = ini->getd("Strategy", "MaxSlide");
    turnP = ini->getd("Strategy", "TurnP", 1.0);
    turnI = ini->getd("Strategy","TurnI", 0.0);
    turnD = ini->getd("Strategy", "TurnD", 0.1);
    slideP = ini->getd("Strategy", "SlideP", 1.0);
    slideI = ini->getd("Strategy", "SlideI",0.0);
    slideD = ini->getd("Strategy", "SlideD", 0.1);
    stepP = ini->getd("Strategy","StepP",1.0);
    stepI = ini->getd("Strategy","StepI",0.0);
    stepD = ini->getd("Strategy","StepD",0.1);
    turnPointGain = ini->getd("Strategy","TurnPointGain",1.0);
    slidePointGain = ini->getd("Strategy","SlidePointGain",1.0);
    turnSlopeGain = ini->getd("Strategy","TurnSlopeGain",1.0);
    slideSlopeGain = ini->getd("Strategy","SlideSlopeGain",1.0);
    stepSizeGain = ini->getd("Strategy","StepSizeGain",1.0);

    // print out config settings so we can make sure they look right
    cout << "Controller Settings:" << endl <<
            "\tStep:  [" << minSpeedX << " , " << maxSpeedX << "] (" << stepP << " , " << stepI << " , " << stepD << ") Gain: " << stepSizeGain << endl <<
            "\tTurn:  [" << -maxTurn << " , " << maxTurn << "] (" << turnP << " , " << turnI << " , " << turnD << ") Point: " << turnPointGain << " Slope: " << turnSlopeGain << endl <<
            "\tSlide: [" << -maxSlide << " , " << maxSlide << "] (" << slideP << " , " << slideI << " , " << slideD << ") Point: " << slidePointGain << " Slope: " << slideSlopeGain << endl <<
            endl;

    turnControl = new DoublePidController(&currentTurn,&goalTurn, turnP, turnI, turnD);
    slideControl = new DoublePidController(&currentSlide, &goalSlide, slideP, slideI, slideD);
    stepControl = new DoublePidController(&currentStep, &goalStep, stepP, stepI, stepD);

    if(recordVideo)
    {
        webcamStream = VideoWriter (ini->gets("Files","WebcamStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        debugStream = VideoWriter (ini->gets("Files","ScanlineStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
    }
}

void* videoLoop(void* arg)
{
    (void)arg;
    for(;;)
    {
        handleVideo();

        // sleep 10 ms
        MotionManager::GetInstance()->msleep(4,pauseThread);
    }

    pthread_exit(NULL);
    return NULL; // Unreachable
}

void pauseThread()
{
    pthread_yield();
}

void handleVideo()
{
    CvCamera::GetInstance()->CaptureFrame();

    cv::cvtColor(CvCamera::GetInstance()->yuvFrame,rgbImage,CV_YCrCb2RGB);
    dbgImage = Mat::zeros(dbgImage.rows,dbgImage.cols,dbgImage.type());   // clear the debug image so that the text overlay is legible

    // calculate 1st x and y derivatives with kernel of 3 (-1 = same channels as src)
    //Sobel(rgbImage, edges, -1, 1, 1, 3);
    //Laplacian(CvCamera::GetInstance()->img, edges, -1);
    //imshow("Edges",edges);

    for(int i=bottomSegments.size()-1; i>=0; i--)
    {
        bottomSegments[i]->FindInFrame(CvCamera::GetInstance()->yuvFrame,&dbgImage);
    }

    for(int i=leftSegments.size()-1; i>=0; i--)
    {
        leftSegments[i]->FindInFrame(CvCamera::GetInstance()->yuvFrame,NULL);
    }

    for(int i=rightSegments.size()-1; i>=0; i--)
    {
        rightSegments[i]->FindInFrame(CvCamera::GetInstance()->yuvFrame,NULL);
    }

    pthread_mutex_unlock(&videoSync);


    if(showVideo || recordVideo)
    {
        bottomLine->draw(rgbImage, dbgImage);
        leftLine->draw(rgbImage, dbgImage);
        rightLine->draw(rgbImage, dbgImage);

        // draw the average point
        cv::Scalar colour=cv::Scalar(255,255,255);
        cv::Point pt = cv::Point((int)avgPt.X, (int)avgPt.Y);
        cv::circle(rgbImage, pt, 7, colour, -1);
        cv::circle(dbgImage, pt, 7, colour, -1);

        char buffer[255];
        sprintf(buffer,"X: %0.2f  Y: %0.2f  A: %0.2f", currentStep, currentSlide, currentTurn);
        pt = cv::Point(0,16);
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(dbgImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

        pt = cv::Point(0,32);
        sprintf(buffer,"Goal Turn: %0.2f (e=%0.2f)", goalTurn, (goalTurn - currentTurn));
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(dbgImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

        pt = cv::Point(0,48);
        sprintf(buffer,"Goal Slide: %0.2f (e=%0.2f)", goalSlide, (goalSlide - currentSlide));
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(dbgImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

        pt = cv::Point(0,64);
        sprintf(buffer,"Goal Step: %0.2f (e=%0.2f)", goalStep, (goalStep - currentStep));
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(dbgImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

        pt = cv::Point(0,Camera::HEIGHT-12);
        sprintf(buffer,"Bat. %0.2fV",MotionStatus::CM730_VOLTAGE);
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(dbgImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

        if(showVideo)
        {
            imshow("Webcam", rgbImage);
            imshow("Scanline", dbgImage);
            cvWaitKey(1);
        }
        if(recordVideo)
        {
            webcamStream << (rgbImage);
            debugStream << dbgImage;
        }
    }
}

void handleFall()
{
    if(MotionStatus::FALLEN != STANDUP && !isStandingUp)
    {
        int direction = MotionStatus::FALLEN;

        isStandingUp = true;
        cout << "Fallen over! Stopping walking module" << endl;
        Voice::Speak("Fallen over");

        MotionManager::GetInstance()->SetTorqueEnable(ARM_MOTORS,ENABLE_TRUE);

        Walking::GetInstance()->Stop();
        Walking::GetInstance()->Finish();

        //MotionManager::GetInstance()->AddModule(Action::GetInstance());
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();

        if(direction == FORWARD)
        {
            cout << "Standing up from front" << endl;
            Action::GetInstance()->Start(F_UP_MOTION);//Action::DEFAULT_MOTION_F_UP);   // FORWARD GETUP
        }
        else if(direction == BACKWARD)
        {
            cout << "Standing up from back" << endl;
            Action::GetInstance()->Start(B_UP_MOTION);//Action::DEFAULT_MOTION_B_UP);   // BACKWARD GETUP
        }

        cout << "Watiting for action to finish" << endl;
        while(Action::GetInstance()->IsRunning())
        {
            usleep(1000);
        }
        cout << "Done" << endl;

        cout << "Re-enabling walking module" << endl;
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();

        Walking::GetInstance()->X_MOVE_AMPLITUDE = minSpeedX;
        Walking::GetInstance()->Start();

        MotionManager::GetInstance()->SetTorqueEnable(ARM_MOTORS,ENABLE_FALSE);

        isStandingUp = false;
    }
}

void followLine()
{
    static bool lineAlreadyLost = false;
    double lineAngle = 0.0;

    bottomLine->update();
    leftLine->update();
    rightLine->update();

    double sumX = 0.0;
    double sumY = 0.0;
    int numPoints = 0;
    double sumSlope = 0.0;

    if(bottomLine->numPoints > 0)
    {
        //bottomErrorPerPoint = bottomLine->slopeError / bottomLine->numPoints;
        sumX += bottomLine->center.X;
        sumY += bottomLine->center.Y;
        sumSlope += bottomLine->slope;
        numPoints++;
    }

    if(rightLine->numPoints > 0)
    {
        //rightErrorPerPoint = rightLine->slopeError / rightLine->numPoints;
        sumX += rightLine->center.X;
        sumY += rightLine->center.Y;
        sumSlope += 1/rightLine->slope;
        numPoints++;
    }

    if(leftLine->numPoints > 0)
    {
        //leftErrorPerPoint = leftLine->slopeError / leftLine->numPoints;
        sumX += leftLine->center.X;
        sumY += leftLine->center.Y;
        sumSlope += 1/leftLine->slope;
        numPoints++;
    }

    if(numPoints==0)
    {
        missedFrames++;

        // we've missed too many frames and lost sight of the line
        if(missedFrames >= maxMissedFrames)
        {
            if(!lineAlreadyLost)
                Voice::Speak("Lost the line");

            lineAlreadyLost = true;

            if(avgPt.X < Camera::WIDTH/2)
            {
                avgPt = Point2D(0,Camera::HEIGHT);
                lineAngle = 90;
            }
            else
            {
                avgPt = Point2D(Camera::WIDTH,Camera::HEIGHT);
                lineAngle = -90;
            }
        }
    }
    else
    {
        lineAlreadyLost = false;

        missedFrames = 0;
        avgPt = Point2D(sumX/numPoints, sumY/numPoints);
        sumSlope = sumSlope/numPoints;
        lineAngle = rad2deg(atan2(sumSlope,1));
    }
    currentPointError = (Camera::WIDTH/2 - avgPt.X)/(Camera::WIDTH/2);

    goalTurn = maxTurn * currentPointError * turnPointGain + maxTurn * lineAngle/90.0 * turnSlopeGain;
    if(goalTurn > maxTurn)
        goalTurn = maxTurn;
    else if(goalTurn < -maxTurn)
        goalTurn = -maxTurn;

    turnControl->Update();
    if(currentTurn > maxTurn)
        currentTurn = maxTurn;
    else if(currentTurn <= -maxTurn)
        currentTurn = -maxTurn;

    // if we're recovering from a lost line DO NOT SLIDE
    if(missedFrames < maxMissedFrames)
        goalSlide = maxSlide * currentPointError * slidePointGain + maxSlide * lineAngle/90.0 * slideSlopeGain;
    else
        goalSlide = 0.0;
    if(goalSlide > maxSlide)
        goalSlide = maxSlide;
    else if(goalSlide < -maxSlide)
        goalSlide = -maxSlide;

    slideControl->Update();
    if(currentSlide < -maxSlide)
        currentSlide = -maxSlide;
    else if(currentSlide > maxSlide)
        currentSlide = maxSlide;

    // adjust the X amplitude based on how far ahead we can see and how fast we're turning
    goalStep = maxSpeedX * (Camera::HEIGHT - avgPt.Y)/Camera::HEIGHT * stepSizeGain;
    if(goalStep < minSpeedX)
        goalStep = minSpeedX;
    else if(goalStep > maxSpeedX)
        goalStep = maxSpeedX;

    stepControl->Update();
    if(currentStep < minSpeedX)
        currentStep = minSpeedX;
    else if(currentStep > maxSpeedX)
        currentStep = maxSpeedX;

    Walking::GetInstance()->A_MOVE_AMPLITUDE = currentTurn;
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = currentSlide;
    Walking::GetInstance()->X_MOVE_AMPLITUDE = currentStep;
}
#endif
