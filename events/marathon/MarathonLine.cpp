#include "MarathonLine.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <darwin/framework/Math.h>
#include <darwin/framework/Voice.h>

using namespace std;
using namespace cv;
using namespace Robot;

MarathonLine::MarathonLine()
{
    lineAlreadyLost = false;
    bottomLine = NULL;
    leftLine = NULL;
    rightLine = NULL;
}

MarathonLine::~MarathonLine()
{
    while(!rightSegments.empty())
    {
        delete rightSegments.back();
        rightSegments.pop_back();
    }

    while(!leftSegments.empty())
    {
        delete leftSegments.back();
        leftSegments.pop_back();
    }

    while(!bottomSegments.empty())
    {
        delete bottomSegments.back();
        bottomSegments.pop_back();
    }

    if(bottomLine!=NULL)
       delete bottomLine;
    if(rightLine!=NULL)
       delete rightLine;
    if(leftLine!=NULL)
        delete leftLine;
}

void MarathonLine::LoadIniSettings(minIni &ini, const char *section)
{
    maxMissedFrames = ini.geti(section,"MaxMissedFrames",20);
    std::string lineColour = ini.gets(section,"LineColour");

    cout << "Vision Settings:" << endl <<
            "\tLine Colour: " << lineColour << endl <<
            "\tMax Missed Frames: " << maxMissedFrames << endl <<
            endl;

    int numVSegments = ini.geti(section,"NumVSegments",11);
    int numHSegments = ini.geti(section,"NumHSegments",21);

    MultiBlob *newTarget;
    int top = 0;
    int bottom;
    int height = Camera::HEIGHT/numVSegments;

    top = Camera::HEIGHT/3;
    bottom = Camera::HEIGHT/3-height;
    for(int i=numVSegments/3; i<numVSegments; i++)
    {
        newTarget = new MultiBlob();
        newTarget->LoadIniSettings(ini, lineColour);

        bottom = Camera::HEIGHT-(height+top)+1;

        newTarget->SetIgnoreTop(top);
        newTarget->SetIgnoreBottom(bottom);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(0,128,255);

        top += height;

        bottomSegments.push_back(newTarget);
    }

    // set up the horizontal segments
    int left = 0;
    int right;
    int width = Camera::WIDTH/numHSegments;

    for(int i=0; i<numHSegments/2; i++)
    {
        newTarget = new MultiBlob();
        newTarget->LoadIniSettings(ini, lineColour);

        right = Camera::WIDTH-(width+left)+1;

        newTarget->SetIgnoreLeft(left);
        newTarget->SetIgnoreRight(right);
        newTarget->SetIgnoreTop(Camera::HEIGHT/6);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(255,0,0);

        left += width;

        leftSegments.push_back(newTarget);
    }

    for(int i=numHSegments/2; i<numHSegments; i++)
    {
        newTarget = new MultiBlob();
        newTarget->LoadIniSettings(ini, lineColour);

        right = Camera::WIDTH-(width+left)+1;

        newTarget->SetIgnoreLeft(left);
        newTarget->SetIgnoreRight(right);
        newTarget->SetIgnoreTop(Camera::HEIGHT/6);
        newTarget->SetSubsample(subsample);
        newTarget->SetMarkColour(0,255,0);

        left += width;

        rightSegments.push_back(newTarget);
    }

    maxAllowedDeviation = ini.getd(section,"MaxError",10.0);
    bottomLine = new LineFinder(&bottomSegments, maxAllowedDeviation,true);
    leftLine = new LineFinder(&leftSegments, maxAllowedDeviation, false);
    rightLine = new LineFinder(&rightSegments, maxAllowedDeviation, false);

    bottomLine->LoadIniSettings(ini,section);
    leftLine->LoadIniSettings(ini,section);
    rightLine->LoadIniSettings(ini,section);
}

void MarathonLine::Print()
{

}

void MarathonLine::Draw(cv::Mat &canvas)
{
	if(bottomLine!=NULL)
		bottomLine->Draw(canvas);
	if(leftLine!=NULL)
		leftLine->Draw(canvas);
	if(rightLine!=NULL)
		rightLine->Draw(canvas);

    // draw the average point
    cv::Scalar colour=cv::Scalar(255,255,255);
    cv::Point pt = cv::Point((int)lineCtr.X, (int)lineCtr.Y);
    cv::circle(canvas, pt, 7, colour, -1);

    // draw a line going through the average point at the calculated angle
    const double lineLength = 50;
    Point p = cv::Point(lineCtr.X - sin(deg2rad(lineAngle)) * lineLength,lineCtr.Y - cos(deg2rad(lineAngle)) * lineLength);
    Point q = cv::Point(lineCtr.X + sin(deg2rad(lineAngle)) * lineLength,lineCtr.Y + cos(deg2rad(lineAngle)) * lineLength);
    cv::line(canvas,p,q,Scalar(255,255,255),2);

    char buffer[255];

    // write the stats of the average point
    sprintf(buffer,"%0.2f (%0.2f %0.2f)",lineAngle,lineCtr.X, lineCtr.Y);
    pt.x += 10;
    cv::putText(canvas, buffer,  pt, FONT_HERSHEY_PLAIN, 0.75, colour, 1);
}

int MarathonLine::FindInFrame(cv::Mat &image, cv::Mat *dest)
{
    for(int i=bottomSegments.size()-1; i>=0; i--)
    {
        bottomSegments[i]->SetSubsample(subsample);
        bottomSegments[i]->FindInFrame(image,dest);
    }

    for(int i=leftSegments.size()-1; i>=0; i--)
    {
        leftSegments[i]->SetSubsample(subsample);
        leftSegments[i]->FindInFrame(image,NULL);
    }

    for(int i=rightSegments.size()-1; i>=0; i--)
    {
        rightSegments[i]->SetSubsample(subsample);
        rightSegments[i]->FindInFrame(image,NULL);
    }
    BuildLine();

    return missedFrames == 0 ? 1:0;
}

void MarathonLine::ProcessCandidateImage(cv::Mat &binaryImages)
{
    (void)binaryImages;
    cerr << "MarathonLine cannot process a binary image" << endl;
    return;
}

void MarathonLine::BuildLine()
{
    lineAngle = 0.0;
    bottomLine->Update();
    // CIB FIRA 2014 -- this line was causing a segfault, so we've disabled it for now leftLine->Update();
    rightLine->Update();

    double sumX = 0.0;
    double sumY = 0.0;
    int numPoints = 0;
    double sumAngle = 0.0;

    double a;

    if(bottomLine->numPoints > 0)
    {
        //bottomErrorPerPoint = bottomLine->slopeError / bottomLine->numPoints;
        sumX += bottomLine->center.X;
        sumY += bottomLine->center.Y;
        a = rad2deg(atan2(bottomLine->slope,1));
        //cout << "bottom line angle: " << a << endl;
        sumAngle += a*bottomLine->numPoints;
        numPoints++;
    }

    if(rightLine->numPoints > 0)
    {
        //rightErrorPerPoint = rightLine->slopeError / rightLine->numPoints;
        sumX += rightLine->center.X;
        sumY += rightLine->center.Y;
        a = rad2deg(atan2(rightLine->slope,1));
        //cout << "right line angle: " << a << endl;
        sumAngle += a*rightLine->numPoints;
        numPoints++;
    }

    if(leftLine->numPoints > 0)
    {
        //leftErrorPerPoint = leftLine->slopeError / leftLine->numPoints;
        sumX += leftLine->center.X;
        sumY += leftLine->center.Y;
        a = rad2deg(atan2(leftLine->slope,1));
        //cout << "left line angle: " << a << endl;
        sumAngle += a*leftLine->numPoints;
        numPoints++;
    }

    if(numPoints==0)
    {
        missedFrames++;

        // we've missed too many frames and lost sight of the line
        if(missedFrames >= maxMissedFrames)
        {
            if(!lineAlreadyLost)
                Voice::Speak("Lost the line");

            lineAlreadyLost = true;

            if(lineCtr.X < Camera::WIDTH/2)
            {
                lineCtr = Point2D(0,Camera::HEIGHT);
                lineAngle = 90;
            }
            else
            {
                lineCtr = Point2D(Camera::WIDTH,Camera::HEIGHT);
                lineAngle = -90;
            }
        }
    }
    else
    {
        lineAlreadyLost = false;

        missedFrames = 0;
        lineCtr = Point2D(sumX/numPoints, sumY/numPoints);
        lineAngle = sumAngle/(bottomLine->numPoints+leftLine->numPoints+rightLine->numPoints);
    }
}
