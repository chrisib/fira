#ifndef MARATHONRUNNER_H
#define MARATHONRUNNER_H

#include <darwin/framework/Application.h>
#include <vector>
#include <darwin/framework/Point.h>
#include <opencv2/core/core.hpp>
#include <darwin/framework/DoublePidController.h>
#include <MarathonLine.h>
#include <MarathonSign.h>

class MarathonRunner : public Robot::Application
{
public:
    static const int STAND_UP_MOTION = 100;
    static const int F_UP_MOTION = 101;
    static const int B_UP_MOTION = 102;

    MarathonRunner();
    virtual ~MarathonRunner();

    virtual void Execute();
    virtual void Process();
    virtual bool Initialize();

    virtual void LoadIniSettings();

protected:
    virtual void HandleVideo();

    void HandleButton();
    int lastButton;

    // check if we've fallen over and stand up if necessary
    void HandleFall();

    // adjust the stride length, turn, and side-step amplitudes based on the line
    void FollowLine();
    void RecoverLostLine(Robot::Point2D &pt, double &angle);
    void FollowSign(Robot::Point2D &pt, double &angle);

    // adjust the robot's gait & stance based on the input voltage & sensor data
    void AdjustGait();

private:
    bool recordVideo;
    bool debug;
    bool enableLog;
    bool indicateSigns;
    bool armTorqueOn;
    bool indicateLED;
    int subsample;

    std::string motionFilePath;
    std::string iniFilePath;

    cv::Mat dbgImage;// = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);
    cv::Mat rgbImage;// = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);

    // movement parameters
    double maxSpeedX;
    double minSpeedX;
    double maxTurn;
    double maxSlide;
    double turnP;
    double turnI;
    double turnD;
    double slideP;
    double slideI;
    double slideD;
    double stepP;
    double stepI;
    double stepD;
    double currentTurn;
    double goalTurn;
    double currentSlide;
    double goalSlide;
    double currentStep;
    double goalStep;
    double currentPointError;
    double slidePointGain;
    double slideSlopeGain;
    double turnPointGain;
    double turnSlopeGain;
    double stepSizeGain;

    // dynamic hip pitch control based on sensor data
    double deltaHip;
    int balanceCount;
    double balanceOffset;
    double voltageOffset;
    int fsrFwdLimit, fsrBackLimit;
    int gyroLimit;
    int balanceLimit;
    double maxVoltage;
    double voltageHipPitchGain;

    Robot::DoublePidController *slideControl;
    Robot::DoublePidController *turnControl;
    Robot::DoublePidController *stepControl;

    std::list<int> ARM_MOTORS;

    // is the get-up motion presently running?
    bool isStandingUp;

    cv::VideoWriter webcamStream;
    cv::VideoWriter debugStream;

    pthread_t videoThreadId;
    volatile bool newFrame; // set to true when handleVideo finishes


    enum{
        STATE_IDLE = 0,
        STATE_WALKING,
        STATE_DEBUG,
        NUM_STATES
    };
    volatile int state;

    MarathonLine line;
    MarathonSign sign;
};

#endif // MARATHONRUNNER_H
