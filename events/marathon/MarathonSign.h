#ifndef MARATHONSIGN_H
#define MARATHONSIGN_H

#include <darwin/framework/SingleBlob.h>
#include <darwin/framework/MultiBlob.h>

class MarathonSign : public Robot::Target
{
public:
    MarathonSign();
    ~MarathonSign();

    enum {
        NOT_FOUND = 0,
        DIR_LEFT,
        DIR_FORWARD,
        DIR_RIGHT,
        NUM_DIRECTIONS
    };

    bool DEBUG_PRINT;

    virtual int FindInFrame(cv::Mat &image, cv::Mat *dest);
    virtual void LoadIniSettings(minIni &ini, const char *section);

    virtual void Print();
    virtual void ProcessCandidateImage(cv::Mat &image);

    virtual void Draw(cv::Mat &canvas);

    virtual void SetLinePosition(Robot::Point2D p,double a){linePosition=p; lineAngle = a;}

    int Direction();

    static Robot::Point2D RotatePoint(Robot::Point2D p, Robot::Point2D center, double radians);

private:
    class SignPosition
    {
    public:
        Robot::Point2D topLeft, topRight, bottomLeft, bottomRight;
        Robot::Point2D center;
        double height,width; // measured along the rotated axis

        void Update(Robot::BoundingBox &bbox, double rotationDegrees);

        void Draw(cv::Mat &canvas, int numSegments);
    };

    Robot::MultiBlob bg;
    Robot::MultiBlob fg;

    int minSize;
    int numSegments;
    double segmentFillThreshold;
    double angleThreshold;

    int signDirection;
    int *prevDirections;
    int directionQueueIn;

    int maxMissedFrames;
    int noSignFrames;

    double lineAngle;
    Robot::Point2D linePosition;

    SignPosition signPosition;

    std::vector<Robot::BoundingBox> candidateBackgrounds, candidateForegrounds;
    std::vector<cv::Scalar> spectrum;

    void FindBoundingBoxes(cv::Mat &binary, std::vector<Robot::BoundingBox> &boxes);

    // create a histogram of the region enclosed by the sign, rotated by X degrees
    // returns something from the enum defined above indicating the direction the sign says we should go
    int ReadSign(Robot::BoundingBox &region, double rotationAngle, cv::Mat binaryImage, cv::Mat *dbg);
    int ReadHistogram(int pixelCounts[], int regionArea);
    int Histogram2Direction(int rowCounts[3], int colCounts[3], bool regions[3][3]);
};

#endif // MARATHONSIGN_H
