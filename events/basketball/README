Basketball Event
Chris I-B <ve4cib@gmail.com>
Simon B-D

----------------------------------------------------------------------------------
Compilation:
Run make command using the included Makefile.  There are some pre-processor
definitions inside the Makefile that can be enabled/disabled to change
the program slightly.  Consult the Makefile for details.

---------------------------------------------------------------------------------
Execution:
To execute the basketball run:
    $ ./basketball [--record] [--show] [--no-mark]

--record will stream the processed camera frames to an avi file defined in
config.ini

--show will display the video stream in an x-window

--no-mark will disable drawing the object bounding boxes on the video

Additionally, --vision-test and --shoot are present to use
to test the program. Both imply --show.

------------------------------------------------------------------------------
How to Configure Basketball:

Calibrate the ball and net colours.
Ball post colour can be added with some tweaking to the code, but it's not tested (or necessary)

Important fields in Strategy:
Arm reach: should be the length of the arm
Lower arm reach: should be length of lower arm
Dump arm reach: Should be length of lower arm + ~30mm (ish)

Need to configure back scoot (BackUpRate) for surface.
Robot should turn in place, drifting back slightly is okay
Need to configure hand offset and correct distance based on lighting (configure with colours)

Configure ball and hoop height. The distances in the config file are in mm's,
and are the values used by the IK for the Z value.

If the grab is consistently not getting the ball, look at Grab.
HeadPan is the angle the ball is at (give or take the TurnLimit) when the robot stops turning.
If the robot is off, tweaking this sometimes helps.
The robot's hand position is also in here if you don't like it.

Dump is the same as grab, finds a head angle, with an error. These two should probably be similar.

-------------------------------------------------------------------------------
Notes:
The program refers to inching towards the ball when grabbing the ball as "scooting".

The steps of the program are:
Walk to ball
Turn
Scoot
Grab ball
Face basket
Walk to basket
Turn
Drop ball
