#ifndef BASKETBALLEVENT_H
#define BASKETBALLEVENT_H

#include <unistd.h>
#include <string.h>
#include <libgen.h>
#include <iostream>

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include "LinuxDARwIn.h"
#include "CvCamera.h"
#include "SingleTarget.h"
#include "minIni.h"
#include "Point.h"
//#include "config.h"

#define INI_FILE_PATH           "config.ini"

#define SAFE_POSITION       9       /* WALK_READY */
#define SCOOP_READY_MOTION  94
#define SCOOP_START_MOTION  95
#define THROW_READY_MOTION  101
#define THROW_START_MOTION  102
#define TURN_LEFT_MOTION    110
#define TURN_RIGHT_MOTION   120
#define DUMP_READY_MOTION   130
#define DUMP_START_MOTION   131

class BasketballEvent
{
    public:
        // required for initialization
        Robot::CM730 *cm730;
        //Robot::LinuxCM730 *linux_cm730;

        ~BasketballEvent();

		static BasketballEvent* GetInstance();
		void Initialize(bool showCamera, bool recordVideo, bool markVideo);

        // put Darwin in a safe, standing position
        void enterSafePosition();

        // run the basketball event
        void execute();

        // Tests
        void visionTest();		// Simply handles the video in an infinite loop
		void ShootingDrill();	// Test shooting ability
		void turningTest();		// Test turning on the current surface
		void grabTest();		// Test grabbing

        void reset();
        
        // flags indicating whether we stream/record/mark the video frames
        bool recordVideo;
        bool showCamera;
        bool markVideo;
        bool cameraInitialized;
        
        // draw targets' bounding boxes on the current video frame
        void markTargetBoxes();
        
        // show the X11 window with the current video frame
        void showWebcamView();

    private:
		// create a new BasketballEvent
        // showCamera: show an X11 window with the webcam view
        // recordVideo: write each camera frame to an AVI file for diagnosis later
        // markVideo: draw boxes around target objects on each frame (useful for debugging)
		BasketballEvent();
		static BasketballEvent* m_UniqueInstance;
    
        // the various targets we are concerned with in Basketball
        Robot::SingleTarget *ballTee;
        Robot::SingleTarget *ball;
        Robot::SingleTarget *backboard;
        Robot::SingleTarget *hand;
        Robot::SingleTarget *hoop;

        minIni *ini;

        bool disableWalking;
        bool disableThrowing;
        bool isRunning;
        
        // Keeps track if we're close to the target already
        bool targetIsClose;
        
        // The last known location of the target
        // X is pan, Y is tilt
        Robot::Point2D lastTargetLocation;
        
        // Current direction
        bool goingForwards;

        int fwdStepSize;
        int leftTurnSize;
        int stepHeight;
        int leftSlideSize;
        int sidestepTime;
        int aOffset;
        int xOffset;
        int yOffset;
        int headTilt;
        
        int armsReach;
		int dumpArmsReach;
		int lowerArmReach;
		int reachOffset;
		int shuffleOffset;
		int ballHeight;
		int hoopHeight;
		int driftOffset;
		int visionChecks;
		int maxTurn;
		int maxMissingFrames;
		int backUpRate;
		int scootSpeed;
        
        // Grab variables
        int grabOpenAngle;
        int grabCloseAngle;
        int grabCloseSpeed;
        int grabTurnSpeed;
        int grabPan;
        int grabTilt;
		int grabTurnLimit;
		int grabTurnTime;
		int grabXHandOffset;
		int grabYHandOffset;
		int grabShuffleSpeed;
		Robot::Point3D grabUp;
		
		// Dump variables
		int dumpTargetAngle;
		int dumpTargetAngleErr;
		
		bool inLeftHand;

        // output stream to record the robot's POV
        cv::VideoWriter yuvStream;
        cv::VideoWriter scanlineStream;
        
        // Video thread variables
        pthread_t videoThreadId;
		pthread_mutex_t videoLock;
        
        //Point2D lastBallPosition;

        // initialize all the private members -- called from constructor
        void initialize();
        void initVideo();
        void loadIniSettings();

        // walk to a specific target by using the Walking module
		void walkTo(Robot::SingleTarget &target);
		//void walkTo2(Robot::Target& target, int height);
		void walkToTarget(Robot::SingleTarget &target, int height);
		void scanForTarget(Robot::SingleTarget &target);
		void lookAround(Robot::SingleTarget& target);
		int  getTurnTime(int turnSpeed, int targetAngle);

		// handle updating the X11 window/video file (if necessary)
		void handleVideo();

		// perform a pre-programmed motion created with the motion manager
		void executeAction(int actionId);
		void executeAction(int actionId, bool reenableWalking);

        // alternate versions of the final step of the event
        // putting the ball in the basket
        void throwBall();
        void dumpBall();
    
        // Grab a given target
        void grabTarget(Robot::SingleTarget& target);
        
        // Turn to face a target
        void faceTarget(Robot::SingleTarget& target); 

		static void pauseThread();
#ifndef NO_VIDEO_THREAD
        static void* videoLoop(void* arg);
#endif
};

#endif // BASKETBALLEVENT_H
