// FIXME: Change "Offset" into "Err" where appropriate
#include "basketballevent.h"
#include "Voice.h"
#include <pthread.h>
#include <vector>
#include "LeftArm.h"
#include "RightArm.h"
#include "Kinematics.h"

#define WEBCAM_VIEW_WINDOW_NAME "Webcam"

#ifdef NO_VIDEO_THREAD
	void (*syncVideoPtr)(void) = handleVideo;
	#define syncVideo() handleVideo();
#else
	void (*syncVideoPtr)(void) = NULL;
	#define syncVideo() ((void) 0)
#endif

using namespace std;
using namespace Robot;
using namespace cv;

BasketballEvent* BasketballEvent::m_UniqueInstance = new BasketballEvent(); 

Mat dbg = Mat::zeros(Camera::HEIGHT, Camera::WIDTH, CvCamera::GetInstance()->yuvFrame.type());
//Mat toProcess = Mat::zeros(Camera::HEIGHT, Camera::WIDTH, CvCamera::GetInstance()->yuvFrame.type());

int missingFrames = 0;
bool grabbedBall = false;
volatile bool frameUsed = false;

vector<Target*> targets;

// Returns the distance to a give target, given the head angles and target height
int FindDistance(SingleTarget& target, double panAngle, double tiltAngle, double height)
{
	BoundingBox* box = target.GetBoundingBox();
	
	double angleOffset = (Camera::VIEW_V_ANGLE / (double) Camera::HEIGHT) * box->height/2;
	
	double angle = tiltAngle + angleOffset + Kinematics::EYE_TILT_OFFSET_ANGLE;
	
	/*cout << "DEBUG FD: " << angleOffset << " = ";
	cout << (Camera::VIEW_V_ANGLE / (double) Camera::HEIGHT) << " * ";
	cout << (box->height/2) << endl;
	cout << angle << " = " << tiltAngle << " + " << angleOffset << " + ";
	cout << Kinematics::EYE_TILT_OFFSET_ANGLE << endl;*/
	
	angle = deg2rad(angle);
	
	return (int) (Kinematics::HEAD_TO_SHOULDER - height) * tan(angle);
}

// Initialize the event
void BasketballEvent::Initialize(bool showCamera, bool recordVideo, bool markVideo)
{
    this->showCamera = showCamera;
    this->recordVideo = recordVideo;
    this->markVideo = markVideo;
    this->cameraInitialized = false;

    ball = NULL;
    ballTee = NULL;
    hand = NULL;
    hoop = NULL;
    backboard = NULL;
    ini = NULL;

    initialize();
    
    cm730 = &LinuxDARwIn::cm730;
}

BasketballEvent* BasketballEvent::GetInstance()
{
	return m_UniqueInstance;
}

BasketballEvent::BasketballEvent()
{
}

BasketballEvent::~BasketballEvent()
{
	throw "Don't do this right now...";
	exit(EXIT_FAILURE); // REALLY don't do this right now...
	
    cerr << "[debug] cleaning up..." << endl;

	// Should we be doing this? We don't construct it...
    // cerr << "[debug] delete cm730" << endl;
    // delete cm730;

	// We don't deven initialize this
    //cerr << "[debug] delete linux_cm730" << endl;
    //delete linux_cm730;

    cerr << "[debug] delete ballTee" << endl;
    delete ballTee;

    cerr << "[debug] delete ball" << endl;
    delete ball;

    cerr << "[debug] delete backboard" << endl;
    delete backboard;
    
    cerr << "[debug] delete hand" << endl;
    delete hand;

    cerr << "[debug] delete hoop" << endl;
    delete hoop;

    cerr << "[debug] delete ini" << endl;
    delete ini;
}

// Run the event
void BasketballEvent::execute()
{
    bool targetAcquired = false;

    initVideo();
    
    cout << "+++ Enabling motion +++" << endl;
    MotionManager::GetInstance()->SetEnable(true);
    cout << "\tdone" << endl;
    
    cout << "Ready when you are!" << endl;
    MotionManager::WaitButton(MotionStatus::MIDDLE_BUTTON);

    // find the ball on the tee
    cout << "+++ Looking for the ball +++" << endl;
#ifndef NO_VOICE
    Voice::Speak("Looking for the ball");
#endif

	// Back up a bit to start
	//double tempSpeed = Walking::GetInstance()->X_MOVE_AMPLITUDE;
	//Walking::GetInstance()->X_MOVE_AMPLITUDE = -10;
	//Walking::GetInstance()->Start(4);
	//Walking::GetInstance()->Finish();
	//Walking::GetInstance()->X_MOVE_AMPLITUDE = tempSpeed;

	scanForTarget(*ball);

	// walk to the ball and pick it up
    while(!targetAcquired)
    {
		syncVideo();
		
		// Double check the distance a few times, smallest distance is correct
		double distance = ball->GetDistance();
		for(int i = 0; i < visionChecks; i++)
		{
			pauseThread();
			double tempDistance = ball->GetDistance();
			if(tempDistance < distance) distance = tempDistance;
			frameUsed = true;
		}
		
		cout << "Distance: " << armsReach-reachOffset << " < " << distance << " < " << armsReach+reachOffset << endl;
        if(distance < armsReach-reachOffset || distance > armsReach+reachOffset)
        {
            // walk to the ball if we found it; otherwise try walking to the tee
            //if(ball->WasFound())
                //walkToTarget(*ball);
                walkTo(*ball);
            //else
            //    walkToTarget(*ballTee);
        }
        else
        {
			targetAcquired = true;
        }
    }
    cout << "\tIn position to grab ball" << endl << endl;
    
    // Make sure we stopped walking
    Walking::GetInstance()->Stop();
    Walking::GetInstance()->Finish(syncVideoPtr);

    // grab the ball off the tee
    cout << "+++ Grabbing ball +++" << endl;
#ifndef NO_VOICE
    Voice::Speak("Grabbing the ball");
#endif

    grabTarget(*ball);
    grabbedBall = true;
    
    // Looking for a new target now
    targetIsClose = false;
    cout << "\tdone" << endl;

    Head::GetInstance()->MoveByAngle(0, headTilt);

    cout << "+++ Maneuvering around the tee +++" << endl;
	
	// Walk away from tee a bit
	Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
    Walking::GetInstance()->X_MOVE_AMPLITUDE = 10;
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
	Walking::GetInstance()->Start(6);
	Walking::GetInstance()->Finish(syncVideoPtr);

    // Find the basket
    cout << "+++ Finding basket +++" << endl;
#ifndef NO_VOICE
    Voice::Speak("Looking for the basket");
#endif
	faceTarget(*hoop);
	cout << "Facing basket!" << endl;

    if(this->disableThrowing)
        dumpBall();
    else
        throwBall();

    // Wait 15 seconds before quitting
    // 10 before stopping the video (to record right to the end)
    // and 5 after stopping (to allow for clean-up to occur)
    MotionManager::msleep(10e3, syncVideoPtr);

    isRunning = false;      // this will stop the pthread
    pthread_join(videoThreadId, NULL);
    
    MotionManager::msleep(5e3, syncVideoPtr);
}

// Given the turn speed and the angle we want to go, returns how long the turn should go
int BasketballEvent::getTurnTime(int turnSpeed, int targetAngle)
{
	const int factor = 7;
	//cout << "[DEBUG]: " << turnSpeed << " " << targetAngle << endl;
	return fabs(turnSpeed * targetAngle * factor);
} 

// Grab the given target (the ball probably)
void BasketballEvent::grabTarget(SingleTarget& target)
{
		cerr << "Grabbing target..." << endl;
		
		Arm* arm;
		double panAngle, tiltAngle;
		double headPan = this->grabPan;
		Point3D up = this->grabUp;
		int openAngle = this->grabOpenAngle;
		int closeAngle = this->grabCloseAngle;
		int turnTime = this->grabTurnTime;
		
		const int lrTurn = this->grabTurnSpeed;
		const double turnOffset = this->grabTurnLimit;
		const double xHandOffset = this->grabXHandOffset;
		const double yHandOffset = this->grabYHandOffset;
		const int closeSpeed = this->grabCloseSpeed;
		
		// make sure we're not walking
		if(Walking::GetInstance()->IsRunning()) {
			Walking::GetInstance()->Stop();
			Walking::GetInstance()->Finish();
		}
		
		pauseThread();
		if (target.WasFound()) {
			Head::GetInstance()->LookAt(target);
			MotionManager::Sync();
			lastTargetLocation.X = Head::GetInstance()->GetPanAngle();
			lastTargetLocation.Y = Head::GetInstance()->GetTiltAngle();
		} else {
			Head::GetInstance()->MoveByAngle(lastTargetLocation.X, lastTargetLocation.Y);
			MotionManager::Sync();
		}

		panAngle = Head::GetInstance()->GetPanAngle();
		
		// Choose the correct arm
		bool isLeft = panAngle >= 0;
		if(isLeft) {
			arm = LeftArm::GetInstance();
		} else {
			arm = RightArm::GetInstance();
		}
		this->inLeftHand = isLeft;
		
		Walking::GetInstance()->HIP_PITCH_OFFSET = 9;
		arm->SetGoalPosition(up, 0);
		arm->Finish();

		bool turning = true;

		Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
		Walking::GetInstance()->Z_MOVE_AMPLITUDE = stepHeight;
		MotionManager::Sync(); // update
		
		// Initial turn
		double angle = (headPan-fabs(panAngle)-8);
		panAngle = Head::GetInstance()->GetPanAngle();
		tiltAngle = Head::GetInstance()->GetTiltAngle();
		turnTime = getTurnTime(lrTurn, angle);
		cout << "Initial turn: " << turnTime << " " << lrTurn << " " << angle << endl; 
		const int direction = (isLeft)? 1 : -1;
		Head::GetInstance()->MoveByAngle(direction * (headPan-8), tiltAngle);
		
		if (fabs(panAngle) < headPan) {
			Walking::GetInstance()->A_MOVE_AMPLITUDE = direction * lrTurn;
		} else {
			Walking::GetInstance()->A_MOVE_AMPLITUDE = direction * -lrTurn;
		}
		Walking::GetInstance()->X_MOVE_AMPLITUDE = this->backUpRate;
		
		//if(isLeft) {
		//	Walking::GetInstance()->Y_MOVE_AMPLITUDE = driftOffset;
		//}
		
		Walking::GetInstance()->Start();
		MotionManager::msleep(turnTime, syncVideoPtr);
		Walking::GetInstance()->Stop();
		Walking::GetInstance()->Finish();
			
		MotionManager::msleep(500, syncVideoPtr);
		
		cout << "Adjusting turns" << endl;
		for(int i = 0; i < visionChecks; i++) {
			pauseThread();
			Head::GetInstance()->LookAt(target);
			frameUsed = true;
		}
		
		while(turning) {
			pauseThread();
			
			if (!target.WasFound()) {
				cout << "Moving to last known location, pan: " << lastTargetLocation.X << " tilt: " << lastTargetLocation.Y << endl;
				Head::GetInstance()->MoveByAngle(lastTargetLocation.X, lastTargetLocation.Y);
				MotionManager::Sync();
			}
			
			BoundingBox* box = target.GetBoundingBox();
			const int sideLimit = 50;
			cout << "Ball at " << box->center.X << endl;
			if(box != NULL && (box->center.X < sideLimit || box->center.X > Camera::WIDTH-sideLimit)) {
				cout << "Ball drifting; adjusting head" << endl;
				if(box->center.X < sideLimit) {
					Head::GetInstance()->MoveByAngleOffset(-20, 0);
				} else {
					Head::GetInstance()->MoveByAngleOffset(20, 0);
				}
				Head::GetInstance()->Finish();
				pauseThread();
			}

			panAngle = Head::GetInstance()->GetPanAngle();
			tiltAngle = Head::GetInstance()->GetTiltAngle();

			turning = fabs(panAngle) <= headPan-turnOffset ||
						fabs(panAngle) >= headPan+turnOffset;
						
			cout << "Head angle: " << panAngle << endl;
						
			Point3D* point = target.GetPosition3D(panAngle, tiltAngle); 
			if(point != NULL) {
				if(turning) {
					// set the walking parameters
					//direction = (isLeft)? 1 : -1;
					if (fabs(panAngle) < headPan) {
						Walking::GetInstance()->A_MOVE_AMPLITUDE = direction * lrTurn;
					} else {
						Walking::GetInstance()->A_MOVE_AMPLITUDE = direction * -lrTurn;
					}
					Walking::GetInstance()->X_MOVE_AMPLITUDE = backUpRate;
					
					double angle = (headPan-fabs(panAngle));
					cout << "Head Angles - target: " << headPan;
					cout << " current: " << panAngle << " diff: " << angle << endl;
					if(angle > maxTurn) angle = maxTurn; // Don't lose sight of ball by turning too far
					turnTime = getTurnTime(lrTurn, angle);
					turning = ((int) turnTime) != 0;
					
					cout << "Turning for " << turnTime << " by " << angle;
					cout << " degrees to the " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl;

					//int headDirection = (Walking::GetInstance()->A_MOVE_AMPLITUDE < 0)? 1 : -1;
					cout << "Moving head by " << angle * direction;
					cout << " to " << panAngle + angle * direction << endl;
					Head::GetInstance()->MoveByAngleOffset(angle * direction, 0);
					Walking::GetInstance()->Start();
					MotionManager::msleep(turnTime, syncVideoPtr);
					Walking::GetInstance()->Stop();
					Walking::GetInstance()->Finish();
				}
				frameUsed = true;
				const int sideLimit = 80;
				BoundingBox* box = target.GetBoundingBox();
				if(box != NULL && (box->center.X < sideLimit || box->center.X > Camera::WIDTH-sideLimit)) {
					pauseThread();
					cout << "Getting the ball in view" << endl;
					if(box->center.X < sideLimit) {
						Head::GetInstance()->MoveByAngleOffset(15, 0);
					} else {
						Head::GetInstance()->MoveByAngleOffset(-15, 0);
					}
					Head::GetInstance()->Finish(pauseThread);
					MotionManager::msleep(1500, pauseThread); // Give the video a change to catch up
					turning = true;
				}
				
				if (!turning) {
					cout << "Done adjusting turns" << endl;
				}
				delete point;
			} else {
				cout << "Uh oh..." << endl;
				lookAround(target);
			}
			MotionManager::msleep(2000, syncVideoPtr);
			frameUsed = true;
		}

		cout << "Opening hand to " << openAngle << " degrees" << endl;
		arm->MoveHand(openAngle, 1);
		while(arm->IsRunning()) pauseThread();
		MotionManager::msleep(3000);
		
		Point3D* grab = NULL;
		BoundingBox* box = NULL;
		while(!grab) {
			pauseThread();
			MotionManager::msleep(2000);
			panAngle = Head::GetInstance()->GetPanAngle();
			tiltAngle = Head::GetInstance()->GetTiltAngle();
			box = target.GetBoundingBox();
			grab = target.GetPosition3D(panAngle, tiltAngle);
			frameUsed = true;
			
			cout << "Distance: ";
			if(grab) {
				cout << grab->Magnitude() << endl;
			} else {
				cout << "Unknown..." << endl;
			}
			
			// TODO: Backup a bit if too close
			const int sideLimit = 80;
			if(box != NULL && (box->center.Y < sideLimit || box->center.Y > Camera::HEIGHT-sideLimit)) {
				cout << "Getting the ball in view" << endl;
				if(box->center.Y < sideLimit) {
					Head::GetInstance()->MoveByAngleOffset(0, 12);
				} else {
					Head::GetInstance()->MoveByAngleOffset(0, -12);
				}
				delete grab;
				grab = NULL;
				Head::GetInstance()->Finish(pauseThread);
				MotionManager::msleep(2000, pauseThread); // Give the video a change to catch up
			} else if (grab != NULL && (grab->Z < -120 || grab->Z > -70)) {
				static int k = 4;
				k--;
				cout << "Distance is wrong" << endl;
				if(k>0)
				{
					delete grab;
					grab = NULL;
				}
				else
				{
					cout << "Forcing a grab anyway" << endl;
				}
				
			} else if (grab != NULL && grab->X > lowerArmReach) {
				cout << "Scooting forwards: " << grab->X;
				cout << " > " << lowerArmReach << endl;
				delete grab;
				grab = NULL;

				Walking::GetInstance()->X_MOVE_AMPLITUDE = this->scootSpeed;
				Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
				Walking::GetInstance()->Start(2);

			} else if (grab) {
				cout << "Done scooting: " << grab->X << " " << grab->Y << " " << grab->Z << endl;
			} else {
				cout << "Lost the ball..." << endl;
				lookAround(target);
			}
			
			MotionManager::msleep(2000);
		}
		
		// If this this next line isn't enough to clear the post, pop it up by five
		//grab->Z += Kinematics::EYE_TO_SHOULDER;
		grab->Y = abs(grab->Y) - Kinematics::HEAD_TO_SHOULDER + yHandOffset;
		//if (isLeft) grab->Y += 20; // FIXME: Correct the left hand being wrong all the time
		grab->Z = ballHeight;
		
		// Add some offsets so the ball is in the middle of the hand, not the tip
		grab->X += xHandOffset;
		
		cout << "Moving hand to x: " << grab->X << ", y: ";
		cout << grab->Y << ", z: " << grab->Z << endl;
		
		arm->SetGoalPosition(*grab, 0);
		
		MotionManager::msleep(3000);
		arm->SetEnableHand(true, true);
		//arm->RelaxHand();
		arm->MoveHand(closeAngle, closeSpeed);
		while(arm->IsRunning()) MotionManager::Sync();
		
		delete grab;
		cerr << "Grabbed target" << endl;
}

// Turn to face the give target
void BasketballEvent::faceTarget(SingleTarget& target)
{
	int speed = fwdStepSize;
	Walking::GetInstance()->Stop();
	Walking::GetInstance()->Finish();
	
	// Sync video
	pauseThread();
	
	while(!target.WasFound()) {
		scanForTarget(target);
	}
	
	MotionManager::Sync();
	Head::GetInstance()->LookAt(target);
	double panAngle = Head::GetInstance()->GetPanAngle();
	double tiltAngle = Head::GetInstance()->GetTiltAngle();
	Walking::GetInstance()->Z_MOVE_AMPLITUDE = stepHeight;
	
	bool bodyFacing = -7 < panAngle && panAngle < 7;
	bool headFacing = abs(Camera::WIDTH/2 - target.GetBoundingBox()->center.X) < 30;
	
	while( !(bodyFacing && headFacing) ) {
		pauseThread();
		if(target.WasFound()) {
			int direction;
			if(!bodyFacing) {
				direction = (panAngle >= 0)? 1 : -1;
			} else {
				direction = (Camera::WIDTH/2 - target.GetBoundingBox()->center.X > 0)? 1 : -1;
			}
			double angle;
			if(!bodyFacing) {
				cout << "By head" << endl;
				angle = panAngle;
			} else {
				cout << "By body" << endl;
				angle = target.GetBoundingBox()->center.X / Camera::WIDTH * 60; // FIXME: 60 => Camera angle. Look up constant
			}
			int turnTime = getTurnTime(speed, angle);
			cout << "Turn for " << turnTime/2 << " at " << speed << " to go " << angle << " degrees" << endl;
			if (turnTime < 200) turnTime = 200; 
			
			Walking::GetInstance()->A_MOVE_AMPLITUDE = direction * speed;
			Walking::GetInstance()->X_MOVE_AMPLITUDE = backUpRate-4;
			
			Head::GetInstance()->MoveByAngle(0, tiltAngle);
			Walking::GetInstance()->Start();
			MotionManager::msleep(turnTime/2, syncVideoPtr);
			Walking::GetInstance()->Stop();
			Walking::GetInstance()->Finish();
		} else {
			scanForTarget(target);
		}
		
		MotionManager::msleep(300, syncVideoPtr);
		Head::GetInstance()->LookAt(target);
		panAngle = Head::GetInstance()->GetPanAngle();
		tiltAngle = Head::GetInstance()->GetTiltAngle();
		bodyFacing = -7 < panAngle && panAngle < 7;
		headFacing = abs(Camera::WIDTH/2 - target.GetBoundingBox()->center.X) < 30;
		frameUsed = true;
	}
}

// Walk to the target by finding the target,
// figuring out how many steps it takes to get to it, and doing it.
void BasketballEvent::walkTo(SingleTarget& target)
{
	int speed = fwdStepSize;
	Walking::GetInstance()->Stop();
	Walking::GetInstance()->Finish();
	
	pauseThread();
	if(!target.WasFound() || target.GetBoundingBox()->height==0 || target.GetBoundingBox()->width==0)
    {
		missingFrames++;
		
		if(missingFrames > maxMissingFrames) {
			scanForTarget(target);
		}
    }
    else
    {
		missingFrames = 0;
		int distance = target.GetDistance();
		for(int i = 0; i < visionChecks; i++)
		{
			pauseThread();
			int tempDistance = target.GetDistance();
			if(tempDistance < distance) distance = tempDistance;
			frameUsed = true;
		}
		
		cout << "Distance: " << distance << endl;
		
		int turnAngle = Head::GetInstance()->GetPanAngle() / 2;
		int tiltAngle = Head::GetInstance()->GetTiltAngle();
		int targetDistance = (distance-armsReach);
		int numSteps = Walking::GetSteps(targetDistance, speed);
		
		cout << "DEBUG: " << numSteps << " " << targetDistance << " " << speed << endl;
		
		if(numSteps < 0) {
			numSteps = 0;
		} else if(numSteps <= 3) {
			targetIsClose = true;
		} else if(targetIsClose && numSteps > 3) {
			numSteps = 3;
		}
		
		if(distance < armsReach || tiltAngle < -3) {
			speed *= -1;
			//speed -= 10;
			numSteps = 4;
		} else if(numSteps == 1) {
			speed /= 2;
			numSteps = 2;
			turnAngle = 0;
		}
		
		Walking::GetInstance()->X_MOVE_AMPLITUDE = speed;
		Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
		Walking::GetInstance()->Z_MOVE_AMPLITUDE = stepHeight;
		Walking::GetInstance()->A_MOVE_AMPLITUDE = turnAngle;
		
		if(speed < 0) Walking::GetInstance()->Z_MOVE_AMPLITUDE += -backUpRate;
	
		if(numSteps == 1) { cerr << "Assertion failed: Can't take one step" << endl; exit(EXIT_FAILURE); }
		
		if(speed >= 0 && numSteps > 0) {
			cout << "Taking " << numSteps << " steps with a speed of " << speed << endl;
			Walking::GetInstance()->Start(numSteps);
		} else {
			//Walking::GetInstance()->X_MOVE_AMPLITUDE = -speed-10;
			//Walking::GetInstance()->Z_MOVE_AMPLITUDE += 10; // Fight the traction!
			cout << "Taking 3 steps with a speed of " << speed << " (backwards)" << endl;
			Walking::GetInstance()->Start(3);
			Walking::GetInstance()->Finish();
		}
		
		//cout << "[DEBUG] Speed: " << Walking::GetInstance()->X_MOVE_AMPLITUDE << endl;
		
		Walking::GetInstance()->Finish();
		
		MotionManager::Sync();
		if(target.WasFound())
		{
			Head::GetInstance()->LookAt(target);
			MotionManager::msleep(2000);
		}
	}
}

// pivot on the spot and execute the throwing motion once we're facing the basket
void BasketballEvent::throwBall()
{
	cerr << "Throwing the ball is not yet implemented" << endl;
	//faceTarget(*hoop);
	
	// throw ball
}

// walk to the basket and drop the ball into it
void BasketballEvent::dumpBall()
{    
    Arm* arm;
    int direction, openAngle;
    if (inLeftHand) {
		arm = LeftArm::GetInstance();
		direction = 1;
		openAngle = 90;
	} else {
		arm = RightArm::GetInstance();
		direction = -1;
		openAngle = -90;
	}
    
    double angle;
	Point3D up(-5, 85, 130);
	Point3D out(120, 85, 50);
	
	const int turnTime = this->grabTurnTime;	
	const int lrTurn = this->grabTurnSpeed;
	
	arm->RelaxHand();
	arm->SetGoalPosition(up, 0);
	
	const int tries = 20;
	int distance = 0;
	int speed = fwdStepSize;
	int i = 0;
	vector<int> results;
	for(int i = 0; i < tries; i++) {
		pauseThread();
		int temp = hoop->GetDistance();
		frameUsed = true;
		results.push_back(temp);
	}
	sort(results.begin(), results.end());
	
	vector<int>::iterator iter;
	for(iter = results.begin() + tries/4; iter != results.end()-tries/4; iter++) {
		distance += *iter;
		i++;
	}
	
	distance /= i;
	distance -= dumpArmsReach;
	
	int turnAngle = 0; // Head::GetInstance()->GetPanAngle() / 2;
	int numSteps = Walking::GetSteps(distance, speed);
	
	cout << "Taking " << numSteps << " steps to go " << distance << " with speed " << speed << endl;
	
	Walking::GetInstance()->X_MOVE_AMPLITUDE = speed;
	Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
	Walking::GetInstance()->Z_MOVE_AMPLITUDE = stepHeight;
	Walking::GetInstance()->A_MOVE_AMPLITUDE = turnAngle;
	
	Walking::GetInstance()->Start(numSteps);
	while(Walking::GetInstance()->IsRunning()) {
		pauseThread();
		MotionManager::msleep(200);
		Head::GetInstance()->LookAt(*hoop);
		frameUsed = true;
		angle = Head::GetInstance()->GetTiltAngle();
		if(angle < 3) {
			cout << "Stopping early: " << angle << endl;
			Walking::GetInstance()->X_MOVE_AMPLITUDE = speed/2;
			Walking::GetInstance()->Stop();
			Walking::GetInstance()->Finish();
		}
	}
	cout << "Done walking to basket" << endl;
	
	MotionManager::Sync();
	angle = Head::GetInstance()->GetTiltAngle();
	if (angle < 0) {
		Walking::GetInstance()->X_MOVE_AMPLITUDE = -speed;
		Walking::GetInstance()->Start(2+log2(-angle));
		Walking::GetInstance()->Finish();
	} else if (angle > 5) {
		Walking::GetInstance()->X_MOVE_AMPLITUDE = speed;
		Walking::GetInstance()->Start(2);
		Walking::GetInstance()->Finish();
	}
	
	pauseThread();
	Head::GetInstance()->LookAt(*hoop);
	frameUsed = true;
	
	int targetAngle = direction * dumpTargetAngle;
	int targetAngleErr = dumpTargetAngleErr;
	
	angle = Head::GetInstance()->GetPanAngle();
	cout << "Start turning at " << angle << endl;
	while (angle < targetAngle - targetAngleErr || angle > targetAngle + targetAngleErr) {
		cout << "Angle is " << angle << endl;
		int direction = (angle < targetAngle)? 1 : -1;
		
		Walking::GetInstance()->X_MOVE_AMPLITUDE = -14;
		Walking::GetInstance()->A_MOVE_AMPLITUDE = direction * lrTurn;

		Walking::GetInstance()->Start();
		MotionManager::msleep(turnTime/2, syncVideoPtr);
		Walking::GetInstance()->Stop();
		Walking::GetInstance()->Finish();
			
		pauseThread();
		MotionManager::msleep(500);
		Head::GetInstance()->LookAt2(*hoop);
		Head::GetInstance()->Finish();
		
		if(!hoop->WasFound()) lookAround(*hoop);
			
		angle = Head::GetInstance()->GetPanAngle();
	}
	cout << "Turning complete!" << endl;
	cout << "Dropping ball at angle: " << angle << endl;
	
	Point3D drop(100, 85, 70);
	arm->SetGoalPosition(drop, 0);
	arm->Finish();
	
	arm->SetEnableHand(true, true);
	arm->MoveHand(1, openAngle);
	arm->Finish();
		
	MotionManager::msleep(3000, syncVideoPtr);
	cout << "Done dumping the ball" << endl;
}

void BasketballEvent::walkToTarget(SingleTarget& target, int height)
{
	enum State {
		Stop,
		Forward,
		Close,
		Backwards,
	};
	
	static State current = Stop;

    int lrAmplitude = 0;
    int fwAmplitude = 0;
    int lrSlide = 0;
    
    static int runs = 0;
    if (runs == 2) {
		Head::GetInstance()->LookAt(target);
		pauseThread();
		runs = 0;
	} else {
		runs++;
	}
    
    if(!target.WasFound() || target.GetBoundingBox()->height==0 || target.GetBoundingBox()->width==0)
    {
		missingFrames++;
		
		if(missingFrames > 10)
			scanForTarget(target);
    }
    else
    {
		// TODO: re-add relevant debug info
		bool isCentreY = false;
		bool isCentreX = false;
		
		double panAngle = Head::GetInstance()->GetPanAngle();
		double tiltAngle = Head::GetInstance()->GetTiltAngle();

		double distance = FindDistance(target, panAngle, tiltAngle, height);
		for(int i = 0; i < visionChecks; i++) {
			MotionManager::Sync();
			int tempDistance = FindDistance(target, panAngle, tiltAngle, height);
			if(tempDistance < distance) distance = tempDistance;
		}
		double headAngle;
		missingFrames = 0;
		
		switch (current) {
		case Stop:
			cout << "State: Stop" << endl;
		
			Walking::GetInstance()->Stop();
			Walking::GetInstance()->Finish();
			
			pauseThread();

			distance = FindDistance(target, panAngle, tiltAngle, height);
			
			for(int i = 0; i < visionChecks; i++) {
				MotionManager::Sync();
				int tempDistance = FindDistance(target, panAngle, tiltAngle, height);
				if(tempDistance < distance) distance = tempDistance;
			}
			
			if(armsReach-reachOffset <= distance && distance <= armsReach+reachOffset) {
				cout << "\tTarget is centred F/B: " << distance << endl;
				isCentreY = true;
			} else if (distance < armsReach || tiltAngle < -4) {
				cout << "\tTarget is too close: " << distance << endl;
				current = Backwards;
			} else {
				cout << "\tTarget is too far: " << distance << endl;
				if (tiltAngle < -2) {
					cout << "\tNot actually..." << endl;
					isCentreY = true;
				} else if(distance < armsReach * 2 || targetIsClose) {
					current = Close;
				} else {
					current = Forward;
				}
			}
			break;
		case Forward:
			cout << "State: Forward" << endl;
		
			if(distance <= armsReach) {
				current = Stop;
			} else if(distance < armsReach * 2) {
				current = Close;
			}
			
			fwAmplitude = fwdStepSize;
		
			headAngle = Head::GetInstance()->GetPanAngle();
			isCentreX = headAngle > -10 && headAngle < 10;
			if(isCentreX) {
				cout << "\tTarget is centred L/R" << endl;
			} else {
				if(isCentreY) {
					lrAmplitude = 0;
					lrSlide = leftSlideSize;
				} else {
					lrAmplitude = leftTurnSize;
					lrSlide = 0;
				}
				
				if (headAngle >= 10) {
					cout << "\tTarget is to the left" << endl;
				} else {
					cout << "\tTarget is to the right" << endl;
					lrAmplitude *= -1;
					lrSlide *= -1;
				}
			}
			break;
		case Close:
			cout << "State: Close" << endl;
			cout << "\tDistance: " << distance << endl;
			
			targetIsClose = true;
		
			headAngle = Head::GetInstance()->GetTiltAngle();
		
			if(distance <= armsReach+reachOffset || headAngle < -4 || distance > armsReach * 2) {
				current = Stop;
			}
			
			fwAmplitude = fwdStepSize/2;

			headAngle = Head::GetInstance()->GetPanAngle();
			if(headAngle > -10 && headAngle < 10) {
				cout << "\tTarget is centred L/R" << endl;
			} else {
				lrAmplitude = leftTurnSize;
				lrSlide = 0;
				
				if (headAngle > 10) {
					cout << "\tTarget is to the left" << endl;
				} else {
					cout << "\tTarget is to the right" << endl;
					lrAmplitude *= -1;
					lrSlide *= -1;
				}
			}
			
			break;
		case Backwards:
			cout << "State: Backwards" << endl;
			cout << "\tDistance: " << distance << endl;
		
			if(distance > armsReach+reachOffset) {
				current = Stop;
			}
			
			fwAmplitude = -fwdStepSize;
			break;
			
		default:
			cerr << "Unknown state: " << current << endl;
			exit(EXIT_FAILURE);
		}

		if (isCentreY /*&& isCentreX*/) {
			// Centred, don't walk!
			if(Walking::GetInstance()->IsRunning()) {
				Walking::GetInstance()->Stop();
				Walking::GetInstance()->Finish();
			}
		} else {
			// make sure we're actually walking
			if(!Walking::GetInstance()->IsRunning())
				Walking::GetInstance()->Start();

			// set the walking parameters
			Walking::GetInstance()->X_MOVE_AMPLITUDE = fwAmplitude;
			Walking::GetInstance()->A_MOVE_AMPLITUDE = lrAmplitude;
			Walking::GetInstance()->Y_MOVE_AMPLITUDE = lrSlide;
			Walking::GetInstance()->Z_MOVE_AMPLITUDE = stepHeight;
		}
    }
}	

void BasketballEvent::scanForTarget(SingleTarget &target)
{
	const int tilt = (!grabbedBall)? 0 : 15;
    const int pan = (!grabbedBall)? -45 : -60;

    int maxArea = 0;
    int bestPan = 0;
    int centerXAtBestPan = 0;

    // stop walking so we can scan properly
    Walking::GetInstance()->Stop();
    Walking::GetInstance()->Finish(syncVideoPtr);

    Walking::GetInstance()->X_MOVE_AMPLITUDE = (-2);
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = (0);
    Walking::GetInstance()->A_MOVE_AMPLITUDE = (0);

    cout << "Target not in FOV; scanning" << endl;

	const int n = 3;
	const double waitTime = 3000; // 3s
	int panAngle = -pan;
	const string directions[] = {"Left", "Ahead", "Right"};
	
	Head::GetInstance()->MoveByAngle(lastTargetLocation.X, lastTargetLocation.Y);
    
    MotionManager::msleep(waitTime);
    
    if (target.WasFound()) {
		cout << "Right where we left it" << endl;
		missingFrames = 0;
		return;
	}
	
	for(int i = 0; i < n; i++) {
		pauseThread();
		cout << "Looking " << directions[i] << endl;
		Head::GetInstance()->MoveByAngle(panAngle,tilt);

		MotionManager::msleep(waitTime);

		if(target.WasFound())
		{
			if(target.GetBoundingBox()->width * target.GetBoundingBox()->height > maxArea)
			{
				bestPan = panAngle;
				centerXAtBestPan = target.GetBoundingBox()->center.X;
			}
			missingFrames = 0;
			targetIsClose = false;
			break;
		}
		panAngle += pan;
		frameUsed = true;
	}

    // re-center the head
    pauseThread();
    Head::GetInstance()->MoveByAngle(0, tilt);
    
    if(!target.WasFound()) {
		if (targetIsClose) {
			cout << "Looking Down" << endl;
			Head::GetInstance()->MoveByAngle(0,-10);

			MotionManager::msleep(waitTime);
			cout << "Woke up" << endl;
			if(target.WasFound())
			{
				bestPan = 0;
				missingFrames = 0;
			} else {
				cout << "Didn't find the target..." << endl;
			}
		} else {
			cout << "Didn't find the target..." << endl;
		}
	} else {
		cout << "Best angle: " << bestPan << endl;
		int dx = 0;
		if(bestPan == 0)
		{
			Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
			MotionManager::msleep(1000, syncVideoPtr);
			return;
		}
		else if(bestPan > 0)
		{
			// target is left
			Walking::GetInstance()->A_MOVE_AMPLITUDE = leftTurnSize;
			cout << "Target is left of FOV" << endl;

			dx = Camera::WIDTH - centerXAtBestPan;
		}
		else if(bestPan < 0)
		{
			// target is right
			Walking::GetInstance()->A_MOVE_AMPLITUDE = -leftTurnSize;
			cout << "Target is right of FOV" << endl;

			dx = centerXAtBestPan = target.GetBoundingBox()->center.X;
		}
		cout << "dx = " << dx << endl;

		// turn to face the direction of the target
		if(bestPan != 0)
		{
			int turnTime = max(2, dx/80);   // 1s per 50 pixels, or a minimum of 2 seconds
			cout << "Turning for " << turnTime << "s" << endl;

			Walking::GetInstance()->X_MOVE_AMPLITUDE = backUpRate;
			Walking::GetInstance()->Start();
			MotionManager::msleep(turnTime * 1000, syncVideoPtr);
		}
		
		Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
	}
	frameUsed = true;
	
	if(Walking::GetInstance()->IsRunning()) {
		Walking::GetInstance()->Stop();
		Walking::GetInstance()->Finish();
	}
    
    // Centre head on target
    if(target.WasFound())
    {
		cout << "Looking at the target" << endl;
		Head::GetInstance()->LookAt(target);
		pauseThread();
		
		lastTargetLocation.X = Head::GetInstance()->GetPanAngle();
		lastTargetLocation.Y = Head::GetInstance()->GetTiltAngle();

		MotionManager::msleep(1000, syncVideoPtr);
	}
}

// Look around for the target
void BasketballEvent::lookAround(SingleTarget& target)
{
	syncVideo();
	for(int y = 10; y >= -10 && !target.WasFound(); y -= 10) {
		for(int x = 60; x >= -60 && !target.WasFound(); x -= 30) {
			pauseThread();
			Head::GetInstance()->MoveByAngle(x, y);
			frameUsed = true;
			Head::GetInstance()->Finish();
			MotionManager::msleep(3000, syncVideoPtr);
		}
	}
	
	Head::GetInstance()->LookAt(target);
}

// Update the current video, get framerate
void BasketballEvent::handleVideo()
{
	const int inputSize = 8;
	static double inputs[inputSize];
	static struct timeval last_time;
	static int count = 0;
	static string msg = "";
	struct timeval current_time;
	gettimeofday(&current_time, NULL);
	
	long secs = (long) (current_time.tv_sec - last_time.tv_sec);
	long usecs = (long) (current_time.tv_usec - last_time.tv_usec);
	last_time = current_time;
	
	double diff = static_cast<double>(secs * 1e6 + usecs) / 1e6;

	// Get the FPS
	// Note: the first framerate printed is meaningless. Ignore it.
	if (count < inputSize) {
		inputs[count++] = diff;
	} else {
		stringstream ss;
		for(int i = 0; i < inputSize; i++) {
			diff += inputs[i];
		}
		diff = inputSize / diff;
		ss << "FPS: ";
		ss << diff;
		msg = ss.str();
		count = 0;
	}
	
    CvCamera::GetInstance()->CaptureFrame();
	Mat& yuv = CvCamera::GetInstance()->yuvFrame;
	Mat& rgb = CvCamera::GetInstance()->rgbFrame;
	Mat* rgbc = Robot::Target::CreateCompliment(rgb);
	Target::FindTargets(targets, yuv, rgb, *rgbc, &dbg);
	delete rgbc;
    
    putText(yuv, msg, Point(0, Camera::HEIGHT-5), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 0));

    if(markVideo && (recordVideo || showCamera)) {
        markTargetBoxes();
    }
	if(showCamera) {
        showWebcamView();
	}
	if(recordVideo) {
        yuvStream << yuv;
        scanlineStream << dbg;
	}
	
	frameUsed = false;
}

void BasketballEvent::showWebcamView()
{
    //cvtColor(YuvCamera::GetInstance()->img,YuvCamera::GetInstance()->img,CV_YCrCb2RGB);
    imshow( "Webcam", CvCamera::GetInstance()->yuvFrame);
    //imshow( "Ball", ballDbg);
    //imshow( "Hoop", hoopDbg);
    //imshow( "Tee", teeDbg);
    imshow( "ScanLine", dbg);
    
    char c = (char) waitKey(10);
    if (c == 's')
        imwrite("image.ppm",CvCamera::GetInstance()->yuvFrame);
}

void BasketballEvent::markTargetBoxes()
{
	if(ball->WasFound())
		ball->Draw(CvCamera::GetInstance()->yuvFrame);
    
    if(ballTee->WasFound())
		ballTee->Draw(CvCamera::GetInstance()->yuvFrame);
    
    if(hoop->WasFound())
		hoop->Draw(CvCamera::GetInstance()->yuvFrame);
    
    //if(backboard->WasFound())
	//	backboard->Draw(YuvCamera::GetInstance()->img);

    //ballTee->DrawScanRange(YuvCamera::GetInstance()->img);
    //ball->DrawScanRange(YuvCamera::GetInstance()->img);
    //hoop->DrawScanRange(YuvCamera::GetInstance()->img);

    // TODO: mark other targets
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///// Initialization & Setup /////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BasketballEvent::initialize()
{
    cout << "Initializing... " << endl;

    cout << "Initializing ini..." << endl;
    if(ini == NULL)
        ini = new minIni(INI_FILE_PATH);
    cout << "done" << endl;
    
    cout << "Initializing Robot..." << endl;
    LinuxDARwIn::Initialize(ini->gets("Files","MotionFile").c_str(), SAFE_POSITION);
    cout << "done" << endl;

	cout << "Initializing" << endl;
    cout << "\tcamera... " << endl;
    if(!cameraInitialized)
    {
        CvCamera::GetInstance()->Initialize(0);
		CvCamera::GetInstance()->LoadINISettings(ini);
        //CvCamera::GetInstance()->SetAutoWhiteBalance(0);
        cameraInitialized = true;
    }
    namedWindow(WEBCAM_VIEW_WINDOW_NAME, 1);
    if(recordVideo)
    {
		cerr << "Setting up video recording" << endl;
        yuvStream = VideoWriter (ini->gets("Files","WebcamStream","webcam.avi"), CV_FOURCC('D', 'I', 'V', 'X'), 10, cvSize(Camera::WIDTH,Camera::HEIGHT), true);
        scanlineStream = VideoWriter (ini->gets("Files","ScanlineStream","scanline.avi"), CV_FOURCC('D', 'I', 'V', 'X'), 10, cvSize(Camera::WIDTH,Camera::HEIGHT), true);
    }
    
    if(showCamera)
    {
		namedWindow("Webcam");
		//namedWindow("Ball");
		//namedWindow("Hoop");
		//namedWindow("Tee");
		
		cvMoveWindow("Webcam",0,0);
		//cvMoveWindow( "Ball", 310, 0 );
		//cvMoveWindow( "Hoop", 620, 0 );
		//cvMoveWindow( "Tee", 930, 0 );
	}
    cout << "done" << endl;

    cout << "\tinserting motion modules... ";
    MotionManager::GetInstance()->SetEnable(false);
	MotionManager::GetInstance()->AddModule(Head::GetInstance());
	MotionManager::GetInstance()->AddModule(Walking::GetInstance());
	MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
	MotionManager::GetInstance()->AddModule(RightArm::GetInstance());
	cout << "done" << endl;

    cout << "\tenabling motion modules... ";
	Action::GetInstance()->m_Joint.SetEnableBody(false);
	Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
	LeftArm::GetInstance()->m_Joint.SetEnableLeftArmOnly(true, true);
	RightArm::GetInstance()->m_Joint.SetEnableRightArmOnly(true, true);
	//Walking::GetInstance()->m_Joint.SetEnableBody(true);
	//Walking::GetInstance()->m_Joint.SetEnableHeadOnly(false);
	Walking::GetInstance()->m_Joint.SetEnableLowerBody(true, true);
	cout << "done" << endl;

	cout << "\tcreating target objects... ";

    if(ballTee == NULL)
        ballTee = new SingleTarget("Tee");

    if(ball == NULL)
        ball = new SingleTarget("Ball");

    if(backboard == NULL)
        backboard = new SingleTarget("Backboard");
        
    if(hand == NULL)
		hand = new SingleTarget("Hand");

    if(hoop == NULL)
        hoop = new SingleTarget("Hoop");

	targetIsClose = true;
	goingForwards = true;
	inLeftHand = false;
	lastTargetLocation = Point2D(0, 0);

    cout << "done" << endl;

	cout << "Loading configuration..." << endl;
	loadIniSettings();
	cout << "Done" << endl;

    cout << "Initialization done" << endl;

    cout << "\tentering starting position... ";

    Walking::GetInstance()->Finish();
    
    // Pull the arms back in
    Point3D close(60, 20, -50);
    LeftArm::GetInstance()->SetGoalPosition(close, -50);
    RightArm::GetInstance()->SetGoalPosition(close, -50);
    MotionManager::Sync();
    
    cout << "done" << endl;
}

void BasketballEvent::initVideo()
{
	// get the initial frame from the camera (handleVideo() will deal with the rest)
    CvCamera::GetInstance()->CaptureFrame();
    this->isRunning = true;

#ifdef NO_VIDEO_THREAD
    cerr << "[info] Video processing will be synchronous" << endl;
#else
    // start the video-handling thread
    pthread_t videoThreadId;
    pthread_create(&videoThreadId, NULL, &videoLoop, (void*) this);
#endif
}

void BasketballEvent::loadIniSettings()
{
    cout << "Loading INI settings... ";

    CvCamera::GetInstance()->LoadINISettings(ini);
    Walking::GetInstance()->LoadINISettings(ini, "Walking");

    this->disableWalking = (bool) (ini->geti("Strategy","DisableWalk"));
    this->disableThrowing = (bool) (ini->geti("Strategy","DisableThrow"));
    this->fwdStepSize = ini->geti("Strategy","FwdStepSize");
    this->leftTurnSize = ini->geti("Strategy","LeftTurnSize");
    this->leftSlideSize = ini->geti("Strategy","LeftSlideSize");
    this->sidestepTime = ini->geti("Strategy","SideStepTime");
    this->aOffset = ini->geti("Strategy","AOffset");
    this->xOffset = ini->geti("Strategy","XOffset");
    this->yOffset = ini->geti("Strategy","YOffset");
    this->stepHeight = ini->geti("Strategy","StepHeight");
    this->headTilt = ini->geti("Strategy","HeadTilt");
	this->backUpRate = ini->geti("Strategy", "BackUpRate");
	this->scootSpeed = ini->geti("Strategy", "ScootSpeed");
    
    this->armsReach = ini->geti("Strategy", "ArmsReach");
	this->dumpArmsReach = ini->geti("Strategy", "DumpArmsReach");
	this->lowerArmReach = ini->geti("Strategy", "LowerArmReach");
	this->reachOffset = ini->geti("Strategy", "ReachOffset");
	this->shuffleOffset = ini->geti("Strategy", "ShuffleOffset");
	this->ballHeight = ini->geti("Strategy", "BallHeight");
	this->hoopHeight = ini->geti("Strategy", "HoopHeight");
	this->driftOffset = ini->geti("Strategy", "DriftOffset");
	this->visionChecks = ini->geti("Strategy", "VisionChecks");
	this->maxMissingFrames = ini->geti("Strategy", "MaxMissingFrames");
    
    this->grabOpenAngle = ini->geti("Grab","HandOpenAngle");
    this->grabCloseAngle = ini->geti("Grab","HandCloseAngle");
    this->grabTurnSpeed = ini->geti("Grab","LeftTurnSize");
    this->grabTurnTime = ini->geti("Grab","TurnTime");
    this->grabPan = ini->geti("Grab","HeadPan");
    this->grabTilt = ini->geti("Grab","HeadTilt");
    this->grabTurnLimit = ini->geti("Grab","TurnLimit");
    this->grabXHandOffset = ini->geti("Grab","XHandOffset");
    this->grabYHandOffset = ini->geti("Grab","YHandOffset");
    this->grabCloseSpeed = ini->geti("Grab","HandCloseSpeed");
    this->grabShuffleSpeed = ini->geti("Grab","ShuffleSpeed");
    this->maxTurn = ini->geti("Grab", "MaxTurn");
    int upX = ini->geti("Grab","UpX");
	int upY = ini->geti("Grab","UpY");
	int upZ = ini->geti("Grab","UpZ");
	this->grabUp = Point3D(upX, upY, upZ);
	
	this->dumpTargetAngle = ini->geti("Dump","TargetAngle");
	this->dumpTargetAngleErr = ini->geti("Dump","TargetAngleErr");
	
    if(fwdStepSize <= 0)
    {
        cerr << "[debug] fwdStepSize is " << fwdStepSize << "; resetting to 5";
        fwdStepSize = 5;
    }
    if(leftTurnSize <= 0)
    {
        cerr << "[debug] leftTurnSize is " << leftTurnSize << "; resetting to 5";
        leftTurnSize = 5;
    }

	ball->LoadIniSettings(*ini,"Yellow");
	ballTee->LoadIniSettings(*ini,"Blue");
	//hand->LoadIniSettings(*ini, "Pink");
	hoop->LoadIniSettings(*ini,"Red");
	//backboard->LoadIniSettings(*ini,"Green");
	
	targets.push_back(ball);
	targets.push_back(ballTee);
	//targets.push_back(hand);
	targets.push_back(hoop);
	//targets.push_back(backboard);

    cout << "done" << endl;
}

void BasketballEvent::reset()
{
    delete ini;
    ini = new minIni(INI_FILE_PATH);
    loadIniSettings();
}

void BasketballEvent::enterSafePosition()
{
	Action::GetInstance()->Start(SAFE_POSITION);    /* Init(stand up) pose */
	Action::GetInstance()->Finish();
}

#ifndef NO_VIDEO_THREAD
// Do NOT call this in the vision thread unless you like deadlocks
void BasketballEvent::pauseThread() {
	MotionManager::Sync();
	while(frameUsed) {
		pthread_yield();
	}
}

// used by a pthread to call handleVideo() once every 10 ms
// and stream the results to the appropriate files/video windows
void* BasketballEvent::videoLoop(void* arg)
{
    BasketballEvent *instance = (BasketballEvent*) arg;

    while(instance->isRunning)
    {
        // grab the next frame of video and process it
        instance->handleVideo();

        // sleep 10 ms
        MotionManager::msleep(10);
    }

    pthread_exit(NULL);
    return NULL; // Unreachable
}
#endif

// Test the robot's vision (calibration mostly)
void BasketballEvent::visionTest()
{
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true);
    Head::GetInstance()->MoveByAngle(0, headTilt);

	bool didPrint = false;
	int i = 0;
	
	initVideo();

    for(;;)
    {
        //handleVideo();
        didPrint = false;
        
        if(ball->WasFound())
        {
			didPrint = true;
			ball->Print();
			
			if(i == 4) {
				double panAngle = Head::GetInstance()->GetPanAngle();
				double tiltAngle = Head::GetInstance()->GetTiltAngle();
				cout << "Angles: " << panAngle << ", " << tiltAngle << endl;
				cout << "Distance: " << ball->GetDistance() << endl;
				Point3D* p1 = ball->GetPosition3D(panAngle, tiltAngle);
				cout << "Position: " << p1->X << ", " << p1->Y << ", " << p1->Z << endl;
				delete p1;
				i = 0;
				
				pauseThread(); // yield to vision
			
				//cout << "Distance: " << FindDistance(*ball, BALL_HEIGHT) << endl;
			} else {
				i++;
			}
		} else {
			missingFrames++;
			if(missingFrames >= 10) {	
				lookAround(*ball);
				pauseThread();
				missingFrames = 0;
			}
        }
        
        if(ballTee->WasFound())
		{
			didPrint = true;
			ballTee->Print();
		}
		
		if(hand->WasFound())
		{
			didPrint = true;
			hand->Print();
		}
			
		if(hoop->WasFound())
		{
			didPrint = true;
			hoop->Print();
		}
		
		if (backboard->WasFound())
		{
			didPrint = true;
			backboard->Print();
		}
		
		if(didPrint)
			cout << endl;
        
        pauseThread();
    }
}

void BasketballEvent::turningTest()
{
	// FIXME: Turning drift should be in config
	const int lrTurn = leftTurnSize;
	int direction = 1;
	int angle;
	int turnTime;
	
	Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
	Walking::GetInstance()->Z_MOVE_AMPLITUDE = stepHeight;
	
	while(true) {
		const int speed = direction * lrTurn;
		Walking::GetInstance()->X_MOVE_AMPLITUDE = -8;
		Walking::GetInstance()->A_MOVE_AMPLITUDE = speed;
		
		cout << "Enter turn angle: ";
		cin >> angle;
		turnTime = getTurnTime(speed, angle);
		
		cout << "Turning for " << turnTime;
		cout << " with speed " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl;

		Walking::GetInstance()->Start();
		MotionManager::msleep(turnTime, syncVideoPtr);
		Walking::GetInstance()->Stop();
		Walking::GetInstance()->Finish();
	
		MotionManager::msleep(2000, syncVideoPtr);
		direction *= -1;
	}
}

void BasketballEvent::grabTest()
{
	initVideo();
	
	cout << "Let me at 'em!" << endl;
	MotionManager::WaitButton(MotionStatus::MIDDLE_BUTTON);
	grabTarget(*ball);
	
	cout << "Grab test complete" << endl;
	this->isRunning = false;
	pthread_join(this->videoThreadId, NULL);
	MotionManager::Sync();
	exit(EXIT_SUCCESS);
}

// Run just the shooting part of the program (testing)
void BasketballEvent::ShootingDrill()
{
	initVideo();

	Point3D up(-5, 85, 130);
	Point3D out(120, 85, 50);
	
	this->inLeftHand = true;
	LeftArm::GetInstance()->SetGoalPosition(up, 0);
	LeftArm::GetInstance()->MoveHand(1, 90);
	LeftArm::GetInstance()->Finish();
	
	cout << "Provide ball, then press middle button" << endl;
	//Voice::Speak("Provide ball, then press middle button");
	MotionManager::WaitButton(MotionStatus::MIDDLE_BUTTON);
	cout << "Button pressed" << endl;
	
	LeftArm::GetInstance()->CloseHand(2);
	LeftArm::GetInstance()->Finish();
	
	LeftArm::GetInstance()->SetGoalPosition(out, 0);
	
	cout << "Turning towards hoop" << endl;
	faceTarget(*hoop);
	cout << "Facing hoop" << endl;
	MotionManager::msleep(500);
	
	dumpBall();
	
	cout << "End of Drill" << endl;
	
	// End program
	this->isRunning = false;
	pthread_join(videoThreadId, NULL);
	exit(EXIT_SUCCESS);
}

#undef syncVideo
