/***********************************************************************

Chris Iverach-Brereton <ve4cib@gmail.com>

FIRA 2011 Basketball event

Lab testing: uses blue, 20cm stick with ball on it.  Use cv + webcam to
identify stick, walk towards it until the pixel height in the image is
correct (meaning we are close enough to scoop the ball).

Execute scoop motion (created with Roboplus), then shuffle around stick,
advance, and execute throw motion.

TODO: well, everything described above
TODO: allow dynamically changing the stick target colour
TODO: aiming (based on hoop & backstop colour)
TODO: use inverse kinematics to dynamically plan the scoop motion
TODO: tie in walking/searching for the stick
TODO: adjust head angle to look down for the stick
TODO: determine appropriate target size
TODO: use target position as well as colour/size before performing grab & throw

***********************************************************************/

#include <unistd.h>
#include <string.h>
#include <libgen.h>
#include <iostream>

//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include "LinuxDARwIn.h"
#include "Voice.h"
#include "basketballevent.h"

using namespace cv;
using namespace Robot;
using namespace std;

void change_current_dir()
{
    char exepath[1024] = {0};
    if(readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
    {
        if(chdir(dirname(exepath)) != 0)
        {
            printf("error: could not change directory to the executable.\n");
            exit(EXIT_FAILURE);
        }
    }
}

int main(int argc, char** argv)
{
    bool recordVideos = false;
    bool showVideo = false;
    bool markVideo = true;
    bool visionTest = false;
    bool turnTest = false;
    bool grabTest = false;
    bool shootingDrill = false;

    for(int i=1; i<argc; i++)
    {
        if(!strcmp(argv[i], "--record"))
        {
            cout << "Video will be recorded to basketball.avi" << endl;
            recordVideos = true;
        }
        else if(!strcmp(argv[i], "--show"))
        {
            cout << "Video streaming enabled" << endl;
            showVideo = true;
        }
        else if(!strcmp(argv[i], "--no-mark"))
        {
            markVideo = false;
        }
        else if(!strcmp(argv[i], "--vision-test"))
        {
            visionTest = true;
            showVideo = true;
        }
        else if(!strcmp(argv[i], "--shoot"))
        {
			shootingDrill = true;
			showVideo = true;
		}
		else if(!strcmp(argv[i], "--grab-test"))
		{
			grabTest = true;
			showVideo = true;
		}
		else if(!strcmp(argv[i], "--turn-test"))
		{
			turnTest = true;
		}
        else
        {
            if (strcmp(argv[i], "--help")) cout << "Unknown argument " << argv[1] << endl;
            cout << "\t$ basketball [--show][--record]";
            cout << "[--no-mark][--vision-test][--turn-test][--shoot]" << endl;
            return EXIT_FAILURE;
        }
    }

    change_current_dir();
    BasketballEvent* bball = BasketballEvent::GetInstance(); //new BasketballEvent(showVideo, recordVideos, markVideo);
	bball->Initialize(showVideo, recordVideos, markVideo);
	
	MotionManager::GetInstance()->SetEnable(true);

    if(visionTest)
        bball->visionTest();
        
    if(grabTest)
		bball->grabTest();
        
    if(shootingDrill)
		bball->ShootingDrill();
		
	if(turnTest)
		bball->turningTest();

#ifndef NO_VOICE
    Voice::Initialize();
#endif

//    for(;;)
    {
#ifdef DEBUG
        cout << "Press ENTER to begin" << endl;
        char ch;
        cin >> ch;
#else
        //cout << "Press the START button to begin" << endl;
 #ifndef NO_VOICE
        Voice::Speak("Press the middle button");
 #endif
        //MotionManager::WaitButton(MotionStatus::MIDDLE_BUTTON);
#endif

        cout << endl << "===== Begin Basketball Trial =====" << endl << endl;
        bball->execute();
        cout << endl << "====== End Basketball Trial ======" << endl << endl;

#ifndef NO_VOICE
        Voice::Speak("Press the middle button to finish");
#endif
		cout << "Press the middle button to finish..." << endl;
        MotionManager::WaitButton(MotionStatus::MIDDLE_BUTTON);

//        bball->reset();

//        bball->enterSafePosition();
    }

    //delete bball;
}
