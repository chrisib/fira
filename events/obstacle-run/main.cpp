#include <ObstacleCourse.h>

using namespace std;

int main(int argc, char** argv)
{
    ObstacleCourse *course = new ObstacleCourse();
    course->ParseArguments(argc, argv);
    course->Initialize("config.ini");
    course->Execute();
    delete course;
    return 0;
}
