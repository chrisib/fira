##################################################
# PROJECT: DARwIn-OP Sample Makefile
# AUTHOR : Stela H. Seo <shb8775@hotmail.com>
#          Chris Iverach-Brereton <ve4cib@gmail.com>
##################################################

#---------------------------------------------------------------------
# Qt Project template for Darwin projects
#
# This file is intended to be a blank template used for AA Lab
# projects done using the Darwin robots.
#
# Please make sure to follow these instructions when setting up your
# own copy of this file:
#
#   1- Enter the name of the target (the TARGET variable)
#   2- Add additional source files to the SOURCES variable
#   3- Add additional header files to the HEADERS variable
#   4- Add any additional libraries to the LIBS variable
#   5- Optionall set up the INSTALLS variable to allow "sudo make install"
#
# note: libdarwin and libmlibrary must be compiled & installed
# libdarwin is included in this git project
# libmlibrary is part of MotionController_v2.0, also available on
# scm@sculpin.cs.umanitoba.ca
#
# This makefile will link against several libraries, not all of which
# are necessarily needed for your project.  Please feel free to
# remove libaries you do not need.
#---------------------------------------------------------------------

TEMPLATE = app
Qt -= core gui

INCLUDEPATH +=  /usr/local/include/ \
                /usr/local/include/darwin/framework/ \
                /usr/local/include/darwin/linux/ \

TARGET = obstaclerun

SOURCES +=  main.cpp \
    ObstacleCourse.cpp
            # ADDITIONAL SOURCES GO HERE
            
HEADERS += \   # ANY HEADERS GO HERE
    ObstacleCourse.h

LIBS += -ldl \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lespeak \
        -ljpeg \
        -lpthread \
        -ldarwin \
        # ANY ADDITIONAL LIBRARIES GO HERE

##################################################
# Installation Directories                       #
# Uncomment out to create install & uninstall    #
# targets in the Makefile                        #
##################################################
#INSTALLBASE = /usr/local
#target.path    = $$INSTALLBASE/lib/
#INSTALLS   += target

OTHER_FILES += \
    config.ini
