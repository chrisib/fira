#include "ObstacleCourse.h"
#include <darwin/framework/Walking.h>
#include <darwin/framework/Head.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/MotionManager.h>
#include <darwin/framework/Math.h>
#include <cstdio>

//Robotis stock code
using namespace Robot;
//C++ standard namespace, basics
using namespace std;
//Opencv namespace
using namespace cv;

//Constructor
ObstacleCourse::ObstacleCourse() : Application()
{
    obstacles.push_back(&walls);
    obstacles.push_back(&walls2);
    obstacles.push_back(&puddles);
    obstacles.push_back(&gates);
    obstacles.push_back(&target);

    dbgImg = NULL;

    goalTurn = 0;
    setTurn = 0;
}

//Deconstructor
ObstacleCourse::~ObstacleCourse()
{
    if(dbgImg != NULL)
        delete dbgImg;

    delete turnPid;
}

bool ObstacleCourse::Initialize(const char *iniFilePath, const char *motionFilePath, int safePosition)
{
    bool ok = Application::Initialize(iniFilePath, motionFilePath,safePosition);

    //Tells the motion moduls that they don't control anything yet.
    Walking::GetInstance()->m_Joint.SetEnableBody(false);
    LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
    RightArm::GetInstance()->m_Joint.SetEnableBody(false);
    Head::GetInstance()->m_Joint.SetEnableBody(false);

    //Causes motion manager to look elsewhere for moving information.
    MotionManager::GetInstance()->AddModule(Walking::GetInstance());
    MotionManager::GetInstance()->AddModule(Head::GetInstance());
    MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
    MotionManager::GetInstance()->AddModule(RightArm::GetInstance());

    // give the Arm modules control over the hands just in case the motion module doesn't have data for those joints
    LeftArm::GetInstance()->m_Joint.SetEnableLeftHandOnly(true,true);
    RightArm::GetInstance()->m_Joint.SetEnableRightHandOnly(true,true);
    
    // give the Head module control over the neck joints
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);

    return ok;
}

void ObstacleCourse::LoadIniSettings()
{
    //Loads the settings for each of the types of obstacles defined
    //  in ObstacleCourse.h
    walls.LoadIniSettings(*ini, ini->gets("Obstacles", "wallColour", "Blue"));
    //I ADDED THIS LINE FOR EXTRA WALLS
    walls2.LoadIniSettings(*ini, ini->gets("Obstacles", "wall2Colour", "Aqua"));
    puddles.LoadIniSettings(*ini, ini->gets("Obstacles", "puddleColour", "Yellow"));
    gates.LoadIniSettings(*ini, ini->gets("Obstacles", "gateColour", "Red"));
    sidelines.LoadIniSettings(*ini,ini->gets("Obstacles","sidelineColour","Green"));
    target.LoadIniSettings(*ini, ini->gets("Obstacles","goalColour","Orange"));

    // load the walking parameters
    Walking::GetInstance()->LoadINISettings(ini);

    CvCamera::GetInstance()->RGB_ENABLE = showVideo || visionOnly;
    if(showVideo || visionOnly)
    {
        //Creates two windows, one debug, one webcam, top left
        //  corner, stacked on top of eachother
        cv::namedWindow("Debug");
        cv::namedWindow("Webcam");

        cvMoveWindow("Debug",0,Camera::HEIGHT+32);
        cvMoveWindow("Webcam",0,0);

        //Creates the debug image
        dbgImg = new cv::Mat(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    }

    blobGain = ini->getd("Navigation","blobGain",1.0);
    lineGain = ini->getd("Navigation","lineGain",1.0);
    turnBias = ini->getd("Navigation","turnBias",0.0);
    maxTurn = ini->getd("Navigation","maxTurn",35.0);

    wallHeight = ini->getd("Obstacles","wallHeight");
    gateHeight = ini->getd("Obstacles","gateHeight");

    turnP = ini->getd("Navigation","P",1.0);
    turnI = ini->getd("Navigation","I",1.0);
    turnD = ini->getd("Navigation","D",1.0);
    turnPid = new DoublePidController(&setTurn,&goalTurn,turnP,turnI,turnD);
}

void ObstacleCourse::HandleVideo()
{
    //This grabs a frame from the camera
    CvCamera::GetInstance()->CaptureFrame();

    // Finds where the targets are in the frame
    // The Darwins' vision is designed to work with the YUV colour space
    // pass a pointer to our debug image so we can see the output from the algorithm if necessary
    sidelines.FindInFrame(CvCamera::GetInstance()->yuvFrame,dbgImg);   // line targets do their own thing that's different from blob targets
    BlobTarget::FindTargets(obstacles, CvCamera::GetInstance()->yuvFrame, dbgImg);


    //If program is not run with this, doesn't show video.
    if(showVideo || visionOnly)
    {
        //Draws the boxes around the different targets in frame
        walls.Draw(CvCamera::GetInstance()->rgbFrame);
        walls2.Draw(CvCamera::GetInstance()->rgbFrame);
        puddles.Draw(CvCamera::GetInstance()->rgbFrame);
        gates.Draw(CvCamera::GetInstance()->rgbFrame);
        sidelines.Draw(CvCamera::GetInstance()->rgbFrame);
        target.Draw(CvCamera::GetInstance()->rgbFrame);
        //Draws boxes in debug image
        walls.Draw(*dbgImg);
        walls2.Draw(*dbgImg);
        puddles.Draw(*dbgImg);
        gates.Draw(*dbgImg);
        sidelines.Draw(*dbgImg);
        target.Draw(*dbgImg);

        // write some extra information on the screen
        char buffer[64];
        sprintf(buffer,"Turn: %0.2f",Walking::GetInstance()->A_MOVE_AMPLITUDE);
        cv::putText(CvCamera::GetInstance()->rgbFrame,buffer,Point(0,Camera::HEIGHT-12),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,255));

        //Pulls up a screen that shows the current frame and the targets on it
        cv::imshow("Debug", *dbgImg);
        cv::imshow("Webcam", CvCamera::GetInstance()->rgbFrame);

        //This, waitkey, is set so the camera does not wait for us to press a key
        cv::waitKey(1);
    }

    newFrame = true;
}

double ObstacleCourse::GetTurnForLine(Line2D &line)
{
    double range = (cameraPosition.CalculateRange(line.start) + cameraPosition.CalculateRange(line.end))/2.0;
    double angle = (cameraPosition.CalculateAngle(line.start) + cameraPosition.CalculateAngle(line.end))/2.0;

    Point2D a = Point2D(line.start.X * cos(deg2rad(line.start.Y)),
                        line.start.X * sin(deg2rad(line.start.Y)));
    Point2D b = Point2D(line.end.X * cos(deg2rad(line.end.Y)),
                        line.end.X * sin(deg2rad(line.end.Y)));

    // sort A and B such that A lies higher in the frame than B (i.e. A has a lower Y value)
    if(a.Y > b.Y)
    {
        Point2D tmp = a;
        a=b;
        b=tmp;
    }

    // avoid NaN issues
    if(range == 0)
        range = 0.001;

    if(angle == 0)
        angle = 0.001;

    // calculate the angle of the line with zero being vertical
    // positive slopes will angle to the left, negative slopes angle to the right
    double slope = atan2(b.X-a.X,b.Y-a.Y);

    // normalize the slope to lie in the [-1,1] range
    // the angle will be in the [-PI/2,PI/2] range already
    slope /= PI/2;

    double turn = 1/range * -1/angle * slope * lineGain;

    return turn;
}

double ObstacleCourse::GetTurnForBlob(BoundingBox &object, double heightAboveGround)
{
    double range = cameraPosition.CalculateRange(object,heightAboveGround);
    double angle = cameraPosition.CalculateAngle(object,heightAboveGround);

    // avoid NaN issues
    if(range == 0)
        range = 0.001;

    if(angle == 0)
        angle = 0.001;

    double turn = 1/range * -1/angle * blobGain;

    //cout << range << " " << angle << " " << turn << endl;

    return turn;
}

double ObstacleCourse::ProcessBlobs(std::vector<BoundingBox *> *blobs,double heightAboveGround)
{
    // go through every blob of this type and sum up how much they're telling us to turn
    double r = 0.0;
    for(vector<BoundingBox*>::iterator i = blobs->begin(); i!=blobs->end(); i++)
    {
        BoundingBox *b = *i;
        r += GetTurnForBlob(*b,heightAboveGround);
    }
    return r;
}

double ObstacleCourse::ProcessLines(std::vector<Robot::Line2D*> *lines)
{
    // go through every sideline we found and sum up how much they're telling us we need to turn
    double r = 0.0;
    for(vector<Line2D*>::iterator i = lines->begin(); i!=lines->end(); i++)
    {
        Line2D *l = *i;
        r += GetTurnForLine(*l);
    }
    return r;
}

void ObstacleCourse::Execute()
{

    if(!visionOnly)
    {
        // stand up and get ready to start walking
        Action::GetInstance()->Start(Action::DEFAULT_MOTION_WALKREADY);
        Action::GetInstance()->Finish();
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);

        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Head::GetInstance()->MoveByAngle(0,ini->getd("Navigation","headTilt",20.0));

        // wait for a button-press
        cout << "Press the middle button" << endl;
        MotionManager::GetInstance()->WaitButton(CM730::MIDDLE_BUTTON);

        // start walking forwards
        Walking::GetInstance()->Start();
        Walking::GetInstance()->X_MOVE_AMPLITUDE = ini->getd("Navigation","StepLength",15.0);
    }

    for(;;)
    {
        HandleFall();

        if(newFrame)
        {
            newFrame = false;
            Process();
        }
    }
}

void ObstacleCourse::Process()
{
    cameraPosition.RecalculatePosition(); // determine the position of the camera relative to the ground

    //calculate turn from objects found
    double aAmplitude = ProcessBlobs(walls.GetBoundingBoxes(),wallHeight/2) + ProcessBlobs(walls2.GetBoundingBoxes(),wallHeight/2) + ProcessBlobs(puddles.GetBoundingBoxes(),0.0) + ProcessBlobs(gates.GetBoundingBoxes(),wallHeight+gateHeight/2) +
                        ProcessLines(sidelines.GetAllLines());

    // turn towards the target, if we can see it
    aAmplitude -= ProcessBlobs(target.GetBoundingBoxes(),0);

    if(aAmplitude < -maxTurn)
        aAmplitude = -maxTurn;
    else if(aAmplitude > maxTurn)
        aAmplitude = maxTurn;

    aAmplitude += turnBias;

    goalTurn = aAmplitude;
    turnPid->Update();


    //apply turn
    Walking::GetInstance()->A_MOVE_AMPLITUDE = setTurn;
}

void ObstacleCourse::HandleFall()
{
    // if we're not standing then get up
    if(MotionStatus::FALLEN != STANDUP)
    {
        Walking::GetInstance()->Stop();
        Walking::GetInstance()->Finish();

        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

        // go to a default standing position
        Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
        Action::GetInstance()->Finish();

        // execute the stand-up motion as needed
        if(MotionStatus::FALLEN == FORWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_F_UP);   // FORWARD GETUP
        else if(MotionStatus::FALLEN == BACKWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_B_UP);   // BACKWARD GETUP
        Action::GetInstance()->Finish();

        // give control back to the Walking module and start walking again
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

        Walking::GetInstance()->Start();
        Walking::GetInstance()->X_MOVE_AMPLITUDE = ini->getd("Navigation","StepLength",15.0);
    }
}
