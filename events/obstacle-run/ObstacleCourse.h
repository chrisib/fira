#ifndef OBSTACLECOURSE_H
#define OBSTACLECOURSE_H

#include <darwin/framework/Application.h>
#include <darwin/framework/MultiBlob.h>
#include <darwin/framework/SingleBlob.h>
#include <darwin/framework/Line.h>
#include <darwin/framework/BoundingBox.h>
#include <darwin/framework/MultiLine.h>
#include <darwin/framework/Action.h>
#include <darwin/framework/CameraPosition.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <darwin/framework/DoublePidController.h>


class ObstacleCourse : public Robot::Application
{
public:
    ObstacleCourse();
    ~ObstacleCourse();
    //These are virtual because they are better defined in ObstacleCourse.cpp
    virtual void LoadIniSettings();
    virtual void HandleVideo();
    virtual void Process();
    virtual void Execute();
    virtual bool Initialize(const char *iniFilePath="config.ini", const char *motionFilePath="motion.bin", int safePosition=Robot::Action::DEFAULT_MOTION_SIT_DOWN);

protected:
    //These are the types of obstacles the robot may encounter in the
    //  obstacle course
    Robot::MultiBlob walls;
    //I ADDED THIS LINE FOR THE SECOND BOUT OF WALLS
    Robot::MultiBlob walls2;
    Robot::MultiBlob puddles;
    Robot::MultiBlob gates;
    Robot::MultiBlob target;

    Robot::DoublePidController *turnPid;
    double turnP, turnI, turnD;
    double goalTurn, setTurn;

    double maxTurn;

    //These are the boundry lines that the robot must remain between
    Robot::MultiLine sidelines;

    double wallHeight, gateHeight;

    // set to true when we're done capturing a frame, set to false when we make a decision based on that frame
    volatile bool newFrame;

    //Creates a vector that contains the possible targets.
    std::vector<Robot::BlobTarget*> obstacles;

    // openCV image used for displaying the output from Scanline + Hough detection
    cv::Mat *dbgImg;

    Robot::CameraPosition cameraPosition;
    double blobGain, lineGain, turnBias;

    double ProcessBlobs(std::vector<Robot::BoundingBox*> *blobs, double heightAboveGround);
    double GetTurnForBlob(Robot::BoundingBox &object, double heightAboveGround);

    double ProcessLines(std::vector<Robot::Line2D*> *lines);
    double GetTurnForLine(Robot::Line2D &line);

    void HandleFall();

};

#endif // OBSTACLECOURSE_H
