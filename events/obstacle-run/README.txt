TERRIBLE OBSTACLE
Author: Josh Jung

This code was thrown together in a day on-site at FIRA 2013.  I'm not going to pretend that it's good.  It worked 1/10 times at FIRA, which was pretty bad, but enough to score us some points.  So, that's kind of a win, I guess.  In any case, it needs significant commenting and rewriting.

A lot of the code was stolen directly from the penalty kick and as a result, there are a lot of files that are not actually used.  Most of these are related to the absolute positioning map.

Vision is not handled in a separate thread.  It is processed only when needed.

For a competitive run, the command line argument --no-video should be added.

Once initialization is finished, start the program by pushing either the left or middle button.  This will also give it the direction of its first turn (left->left, middle->right).

There are some important things to calibrate before running:
Vision - Standard calibration of Binders (blue), Holes (green), and Boundaries (pink) with DarwinViewer. There is a place in the code for finding Walls (red), but this is best ignored since they always sit on top of binders anyway.
Walking Config - Standard walking calibration.  Make sure it doesn't fall over (when it doubt, change hip_pitch_offset).

There are also some constants in main.h that need to be set:
OBJECT_CLOSE_THRESHOLD - The range (in mm) at which an object will be found to be close enough to warrant turning away from
HEAD_TILT - The angle at which be tilted for the entire run
The rest are leftovers from the original penalty kick code and can be ignored.  Some can probably be removed altogether.

The state machine has 3 states:
Idle - Stand there.
Walking Forward - Walk forward until some obstacle comes within a threshold distance, then enter Turning state.
Turning - Turn until you can't see the obstacle anymore, then enter Walking Forward again.  When the Turning state is first entered, turn in the direction opposite to the one you last turned in unless the obstacle is the boundary line, in which case turn in the direction that maximizes the line's slope. The first turn of the run is determined by starting the robot with the left or middle button.

The strategy here is essentially to try to plinko between the obstacles while going from one end to the other.  It struggles, though, with very long walls, or walls that angle back toward the start.  There is no way to recover from a 180 degree turn-around besides dumb luck.  The fact the robot has trouble walking straight is also a problem.

For next year, I recommend implementing a system some of the other robots were using: as you go around an obstacle, keep looking at it and turn back to the forwards direction once you are clear.  While this approach is still naive in that it uses no absolute positioning map and will therefore be unable to get out of dead ends, it proved to be significantly better than plinko in competition.  Sidestepping may also be viable, but is even harder to keep straight and is excruciatingly slow.

Good luck with this.  I don't envy you.
