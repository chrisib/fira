#ifndef SOCKETMONITOR_H
#define SOCKETMONITOR_H

#include <vector>
#include <pthread.h>
#include <pthread.h>
#include <stdint.h>
#include <SoccerField.h>
#include <darwin/framework/Point.h>
#include <FiraPackets.h>

//#define USE_Q_SOCKET
#ifdef USE_Q_SOCKET
#    include <QUdpSocket>
#else
#    include <sys/types.h>
#    include <sys/socket.h>
#    include <netinet/in.h>
#    include <netdb.h>
#endif

//#define SOCKET_DEBUG

// forward definition
class SoccerPlayer;

class SocketMonitor
{
public:
    SocketMonitor(SoccerField &field);
    SocketMonitor(int portNo, SoccerField &field);
    virtual ~SocketMonitor();

    virtual void setPort(int portNo);

    // abstract-ish game states
    enum {
        STATE_INITIAL   = 0,    // idle, waiting state
        STATE_READY     = 1,    // move into position to begin
        STATE_SET       = 2,    // about to begin play; don't move at all
        STATE_PLAY      = 3,    // playing the game; do whatever our position dictates
        STATE_FINISHED  = 4,    // game is finished; sit down and stop moving
        STATE_DEFEND    = 5,    // receiving kickoff; do not kick the ball but be ready to receive [only used for FIRA]
        NUM_GAME_STATES
    };

    class RobotStateInformation
    {
    public:
        std::vector<PacketObject> lastPacketData;

        int sequence;

        int state;
        int substate;

        Robot::Point3D presentPosition; // absolute position
        int positionConfidence;

        Robot::Point2D ballPosition;    // absolute position
        int ballConfidence;

        // polar positions
        Robot::Point2D observedBall;
        Robot::Line2D observedGoal;
        std::vector<Robot::Line2D> observedLines;
        std::vector<Robot::Point2D> myTeam;
        std::vector<Robot::Point2D> theirTeam;

        void print();
    };

    static const int MAX_PLAYERS_PER_TEAM = 11; // technically we will never have more than 4 players for RoboCup, but this is a full soccer team
    static const int DEFAULT_PORT = 3838;       // arbitrary default port (used by FIRA)
    static const int PACKET_LENGTH = 65536;     // maximum allowable packet length

    virtual void loadIniSettings(minIni *ini);

    // can a given robot play? (i.e. will the referee allow it?)
    virtual bool canRobotPlay(int team, int id) = 0;
    virtual bool isRobotOnPenalty(int team, int id) = 0;


    virtual int getGameState() = 0;
    virtual int getTimeRemaining() = 0;
    virtual int getKickoffTeam() = 0;

    virtual void processUdpDatagram(char *data, int numBytes) = 0;
    virtual void updateControlData(char *packetData, int numBytes) = 0;
    virtual void updatePlayerData(char *data, int numBytes);

    virtual void transmitState(SoccerPlayer &player);
    virtual RobotStateInformation &getTeamData(int id);

    // print the last packet we received
    virtual void printLastPacket();

    // used for robot-to-robot communication
    static const int PACKET_DEGREE_PRECISION = 100.0;
    static void Point2Packet(Robot::Point3D &position, uint16_t &positionX, uint16_t &positionY, uint16_t &orientation, SoccerField &field);
    static void Packet2Point(uint16_t &positionX, uint16_t &positionY, uint16_t &orientation, Robot::Point3D &position, SoccerField &field);

    static void Point2Packet(Robot::Point2D &point, uint16_t &positionX, uint16_t &positionY, SoccerField &field);
    static void Packet2Point(uint16_t &positionX, uint16_t &positionY, Robot::Point2D &point, SoccerField &field);

    static uint16_t Angle2Packet(double angle);
    static uint16_t Range2Packet(double range);
    static double Packet2Angle(uint16_t angle);
    static double Packet2Range(uint16_t range);

    bool DEBUG_PRINT;

protected:
    int myTeam;

    int portNo;
#ifndef USE_Q_SOCKET
    int sockFD;

    struct sockaddr_in sockAddr;
#else
    QUdpSocket qSocket;
#endif
    pthread_t monitorThreadId;

    static const int BUFFER_LENGTH = 65536;
    char sockBuffer[BUFFER_LENGTH];
    int bufferLength;
    pthread_mutex_t socketBufferLock;

    virtual void transmitPacket(SoccerPacket &packet);
    virtual uint8_t calculateChecksum(uint8_t *data, int numBytes);

    // threaded functions for reading & validating incoming packets
    static void *listenThread(void* arg);

    static std::string gameState2string(int gameState);

    // data recv'd from each robot (including me)
    RobotStateInformation teamData[SocketMonitor::MAX_PLAYERS_PER_TEAM*2];

    SoccerField *field = NULL;
};

#endif // SOCKETMONITOR_H
