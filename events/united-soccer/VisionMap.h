#ifndef VISIONMAP_H
#define VISIONMAP_H

#include <darwin/framework/MultiLine.h>
#include <darwin/framework/MultiBlob.h>
#include <darwin/framework/SingleBlob.h>
#include <GoalTarget.h>
#include <PlayerTarget.h>
#include <vector>
#include <list>
#include <opencv2/opencv.hpp>
#include <darwin/framework/CameraPosition.h>

/* Wrapper class for all vision-handling in the entire robot
 * Looks for all the objects we care about and can display
 * them on a polar map.
 *
 * Used in conjunction with the SoccerField class for
 * particle filter-based localization
 */
class VisionMap
{
public:
    VisionMap();

    static const int IMAGE_SIZE = 500;
    static const int LEFT = 0;
    static const int RIGHT = 1;
    static const int TOP = 0;
    static const int BOTTOM = 1;
    static const int OTHER_PLAYERS_ON_MY_TEAM = 10;
    static const int PLAYERS_ON_THEIR_TEAM = 11;

    static const int MM_PER_PIXEL = 5;

    void LoadIniSettings(minIni *ini, const char *section);
    void ProcessFrame(cv::Mat &frame);

    void AnnotateFrames(std::list<cv::Mat> frames, int playerState);
    void Draw();

    // polar coordinates of the objects of interest
    // x is the range, y is the angle (0 = forward, <0 = left, >0 = right)
    // ranges are in mm
    Robot::Point2D ballPosition;
    std::vector<Robot::Line2D> lines;   // endpoints are polar coordinates
    Robot::Line2D goal;
    std::vector<Robot::Point2D> myTeam;
    std::vector<Robot::Point2D> theirTeam;

    // hacky work-around for some goalie-specific nonsense
    Robot::SingleBlob *getBall(){return &ball;}
    GoalTarget *getGoal(){return &goalTarget;}

    void setSubsample(int x){subsample = x;}

    cv::Mat dbgImage;
    cv::Mat mapImage;

    Robot::CameraPosition cameraPosition;

private:
    GoalTarget goalTarget;
    Robot::SingleBlob ball;
    Robot::MultiLine fieldLines;
    PlayerTarget myTeamPlayers;
    PlayerTarget theirTeamPlayers;

    int runUpDistance;

    std::vector<Robot::Target*> blobTargets;

    int subsample;

    void UpdatePolarCoordinates();
};

#endif // VISIONMAP_H
