#ifndef ROBOCUPSOCKETMONITOR_H
#define ROBOCUPSOCKETMONITOR_H

#include <SocketMonitor.h>
#include <inttypes.h>
#include <SoccerPlayer.h>

class RoboCupSocketMonitor : public SocketMonitor
{
public:
    RoboCupSocketMonitor(SoccerField &field);
    RoboCupSocketMonitor(int portNo,SoccerField &field);
    virtual ~RoboCupSocketMonitor();

    // headers so we can identify packets
    static const char *REFEREE_HEADER;

    static const int PROTOCOL_VERSION = 7;

    struct RobotInfo {
        uint16_t penalty;             // penalty state of the player
        uint16_t secsTillUnpenalised; // estimate of time till unpenalised
    };

    struct TeamInfo {
        uint8_t teamNumber;          // unique team number
        uint8_t teamColour;          // colour of the team
        uint8_t goalColour;          // colour of the goal
        uint8_t score;               // team's score
        RobotInfo players[SocketMonitor::MAX_PLAYERS_PER_TEAM];       // the team's players
    };

    struct RoboCupGameControlData {
        char   header[4];           // header to identify the structure
        uint32_t version;             // version of the data structure
        uint8_t playersPerTeam;       // The number of players on a team
        uint8_t state;                // state of the game (STATE_READY, STATE_PLAYING, etc)
        uint8_t firstHalf;            // 1 = game in first half, 0 otherwise
        uint8_t kickOffTeam;          // the next team to kick off
        uint8_t secondaryState;       // Extra state information - (STATE2_NORMAL, STATE2_PENALTYSHOOT, etc)
        uint8_t dropInTeam;           // team that caused last drop in
        uint16_t dropInTime;          // number of seconds passed since the last drop in.  -1 before first dropin
        uint32_t secsRemaining;       // estimate of number of seconds remaining in the half
        TeamInfo teams[2];
    };

    enum {
        STATE_INITIAL   = 0,
        STATE_READY     = 1,
        STATE_SET       = 2,
        STATE_PLAY      = 3,
        STATE_FINISHED  = 4

    };

    enum {
        NO_PENALTY                              = 0,        // CIB (non-standard, but pretty intuitive)
        PENALTY_BALL_MANIPULATION               = 1,
        PENALTY_PHYSICAL_CONTACT                = 2,
        PENALTY_ILLEGAL_ATTACK                  = 3,
        PENALTY_ILLEGAL_DEFENSE                 = 4,
        PENALTY_REQUEST_FOR_PICKUP              = 4,
        PENALTY_REQUEST_FOR_SERVICE             = 5,
        PENALTY_REQUEST_FOR_PICKUP_2_SERVICE    = 7
    };
    // copied from GameController
    /***************************************************************************************************************/

    static const int PLAYER_INFO_LENGTH = 4;
    static const int TEAM_INFO_LENGTH = 4 + SocketMonitor::MAX_PLAYERS_PER_TEAM * PLAYER_INFO_LENGTH;
    static const int REFEREE_PACKET_LENGTH = 4 +                            // header
                                            16 +                            // GameControlData
                                            2* TEAM_INFO_LENGTH;            // 2x teams

    // can a given robot play? (i.e. will the referee allow it?)
    virtual bool canRobotPlay(int team, int id);
    virtual bool isRobotOnPenalty(int team, int id);

    virtual int getGameState() { return gameControlData.state; }
    virtual int getTimeRemaining() { return gameControlData.secsRemaining; }
    virtual int getKickoffTeam() { return gameControlData.kickOffTeam; }
    virtual bool isFirstHalf() { return gameControlData.firstHalf; }
    virtual bool isSecondtHalf() { return !gameControlData.firstHalf; }

    virtual struct RoboCupGameControlData *getGameControlData() { return &gameControlData; }

    virtual void processUdpDatagram(char *data, int numBytes);
    virtual void updateControlData(char *packetData, int numBytes);

protected:

    struct RoboCupGameControlData gameControlData;
};

#endif // ROBOCUPSOCKETMONITOR_H
