#!/bin/bash

cd /home/darwin/fira/events/united-soccer

until ./united-soccer --goalie; do
    err=$?
    echo "united-soccer crashed with error code $err. Restarting"
    espeak "Ouch! Error code $err"
    sleep 1
done
