#include "VisionMap.h"
#include <SoccerPlayer.h>

#include <iostream>
#include <darwin/framework/Kinematics.h>

using namespace std;
using namespace cv;
using namespace Robot;

VisionMap::VisionMap()
{
    dbgImage = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    mapImage = Mat::zeros(IMAGE_SIZE,IMAGE_SIZE,CV_8UC3);

    blobTargets.push_back(&ball);
    blobTargets.push_back(&myTeamPlayers);
    blobTargets.push_back(&theirTeamPlayers);
}

void VisionMap::LoadIniSettings(minIni *ini, const char *section)
{
    (void)section;

    ball.LoadIniSettings(*ini, ini->gets("Soccer","BallColour","Orange").c_str());
    cout << "Subsample: " << ball.GetSubsample() << endl;
    goalTarget.LoadIniSettings(*ini,"Goal");

    int myColour = ini->gets("Soccer","Colour","red") == "red" ? SoccerPlayer::TEAM_RED : SoccerPlayer::TEAM_BLUE;

    if(myColour == SoccerPlayer::TEAM_RED)
    {
        myTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","RedTeam","Red"));
        theirTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","BlueTeam","Blue"));
    }
    else
    {
        myTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","BlueTeam","Blue"));
        theirTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","RedTeam","Red"));
    }

    cout << "Loading field lines colour profile from section " << ini->gets("Field","LineColour","Green") << endl;
    fieldLines.LoadIniSettings(*ini,ini->gets("Field","LineColour","Green"));

    runUpDistance = ini->geti("Soccer","RunUpDistance",-1);

}

void VisionMap::ProcessFrame(cv::Mat &frame)
{
    // update where the robot's head is based on its body position
    cameraPosition.RecalculatePosition();

    //double pan = cameraPosition.GetCameraPanOffset() + Head::GetInstance()->GetPanAngle();
    double tilt = cameraPosition.GetCameraTiltOffset() + Head::GetInstance()->GetTiltAngle() - Kinematics::EYE_TILT_OFFSET_ANGLE;

    // set the ignoreTop value so we're not looking for the ball or the field lines higher than perfectly horizontal
    double cameraTop = tilt + Camera::VIEW_V_ANGLE/2;
    double horizon = 0;
    if(cameraTop > 0)
    {
        double degreesPerPixel = Camera::VIEW_V_ANGLE/Camera::HEIGHT;
        horizon = cameraTop / degreesPerPixel;
    }
    ball.SetIgnoreTop(horizon);
    fieldLines.SetIgnoreTop(horizon);

    // find the ball and the other robots in the frame
    //BlobTarget::FindTargets(blobTargets, frame, &dbgImage,subsample);
    Target::FastFindTargets(blobTargets,frame,&dbgImage);

    // find the field lines
    fieldLines.FindInFrame(frame);

    // find the goals
    goalTarget.FindInFrame(frame,NULL);

    UpdatePolarCoordinates();
}

void VisionMap::UpdatePolarCoordinates()
{
    if(ball.WasFound())
    {
        ballPosition.X = ball.GetRange();
        ballPosition.Y = ball.GetAngle();
    }
    else
        ballPosition.X = -1;

    if(goalTarget.WasFound())
    {
        if(goalTarget.NumFound() == GoalTarget::FOUND_BOTH)
        {
            goal.start.X = goalTarget.GetLeftPostRange();
            goal.start.Y = goalTarget.GetLeftPostAngle();
            goal.end.X = goalTarget.GetRightPostRange();
            goal.end.Y = goalTarget.GetRightPostAngle();
        }
        else if(goalTarget.NumFound() == GoalTarget::FOUND_LEFT)
        {
            goal.start.X = goalTarget.GetLeftPostRange();
            goal.start.Y = goalTarget.GetLeftPostAngle();
            goal.end.X = -1;
            goal.end.Y = 0;
        }
        else if(goalTarget.NumFound() == GoalTarget::FOUND_RIGHT)
        {
            goal.start.X = -1;
            goal.start.Y = 0;
            goal.end.X = goalTarget.GetRightPostRange();
            goal.end.Y = goalTarget.GetRightPostAngle();
        }
    }
    else
    {
        goal.start.X = -1;
        goal.start.Y = 0;
        goal.end.X = -1;
        goal.end.Y = 0;
    }

    myTeam.clear();
    for(vector<BoundingBox*>::iterator it = myTeamPlayers.GetBoundingBoxes()->begin(); it!=myTeamPlayers.GetBoundingBoxes()->end(); it++)
    {
        myTeam.push_back(Point2D(cameraPosition.CalculateRange(**it),cameraPosition.CalculateAngle(**it)));
    }

    theirTeam.clear();
    for(vector<BoundingBox*>::iterator it = theirTeamPlayers.GetBoundingBoxes()->begin(); it!=theirTeamPlayers.GetBoundingBoxes()->end(); it++)
    {
        theirTeam.push_back(Point2D(cameraPosition.CalculateRange(**it),cameraPosition.CalculateAngle(**it)));
    }

    lines.clear();
    for(vector<Line2D*>::iterator it = fieldLines.GetAllLines()->begin(); it!= fieldLines.GetAllLines()->end(); it++)
    {
        lines.push_back(Line2D(
                            Point2D(
                                cameraPosition.CalculateRange(((Line2D*)*it)->start),
                                cameraPosition.CalculateAngle(((Line2D*)*it)->start)
                            ),
                            Point2D(
                                cameraPosition.CalculateRange(((Line2D*)*it)->end),
                                cameraPosition.CalculateAngle(((Line2D*)*it)->end)
                            )
        ));
    }
}

void VisionMap::AnnotateFrames(std::list<Mat> frames, int myState)
{
    double pan = cameraPosition.GetCameraPanOffset() + Head::GetInstance()->GetPanAngle();
    double tilt = cameraPosition.GetCameraTiltOffset() + Head::GetInstance()->GetTiltAngle() - Kinematics::EYE_TILT_OFFSET_ANGLE;

    for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
        ball.DrawScanRange(*it);

    // draw some diagnostic data on the image
    char buffer[255];
    sprintf(buffer,"Ball: (%0.2f,%0.2f) State: %d Tilt: %0.1f", ballPosition.X, ballPosition.X, myState, tilt);
    cv::Scalar colour = cv::Scalar(255,255,255);
    cv::Point pt = cv::Point(0,Camera::HEIGHT-4);
    for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
        cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

    // write the ranges/angles of the objects we can see
    if(ball.WasFound())
    {
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            ball.Draw(*it);

        sprintf(buffer,"r:%0.2f", ball.GetRange()/10.0);
        pt = cv::Point(ball.GetBoundingBox()->center.X, ball.GetBoundingBox()->center.Y-6);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"a:%0.2f", ball.GetAngle());
        pt = cv::Point(ball.GetBoundingBox()->center.X, ball.GetBoundingBox()->center.Y+6);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);
    }

    if(goalTarget.NumFound() > 0)
    {
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            goalTarget.Draw(*it);

        sprintf(buffer,"r:%0.2f", goalTarget.GetLeftPostRange()/10.0);
        pt = cv::Point(goalTarget.leftPost.end.X-20, goalTarget.leftPost.end.Y);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"a:%0.2f", goalTarget.GetLeftPostAngle());
        pt = cv::Point(goalTarget.leftPost.end.X-20, goalTarget.leftPost.end.Y+12);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"r:%0.2f", goalTarget.GetRightPostRange()/10.0);
        pt = cv::Point(goalTarget.rightPost.end.X-20, goalTarget.rightPost.end.Y);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"a:%0.2f", goalTarget.GetRightPostAngle());
        pt = cv::Point(goalTarget.rightPost.end.X-20, goalTarget.rightPost.end.Y+12);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"r:%0.2f", goalTarget.GetRange()/10.0);
        pt = cv::Point((goalTarget.leftPost.end.X + goalTarget.rightPost.end.X)/2-20, (goalTarget.leftPost.end.Y + goalTarget.rightPost.end.Y)/2);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"a:%0.2f", goalTarget.GetAngle());
        pt = cv::Point((goalTarget.leftPost.end.X + goalTarget.rightPost.end.X)/2-20, (goalTarget.leftPost.end.Y + goalTarget.rightPost.end.Y)/2+12);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);
    }

    // draw where players on the field are
    BoundingBox* bbox;
    for(vector<BoundingBox*>::iterator it=myTeamPlayers.GetBoundingBoxes()->begin(); it!=myTeamPlayers.GetBoundingBoxes()->end(); it++)
    {
        bbox = ((BoundingBox*)(*it));
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            bbox->Draw(*it,*(myTeamPlayers.GetMarkColour()),1);

        sprintf(buffer,"%0.2f",cameraPosition.CalculateRange(*bbox)/10.0);
        pt = cv::Point(bbox->center.X, bbox->center.Y-6);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"%0.2f",cameraPosition.CalculateAngle(*bbox));
        pt = cv::Point(bbox->center.X, bbox->center.Y+6);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);
    }
    for(vector<BoundingBox*>::iterator it=theirTeamPlayers.GetBoundingBoxes()->begin(); it!=theirTeamPlayers.GetBoundingBoxes()->end(); it++)
    {
        bbox = ((BoundingBox*)(*it));
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            bbox->Draw(*it,*(theirTeamPlayers.GetMarkColour()),1);

        sprintf(buffer,"%0.2f",cameraPosition.CalculateRange(*bbox)/10.0);
        pt = cv::Point(bbox->center.X, bbox->center.Y-6);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);

        sprintf(buffer,"%0.2f",cameraPosition.CalculateAngle(*bbox));
        pt = cv::Point(bbox->center.X, bbox->center.Y+6);
        for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
            cv::putText(*it,buffer,pt,FONT_HERSHEY_PLAIN,1.0,colour,1);
    }

    for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
        fieldLines.Draw(*it);


    // draw "crosshairs" that indicate where the head's zero position is
    pan = (pan + 70)/140;
    tilt = (tilt + 75)/150;

    pt = cv::Point(0, tilt * Camera::HEIGHT);
    cv::Point pt2 = cv::Point(Camera::WIDTH, tilt * Camera::HEIGHT);
    for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
        cv::line(*it, pt, pt2, colour);

    pt = cv::Point(pan * Camera::WIDTH, 0);
    pt2 = cv::Point(pan * Camera::WIDTH, Camera::HEIGHT);
    for(std::list<Mat>::iterator it=frames.begin(); it!=frames.end(); it++)
        cv::line(*it, pt, pt2, colour);
}

void VisionMap::Draw()
{
    Point p,q;
    char rangeTxt[8];

    // fade the old image out by 3/4 so we get a trail effect
    IplImage iplImg(mapImage);
    uint8_t *data = (uint8_t*)iplImg.imageData;
    int maxIndex = IMAGE_SIZE * IMAGE_SIZE * mapImage.channels();
    for(int i=0; i < maxIndex; i++)
    {
        data[i] = (uint8_t)(data[i] * 0.75);
    }

    // draw circles indicating 25cm range increments
    p = Point(IMAGE_SIZE/2, IMAGE_SIZE/2);
    for(int r=250; r<=IMAGE_SIZE/2*MM_PER_PIXEL; r+=250)
    {
        q = Point(IMAGE_SIZE/2,IMAGE_SIZE/2-r/MM_PER_PIXEL);
        sprintf(rangeTxt,"%3dcm",r/10);
        putText(mapImage,rangeTxt,q,CV_FONT_HERSHEY_PLAIN,0.5,SoccerField::WHITE);

        circle(mapImage, p, r/MM_PER_PIXEL, SoccerField::WHITE, 1);
    }

    // draw a red circle for the run-up distance (the distance at which orientation ceases to matter and we just b-line it for the ball)
    if(runUpDistance > 0)
    {
        circle(mapImage, p, runUpDistance/MM_PER_PIXEL,SoccerField::RED,1);
    }

    // draw 45-degree lines
    p = Point(0,0);
    q = Point(IMAGE_SIZE,IMAGE_SIZE);
    line(mapImage,p,q,SoccerField::WHITE,1);
    p = Point(0, IMAGE_SIZE/2);
    q = Point(IMAGE_SIZE,IMAGE_SIZE/2);
    line(mapImage,p,q,SoccerField::WHITE,1);
    p = Point(IMAGE_SIZE, 0);
    q = Point(0,IMAGE_SIZE);
    line(mapImage,p,q,SoccerField::WHITE,1);
    p = Point(IMAGE_SIZE/2,0);
    q = Point(IMAGE_SIZE/2,IMAGE_SIZE);
    line(mapImage,p,q,SoccerField::WHITE,1);

    // mark the directions for reference
    p = Point(IMAGE_SIZE/2, 12);
    putText(mapImage, "Front",p,FONT_HERSHEY_PLAIN, 1.0, SoccerField::WHITE, 1);
    p = Point(IMAGE_SIZE/2,IMAGE_SIZE-12);
    putText(mapImage, "Back",p,FONT_HERSHEY_PLAIN, 1.0, SoccerField::WHITE, 1);
    p = Point(12,IMAGE_SIZE/2);
    putText(mapImage, "Left",p,FONT_HERSHEY_PLAIN, 1.0, SoccerField::WHITE, 1);
    p = Point(IMAGE_SIZE-48,IMAGE_SIZE/2);
    putText(mapImage, "Right",p,FONT_HERSHEY_PLAIN, 1.0, SoccerField::WHITE, 1);

    // mark the centre with a circle
    p = Point(IMAGE_SIZE/2, IMAGE_SIZE/2);
    circle(mapImage, p, 3, SoccerField::WHITE, -1);

    // mark the ball in orange
    if(ballPosition.X > 0)
    {
        p = Point(IMAGE_SIZE/2 - ballPosition.X * sin(deg2rad(ballPosition.Y))/MM_PER_PIXEL, IMAGE_SIZE/2 - ballPosition.X * cos(deg2rad(ballPosition.Y))/MM_PER_PIXEL);
        circle(mapImage, p, 5, SoccerField::ORANGE,-1);
    }

    // mark goal posts as hollow yellow circles
    if(goal.start.X > 0)
    {
        p = Point(IMAGE_SIZE/2 - goal.start.X * sin(deg2rad(goal.start.Y))/MM_PER_PIXEL, IMAGE_SIZE/2 - goal.start.X * cos(deg2rad(goal.start.Y))/MM_PER_PIXEL);
        circle(mapImage,p,6,SoccerField::YELLOW,2);
    }
    if(goal.end.X > 0)
    {
        q = Point(IMAGE_SIZE/2 - goal.end.X * sin(deg2rad(goal.end.Y))/MM_PER_PIXEL, IMAGE_SIZE/2 - goal.end.X * cos(deg2rad(goal.end.Y))/MM_PER_PIXEL);
        circle(mapImage,q,6,SoccerField::YELLOW,2);
    }

    // connect-the-dots to draw the plane of our goal
    if(goal.start.X > 0 && goal.end.X > 0)
    {
        line(mapImage, p, q, SoccerField::YELLOW, 3);
    }


    // mark our players as solid green circles
    for(vector<Point2D>::iterator it=myTeam.begin(); it!=myTeam.end(); it++)
    {
        Point2D &x = *it;
        p = Point(IMAGE_SIZE/2 - x.X * sin(deg2rad(x.Y))/MM_PER_PIXEL, IMAGE_SIZE/2 - x.X * cos(deg2rad(x.Y))/MM_PER_PIXEL);
        circle(mapImage,p,4,SoccerField::GREEN,-1);
    }

    // mark their players as solid red circles
    for(vector<Point2D>::iterator it=theirTeam.begin(); it!=theirTeam.end(); it++)
    {
        Point2D &x = *it;
        p = Point(IMAGE_SIZE/2 - x.X * sin(deg2rad(x.Y))/MM_PER_PIXEL, IMAGE_SIZE/2 - x.X * cos(deg2rad(x.Y))/MM_PER_PIXEL);
        circle(mapImage,p,4,SoccerField::RED,-1);
    }

    // draw the field lines in white
    for(vector<Line2D>::iterator it=lines.begin(); it!=lines.end(); it++)
    {
        Line2D &l = *it;
        p = Point(IMAGE_SIZE/2-l.start.X * sin(deg2rad(l.start.Y))/MM_PER_PIXEL, IMAGE_SIZE/2-l.start.X*cos(deg2rad(l.start.Y))/MM_PER_PIXEL);
        q = Point(IMAGE_SIZE/2-l.end.X * sin(deg2rad(l.end.Y))/MM_PER_PIXEL, IMAGE_SIZE/2-l.end.X*cos(deg2rad(l.end.Y))/MM_PER_PIXEL);

        line(mapImage, p, q, SoccerField::WHITE, 3);
    }
}
