#include "SoccerField.h"
#include <iostream>
#include <darwin/framework/Math.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <darwin/framework/Head.h>
#include <cstdio>
#include <SocketMonitor.h>

using namespace Robot;
using namespace cv;
using namespace std;

#define DRAND() ((double)rand()/(double)RAND_MAX)
#define SIGNUM(x) (x < 0 ? -1 : 1)


const Scalar SoccerField::WHITE = Scalar(255,255,255);
const Scalar SoccerField::BLACK = Scalar(0,0,0);
const Scalar SoccerField::GREY = Scalar(128,128,128);
const Scalar SoccerField::BLUE = Scalar(255,0,0);
const Scalar SoccerField::RED = Scalar(0,0,255);
const Scalar SoccerField::YELLOW = Scalar(0,255,255);
const Scalar SoccerField::GREEN = Scalar(0,255,0);
const Scalar SoccerField::ORANGE = Scalar(0,128,255);
const Scalar SoccerField::CYAN = Scalar(255,255,0);
const Scalar SoccerField::MAGENTA = Scalar(255,0,255);
const Scalar SoccerField::DK_GREEN = Scalar(0,128,0);

SoccerField::SoccerField()
{
    presentPosition = Point3D(UNKNOWN,UNKNOWN,UNKNOWN);
    positionConfidence = 0;
    ballConfidence = 0;

    for(int i=0; i<SocketMonitor::MAX_PLAYERS_PER_TEAM-1; i++)
    {
        myTeam.push_back(Point2D(UNKNOWN,UNKNOWN));
    }

    for(int i=0; i<SocketMonitor::MAX_PLAYERS_PER_TEAM; i++)
    {
        theirTeam.push_back(Point2D(UNKNOWN,UNKNOWN));
    }

    ballPosition = Point2D(UNKNOWN,UNKNOWN);
}

SoccerField::~SoccerField()
{
    fieldLines.clear();
    particles.clear();
    myTeam.clear();
    theirTeam.clear();
}

void SoccerField::Initialize(VisionMap *polarMap, minIni &ini)
{
    this->polarMap = polarMap;

    LoadIniSettings(ini,"Field");

    // set up the particle filter
    for(int i=0; i<NUM_PARTICLES; i++)
        particles.push_back(Particle());
    UnknownPosition();

    this->fieldImage = Mat::zeros((fieldLength + 2*goalDepth)/imageScaleFactor, (fieldWidth + 2*marginWidth)/imageScaleFactor, CV_8UC3);

    myGoalLeftPost = Point2D(fieldWidth/2 - goalWidth/2, 0);
    myGoalRightPost = Point2D(fieldWidth/2 + goalWidth/2, 0);
    myGoalCenter = Point2D(fieldWidth/2, 0);
    centre = Point2D(fieldWidth/2, fieldLength/2);
    theirGoalLeftPost = Point2D(fieldWidth/2 - goalWidth/2, fieldLength);
    theirGoalRightPost = Point2D(fieldWidth/2 + goalWidth/2, fieldLength);
    theirGoalCenter = Point2D(fieldWidth/2, fieldLength);

    kickoffPosition = Point2D(centre.X, centre.Y - 30.0);   // 30cm back from the center point

    MakeFieldLines();
}

void SoccerField::SetPosition(Point3D position)
{
    // set all of the particles to the new position
    for(vector<Particle>::iterator it=particles.begin(); it!=particles.end(); it++)
    {
        Particle &p = *it;
        p.position = position;
        p.weight = 1.0/NUM_PARTICLES;
    }
}

void SoccerField::SetCandidatePositions(std::vector<Point3D> &possiblePositions)
{
    int index;
    for(vector<Particle>::iterator it=particles.begin(); it!=particles.end(); it++)
    {
        Particle &p = *it;
        index = (int)(DRAND()*possiblePositions.size());

        p.position = possiblePositions[index];
        p.weight = 1.0/NUM_PARTICLES;
    }
}

void SoccerField::UnknownPosition()
{
    for(vector<Particle>::iterator it=particles.begin(); it!=particles.end(); it++)
    {
        Particle &p = *it;

        p.position = Point3D(DRAND()*fieldWidth, DRAND()*fieldLength, normalizeAngle(DRAND()*360));
        p.weight = 1.0/NUM_PARTICLES;
    }
}

void SoccerField::Draw(bool hideParticles)
{
    Mat fullSize = Mat::zeros(fieldLength + 2*goalDepth, fieldWidth + 2*marginWidth, CV_8UC3);

    const Point ctr = Point(fullSize.cols/2, fullSize.rows/2);
    const Point tl = Point(marginWidth, goalDepth);
    const Point bl = Point(marginWidth, fieldLength + goalDepth);

    Point me, pt;

    // clear the entire field
    rectangle(fullSize,Point(0,0),Point(fullSize.cols, fullSize.rows),BLACK,-1);

    // draw the field lines
    for(vector<Line2D>::iterator it=fieldLines.begin(); it!=fieldLines.end(); it++)
    {
        Line2D &l = *it;

        if(IsVisible(l,this->presentPosition))
            line(fullSize,Point(bl.x + l.start.X,bl.y-l.start.Y),Point(bl.x+l.end.X,bl.y-l.end.Y),WHITE,5);
        else
            line(fullSize,Point(bl.x + l.start.X,bl.y-l.start.Y),Point(bl.x+l.end.X,bl.y-l.end.Y),GREY,5);
    }

    // our goal (at the bottom)
    rectangle(fullSize, Point(ctr.x - goalWidth/2, bl.y), Point(ctr.x + goalWidth/2, bl.y + goalDepth), GREEN, 5);

    // their goal (at the top)
    rectangle(fullSize, Point(ctr.x - goalWidth/2, tl.y), Point(ctr.x + goalWidth/2, tl.y - goalDepth), RED, 5);

    // draw the particle cloud
    // each particle is a small green circle with a line indicating the orientation
    if(!hideParticles)
    {
        for(vector<Particle>::iterator it=particles.begin(); it!=particles.end(); it++)
        {
            Particle &p = *it;
            //cout << i << " " << particles[i].X << " " << particles[i].Y << " " << particles[i].Z << endl;

            me = Point(bl.x + p.position.X, bl.y - p.position.Y);
            circle(fullSize, me, 5,DK_GREEN, -1);

            pt = Point(me.x - sin(deg2rad(p.position.Z)) * 10, me.y - cos(deg2rad(p.position.Z))*10);
            line(fullSize,me,pt,DK_GREEN,2);
        }
    }

    // our position on the field
    me = Point(bl.x + presentPosition.X, bl.y - presentPosition.Y);

    // draw us with a line indicating orientation
    pt = Point(me.x - sin(deg2rad(presentPosition.Z)) * 20, me.y - cos(deg2rad(presentPosition.Z))*20);
    circle(fullSize, me, 15, GREEN, -1);
    line(fullSize, me, pt, GREEN, 4);

    // draw the other players on our team (if we know where they are)
    for(vector<Point2D>::iterator it = myTeam.begin(); it!=myTeam.end(); it++)
    {
        Point2D &p = *it;
        if(p.X != UNKNOWN)
        {
            pt.x = bl.x + p.X;
            pt.y = bl.y + p.Y;

            circle(fullSize, pt, 10, CYAN, -1);
        }
    }

    // draw opposing players
    for(vector<Point2D>::iterator it = theirTeam.begin(); it!=theirTeam.end(); it++)
    {
        Point2D &p = *it;
        if(p.X != UNKNOWN)
        {
            pt.x = bl.x + p.X;
            pt.y = bl.y + p.Y;

            circle(fullSize, pt, 10, RED, -1);
        }
    }

    // draw the ball
    if(ballPosition.X != UNKNOWN && ballConfidence != 0)
    {
        pt = Point(bl.x + ballPosition.X, bl.y - ballPosition.Y);

        circle(fullSize, pt, 8, ORANGE, -1);
    }

    // write our position in the corner
    char buffer[255];
    sprintf(buffer,"Plyr: (%0.2f %0.2f) @ %+0.2f [conf. %d]   Ball: (%0.2f %0.2f) [conf. %d]",
            presentPosition.X, presentPosition.Y, presentPosition.Z, positionConfidence,
            ballPosition.X, ballPosition.Y, ballConfidence
    );
    putText(fullSize, buffer, Point(0, fullSize.rows-12),FONT_HERSHEY_PLAIN, 1.0, WHITE);

    cv::resize(fullSize,fieldImage,cv::Size(fieldImage.cols,fieldImage.rows));
}

void SoccerField::LoadIniSettings(minIni &ini, const char* section)
{
    fieldLength = ini.geti(section,"FieldLength");
    fieldWidth = ini.geti(section,"FieldWidth");
    goalWidth = ini.geti(section,"GoalWidth");
    goalDepth = ini.geti(section,"GoalDepth");
    centreRadius = ini.geti(section,"CentreRadius");
    creaseWidth = ini.geti(section,"CreaseWidth");
    creaseLength = ini.geti(section,"CreaseLength");
    marginWidth = ini.geti(section,"MarginWidth");

    circleApprox = ini.geti(section,"CircleApprox",36);

    // drift margins for the walking module; how much does each amplitude affect the others?
    xGainX = ini.getd("Particle Filter","XgainX",1.0);
    xGainY = ini.getd("Particle Filter","XgainY",0.0);
    xGainA = ini.getd("Particle Filter","XgainA",0.0);
    yGainX = ini.getd("Particle Filter","YgainX",0.0);
    yGainY = ini.getd("Particle Filter","YgainY",1.0);
    yGainA = ini.getd("Particle Filter","YgainA",0.0);
    aGainX = ini.getd("Particle Filter","AgainX",0.0);
    aGainY = ini.getd("Particle Filter","AgainY",0.0);
    aGainA = ini.getd("Particle Filter","AgainA",1.0);

    particleXwander = ini.getd("Particle Filter","Xwander",5.0);
    particleYwander = ini.getd("Particle Filter","Ywander",5.0);
    particleAwander = ini.getd("Particle Filter","Awander",5.0);

    imageScaleFactor = ini.geti(section,"ImageScaleFactor",1);

    NUM_PARTICLES = ini.geti("Particle Filter","NumParticles",DEFAULT_NUM_PARTICLES);

    goalWeightScaling = ini.getd("Particle Filter","GoalWeightScaling",1.0);
    lineWeightScaling = ini.getd("Particle Filter","LineWeightScaling",1.0);

    ballConfidenceDecay = ini.getd("Particle Filter","BallConfidenceDecay",0.5);
}

void SoccerField::MakeFieldLines()
{
    // create all of the lines on the whole field...

    // the outside perimeter
    fieldLines.push_back(Line2D(Point2D(0,0), Point2D(0,fieldLength)));
    fieldLines.push_back(Line2D(Point2D(0,fieldLength),Point2D(fieldWidth,fieldLength)));
    fieldLines.push_back(Line2D(Point2D(fieldWidth,fieldLength),Point2D(fieldWidth,0)));
    fieldLines.push_back(Line2D(Point2D(fieldWidth,0),Point2D(0,0)));

    // the centre line
    fieldLines.push_back(Line2D(Point2D(0,fieldLength/2),Point2D(fieldWidth,fieldLength/2)));

    // our goal crease
    fieldLines.push_back(Line2D(Point2D(fieldWidth/2-creaseWidth/2,creaseLength),
                                    Point2D(fieldWidth/2+creaseWidth/2,creaseLength)));
    fieldLines.push_back(Line2D(Point2D(fieldWidth/2-creaseWidth/2,0),
                                    Point2D(fieldWidth/2-creaseWidth/2,creaseLength)));
    fieldLines.push_back(Line2D(Point2D(fieldWidth/2+creaseWidth/2,0),
                                    Point2D(fieldWidth/2+creaseWidth/2,creaseLength)));

    // their goal crease
    fieldLines.push_back(Line2D(Point2D(fieldWidth/2-creaseWidth/2,fieldLength-creaseLength),
                                    Point2D(fieldWidth/2+creaseWidth/2,fieldLength-creaseLength)));
    fieldLines.push_back(Line2D(Point2D(fieldWidth/2-creaseWidth/2,fieldLength),
                                    Point2D(fieldWidth/2-creaseWidth/2,fieldLength-creaseLength)));
    fieldLines.push_back(Line2D(Point2D(fieldWidth/2+creaseWidth/2,fieldLength),
                                    Point2D(fieldWidth/2+creaseWidth/2,fieldLength-creaseLength)));

    // the centre circle, approximated as a dodecagon
    double startAngle, endAngle;
    startAngle = 0;
    endAngle = 360.0/circleApprox;
    Point2D a,b;
    for(int i=0; i<circleApprox; i++)
    {
        a = Point2D(centre.X + cos(deg2rad(startAngle))*centreRadius,
                    centre.Y + sin(deg2rad(startAngle))*centreRadius);
        b = Point2D(centre.X + cos(deg2rad(endAngle))*centreRadius,
                    centre.Y + sin(deg2rad(endAngle))*centreRadius);

        fieldLines.push_back(Line2D(a,b));

        startAngle = endAngle;
        endAngle += 360.0/circleApprox;
    }
}

void SoccerField::Update(double xAmplitude, double yAmplitude, double aAmplitude)
{
    Point3D bestGuessPosition;

    // move and weigh the particles all in the same step
    double sumWeights = 0;
    for(int i=0; i<NUM_PARTICLES; i++)
    {
        // displace the particle randomly from its new position
        MoveParticle(particles[i],xAmplitude,yAmplitude,aAmplitude);
        particles[i].weight = WeighParticle(particles[i]);
        //cout << i << " " << particles[i].weight << endl;
        sumWeights += particles[i].weight;
    }

    // ensure that the sum of the weights adds up to 1
    for(int i=0; i<NUM_PARTICLES; i++)
        particles[i].weight /= sumWeights;

    // merge the particles based on their weights (i.e. each has a probability of being drawn equal to its weight)
    Point3D newParticles[NUM_PARTICLES];
    for(int i=0; i<NUM_PARTICLES; i++)
    {
        double r = DRAND();
        double sum = particles[0].weight;
        int j;
        for(j=0; j<NUM_PARTICLES-1 && sum<r; j++)
        {
            //cout << j << " " << sum << " <=? " << r << endl;
            sum += particles[j+1].weight;
        }
        assert(j < NUM_PARTICLES);

        newParticles[i] = particles[j].position;

        //cout << i << " " << j << " " << r << " " << weights[j] << " " << newParticles[i].X << " " << newParticles[i].Y << endl;
    }
    for(int i=0; i<NUM_PARTICLES; i++)
    {
        // merge the particle with an existing one
        particles[i].position = newParticles[i];
    }

#if 0
    // perform a weighted-average of all of the particles to guess where we are
    bestGuessPosition = Point3D(0,0,0);
    for(int i=0; i<NUM_PARTICLES; i++)
    {
        bestGuessPosition.X += particles[i]->position.X * particles[i]->weight;
        bestGuessPosition.Y += particles[i]->position.Y * particles[i]->weight;
        bestGuessPosition.Z += particles[i]->position.Z * particles[i]->weight;
    }
    bestGuessPosition.Z = normalizeAngle(bestGuessPosition.Z);
#else
    // use the unweighted average of all particles
    bestGuessPosition = Point3D(0,0,0);
    for(int i=0; i<NUM_PARTICLES; i++)
    {
        bestGuessPosition.X += particles[i].position.X;
        bestGuessPosition.Y += particles[i].position.Y;
        bestGuessPosition.Z += particles[i].position.Z;
    }
    bestGuessPosition /= NUM_PARTICLES;
    bestGuessPosition.Z = normalizeAngle(bestGuessPosition.Z);
#endif

    presentPosition.X = bestGuessPosition.X;
    presentPosition.Y = bestGuessPosition.Y;
    presentPosition.Z = bestGuessPosition.Z;

    // calculate the confidence in this position
    CalculateConfidence();

    // determine the absolute positions of everything else based on where we think we are
    for(unsigned int i=0; i<polarMap->myTeam.size() && i<myTeam.size(); i++)
    {
        Point2D p = polarMap->myTeam[i];
        if(p.X >0)
        {
            double range = p.X/10; //convert to cm
            double angle = normalizeAngle(presentPosition.Z + p.Y);

            myTeam[i].X = presentPosition.X - sin(deg2rad(angle)) * range;
            myTeam[i].Y = presentPosition.Y - cos(deg2rad(angle)) * range;
        }
        else
        {
            myTeam[i].X = UNKNOWN;
            myTeam[i].Y = UNKNOWN;
        }
    }
    for(unsigned int i=0; i<polarMap->theirTeam.size() && i<theirTeam.size(); i++)
    {
        Point2D p = polarMap->theirTeam[i];
        if(p.X >0)
        {
            double range = p.X/10; //convert to cm
            double angle = normalizeAngle(presentPosition.Z + p.Y);

            theirTeam[i].X = presentPosition.X - sin(deg2rad(angle)) * range;
            theirTeam[i].Y = presentPosition.Y - cos(deg2rad(angle)) * range;
        }
        else
        {
            theirTeam[i].X = UNKNOWN;
            theirTeam[i].Y = UNKNOWN;
        }
    }
    if(polarMap->ballPosition.X > 0)
    {
        double rangeToBall = polarMap->ballPosition.X/10.0; // convert from mm to cm

        // absolute angle to the ball from our current position on the field with zero being toward their goal
        double angleToBall = normalizeAngle(polarMap->ballPosition.Y + presentPosition.Z);

        // absolute X position of the ball [X being parallel to the goal line, origin on the left)
        ballPosition.X = presentPosition.X - sin(deg2rad(angleToBall)) * rangeToBall;

        // absolute Y position of the ball [Y being parallel to the sidelines, origin at our own goal)
        ballPosition.Y = presentPosition.Y + cos(deg2rad(angleToBall)) * rangeToBall;

        // our confidence in the ball's position is the same as our confidence in our own position
        ballConfidence = positionConfidence;
    }
    else
    {
        // if we don't see the ball then assume it's wherever we last saw it [i.e. it hasn't moved]
        // indicate that we have less confidence in our estimate of the ball's position
        ballConfidence *= ballConfidenceDecay;
        if(ballConfidence <=1)
        {
            LostBall();
        }
    }
}

void SoccerField::LostBall()
{
    ballConfidence = 0;
    ballPosition.X = UNKNOWN;
    ballPosition.Y = UNKNOWN;
}

void SoccerField::CalculateConfidence()
{
    // find the distance to the single particle that's the furthest away from our estimated position
    double distance = (presentPosition - particles[0].position).Magnitude();
    double d;
    for(int i=1; i<NUM_PARTICLES; i++)
    {
        d = (presentPosition - particles[i].position).Magnitude();

        if(d > distance)
        {
            distance = d;
        }
    }

    // the absolute maximum distance we could have would be the corner-to-corner distance
    const double MAX_DISTANCE = sqrt(fieldWidth*fieldWidth + fieldLength*fieldLength);
    double scale = distance/MAX_DISTANCE;

    positionConfidence = 255 - (int)(255*scale);
}

void SoccerField::MoveParticle(Particle &particle, double xAmplitude, double yAmplitude, double aAmplitude)
{
    // generate random numbers for the X, Y and A change of position for the particle
    double dx = DRAND() * xAmplitude * xGainX + DRAND() * yAmplitude * yGainX + DRAND() * aAmplitude * aGainX + DRAND() * 2 * particleXwander - particleXwander;
    double dy = DRAND() * xAmplitude * xGainY + DRAND() * yAmplitude * yGainY + DRAND() * aAmplitude * aGainY + DRAND() * 2 * particleYwander - particleYwander;
    double da = DRAND() * xAmplitude * xGainA + DRAND() * yAmplitude * yGainA + DRAND() * aAmplitude * aGainA + DRAND() * 2 * particleAwander - particleAwander;

    // dx, dy are in the robot's corrdinate system, but the particle position is in the field coordinate system
    double newDx = dx * cos(deg2rad(particle.position.Z)) + dy * sin(deg2rad(particle.position.Z));
    double newDy = dy * cos(deg2rad(particle.position.Z)) + dx * sin(deg2rad(particle.position.Z));

    //cout << particle.X << " --> " << (particle.X+newDx) << "  " <<
    //        particle.Y << " --> " << (particle.Y+newDy) << "  " <<
    //        particle.Z << " --> " << normalizeAngle(particle.Z+da) << endl;

    particle.position.X += newDx;
    particle.position.Y += newDy;
    particle.position.Z += da;

    particle.position.Z = normalizeAngle(particle.position.Z);
}

// the maximum weight we can assign should be 1.0
// the minimum weight we can assign should be MIN_WEIGHT
double SoccerField::WeighParticle(Particle &particle)
{
    double weight = 0.0;

    Point2D particlePosition(particle.position.X,particle.position.Y);
    Point2D orientationVector(cos(deg2rad(particle.position.Z+90)),sin(deg2rad(particle.position.Z+90)));

    // only bother considering this line if it's actually visible to the robot
    vector<Line2D> viewableLines;
    for(vector<Line2D>::iterator fi=fieldLines.begin(); fi!=fieldLines.end(); fi++)
    {
        Line2D &fieldLine = *fi;

        if(IsVisible(fieldLine,particlePosition,orientationVector))
        {
            viewableLines.push_back(fieldLine);
        }
    }

    // assign a weight to this particle based on how well id describes the lines and goal posts visible
    // in the polar map
    double lineWeight = WeighLines(polarMap->lines, viewableLines, particle.position);


    // assign a weight based on the presence/absence of a goal post
    double goalWeight = WeighGoalPost(polarMap->goal.start, myGoalLeftPost, theirGoalLeftPost,particlePosition,orientationVector) +
                        WeighGoalPost(polarMap->goal.end, myGoalRightPost, theirGoalRightPost,particlePosition,orientationVector);

    //cout << goalWeight << " " << lineWeight << endl;

    //printf("%+03.2f %+03.2f %+03.2f\tGoal: %0.2f Lines: %0.2f\n",particle.X, particle.Y, particle.Z, goalWeight, lineWeight);
    weight = lineWeight + goalWeight;


    if(weight <MIN_WEIGHT)
        weight = MIN_WEIGHT;

    particle.weight = weight;

    return weight;
}

double SoccerField::WeighGoalPost(Point2D &polarPost, Point2D& myPost, Point2D& theirPost, Point2D &position, Point2D &orientationVector)
{
    double weight;

    bool myPostVisible = IsVisible(myPost,position,orientationVector);
    bool theirPostVisible = IsVisible(theirPost,position,orientationVector);

    if(polarPost.X > 0) // we did actually see a goal post
    {
        // convert the angular coordinates so they match with the field's cartesean ones
        double phi = deg2rad(polarPost.Y + 90);
        Point2D viewedPost = Point2D(cos(phi)*polarPost.X/10, sin(phi)*polarPost.X/10); // convert range from mm to cm

        if(myPostVisible)
        {
            weight = WeighGoalPost(viewedPost,myPost,position,orientationVector);
        }
        else if(theirPostVisible)
        {
            weight = WeighGoalPost(viewedPost,theirPost,position,orientationVector);
        }
        else
        {
            // we saw a left post, but neither should be visible right now
            weight = MIN_WEIGHT;
        }
    }
    else
    {
        if(myPostVisible || theirPostVisible)
        {
            // we should have seen a post but didn't
            // TODO: assign a flexible weight based on the range to the post?
            weight = MIN_WEIGHT;
        }
        else
        {
            // we didn't see a post nor should we have
            weight = MAX_WEIGHT;
        }
    }

    return weight;
}

double SoccerField::WeighGoalPost(Point2D &seenPost, Point2D visiblePost, Point2D &position, Point2D &orientationVector)
{
    (void)position;
    (void)orientationVector;

    double rangeErr = fabs(seenPost.X - visiblePost.X);
    double angleErr = fabs(seenPost.Y - visiblePost.Y);

    double weight = 1/(rangeErr < 1 ? 1 : rangeErr) +
                    1/(angleErr < 1 ? 1 : angleErr);

    return weight * goalWeightScaling;
}

double SoccerField::WeighLines(std::vector<Line2D> &seenLines, std::vector<Line2D> &visibleLines, Point3D &position)
{
    double weight = 0.0;

#if 0

    // convert all of the viewed lines into the same coordinate system as the field
    Point2D a,b;
    vector<Line2D> projectedLines;
    for(vector<Line2D>::iterator vi = seenLines.begin(); vi!=seenLines.end(); vi++)
    {
        Line2D vl = Line2D(*vi);   // viewed line

        // convert the polar range from mm to cm
        vl.start.X /= 10;
        vl.end.X /= 10;

        double phi = deg2rad(vl.start.Y + 90);  // rotate the angular coordinates by 90 degrees since we're converting to std cartesean
        a = Point2D(cos(phi)*(vl.start.X),sin(phi)*(vl.start.X));

        phi = deg2rad(vl.end.Y+90);
        b = Point2D(cos(phi)*(vl.end.X),sin(phi)*(vl.end.X));

        // create a nice cartesean line to use instead of the polar one
        projectedLines.push_back(Line2D(a,b));
    }

    weight = (WeighLinesByExplanation(visibleLines,projectedLines,position, orientationVector) +
              WeighLinesByObservation(visibleLines,projectedLines,position,orientationVector)) /2.0;

    //if(visibleLines.size() != 0)
    //    weight /= visibleLines.size();

#else

    Point2D position2D(position.X,position.Y);

    // convert the potentially-visible field lines into polar coordinates (mm,deg)
    vector<Line2D> polarLines;
    for(vector<Line2D>::iterator it=visibleLines.begin(); it!=visibleLines.end(); it++)
    {
        Line2D &l = *it;
        Point2D start,end;

        start = Point2D(
                    (l.start - position2D*10).Magnitude(),
                    GetAngleToPoint(l.start,position)
        );
        end = Point2D(
                    (l.end - position2D*10).Magnitude(),
                    GetAngleToPoint(l.end,position)
        );

        polarLines.push_back(Line2D(start,end));
    }

    weight = (WeighLinesByExplanation(polarLines,seenLines,position) + WeighLinesByObservation(polarLines,seenLines,position))/2.0;

#endif

    return weight;
}

// for each line we did see find out how well it is explained by a line we *could* see
// The weight is the [min(angles)/max(angles) + min(closest distance)/max(closest distance)]/2 * 1/n | n=numer of possibly viewable lines
// If all lines match perfectly with a visible line we should get 1.0 (but realistically we never will)
// Note that we assume we will always have extra noise lines; the hough detection used by MultiLine doesn't always merge lines
// well, and we may have lines that are co-linear but disconnected
double SoccerField::WeighLinesByExplanation(std::vector<Robot::Line2D> &visibleFieldLines, std::vector<Robot::Line2D> &observedLines, Robot::Point3D &position)
{
#if 0
    (void)orientationVector;

    double weight = MIN_WEIGHT;

    double bestWeight = MIN_WEIGHT;
    double newWeight = 0;
    for(vector<Line2D>::iterator vi = observedLines.begin(); vi!=observedLines.end(); vi++)
    {
        newWeight = 0.0;
        bestWeight = MIN_WEIGHT;

        Line2D &vl = *vi;   // viewed line

        double dV = MinDistance(vl, position)/fieldLength;
        double thetaV = rad2deg(atan2(vl.start.Y-vl.end.Y, vl.start.X-vl.end.X))/360;

        for(vector<Line2D>::iterator li=visibleFieldLines.begin(); li!=visibleFieldLines.end(); li++)
        {
            Line2D &fl = *li;   // field line


            double dF = MinDistance(fl, position)/fieldLength;
            double thetaF = rad2deg(atan2(fl.start.Y-fl.end.Y, fl.start.X-fl.end.X))/360;

            newWeight = MIN(dF,dV)/MAX(dF,dV)/2.0 + MIN(thetaF,thetaV)/MAX(thetaF,thetaV)/2.0;

            if(newWeight > bestWeight)
                bestWeight = newWeight;
        }

        weight += bestWeight;
    }

    if(observedLines.size() > 0)
        weight /= observedLines.size();

    return weight;
#else

    (void)position;

    double weight = MIN_WEIGHT;

    double bestWeight = MIN_WEIGHT;
    double newWeight = 0;
    for(vector<Line2D>::iterator vi = observedLines.begin(); vi!=observedLines.end(); vi++)
    {
        newWeight = 0.0;
        bestWeight = MIN_WEIGHT;

        Line2D &vl = *vi;   // viewed line
        Line2D vCartesean = Line2D(
                    Point2D(
                        vl.start.X * cos(rad2deg(vl.start.Y)),
                        vl.start.X * sin(rad2deg(vl.start.Y))),
                    Point2D(
                        vl.end.X * cos(rad2deg(vl.end.Y)),
                        vl.end.X * sin(rad2deg(vl.end.Y)))
                    );

        double vSlope = rad2deg(atan2(vCartesean.start.Y - vCartesean.end.Y, vCartesean.start.X - vCartesean.end.X));

        for(vector<Line2D>::iterator li=visibleFieldLines.begin(); li!=visibleFieldLines.end(); li++)
        {
            Line2D &fl = *li;   // field line
            Line2D fCartesean = Line2D(
                        Point2D(
                            fl.start.X * cos(rad2deg(fl.start.Y)),
                            fl.start.X * sin(rad2deg(fl.start.Y))),
                        Point2D(
                            fl.end.X * cos(rad2deg(fl.end.Y)),
                            fl.end.X * sin(rad2deg(fl.end.Y)))
                        );
            double fSlope = rad2deg(atan2(fCartesean.start.Y - fCartesean.end.Y, fCartesean.start.X - fCartesean.end.X));
            newWeight = 1/fabs(vSlope - fSlope);

            double minDistance = MinDistance(fCartesean,vCartesean.start);
            newWeight += 1/(minDistance < 0 ? 1 : minDistance);

            minDistance = MinDistance(fCartesean,vCartesean.end);
            newWeight += 1/(minDistance < 0 ? 1 : minDistance);


            if(newWeight > bestWeight)
                bestWeight = newWeight;
        }

        //cout << "Best weight by explanation: " << bestWeight << endl;

        weight += bestWeight * lineWeightScaling;
    }

    if(observedLines.size() > 0)
        weight /= observedLines.size();

    return weight;

#endif
}

// For each field line we could conceivably see find the viewed line that best
// describes it.  The weight is the [min(angles)/max(angles) + min(closest distance)/max(closest distance)]/2 * 1/n | n=numer of possibly viewable lines
// If all lines match perfectly with a visible line we should get 1.0 (but realistically we never will)
// Note that we assume we will always have extra noise lines; the hough detection used by MultiLine doesn't always merge lines
// well, and we may have lines that are co-linear but disconnected
double SoccerField::WeighLinesByObservation(std::vector<Robot::Line2D> &visibleFieldLines, std::vector<Robot::Line2D> &observedLines, Robot::Point3D &position)
{
#if 0
    (void)orientationVector;

    double weight = 0;
    double bestWeight = 0;
    double newWeight = 0;
    for(vector<Line2D>::iterator li=visibleFieldLines.begin(); li!=visibleFieldLines.end(); li++)
    {
        newWeight = 0.0;
        bestWeight = MIN_WEIGHT;

        Line2D fl = *li;   // field line

        double dF = MinDistance(fl, position)/fieldLength;
        double thetaF = rad2deg(atan2(fl.start.Y-fl.end.Y, fl.start.X-fl.end.X))/360;

        for(vector<Line2D>::iterator vi = observedLines.begin(); vi!=observedLines.end(); vi++)
        {
            Line2D vl = *vi;   // viewed line

            double dV = MinDistance(vl, position)/fieldLength;
            double thetaV = rad2deg(atan2(vl.start.Y-vl.end.Y, vl.start.X-vl.end.X))/360;

            newWeight = MIN(dF,dV)/MAX(dF,dV)/2.0 + MIN(thetaF,thetaV)/MAX(thetaF,thetaV)/2.0;

            if(newWeight > bestWeight)
                bestWeight = newWeight;
        }

        weight += bestWeight;
    }

    if(visibleFieldLines.size() > 0)
        weight /= visibleFieldLines.size();

    return weight;
#else

    (void)position;

    double weight = MIN_WEIGHT;

    double bestWeight = MIN_WEIGHT;
    double newWeight = 0;
    for(vector<Line2D>::iterator li=visibleFieldLines.begin(); li!=visibleFieldLines.end(); li++)
    {
        newWeight = 0.0;
        bestWeight = MIN_WEIGHT;

        Line2D &fl = *li;   // viewed line
        Line2D fCartesean = Line2D(
                    Point2D(
                        fl.start.X * cos(rad2deg(fl.start.Y)),
                        fl.start.X * sin(rad2deg(fl.start.Y))),
                    Point2D(
                        fl.end.X * cos(rad2deg(fl.end.Y)),
                        fl.end.X * sin(rad2deg(fl.end.Y)))
                    );

        double fSlope = rad2deg(atan2(fCartesean.start.Y - fCartesean.end.Y, fCartesean.start.X - fCartesean.end.X));

        for(vector<Line2D>::iterator vi = observedLines.begin(); vi!=observedLines.end(); vi++)
        {
            Line2D &vl = *vi;   // viewed line
            Line2D vCartesean = Line2D(
                        Point2D(
                            vl.start.X * cos(rad2deg(vl.start.Y)),
                            vl.start.X * sin(rad2deg(vl.start.Y))),
                        Point2D(
                            vl.end.X * cos(rad2deg(vl.end.Y)),
                            vl.end.X * sin(rad2deg(vl.end.Y)))
                        );

            double vSlope = rad2deg(atan2(vCartesean.start.Y - vCartesean.end.Y, vCartesean.start.X - vCartesean.end.X));
            newWeight = 1/fabs(vSlope - fSlope);

            double minDistance = MinDistance(fCartesean,vCartesean.start);
            newWeight += 1/(minDistance < 0 ? 1 : minDistance);

            minDistance = MinDistance(fCartesean,vCartesean.end);
            newWeight += 1/(minDistance < 0 ? 1 : minDistance);


            if(newWeight > bestWeight)
                bestWeight = newWeight * lineWeightScaling;
        }

        //cout << "Best weight by observation: " << bestWeight << endl;

        weight += bestWeight;
    }

    if(observedLines.size() > 0)
        weight /= observedLines.size();

    return weight;

#endif
}

double SoccerField::MinDistance(Line2D &line, Point2D &point)
{
    // re-write the line as Ax + By + C = 0
    double a, b, c;

    a = line.start.Y-line.end.Y;
    b = line.end.X-line.start.X;
    c = line.start.X*line.end.Y - line.end.X*line.start.Y;

    double distance = fabs(a*point.X + b*point.Y + c)/sqrt(a*a + b*b);

    return distance;
}

bool SoccerField::IsVisible(Line2D &line, Point2D &position, Point2D &orientationVector)
{
    // is a line on the field visible from a given position and orientation?
    // if it is then the angle between the orientation and the endpoint in question should be within
    // the camera's field of view, offset by the head's pan angle
    // or both endpoints should lie outside the fov, but in opposite directions

    Point2D vStart = line.start - position;
    vStart /= vStart.Magnitude();

    Point2D vEnd = line.end - position;
    vEnd /= vEnd.Magnitude();

    // calculate the angle between the point in question and the orientation
    double startAngle = rad2deg(acos(orientationVector.Dot(vStart))) * SIGNUM(orientationVector.Cross(vStart).Z);
    startAngle += Head::GetInstance()->GetPanAngle();
    startAngle = normalizeAngle(startAngle);

    double endAngle = rad2deg(acos(orientationVector.Dot(vEnd))) * SIGNUM(orientationVector.Cross(vEnd).Z);
    endAngle += Head::GetInstance()->GetPanAngle();
    endAngle = normalizeAngle(endAngle);

    // the line is in front of us if the sum of the angles to the endpoints forms a triangle to the front
    // if this sum is >180 then the triangle is to the back
    bool front = fabs(startAngle)+fabs(endAngle) <= 180;

    bool visible = (startAngle >= -Camera::VIEW_H_ANGLE/2 && startAngle <= Camera::VIEW_H_ANGLE/2) ||           // one endpoint is visible
                   (endAngle >= -Camera::VIEW_H_ANGLE/2 && endAngle <= Camera::VIEW_H_ANGLE/2) ||               // the other endpoint is visible
                   (front && startAngle <= -Camera::VIEW_H_ANGLE/2 && endAngle >= Camera::VIEW_H_ANGLE/2) ||    // the endpoints span the FoV
                   (front && startAngle >= Camera::VIEW_H_ANGLE/2 && endAngle <= -Camera::VIEW_H_ANGLE/2);

    return visible;
}

bool SoccerField::IsVisible(Line2D &line, Point3D &position)
{
    Point2D pos2d(position.X,position.Y);
    Point2D orientationVector(cos(deg2rad(position.Z+90)),sin(deg2rad(position.Z+90)));

    return IsVisible(line,pos2d,orientationVector);
}

bool SoccerField::IsVisible(Point2D &point, Point2D &position, Point2D &orientationVector)
{
    Point2D displacement = point - position;
    displacement /= displacement.Magnitude();

    // calculate the angle between the point in question and the orientation
    double angle = rad2deg(acos(orientationVector.Dot(displacement)));
    angle += Head::GetInstance()->GetPanAngle();
    angle = normalizeAngle(angle);

    return angle >= -Camera::VIEW_H_ANGLE/2 && angle <= Camera::VIEW_H_ANGLE/2;
}

double SoccerField::normalizeAngle(double degrees)
{
    if(-180 <= degrees && 180 >= degrees)
        return degrees;

    while(degrees < -180)
    {
        degrees += 360;
    }
    while(degrees > 180)
    {
        degrees -= 360;
    }

    return degrees;
}

int SoccerField::GetCurrentQuadrant()
{
    if(presentPosition.X <= fieldWidth/2 && presentPosition.Y <= fieldLength/2)
        return QUADRANT_DEFENCE_LEFT;
    else if(presentPosition.X > fieldWidth/2 && presentPosition.Y <= fieldLength/2)
        return QUADRANT_DEFENCE_RIGHT;
    else if(presentPosition.X <= fieldWidth/2 && presentPosition.Y > fieldLength/2)
        return QUADRANT_OFFENCE_LEFT;
    else
        return QUADRANT_OFFENCE_RIGHT;
}

int SoccerField::GetQuadrantFacing()
{
    switch(GetCurrentQuadrant())
    {
    case SoccerField::QUADRANT_DEFENCE_LEFT:
        if(presentPosition.Z <= 45 && presentPosition.Z > -45) // 90-degree arc to the front
            return QUADRANT_OFFENCE_LEFT;
        else if(presentPosition.Z <= -45 && presentPosition.Z >= -135) // 90-degree arc to the right
            return QUADRANT_DEFENCE_RIGHT;
        else
            return QUADRANT_DEFENCE_LEFT;

    case SoccerField::QUADRANT_DEFENCE_RIGHT:
        if(presentPosition.Z <= 45 && presentPosition.Z >= -45) // 90-degree arc to the front
            return QUADRANT_OFFENCE_RIGHT;
        else if(presentPosition.Z >= 45 && presentPosition.Z <= 135) // 90-degree arc to the leftZ
            return QUADRANT_DEFENCE_LEFT;
        else
            return QUADRANT_DEFENCE_RIGHT;

    case SoccerField::QUADRANT_OFFENCE_LEFT:
        if(presentPosition.Z >= 135 || presentPosition.Z <= -135) // 90-degree arc to the back
            return QUADRANT_DEFENCE_LEFT;
        else if(presentPosition.Z <= -45 && presentPosition.Z >= -135) // 90-degree arc to the right
            return QUADRANT_OFFENCE_RIGHT;
        else
            return QUADRANT_OFFENCE_LEFT;

    case SoccerField::QUADRANT_OFFENCE_RIGHT:
        if(presentPosition.Z >= 135 || presentPosition.Z <= -135) // 90-degree arc to the back
            return QUADRANT_DEFENCE_RIGHT;
        else if(presentPosition.Z >= 45 && presentPosition.Z <= 135) // 90-degree arc to the leftZ
            return QUADRANT_OFFENCE_LEFT;
        else
            return QUADRANT_OFFENCE_RIGHT;

    default:
        return GetCurrentQuadrant();
    }

}

double SoccerField::GetAngleToPoint(Point2D point)
{
    return GetAngleToPoint(point,presentPosition);
}

double SoccerField::GetAngleToPoint(Point2D point, Point3D position)
{
    Point2D pos2d(position.X,position.Y);
    Point2D diff = point - pos2d;

    double angle = rad2deg(atan2(diff.Y,diff.X)); // the the real cartesean angle

    angle -= 90; // rotate to the field's angular coordinate system

    angle -= position.Z; // offset by whatever our real orientation is

    angle = normalizeAngle(angle);

    return angle;
}

double SoccerField::GetRangeToPoint(Point2D point)
{
    return GetRangeToPoint(point,presentPosition);
}

double SoccerField::GetRangeToPoint(Point2D point, Point3D position)
{
    Point2D pos2d(position.X,position.Y);
    return (point-pos2d).Magnitude();
}
