#ifndef THREADPAUSER_H
#define THREADPAUSER_H

#include <pthread.h>
using namespace std;

class ThreadPauser
{
public:
    virtual ~ThreadPauser();
    ThreadPauser();

    //accessors
    bool getHeadReacted();
    bool getBodyReacted();

    //called when there is a new frame ready
    void newFrame();

    //called when the head has reacted to the current frame
    void headHasReacted();

    //called when the rest of the body has reacted to the current frame
    void bodyHasReacted();

    //wait for a new frame
    void threadWait();

    //wait for a new frame if the head has already reacted
    void threadWaitHead();

    //wait for a new frame if the body has already reacted
    void threadWaitBody();

    //called when a new frame is available - unblocks all waiting threads
    void threadBroadcast();

    // pause the thread by yielding
    void pauseThread();

    // busy waits for some fraction of a second (generally enough time for motors to catch up)
    void waitAMoment();

#if 0       // THIS FUNCTION IS BROKEN -- DO NOT USE
    //sleep some number of miliseconds
    void milisleep(int milisec);
#endif

private:
    pthread_cond_t condVar;
    pthread_mutex_t condMutex;
    bool headReacted;
    bool bodyReacted;
};

#endif
