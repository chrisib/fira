#include "GoalPostTracker.h"
#include "util.h"

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <darwin/framework/Math.h>

#include <iostream>
#include <vector>

using namespace cv;
using namespace std;

// names of the windows for training/tracking
const char *GoalPostTracker::TRAINING_WINDOW = "GPT Training";
const char *GoalPostTracker::THRESHOLD_WINDOW = "GPT Threshold Result";
const char *GoalPostTracker::LINES_WINDOW = "GPT Hough Result";
const char *GoalPostTracker::RESULT_WINDOW = "GPT Final Result";

// number maximum goal post positions to average
const int GoalPostTracker::GOAL_POST_BUFFER_SIZE = 4;
const int GoalPostTracker::GOAL_POST_AVERAGE_SIZE = 4;

// singleton instance of the goal post tracker
GoalPostTracker *GoalPostTracker::uniqueInstance = new GoalPostTracker();

// private, forcing us to use a singleton approach
GoalPostTracker::GoalPostTracker()
{

    unTrain();

    houghMinVotes = 30;//90;
    houghMinLength = 55;//70;
    houghMaxGap = 100;//50;

    leftPosts = new Vec4i[GOAL_POST_BUFFER_SIZE];
    rightPosts = new Vec4i[GOAL_POST_BUFFER_SIZE];
    bufferPos = 0;

}

// destructor
GoalPostTracker::~GoalPostTracker()
{
    cout << "GoalPostTracker: Closing Tracking Windows" << endl;

    closeTrackingWindows();

    uniqueInstance = NULL;

    delete[] leftPosts;
    delete[] rightPosts;
}

// widen the color-tracking range based on the given color sample
void GoalPostTracker::train(Vec3i color)
{
    int i;

    for(i = 0; i < 3; i ++)
    {
        minTrackColor[i] = Util::min(minTrackColor[i], color[i]);
        maxTrackColor[i] = Util::max(maxTrackColor[i], color[i]);
    }
}

// erase color-tracking range
void GoalPostTracker::unTrain()
{
    minTrackColor = Vec3i(255, 255, 255);
    maxTrackColor = Vec3i(0, 0, 0);
}

// pass in a frame for the high gui windows and the goal post detection
void GoalPostTracker::update(Mat &frame)
{
    frame.copyTo(originalFrame);
    cvtColor(frame, hsv, CV_BGR2HSV);
}

// returns the last frame passed in
void GoalPostTracker::getOriginalFrame(Mat &frame)
{
    originalFrame.copyTo(frame);
}

// returns the HSV equivalent of the last frame passed in
void GoalPostTracker::getHSVFrame(Mat &frame)
{
    hsv.copyTo(frame);
}

// attempts to locate the goal posts in the given image matrix; returns
// true if found, and updates the location of the posts internally, which
// can be accessed by the get() functions below
bool GoalPostTracker::findGoalPosts()
{

    const Scalar HOUGH_LINE_COLOR(0, 0, 255);		// red
    const Scalar MERGED_LINE_COLOR(0, 255, 255);	// yellow
    const Scalar LEFT_POST_COLOR(255, 255, 0);		// cyan
    const Scalar RIGHT_POST_COLOR(0, 255, 0);		// green

    const int CANNY_THRESHOLD1 = 160;
    const int CANNY_THRESHOLD2 = 200;

    vector<Vec4i> lines;				// initial group of lines found by Hough line detector
    vector<Vec4i> merged;				// group of lines after a narrow merge pass
    vector<Vec4i> mergedAgain;			// group of lines after a second, wider merge pass

    float slope1;						// slope of lines that are reasonable goal post candidates
    float slope2;

    Vec4i left;							// lines representing the left and right goal posts
    Vec4i right;

    unsigned int i;
    bool result = false;				// assume we didn't find the goal posts

    // used training color data to threshold the image, so we are (hopefully)
    // only seeing colors that correspond to the posts
    inRange(hsv, Scalar(minTrackColor[0], minTrackColor[1], minTrackColor[2]),
            Scalar(maxTrackColor[0] + 1, maxTrackColor[1] + 1, maxTrackColor[2] + 1), thresholdFrame);

    //cvtColor(hsv, thresholdFrame, CV_HSV2RGB);
    //cvtColor(thresholdFrame, thresholdFrame, CV_RGB2GRAY);

    // eliminate obvious noise
    erode(thresholdFrame, thresholdFrame, 5);
    dilate(thresholdFrame, thresholdFrame, 9);

    // perform canny edge detection on our thresholded image
    //Canny(thresholdFrame, houghFrame, CANNY_THRESHOLD1, CANNY_THRESHOLD2, 3);
    cvtColor(thresholdFrame, houghFrame, CV_GRAY2BGR);

    // perform probabilistic hough-space line detection
    HoughLinesP(thresholdFrame, lines, 1, CV_PI/180, houghMinVotes, houghMinLength, houghMaxGap);

    // perform two passes of the line merge algorithm at different tolerances
    // to ensure we adequately group together the lines of the goalposts
    mergeLines(lines, merged, 10, 0);
    mergeLines(merged, mergedAgain, 40, 0);

    // ensure that the quality of detected lines is high enough that they may actually be goal posts
    if(voteOnGoalPosts(mergedAgain, left, right, slope1, slope2))
    {

        // assign the found corners internally
        topLeft[0] = left[0];
        topLeft[1] = left[1];
        bottomLeft[0] = left[2];
        bottomLeft[1] = left[3];

        // assign the found corners internally
        topRight[0] = right[0];
        topRight[1] = right[1];
        bottomRight[0] = right[2];
        bottomRight[1] = right[3];

        // save the goal posts in a buffer
        leftPosts[bufferPos][0] = topLeft[0];
        leftPosts[bufferPos][1] = topLeft[1];
        leftPosts[bufferPos][2] = bottomLeft[0];
        leftPosts[bufferPos][3] = bottomLeft[1];
        rightPosts[bufferPos][0] = topRight[0];
        rightPosts[bufferPos][1] = topRight[1];
        rightPosts[bufferPos][2] = bottomRight[0];
        rightPosts[bufferPos][3] = bottomRight[1];
        bufferPos = (bufferPos + 1) % GOAL_POST_BUFFER_SIZE;

        result = true;

    }

    // update the windows with the tracking data
    if(windowsOpen)
    {

        // draw the groups of lines we've detected

        for(i = 0; i < lines.size(); i++ )
        {
            line(houghFrame, Point(lines[i][0], lines[i][1]),
                 Point(lines[i][2], lines[i][3]), HOUGH_LINE_COLOR, 1, CV_AA);
        }

        for(i = 0; i < mergedAgain.size(); i++ )
        {
            line(houghFrame, Point(mergedAgain[i][0], mergedAgain[i][1]),
                 Point(mergedAgain[i][2], mergedAgain[i][3]), MERGED_LINE_COLOR, 3, CV_AA);
        }

        // copy the original frame to our final frame for goal-post labelling, regardless of whether or not
        // the goalposts were found
        originalFrame.copyTo(finFrame);

        // draw the goal posts on the final frame, if we've found them
        if(result)
        {

            left = getLeftPostAverage();
            right = getRightPostAverage();

            line(finFrame, Point(left[0], left[1]), Point(left[2], left[3]), LEFT_POST_COLOR, 2, CV_AA);
            line(finFrame, Point(right[0], right[1]), Point(right[2], right[3]), RIGHT_POST_COLOR, 2, CV_AA);

            circle(finFrame, Point(left[0], left[1]), 7, LEFT_POST_COLOR, 3);		// top left
            circle(finFrame, Point(left[2], left[3]), 4, LEFT_POST_COLOR, 2);		// bottom left
            circle(finFrame, Point(right[0], right[1]), 7, RIGHT_POST_COLOR, 3);		// top right
            circle(finFrame, Point(right[2], right[3]), 4, RIGHT_POST_COLOR, 2);		// bottom right

        }

        // update window contents
        imshow(TRAINING_WINDOW, originalFrame);
        imshow(THRESHOLD_WINDOW, thresholdFrame);
        imshow(LINES_WINDOW, houghFrame);
        imshow(RESULT_WINDOW, finFrame);

    }

    return result;

}

// opens training windows, and a window that labels the locations of the left and right goal posts
void GoalPostTracker::openTrackingWindows()
{

    cout << "GoalPostTracker: Opening Tracking Windows" << endl;

    namedWindow(TRAINING_WINDOW, 1);
    namedWindow(THRESHOLD_WINDOW, 1);
    namedWindow(LINES_WINDOW, 1);
    namedWindow(RESULT_WINDOW, 1);

    // only the first window is responsible for training
    cvCreateTrackbar("min votes", TRAINING_WINDOW, &houghMinVotes, 250, NULL);
    cvCreateTrackbar("min length", TRAINING_WINDOW, &houghMinLength, 250, NULL);
    cvCreateTrackbar("max gap", TRAINING_WINDOW, &houghMaxGap, 250, NULL);
    setMouseCallback(TRAINING_WINDOW, trainMouseEvent, 0);

    windowsOpen = true;

}

// closes the training windows, and the window that labels the locations of the left and right goal posts
void GoalPostTracker::closeTrackingWindows()
{
    destroyWindow(TRAINING_WINDOW);
    destroyWindow(THRESHOLD_WINDOW);
    destroyWindow(LINES_WINDOW);
    destroyWindow(RESULT_WINDOW);
}

// returns the vertices associated with the corners of the goal posts (ie,
// top-left corner, etc.)
// if findGoalPosts() was not successful, then these methods return the last
// known location of the posts, even if this information is out-of-date
// NOTE: these returned values are immediate; they are not averaged or weighted

Vec2i GoalPostTracker::getTopLeft()
{
    return topLeft;
}

Vec2i GoalPostTracker::getBottomLeft()
{
    return bottomLeft;
}

Vec2i GoalPostTracker::getTopRight()
{
    return topRight;
}

Vec2i GoalPostTracker::getBottomRight()
{
    return bottomRight;
}

// returns the (top-down) lines associated with the goal posts
// NOTE: these returned values are averages of the highest
// GOAL_POST_AVERAGE_SIZE lengths in the leftPosts and rightPosts

Vec4i GoalPostTracker::getLeftPostAverage()
{
    return getGoalPostAverage(leftPosts);
}

Vec4i GoalPostTracker::getRightPostAverage()
{
    return getGoalPostAverage(rightPosts);
}

Vec4i GoalPostTracker::getGoalPostAverage(Vec4i *postBuffer)
{

    Vec4i average(0, 0, 0, 0);
    int i;

    // we only want to average the first GOAL_POST_AVERAGE_SIZE longest
    // lines, so sort them first
    //sortLines(postBuffer, GOAL_POST_BUFFER_SIZE);
    for(i = 0; i < GOAL_POST_AVERAGE_SIZE; i ++)
    {
        average += postBuffer[i];
    }

    for(i = 0; i < 4; i ++)
    {
        average[i] /= GOAL_POST_AVERAGE_SIZE;
    }

    return average;

}

void GoalPostTracker::sortLines(Vec4i *arr, int length)
{
    Vec4i tmp;
    int i, j;
    for (i = 1; i < length; i++)
    {
        j = i;
        while (j > 0 && Util::dist(arr[j - 1]) > Util::dist(arr[j]))
        {
            tmp = arr[j];
            arr[j] = arr[j - 1];
            arr[j - 1] = tmp;
            j--;
        }
    }
}

// responds to mouse clicks to train the goal post tracker color range
void GoalPostTracker::trainMouseEvent(int event, int x, int y, int flags, void *param)
{

    Mat frame;
    Vec3b clickColor;

    // left-click trains the robot's tracker, right-click clears all training data
    if(event == CV_EVENT_LBUTTONDOWN || flags == CV_EVENT_FLAG_LBUTTON)
    {
        uniqueInstance -> getHSVFrame(frame);
        clickColor = frame.at<Vec3b>(y, x);
        uniqueInstance -> train(clickColor);
    }
    else if(event == CV_EVENT_RBUTTONDOWN || flags == CV_EVENT_FLAG_RBUTTON)
    {
        uniqueInstance -> unTrain();
    }

}

// maxDistance is the maxmimum distance of two lines' endpoints at which
// the lines will be merged into one; minVotes is the minimum number of lines
// that must be nearby for them to be considered a merged line and not discarded
void GoalPostTracker::mergeLines(vector<Vec4i> &lines, vector<Vec4i> &merged, int maxDistance, int minVotes)
{

    vector<Vec4i>::iterator i;
    vector<Vec4i>::iterator j;
    vector<int>::iterator m;
    vector<int>::iterator n;
    vector<int> votes;

    int voteCount;
    unsigned int k;

    Vec4i curr1;
    Vec4i curr2;

    // each line has an initial merge vote of zero
    for(k = 0; k < lines.size(); k ++)
    {
        votes.push_back(0);
    }

    // begin traversing the lines and the vote count at the beginning
    i = lines.begin();
    m = votes.begin();

    while(i != lines.end())
    {

        // start at the line after this one, so we only consider line-line pairs once,
        // and also avoid comparing the same line
        j = i + 1;
        n = m + 1;

        while(j != lines.end())
        {

            curr1 = *i;
            curr2 = *j;

            // reorder the points of the two lines such that
            // their end points are closest
            Util::orderLinePointsTogether(curr1, curr2);

            // if the two lines' endpoints are sufficiently close,
            // merge the two lines into one
            if(Util::linePointsNearby(curr1, curr2, maxDistance))
            {

                // remove the lines that were used to make the merge;
                // this needs to occur before we add the merged lines
                // so that we don't invalidate the iterators by
                // reallocating space for the vector by adding first
                lines.erase(j);
                i = lines.erase(i);
                j = i + 1;

                // same as above, only with the votes; ensure that
                // we sum the votes for both lines, and add those
                // votes to the newly created merged line
                voteCount = (*m) + (*n);
                votes.erase(n);
                m = votes.erase(m);
                n = m + 1;

                // merge the two lines and increase the vote count of the merged line
                lines.push_back(Util::averageLines(curr1, curr2));
                votes.push_back(voteCount + 1);

            }
            else
            {
                // we only increment the inner counter if we don't merge anything,
                // so that we don't skip over elements when we delete lines
                j ++;
                n ++;
            }

        }

        i ++;
        m ++;

    }

    // now, add the merged lines that have a sufficiently high merge count
    // to the final merge line list
    i = lines.begin();
    m = votes.begin();

    while(i != lines.end())
    {
        if(*m >= minVotes)
        {
            merged.push_back(*i);
        }

        i ++;
        m ++;
    }

}

// uses an upvote/downvote system to determine which pair of lines is most likely
// the left and right goal posts; then, if goal posts are found, it reorders the
// points that form the goal post so that the goal lines travel downwards
bool GoalPostTracker::voteOnGoalPosts(vector<Vec4i> &lines, Vec4i &left, Vec4i &right, float &slopeLeft, float &slopeRight)
{

    const float PREFERRED_MIN_SLOPE = 0.85;		// we'd prefer our goal posts to be vertical

    const float MIN_SLOPE_SIMILARITY = 0.8;		// minimum and maximum preferred percentage slope similarity
    const float MAX_SLOPE_SIMILARITY = 1.2;		// between two lines

    const float MIN_HEIGHT_SIMILARITY = 0.5;	// minimum and maximum preferred percentage height similarity
    const float MAX_HEIGHT_SIMILARITY = 1.5;	// between two lines

    const float MIN_DISTANCE = 45.0;

    unsigned int i, j;
    int votes[lines.size()][lines.size()];      // votes for each pair of lines;
                                                // the more votes, the more likely this
                                                // pair forms the goalposts

    int highestI = 0;				// index i of highest-voted [j, i] pair
    int highestJ = 0;				// index j of highest-voted [j, i] pair
    int highestVote = 0;			// value of highest-voted [j, i] pair
    bool result = false;			// whether or not we've found goalpost candidates

    int swap1, swap2;

    // votes going to each line pair start at zero
    for(j = 0; j < lines.size(); j ++)
    {
        for(i = j + 1; i < lines.size(); i ++)
        {

            Vec4i line1 = lines[j];
            Vec4i line2 = lines[i];
            votes[j][i] = 0;

            // ensure line goes downwards
            if(line1[1] > line1[3])
            {
                swap1 = line1[0];
                swap2 = line1[1];
                line1[0] = line1[2];
                line1[1] = line1[3];
                line1[2] = swap1;
                line1[3] = swap2;
            }

            // ensure line goes downwards
            if(line2[1] > line2[3])
            {
                swap1 = line2[0];
                swap2 = line2[1];
                line2[0] = line2[2];
                line2[1] = line2[3];
                line2[2] = swap1;
                line2[3] = swap2;
            }

            // get the slope of each line so we can see if they are
            // similarly oriented
            float slope1 = (Util::computeSlope(line1));
            float slope2 = (Util::computeSlope(line2));

            // if they have the similar slope, vote this pair up
            float slopeCompare = slope1 / slope2;
            if(slopeCompare > MIN_SLOPE_SIMILARITY && slopeCompare < MAX_SLOPE_SIMILARITY)
            {
                votes[j][i] += 5;
            }

            // if they don't have near-vertical slopes, vote them down
            if(slope1 < PREFERRED_MIN_SLOPE)
            {
                votes[j][i] -= 11;
            }

            // if they have similar lengths, vote them up
            float lengthCompare = Util::dist(line1) / Util::dist(line2);
            if(lengthCompare > MIN_HEIGHT_SIMILARITY && lengthCompare < MAX_HEIGHT_SIMILARITY)
            {
                votes[j][i] += 5;
            }

            // if they are very close together, then vote them down
            if(Util::dist(Point2f(line1[0], line1[1]), Point2f(line2[0], line2[1])) < MIN_DISTANCE ||
               Util::dist(Point2f(line1[0], line1[1]), Point2f(line2[2], line2[3])) < MIN_DISTANCE)
            {
                votes[j][i] -= 9;
            }

            // if this vote is higher than any so far, save it for later
            if(votes[j][i] > highestVote && votes[j][i] > 5)
            {
                highestJ = j;
                highestI = i;
                highestVote = votes[j][i];
                slopeLeft = slope1;
                slopeRight = slope2;
                result = true;
            }

        }
    }

    // if we've found a candidate, return it
    if(result)
    {

        left = lines[highestJ];
        right = lines[highestI];

        // ensure we know which is left and which one is right
        if(left[0] > right[0])
        {
            Vec4i temp = right;
            right = left;
            left = temp;

            // swap the slope values, too
            swap1 = slopeRight;
            slopeRight = slopeLeft;
            slopeLeft = swap1;
        }

        // ensure left goal points go downwards
        if(left[1] > left[3])
        {
            swap1 = left[0];
            swap2 = left[1];
            left[0] = left[2];
            left[1] = left[3];
            left[2] = swap1;
            left[3] = swap2;
        }

        // ensure right goal points go downwards
        if(right[1] > right[3])
        {
            swap1 = right[0];
            swap2 = right[1];
            right[0] = right[2];
            right[1] = right[3];
            right[2] = swap1;
            right[3] = swap2;
        }

    }

    // indicate that we've found the goal posts (or something very similar!)
    return result;

}





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// UTIL CLASS STARTS HERE //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
// returns the minimum of two numbers
int GoalPostTracker::Util::min(int v1, int v2)
{
    if(v1 > v2)
        return v2;
    return v1;
}

// returns the maximum of two numbers
int GoalPostTracker::Util::max(int v1, int v2)
{
    if(v1 > v2)
        return v1;
    return v2;
}

// returns the average of 2 points in space
Point2f GoalPostTracker::Util::average(Point2f a, Point2f b)
{

    Point2f ab = a + b;
    ab.x /= 2.0;
    ab.y /= 2.0;

    return ab;

}

// returns the average of 3 points in space
Point2f GoalPostTracker::Util::average(Point2f a, Point2f b, Point2f c)
{

    Point2f ab = average(a, b);
    Point2f ac = average(a, c);
    Point2f bc = average(b, c);
    Point2f abc;

    abc = ab + ac + bc;
    abc.x /= 3.0;
    abc.y /= 3.0;

    return abc;

}

// returns the average of a vector of points in space
Point2f GoalPostTracker::Util::average(vector<Point2f> points)
{

    Point2f result(0.0, 0.0);
    vector<Point2f>::iterator i;

    for(i = points.begin(); i != points.end(); i ++)
    {
        result = result + *i;
    }

    result.x /= (float)points.size();
    result.y /= (float)points.size();

    return result;

}

// returns the length of a given vector
float GoalPostTracker::Util::length(Point2f v)
{
    return sqrt((v.x * v.x) + (v.y * v.y));
}

// returns the distance between two points in space
float GoalPostTracker::Util::dist(Point2f a, Point2f b)
{
    return length(a - b);
}

// returns the distance of a line
float GoalPostTracker::Util::dist(Vec4i line)
{
    return dist(Point2f(line[0], line[1]), Point2f(line[2], line[3]));
}

// returns the angle of a given vector, from -180 to 180
float GoalPostTracker::Util::angle(Point2f v)
{
    return 180.0 - rad2deg(atan2(v.x, v.y));
}

// normalizes the given vector
Point2f GoalPostTracker::Util::normalize(Point2f v)
{

    const float LENGTH = length(v);
    Point2f result = v;

    result.x /= LENGTH;
    result.y /= LENGTH;

    return result;

}

Point2f GoalPostTracker::Util::vecAtAngle(float angle)
{
    return Point2f(sinf(deg2rad(angle)), cosf(deg2rad(angle)));
}

// this method reorders the points of the two lines such that
// the ends of each line are closest to each other
void GoalPostTracker::Util::orderLinePointsTogether(Vec4i &line1, Vec4i &line2)
{

    int swap1;
    int swap2;

    if(dist(Point2f(line1[0], line1[1]), Point2f(line2[0], line2[1])) >
       dist(Point2f(line1[0], line1[1]), Point2f(line2[2], line2[3])))
    {
        swap1 = line1[0];
        swap2 = line1[1];
        line1[0] = line1[2];
        line1[1] = line1[3];
        line1[2] = swap1;
        line1[3] = swap2;
    }

}

// this method assumes that GoalPostTracker::Util::orderLinePoints() has been
// called on these two parameters; maxDistance is the maximum allowed
// distance that line ends can be apart from each other in order
// to be considered 'nearby'
bool GoalPostTracker::Util::linePointsNearby(Vec4i line1, Vec4i line2, int maxDistance)
{
    return dist(Point2f(line1[0], line1[1]), Point2f(line2[0], line1[1])) < maxDistance &&
           dist(Point2f(line1[2], line1[3]), Point2f(line2[2], line2[3])) < maxDistance;
}

// this method assumes that GoalPostTracker::Util::orderLinePoints() has been
// called on these two parameters
Vec4i GoalPostTracker::Util::averageLines(Vec4i line1, Vec4i line2)
{
    return Vec4i((line1[0] + line2[0]) / 2, (line1[1] + line2[1]) / 2,
                 (line1[2] + line2[2]) / 2, (line1[3] + line1[3]) / 2);
}

// this method computes the slope of a line (rise over run)
float GoalPostTracker::Util::computeSlope(Vec4i line)
{

    int num = (line[3] - line[1]);
    int den = (line[2] - line[0]);
    float result = 1.0;

    if(den != 0)
    {
        result = (float)num / ((float)num + (float)den);
    }

    return result;
}

// wraps the given angle between 0 and 360
float GoalPostTracker::Util::wrapvalue(float val)
{

    float result = val;

    if(val >= 360.0)
        result = val - 360.0;
    else if(val < 0.0)
        result = 360.0 + val;

    return result;

}

Vec3b GoalPostTracker::Util::matToVec3b(Mat m)
{
    return Vec3b(m.at<float>(0, 0), m.at<float>(0, 1), m.at<float>(0, 2));
}

Mat GoalPostTracker::Util::vec3bToMat(Vec3b v)
{
    return (Mat_<float>(3,1) << v[0], v[1], v[2]);
}

Point2f GoalPostTracker::Util::keyPointToPoint(KeyPoint keyPoint)
{
    return keyPoint.pt;
}

int GoalPostTracker::Util::getFurthestFromAverage(vector<Point2f> points)
{

    Point2f average = GoalPostTracker::Util::average(points);

    int i;
    int furthest = 0;
    int furthestDist = 0;
    int dist;

    for(i = 0; i < (int)points.size(); i ++)
    {

        dist = length(points[i] - average);
        if(dist > furthestDist)
        {
            furthest = i;
            furthestDist = dist;
        }

    }

    return furthest;

}

float GoalPostTracker::Util::areaToRadius(float a)
{
    return sqrt(a / 3.14159);
}

void GoalPostTracker::Util::sortKeyPointsDownBySize(vector<KeyPoint>& keypoints)
{

    int i, j;
    KeyPoint curr;

    for(i = 1; i < (int)keypoints.size(); i ++)
    {

        curr = keypoints[i];

        for(j = i; j > 0 && keypoints[j - 1].size < curr.size; j --)
        {
            keypoints[j] = keypoints[j - 1];
        }

        keypoints[j] = curr;

    }

}*/
