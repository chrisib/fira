#ifndef FIRAPACKETS_H
#define FIRAPACKETS_H

#define FIRA_PACKET_HEADER 0xADDE
#define NUM_BYTES_PER_OBJECT 8
#define FIRA_PACKET_ADDITIONAL_BYTES 10
#define FIRA_PACKET_ID_INDEX 4

class PacketObject {
public:
    PacketObject(){}
    PacketObject(uint8_t type, uint8_t payload[7])
    {
        this->type = type;
        for(int i=0; i<7; i++)
            this->payload[i] = payload[i];
    }

    uint8_t type;
    uint8_t payload[7];

    void print();
};

class StateObject : public PacketObject {
public:
    enum {
        TYPE_UNDEFINED = 0,
        TYPE_OWN_POSITION,
        TYPE_BALL,
        TYPE_TEAM_ROBOT,
        TYPE_OPPOSING_ROBOT,
        TYPE_TEAM_GOAL,
        TYPE_OPPOSING_GOAL
    };

    StateObject(){}
    StateObject(uint8_t type, uint8_t payload[7])
    {
        this->type = type;
        for(int i=0; i<7; i++)
            this->payload[i] = payload[i];
    }
    StateObject(PacketObject &o)
    {
        StateObject(o.type, o.payload);
    }

    StateObject(uint8_t type, uint16_t locationX, uint16_t locationY, uint16_t orientation, uint8_t confidence)
    {
        this->type = type;
        payload[0] = (locationX >> 8) & 0xff;
        payload[1] = locationX & 0xff;
        payload[2] = (locationY >> 8) & 0xff;
        payload[3] = locationY & 0xff;
        payload[4] = (orientation >> 8) & 0xff;
        payload[5] = orientation & 0xff;
        payload[6] = confidence;
    }

    inline uint16_t LocationX(){return (payload[0] << 8) | payload[1];}     // mm
    inline uint16_t LocationY(){return (payload[2] << 8) | payload[3];}     // mm
    inline uint16_t Orientation(){return (payload[4] << 8) | payload[5];}   // radians * 1000
    inline uint8_t Confidence(){return payload[6];}
};

class PerceptionObject : public PacketObject {
public:
    enum {
        TYPE_UNDEFINED = 32,
        TYPE_OWN_POSITION,
        TYPE_BALL,
        TYPE_TEAM_ROBOT,
        TYPE_OPPOSING_ROBOT,
        TYPE_TEAM_GOAL,
        TYPE_OPPONENT_GOAL,
        TYPE_FIELD_LINE,
        TYPE_CENTER_ARC
    };

    PerceptionObject(){}
    PerceptionObject(uint8_t type, uint8_t payload[7])
    {
        this->type = type;
        for(int i=0; i<7; i++)
            this->payload[i] = payload[i];
    }
    PerceptionObject(PacketObject &o)
    {
        PerceptionObject(o.type, o.payload);
    }

    PerceptionObject(uint8_t type, uint16_t locationX, uint16_t locationY, uint16_t orientation, uint8_t confidence)
    {
        this->type = type;
        payload[0] = (locationX >> 8) & 0xff;
        payload[1] = locationX & 0xff;
        payload[2] = (locationY >> 8) & 0xff;
        payload[3] = locationY & 0xff;
        payload[4] = (orientation >> 8) & 0xff;
        payload[5] = orientation & 0xff;
        payload[6] = confidence;
    }

    inline uint16_t LocationX(){return (payload[0] << 8) | payload[1];}     // mm
    inline uint16_t LocationY(){return (payload[2] << 8) | payload[3];}     // mm
    inline uint16_t Orientation(){return (payload[4] << 8) | payload[5];}   // radians * 1000
    inline uint8_t Confidence(){return payload[6];}
};

class IntentObject : public PacketObject {
public:
    enum {
        TYPE_UNDEFINED = 64,
        TYPE_SHOT_ON_GOAL,
        TYPE_PLAY_GOALIE,
        TYPE_MOVEMENT
    };

    IntentObject(){}
    IntentObject(uint8_t type, uint8_t payload[7])
    {
        this->type = type;
        for(int i=0; i<7; i++)
            this->payload[i] = payload[i];
    }
    IntentObject(PacketObject &o)
    {
        IntentObject(o.type, o.payload);
    }

    IntentObject(uint8_t type, uint16_t locationX, uint16_t locationY, uint16_t orientation, uint8_t offence)
    {
        this->type = type;
        payload[0] = (locationX >> 8) & 0xff;
        payload[1] = locationX & 0xff;
        payload[2] = (locationY >> 8) & 0xff;
        payload[3] = locationY & 0xff;
        payload[4] = (orientation >> 8) & 0xff;
        payload[5] = orientation & 0xff;
        payload[6] = offence;
    }

    inline uint16_t LocationX(){return (payload[0] << 8) | payload[1];}     // mm
    inline uint16_t LocationY(){return (payload[2] << 8) | payload[3];}     // mm
    inline uint16_t Orientation(){return (payload[4] << 8) | payload[5];}   // radians * 1000
    inline uint8_t Offence(){return payload[6];}
};

class RefereeObject : public PacketObject {
public:
    enum {
        TYPE_UNDEFINED = 96,
        TYPE_GAME_STATE
    };

    enum {
        STATE_STOPPED = 0,
        STATE_SET_KICKOFF_RED,
        STATE_SET_KICKOFF_BLUE,
        STATE_PLAY_KICKOFF_RED,
        STATE_PLAY_KICKOFF_BLUE,
        STATE_PLAY
    };

    RefereeObject(){}
    RefereeObject(uint8_t type, uint8_t payload[7])
    {
        this->type = type;
        for(int i=0; i<7; i++)
            this->payload[i] = payload[i];
    }
    RefereeObject(PacketObject &o)
    {
        RefereeObject(o.type, o.payload);
    }

    RefereeObject(uint8_t type, uint16_t teamRed, uint16_t teamBlue, uint16_t time, uint8_t state)
    {
        this->type = type;
        payload[0] = (teamRed >> 8) & 0xff;
        payload[1] = teamRed & 0xff;
        payload[2] = (teamBlue >> 8) & 0xff;
        payload[3] = teamBlue & 0xff;
        payload[4] = (time >> 8) & 0xff;
        payload[5] = time & 0xff;
        payload[6] = state;
    }

    inline uint16_t TeamRedBitmask(){return (payload[0] << 8) | payload[1];}    // bit j = 1 :: player j is allowed to play
    inline uint16_t TeamBlueBitmask(){return (payload[2] << 8) | payload[3];}   // bit j = 1 :: player j is allowed to play
    inline uint16_t Time(){return (payload[4] << 8) | payload[5];}      // seconds remaining
    inline uint8_t State(){return payload[6];}
};

class UserDefObject : public PacketObject {
public:
    enum {
        TYPE_USER_DEFINED = 128
    };

    UserDefObject(){}
    UserDefObject(uint8_t type, uint8_t payload[7])
    {
        this->type = type;
        for(int i=0; i<7; i++)
            this->payload[i] = payload[i];
    }
    UserDefObject(PacketObject &o)
    {
        UserDefObject(o.type, o.payload);
    }

    UserDefObject(uint8_t type, uint16_t userDef1, uint16_t userDef2, uint16_t userDef3, uint8_t userDef4)
    {
        this->type = type;
        payload[0] = (userDef1 >> 8) & 0xff;
        payload[1] = userDef1 & 0xff;
        payload[2] = (userDef2 >> 8) & 0xff;
        payload[3] = userDef2 & 0xff;
        payload[4] = (userDef3 >> 8) & 0xff;
        payload[5] = userDef3 & 0xff;
        payload[6] = userDef4;
    }

    inline uint16_t UserDef1(){return (payload[0] << 8) | payload[1];}
    inline uint16_t UserDef2(){return (payload[2] << 8) | payload[3];}
    inline uint16_t UserDef3(){return (payload[4] << 8) | payload[5];}
    inline uint8_t UserDef4(){return payload[6];}
};

class UserDefLineObject : public UserDefObject {
public:
    UserDefLineObject(uint16_t range, uint16_t theta, uint16_t objectID, uint8_t typeCode) : UserDefObject(UserDefObject::TYPE_USER_DEFINED,range,theta,objectID,typeCode){}
};

class UserDefPointObject : public UserDefObject {
public:
    UserDefPointObject(uint16_t range, uint16_t theta, uint16_t objectID, uint8_t typeCode) : UserDefObject(UserDefObject::TYPE_USER_DEFINED,range,theta,objectID,typeCode){}
};

class UserDefStateObject : public UserDefObject {
public:
    UserDefStateObject(uint16_t state, uint16_t substate) : UserDefObject(UserDefObject::TYPE_USER_DEFINED,state,substate,0x00,PerceptionObject::TYPE_OWN_POSITION){}
};


class SoccerPacket {
public:
    uint16_t header;
    uint16_t length;
    uint8_t id;
    uint16_t sequence;
    uint8_t numObjects;
    PacketObject objects[255];
    uint8_t dummy;
    uint8_t checksum;

    static const int ID_INDEX = 4;
};

#endif // FIRAPACKETS_H
