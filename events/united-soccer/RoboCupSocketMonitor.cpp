#include "RoboCupSocketMonitor.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <SoccerPlayer.h>

using namespace std;

const char* RoboCupSocketMonitor::REFEREE_HEADER = "RGme";

RoboCupSocketMonitor::RoboCupSocketMonitor(SoccerField &field) : SocketMonitor(field)
{
    // do nothing
}

RoboCupSocketMonitor::RoboCupSocketMonitor(int portNo,SoccerField &field) : SocketMonitor(portNo,field)
{
    // do nothing
}

RoboCupSocketMonitor::~RoboCupSocketMonitor()
{
    // TODO: anything?
}

bool RoboCupSocketMonitor::isRobotOnPenalty(int team, int id)
{
    if(team != SoccerPlayer::TEAM_BLUE && team != SoccerPlayer::TEAM_RED)
    {
        cerr << "[warning] Unknown team colour: " << team << endl;
        return false;
    }
    else if(id < 0 || id >= SocketMonitor::MAX_PLAYERS_PER_TEAM)
    {
        cerr << "[warning] Invalid robot ID" << id << endl;
        return false;
    }
    else
    {
        bool onPenalty = gameControlData.teams[team].players[id-1].penalty != NO_PENALTY;
        return onPenalty;
    }
}

bool RoboCupSocketMonitor::canRobotPlay(int team, int id)
{
    bool onPenalty = isRobotOnPenalty(team,id);

    // TODO: check state of play?

    return !onPenalty;
}

void RoboCupSocketMonitor::processUdpDatagram(char *src, int numBytes)
{
    //cerr << "reading " << numBytes << " bytes" << endl;

    // make a local copy of all the buffered data to keep things thread-safe
    char data[numBytes];
    for(int i=0; i<numBytes; i++)
        data[i] = src[i] & 0xff;

    // check the header to decide what to do with this packet
    if(data[0] == REFEREE_HEADER[0] &&
        data[1] == REFEREE_HEADER[1] &&
        data[2] == REFEREE_HEADER[2] &&
        data[3] == REFEREE_HEADER[3]
    )
    {
        // the game control data does not use a checksum, so we can only check the header & length
        if(numBytes == REFEREE_PACKET_LENGTH)
        {
            updateControlData(data, numBytes);
        }
#ifdef SOCKET_DEBUG
        else
            cerr << "[info] faulty referee packet ignored" << endl;
#endif

    }
    else if((uint8_t)data[0] == (uint8_t)(FIRA_PACKET_HEADER >> 8) && (uint8_t)data[1] == (uint8_t)(FIRA_PACKET_HEADER & 0xff)
    )
    {
        updatePlayerData(data,numBytes);
    }
#ifdef SOCKET_DEBUG
    else
    {
        cerr << "Unknown packet header: " << data[0] << data[1] << data[2] << data[3] << endl;
    }
#endif


}

void RoboCupSocketMonitor::updateControlData(char *data, int numBytes)
{
    (void)numBytes;

    // header data
    for(int i=0; i<4; i++)
        gameControlData.header[i] = data[i];

    // little-endian 32-bit integer for version
    gameControlData.version = (data[4] & 0xff) | ((data[5] & 0xff) << 8) | ((data[6] & 0xff) << 16) | ((data[7] & 0xff) << 24);

    // players per team
    gameControlData.playersPerTeam = data[8] & 0xff;

    // state
    gameControlData.state = data[9] & 0xff;

    // first/second half
    gameControlData.firstHalf = data[10] & 0xff;

    // kickoff team
    gameControlData.kickOffTeam = data[11] & 0xff;

    // secondary state
    gameControlData.secondaryState = data[12] & 0xff;

    // drop in team & time
    gameControlData.dropInTeam = data[13] & 0xff;
    gameControlData.dropInTime = (data[14] & 0xff) | ((data[15] & 0xff) << 8);

    // time remaining (little-endian 32-bit integer)
    gameControlData.secsRemaining = (data[16] & 0xff) | ((data[17] & 0xff) << 8) | ((data[18] & 0xff) << 16) | ((data[19] & 0xff) << 24);

    // 2x team data
    for(int team=0; team<2; team++)
    {
        const int TEAM_START = 20 + team * TEAM_INFO_LENGTH;
        gameControlData.teams[team].teamNumber = data[TEAM_START + 0] & 0xff;
        gameControlData.teams[team].teamColour = data[TEAM_START + 1] & 0xff;
        gameControlData.teams[team].goalColour = data[TEAM_START + 2] & 0xff;
        gameControlData.teams[team].score = data[TEAM_START + 3] & 0xff;

        // n players
        for(int i=0; i<SocketMonitor::MAX_PLAYERS_PER_TEAM; i++)
        {
            const int PLAYER_START = TEAM_START + 4 + i*PLAYER_INFO_LENGTH;

            gameControlData.teams[team].players[i].penalty = (data[PLAYER_START + 0] & 0xff) | ((data[PLAYER_START + 1] & 0xff) << 8);
            gameControlData.teams[team].players[i].secsTillUnpenalised = (data[PLAYER_START + 2] & 0xff) | ((data[PLAYER_START + 3] & 0xff) << 8);

            //cout << "Team " << team << " player " << i << " Penalty: " << hex << gameControlData.teams[team].players[i].penalty <<
            //        " Secs Left: " << dec << (int)gameControlData.teams[team].players[i].secsTillUnpenalised << endl;
        }
    }
}
