#!/bin/bash

cd /home/darwin/fira/events/united-soccer

until ./united-soccer; do
    err=$?
    echo "united-soccer crashed with exit code $err.  Restarting"
    espeak "Ouch! error code $err"
    sleep 1
done
