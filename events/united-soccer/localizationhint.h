#pragma once

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include <vector>

using namespace cv;
using namespace std;

class LocalizationHint
{

	private:
		
		// names of the windows for training/tracking
		static const char *MAIN_WINDOW;
		static const char *THRESHOLD_WINDOW;
		
		// we use this so we can refer to the LocalizationHint from a static context
		static LocalizationHint *trackerInstance;
		
		// OpenCV mat frames
		Mat originalFrame;
		Mat thresholdFrame;
		
		// threshold for tracking goal line and penalty marker
		int fieldThreshold;
		
		// starting values for the Hough-space line detector
		int houghMinVotes;
		int houghMinLength;
		int houghMaxGap;
		
		// indicates if the training and tracking windows are open
		bool windowsOpen;
		
		// maxDistance is the maxmimum distance of two lines' endpoints at which
		// the lines will be merged into one; minVotes is the minimum number of lines
		// that must be nearby for them to be considered a merged line and not discarded
		void mergeLines(vector<Vec4i> &lines, vector<Vec4i> &merged, int maxDistance);

		// sorts a series of lines by non-ascending order of length
		void sortLines(Vec4i *arr, int length);
		
		// of the list of lines passed in, return the number of lines that are parallel (or close to parallel)
		int countParallelLines(vector<Vec4i> &lines, float maxSlopeDifference);
		
		// returns the list of corners detected (by slope and endpoint distance) of
		// the given list of lines
		void findCorners(vector<Vec4i>&, vector<Vec2i>&);
		
		// private, forcing us to use a singleton approach
		LocalizationHint();

	public:
	
		// destructor
		~LocalizationHint();
	
		// factory method that returns our singleton instance
		static LocalizationHint *create();
		
		// pass in a frame for the high gui windows and the goal post detection
		void update(Mat &frame);
		
		// returns the last frame passed in
		void getOriginalFrame(Mat &frame);
		
		// - - localization hint functions - - //
		
		// returns a vector of corners formed only (hopefully) by the field lines
		void findFieldLineCorners(vector<Vec2i>&);
		
		// returns the number of field lines that were found to be parallel
		int countParallelFieldLines();
		
		// - - window handling - - //
		
		// opens training windows, and a window that labels the locations of the left and right goal posts
		void openTrackingWindows();
		
		// closes the training windows, and the window that labels the locations of the left and right goal posts
		void closeTrackingWindows();

};
