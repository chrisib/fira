#ifndef FIRASOCCERFIELD_H
#define FIRASOCCERFIELD_H

#include <SoccerField.h>

class FiraSoccerField : public SoccerField
{
public:
    FiraSoccerField();
    virtual ~FiraSoccerField();

    virtual void LoadIniSettings(minIni &ini, const char *section);

protected:
    int shootingAreaLength;
    int shootingAreaWidth;
    int shootingAreaArc;

    virtual void MakeFieldLines();
};

#endif // FIRASOCCERFIELD_H
