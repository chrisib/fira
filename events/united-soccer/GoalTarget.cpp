#include "GoalTarget.h"
#include "util.h"
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <darwin/framework/Math.h>

#include <iostream>
#include <vector>

using namespace std;
using namespace Robot;
using namespace cv;

const int GoalTarget::FOUND_NEITHER = 0;
const int GoalTarget::FOUND_LEFT = 1;
const int GoalTarget::FOUND_RIGHT = 2;
const int GoalTarget::FOUND_BOTH = 3;

GoalTarget::GoalTarget() : MultiLine()
{
    foundLeftPost = false;
    foundRightPost = false;
    againstWhiteBackground = false;
}

GoalTarget::~GoalTarget() { }

void GoalTarget::LoadIniSettings(minIni &ini, const string &section)
{
    MultiLine::LoadIniSettings(ini,ini.gets(section,"Colour"));

    realHeight = ini.geti(ini.gets(section,"Colour"),"RealHeight");
    realWidth = ini.geti(ini.gets(section,"Colour"),"RealWidth");

    whiteThreshold = ini.geti(section, "WhiteThreshold");
}

// Given a YUV frame find the goal posts
// This is based on Geoff's GoalTarget class, but refactored to fit in
// with the libdarwin Target framework
int GoalTarget::FindInFrame(Mat &image, Mat *dest)
{

    const int MIN_GOALPOST_LENGTH = 40;				// found goal post lines must be this long at least, otherwise
                                                    // they are rejected

    vector<Vec4i> lines;				// initial group of lines found by Hough line detector
    vector<Vec4i> merged;				// group of lines after a narrow merge pass
    vector<Vec2i> corners;				// list of corners between a series of lines
    vector<Vec4i> cornerLines;			// list of lines that form the above list of corners
    vector<Vec4i> leftLines;			// list of lines that are left of the crossbar center
    vector<Vec4i> rightLines;			// list of lines that are right of the crossbar center
    vector<Vec4i>::iterator j;

    Mat binary;                         // processed frames used as part of the algorithm

    BoundingBox *box = NULL;			// bounding box of found goal area, for SingleTarget class

    Vec4i left, right;							// top-down lines representing our goal posts
    int horizontalCenter, verticalCenter;

    int result = FOUND_NEITHER;					// see GoalTarget.h for possible return codes

    foundLeftPost = false;							// indicates which posts we think we've found
    foundRightPost = false;

    // used training color data to threshold the image, so we are (hopefully)
    // only seeing colors that correspond to the posts
    inRange(image, Scalar(yRange.min, uRange.min, vRange.min), Scalar(yRange.max, uRange.max, vRange.max), binary);

    // eliminate obvious noise
    erode(binary, binary, Mat(), Point(-1, -1), 1);
    dilate(binary, binary, Mat(), Point(-1, -1), 1);

    // perform probabilistic hough-space line detection
    HoughLinesP(binary, lines, 1, CV_PI/180, minHoughVotes, minLength, maxGap);

    // do a merge over the lines and then do a search for corners
    // that are formed by relatively right-angled, joined lines
    mergeLines(lines, merged, 10, 1);
    findCorners(merged, corners, cornerLines);

    // search for any relatively vertical lines from our list of connected lines;
    // this only occurs if we've found appropriate corners
    if(cornerLines.size() > 0)
    {

        // look for vertical lines connected to any of our list of found corners
        findVerticalLines(cornerLines, leftLines, rightLines);

        // re-order all the goal post lines so they are drawn top to bottom

        for(j = leftLines.begin(); j != leftLines.end(); j ++)
        {
            Util::orderLineTopToBottom(*j);
        }

        for(j = rightLines.begin(); j != rightLines.end(); j ++)
        {
            Util::orderLineTopToBottom(*j);
        }

        // compute the position of the left post
        if(leftLines.size() > 0)
        {
            foundLeftPost = true;
            left = Util::averageLinesToLongestVertical(leftLines);
        }

        // compute the position of the right post
        if(rightLines.size() > 0)
        {
            foundRightPost = true;
            right = Util::averageLinesToLongestVertical(rightLines);
        }

    }

    // if we haven't found at least one post using the crossbars, then apply another algorithm for
    // detecting our goal posts based on an up/downvoting system that operates on pairs of lines
    if(!foundLeftPost || !foundRightPost)
    {
        if(voteOnGoalPosts(merged, left, right))
        {
            foundLeftPost = true;
            foundRightPost = true;
        }
    }

    // ensure that the lines are vertical enough
    if(foundLeftPost && !foundRightPost)
    {
        foundLeftPost = Util::computeSlope(left) > 0.8;
    }
    else if(!foundLeftPost && foundRightPost)
    {
        foundRightPost = Util::computeSlope(right) > 0.8;
    }

    // ensure that the left line is long enough
    if(foundLeftPost)
    {
        foundLeftPost = Util::length(left) > MIN_GOALPOST_LENGTH;
    }

    // ensure that the right line is long enough
    if(foundRightPost)
    {
        foundRightPost = Util::length(right) > MIN_GOALPOST_LENGTH;
    }

    // assign left and right goal posts internally
    if(foundLeftPost)
    {
        leftPost.start.X = left[0];
        leftPost.start.Y = left[1];
        leftPost.end.X = left[2];
        leftPost.end.Y = left[3];
    }

    // assign left and right goal posts internally
    if(foundRightPost)
    {
        rightPost.start.X = right[0];
        rightPost.start.Y = right[1];
        rightPost.end.X = right[2];
        rightPost.end.Y = right[3];
    }

    // assign bounding box information
    if(foundLeftPost && foundRightPost)
    {

        horizontalCenter = (leftPost.start.X + rightPost.start.X + leftPost.end.X + rightPost.end.X) / 4;
        verticalCenter = (leftPost.start.Y + rightPost.start.Y + leftPost.end.Y + rightPost.end.Y) / 4;

        box = GetBoundingBox();
        box -> center = Point2D(horizontalCenter, verticalCenter);
        box -> width = Util::max((rightPost.start.X - leftPost.start.X), (rightPost.end.X - leftPost.end.X));
        box -> height = Util::max((leftPost.end.Y - leftPost.start.Y), (rightPost.end.Y - rightPost.start.Y));

        needToReSolve = true;
        foundInLastFrame = true;

        postsFound = 2;

    }
    else if(foundLeftPost)
    {

        horizontalCenter = (leftPost.start.X + leftPost.end.X) / 2;
        verticalCenter = (leftPost.start.Y + leftPost.end.Y) / 2;

        box = GetBoundingBox();
        box -> center = Point2D((leftPost.start.X + leftPost.end.X) / 2, verticalCenter);
        box -> width = abs(leftPost.end.X - leftPost.start.X);
        box -> height = abs(leftPost.end.Y - leftPost.end.Y);

        needToReSolve = true;
        foundInLastFrame = true;

        postsFound = 1;

    }
    else if(foundRightPost)
    {

        horizontalCenter = (rightPost.start.X + rightPost.end.X) / 2;
        verticalCenter = (rightPost.start.Y + rightPost.end.Y) / 2;

        box = GetBoundingBox();
        box -> center = Point2D(horizontalCenter, verticalCenter);
        box -> width = abs(rightPost.end.X - rightPost.start.X);
        box -> height = abs(rightPost.end.Y - rightPost.end.Y);

        needToReSolve = true;
        foundInLastFrame = true;

        postsFound = 1;

    }
    else
    {
        foundInLastFrame = false;
        postsFound = 0;
    }

    // if dest is not null then copy the binary image over
    if(dest != NULL)
    {
        IplImage iplSrc = IplImage(binary);
        IplImage iplDst = IplImage(*dest);

        unsigned char *srcData = (unsigned char*)iplSrc.imageData;
        unsigned char *destData = (unsigned char*)iplDst.imageData;

        const int dataLength = binary.rows * binary.cols;
        const int dstChannels = dest->channels();

        for(int i=0; i<dataLength; i++)
        {
            for(int c=0; c<dstChannels; c++)
            {
                destData[i + c] = srcData[i];   // src has 1 channel, but dst may have many
            }
        }
    }

    // build the appropriate return code indicating what was found
    if(foundRightPost && foundLeftPost)
    {
        result = FOUND_BOTH;
    }
    else if(foundRightPost)
    {
        result = FOUND_RIGHT;
    }
    else if(foundLeftPost)
    {
        result = FOUND_LEFT;
    }

    // determine if we're against a white background
    if(result != FOUND_NEITHER)
    {
        determineBackgroundStatus(image);
    }

    return result;
}

// return the range to the centre of the goal
double GoalTarget::GetRange()
{
    camPos.RecalculatePosition();
#if 0
    if(needToReSolve)
        solveTriangle();

    return centerRange;
#else

    double result = 0.0;

    double leftRange = 0.0;
    double rightRange = 0.0;

    if(foundLeftPost)
    {
        leftRange = calculateRange(leftPost.start, leftPost.end);
    }

    if(foundRightPost)
    {
        rightRange = calculateRange(rightPost.start, rightPost.end);
    }

    if(postsFound)
    {
        result = (leftRange + rightRange) / ((double)(postsFound));
    }
    return result;

#endif

}

double GoalTarget::GetRightPostRange()
{
    camPos.RecalculatePosition();
#if 0
    if(needToReSolve)
        solveTriangle();

    return rightPostRange;
#else
    return calculateRange(rightPost.start,rightPost.end);
#endif
}

double GoalTarget::GetLeftPostRange()
{
    camPos.RecalculatePosition();
#if 0
    if(needToReSolve)
        solveTriangle();

    return leftPostRange;
#else
    return calculateRange(leftPost.start,leftPost.end);
#endif
}

double GoalTarget::calculateRange(Point2D &top, Point2D &bottom)
{
    double result = 0.0;
    const int REQUIRED_PADDING = 10;    // required pixel distance from the edge of the frame before we can assume we see the whole post

    if(top.Y > REQUIRED_PADDING) // we can see the top of the goal post; use its height to solve the triangle
    {
        /* given these equations and this lovely ASCII art diagram:
         *               .
         *             . |
         *           .   |
         *         .     |
         *       .O      | Goal Post (height = H)
         *     .  +--    |
         *   .    |      |
         * *---x--L---d--|
         *    Robot (height = h)  (L = the robot's foot -- you could tell, couldn't you?)
         *
         * 1: tan(theta) = h/x
         * 2: tan(theta) = H/(x+d)
         *
         * Doing some algebra we determine that...
         * 3: x+d = H/tan(theta)
         * 4: x = h/tan(theta)
         * 5: d = (H-h)/tan(theta)
         *
         */

        const double theta = deg2rad(camPos.CalculateVerticalAngle(top));
        const double h = camPos.GetCameraHeight();
        const double H = realHeight;

        result = (H-h)/tan(theta) + camPos.GetCameraXOffset();  // add the X offset because the camera is in front of the feet
#ifdef DEBUG
        cerr << "Using top of post..." << endl <<
                "H = " << H << endl <<
                "h = " << h << endl <<
                "a = " << rad2deg(theta) <<  endl <<
                "d = " << result << endl <<
                endl;
#endif
    }
    else if(bottom.Y < Camera::HEIGHT - REQUIRED_PADDING) // we can't see the whole thing, so approximate with viewing angles to the bottom
    {
#ifdef DEBUG
        cerr << "Using bottom of post" << endl << endl;
#endif
        result = camPos.CalculateRange(bottom,0);
    }
    else
    {
        cout << "[info] Unable to calculate range to a goal post; both ends are too close to the frame edges" << endl;
        result = -1;
    }

    return result < 0 ? -result : result;
}

// return the angle to the centre of the goal
double GoalTarget::GetAngle()
{

    double result = 0.0;

    double topLeftAngle = 0.0;
    double bottomLeftAngle = 0.0;
    double topRightAngle = 0.0;
    double bottomRightAngle = 0.0;

    camPos.RecalculatePosition();

    if(foundLeftPost)
    {
        topLeftAngle = camPos.CalculateAngle(leftPost.start, this -> realHeight);
        bottomLeftAngle = camPos.CalculateAngle(leftPost.end, 0);
    }

    if(foundRightPost)
    {
        topRightAngle = camPos.CalculateAngle(rightPost.start, this -> realHeight);
        bottomRightAngle = camPos.CalculateAngle(rightPost.end, 0);
    }

    // we do this to avoid a division by zero if the posts are not found - but, this
    // function should never be called if the posts aren't found, anyways!
    if(postsFound > 0)
    {
        result = (topLeftAngle + bottomLeftAngle + topRightAngle + bottomRightAngle) / ((float)(postsFound * 2.0));//4.0;
    }

    return result;
}

double GoalTarget::GetRightPostAngle()
{
    camPos.RecalculatePosition();

    double topRightAngle = camPos.CalculateAngle(rightPost.start, this->realHeight);
    double bottomRightAngle = camPos.CalculateAngle(rightPost.end, 0);

    //return bottomRightAngle;
    return (topRightAngle + bottomRightAngle) / 2.0;
}

double GoalTarget::GetLeftPostAngle()
{
    camPos.RecalculatePosition();

    double topLeftAngle = camPos.CalculateAngle(leftPost.start, this->realHeight);
    double bottomLeftAngle = camPos.CalculateAngle(leftPost.end, 0);

    //return bottomLeftAngle;
    return (topLeftAngle + bottomLeftAngle) / 2.0;
}

// get whether or not the goal is against a white background
bool GoalTarget::IsAgainstWhiteBackground()
{
    return againstWhiteBackground;
}

// determine whether or not the goal is against a white background
void GoalTarget::determineBackgroundStatus(Mat &frame)
{

    const float MIN_PERCENTAGE_WHITE = 0.5;
    const float DOWNWARDS_COVERAGE = 0.75;
    const int INNER_SAMPLES = 10;
    const int MIN_SAMPLES = 15;

    int total = 0;
    int length;
    int innerIncrements;
    int i, j;
    int innerXWidth;

    Vec2i sample;
    Vec2i start;

    vector<int> values;
    vector<int>::iterator k;

    samplePoints.clear();

    // sample for white points to the right of the left goal post
    if(foundLeftPost)
    {

        length = Util::length(Vec4i(leftPost.start.X, leftPost.start.Y, leftPost.end.X, leftPost.end.Y));
        innerIncrements = length / INNER_SAMPLES;

        start[0] = ((leftPost.start.X + leftPost.end.X) / 2) + innerIncrements;
        start[1] = ((leftPost.start.Y + leftPost.end.Y) / 2) + innerIncrements;

        innerXWidth = start[0] + length;
        if(foundRightPost)
        {
            innerXWidth = Util::min(innerXWidth, (int)rightPost.start.X);
        }

        for(j = leftPost.start.Y; j < leftPost.start.Y + (length * DOWNWARDS_COVERAGE); j += innerIncrements)
        {

            for(i = start[0]; i < innerXWidth/*start[0] + length && i < rightPost.start.X*/; i += innerIncrements)
            {

                sample = Vec2i(i, j);

                // ensure we don't go past the bounds of the image
                if(sample[0] > 0 && sample[1] > 0 && sample[0] < frame.size().width && sample[1] < frame.size().height)
                {
                    values.push_back((int)frame.at<Vec3b>(Util::vec2iToPoint2f(sample))[0]);
                    samplePoints.push_back(Util::vec2iToPoint2f(sample));
                }

            }

        }

    }

    // sample for white points to the left of the right goal post
    if(foundRightPost)
    {

        length = Util::length(Vec4i(rightPost.start.X, rightPost.start.Y, rightPost.end.X, rightPost.end.Y));
        innerIncrements = length / INNER_SAMPLES;

        start[0] = ((rightPost.start.X + rightPost.end.X) / 2) - innerIncrements;
        start[1] = ((rightPost.start.Y + rightPost.end.Y) / 2) + innerIncrements;

        innerXWidth = start[0] - length;
        if(foundLeftPost)
        {
            innerXWidth = Util::max(innerXWidth, (int)leftPost.start.X);
        }

        for(j = rightPost.start.Y; j < rightPost.start.Y + (length * DOWNWARDS_COVERAGE); j += innerIncrements)
        {

            for(i = start[0]; i > innerXWidth/*start[0] - length && i > leftPost.start.X*/; i -= innerIncrements)
            {

                sample = Vec2i(i, j);

                // ensure we don't go past the bounds of the image
                if(sample[0] > 0 && sample[1] > 0 && sample[0] < frame.size().width && sample[1] < frame.size().height)
                {
                    values.push_back((int)frame.at<Vec3b>(Util::vec2iToPoint2f(sample))[0]);
                    samplePoints.push_back(Util::vec2iToPoint2f(sample));
                }

            }

        }

    }

    // we must have sampled sufficient points in order to determine goal background
    if((int)values.size() > MIN_SAMPLES)
    {

        for(k = values.begin(); k != values.end(); k ++)
        {
            if(*k > whiteThreshold)
            {
                total ++;
            }
        }

        againstWhiteBackground = ((float)total / ((float)((int)values.size())) > MIN_PERCENTAGE_WHITE);

        /*
        for(k = values.begin(); k != values.end(); k ++)
        {
            total += *k;
        }

        total /= (int)values.size();
        againstWhiteBackground = total > whiteThreshold;
        */
        // debugging
        //cout << "goal background average: " << total << endl;

    }
    else
    {
        againstWhiteBackground = false;
    }

}

// maxDistance is the maxmimum distance of two lines' endpoints at which
// the lines will be merged into one; minVotes is the minimum number of lines
// that must be nearby for them to be considered a merged line and not discarded
void GoalTarget::mergeLines(vector<Vec4i> &lines, vector<Vec4i> &merged, int maxDistance, int minVotes)
{

    vector<Vec4i>::iterator i;
    vector<Vec4i>::iterator j;
    vector<int>::iterator m;
    vector<int>::iterator n;
    vector<int> votes;

    int voteCount;
    unsigned int k;

    Vec4i curr1;
    Vec4i curr2;

    // each line has an initial merge vote of zero
    for(k = 0; k < lines.size(); k ++)
    {
        votes.push_back(0);
    }

    // begin traversing the lines and the vote count at the beginning
    i = lines.begin();
    m = votes.begin();

    while(i != lines.end())
    {

        // start at the line after this one, so we only consider line-line pairs once,
        // and also avoid comparing the same line
        j = i + 1;
        n = m + 1;

        while(j != lines.end())
        {

            curr1 = *i;
            curr2 = *j;

            // reorder the points of the two lines such that
            // their end points are closest
            //Util::orderLinePointsTogether(curr1, curr2);
            Util::orderLineTopToBottom(curr1);
            Util::orderLineTopToBottom(curr2);

            // if the two lines' endpoints are sufficiently close,
            // merge the two lines into one
            if(Util::linePointsNearby(curr1, curr2, maxDistance))
            {

                // remove the lines that were used to make the merge;
                // this needs to occur before we add the merged lines
                // so that we don't invalidate the iterators by
                // reallocating space for the vector by adding first
                lines.erase(j);
                i = lines.erase(i);
                j = i + 1;

                // same as above, only with the votes; ensure that
                // we sum the votes for both lines, and add those
                // votes to the newly created merged line
                voteCount = (*m) + (*n);
                votes.erase(n);
                m = votes.erase(m);
                n = m + 1;

                // merge the two lines and increase the vote count of the merged line
                lines.push_back(Util::averageLinesToLongestVertical(curr1, curr2));
                votes.push_back(voteCount + 1);

            }
            else
            {
                // we only increment the inner counter if we don't merge anything,
                // so that we don't skip over elements when we delete lines
                j ++;
                n ++;
            }

        }

        i ++;
        m ++;

    }

    // now, add the merged lines that have a sufficiently high merge count
    // to the final merge line list
    i = lines.begin();
    m = votes.begin();

    while(i != lines.end())
    {
        if(*m >= minVotes)
        {
            merged.push_back(*i);
        }

        i ++;
        m ++;
    }

}

void GoalTarget::findVerticalLines(vector<Vec4i> &lines, vector<Vec4i> &leftLines, vector<Vec4i> &rightLines)
{

    const float MIN_VERTICAL_SLOPE = 0.7;
    const float MAX_HORIZONTAL_SLOPE = 0.3;

    vector<Vec4i>::iterator i;
    vector<Vec4i> verticalLines;
    int horizontalLines = 0;
    Vec4i curr;
    int averageXPos = 0;
    float slope;

    leftLines.clear();
    rightLines.clear();

    // gather a list of all vertical lines and add their
    // x positions
    for(i = lines.begin(); i != lines.end(); i ++)
    {

        curr = *i;
        slope = Util::computeSlope(curr);

        if(slope > MIN_VERTICAL_SLOPE)
        {
            verticalLines.push_back(curr);
        }
        else if(slope < MAX_HORIZONTAL_SLOPE)
        {
            horizontalLines ++;
            averageXPos += ((curr[0] + curr[2]) / 2);
        }

    }

    // if we can't see the crossbar, we're totally screwed
    if(horizontalLines > 0)
    {

        averageXPos /= horizontalLines;

        // sort the lines according to whether they are to the left or the right of the middle position
        for(i = verticalLines.begin(); i != verticalLines.end(); i ++)
        {

            curr = *i;
            if(curr[0] < averageXPos)
            {
                leftLines.push_back(curr);
            }
            else
            {
                rightLines.push_back(curr);
            }

        }

    }

}

// returns the list of corners detected (by slope and endpoint distance) of the given list of lines
void GoalTarget::findCorners(vector<Vec4i> &lines, vector<Vec2i> &corners, vector<Vec4i> &cornerLines)
{

    const int MIN_END_POINT_DIST = 25;	// max distance between two line endpoints
                                        // for them to be considered possibly joined

    vector<Vec4i>::iterator i, j;
    float k;							// arbitrary slope center for comparing two lines

    Vec4i line1, line2;					// current pair of lines we're considering
    float slope1, slope2;				// slope of above lines
    float slopeDiff1, slopeDiff2;		// difference in slope from an arbitrary slope value
    Vec2i connect;						// detected corner of two joined lines

    bool added;
    bool slopesApart;
    bool slopesOpposite;
    bool slopesDifferent;

    corners.clear();					// passed by ref, so we clear it first in case caller didn't
    cornerLines.clear();

    for(i = lines.begin(); i != lines.end(); i ++)
    {

        for(j = i + 1; j != lines.end(); j ++)
        {

            line1 = *i;
            line2 = *j;

            // determine if the lines are connected, at least
            if(Util::endPointDistance(line1, line2, connect) < MIN_END_POINT_DIST)
            {

                k = 0.7;
                added = false;
                while(!added && k >= 0.29)
                {

                    // compute the slope of each line using our utilities
                    slope1 = Util::computeSlope(line1);
                    slope2 = Util::computeSlope(line2);

                    // get the slope distance from our arbitrary slope value
                    slopeDiff1 = fabs(k - slope1);
                    slopeDiff2 = fabs(k - slope2);

                    // ensure that the lines are about the same distance away from k
                    slopesApart = (fabs(slopeDiff1 - slopeDiff2) < 0.075);

                    // ensure that the lines' slopes are on either side of k, our
                    // arbitrary slope
                    slopesOpposite = (slope1 < k && slope2 > k) ||
                                     (slope1 > k && slope2 < k);

                    // ensure that we don't have two mostly-overlapping lines
                    slopesDifferent = (fabs(slope1 - slope2) > 0.05);

                    // if all these conditions are met, we've likely found a corner
                    if(slopesApart && slopesOpposite && slopesDifferent)
                    {
                        corners.push_back(connect);
                        added = true;					// stop comparing these two lines

                        // add these two lines to our list of connecting corners
                        cornerLines.push_back(line1);
                        cornerLines.push_back(line2);

                    }

                    k -= 0.07;

                }

            }

        }

    }

}

// uses an upvote/downvote system to determine which pair of lines is most likely
// the left and right goal posts; then, if goal posts are found, it reorders the
// points that form the goal post so that the goal lines travel downwards
bool GoalTarget::voteOnGoalPosts(vector<Vec4i> &lines, Vec4i &left, Vec4i &right)
{

    const float PREFERRED_MIN_SLOPE = 0.85;		// we'd prefer our goal posts to be vertical

    //const float MIN_HEIGHT_SIMILARITY = 0.5;	// minimum and maximum preferred percentage height similarity
    //const float MAX_HEIGHT_SIMILARITY = 1.5;	// between two lines

    const float MIN_DISTANCE = 45.0;

    unsigned int i, j;
    int votes[lines.size()][lines.size()];		// votes for each pair of lines;
                                                // the more votes, the more likely this
                                                // pair forms the goalposts

    int highestI = 0;				// index i of highest-voted [j, i] pair
    int highestJ = 0;				// index j of highest-voted [j, i] pair
    int highestVote = 0;			// value of highest-voted [j, i] pair
    bool result = false;			// whether or not we've found goalpost candidates

    //int swap1, swap2;

    // votes going to each line pair start at zero
    for(j = 0; j < lines.size(); j ++)
    {
        for(i = j + 1; i < lines.size(); i ++)
        {

            Vec4i line1 = lines[j];
            Vec4i line2 = lines[i];
            votes[j][i] = 0;

            // ensure line goes downwards
            Util::orderLineTopToBottom(line1);
            Util::orderLineTopToBottom(line2);

            // get the slope of each line so we can see if they are
            // similarly oriented
            float slope1 = (Util::computeSlope(line1));
            float slope2 = (Util::computeSlope(line2));

            // if they have the similar slope, vote this pair up
            float slopeCompare = fabs(slope1 - slope2);
            if(slopeCompare < 0.2)
            {
                votes[j][i] += 10;
            }

            // if they don't have near-vertical slopes, vote them down
            if(slope1 < PREFERRED_MIN_SLOPE)
            {
                votes[j][i] -= 11;
            }

            // if they have similar lengths, vote them up
            /*
            float lengthCompare = Util::dist(line1) / Util::dist(line2);
            if(lengthCompare > MIN_HEIGHT_SIMILARITY && lengthCompare < MAX_HEIGHT_SIMILARITY)
            {
                votes[j][i] += 5;
            }*/

            // if they are very close together, then vote them down
            if(Util::dist(Point2f(line1[0], line1[1]), Point2f(line2[0], line2[1])) < MIN_DISTANCE ||
               Util::dist(Point2f(line1[0], line1[1]), Point2f(line2[2], line2[3])) < MIN_DISTANCE)
            {
                votes[j][i] -= 10;
            }

            // if this vote is higher than any so far, save it for later
            if(votes[j][i] > highestVote && votes[j][i] > 5)
            {
                highestJ = j;
                highestI = i;
                highestVote = votes[j][i];
                result = true;
            }

        }
    }

    // if we've found a candidate, return it
    if(result)
    {

        left = lines[highestJ];
        right = lines[highestI];

        // ensure we know which is left and which one is right
        if(left[0] > right[0])
        {
            Vec4i temp = right;
            right = left;
            left = temp;
        }

        // ensure goal lines go downwards
        Util::orderLineTopToBottom(left);
        Util::orderLineTopToBottom(left);

    }

    // indicate that we've found the goal posts (or something very similar!)
    return result;

}

void GoalTarget::Draw(Mat &canvas)
{

    vector<Point2f>::iterator i;

    const Scalar CROSSBAR_COLOR(0, 255, 255);											// yellow
    const Scalar LEFT_POST_COLOR[2] = {Scalar(255, 255, 0), Scalar(255, 0, 255)};		// cyan, purple
    const Scalar RIGHT_POST_COLOR[2] = {Scalar(0, 255, 0), Scalar(255, 0, 0)};			// green, blue

    // label the left goal post
    if(foundLeftPost)
    {
        line(canvas, Point(leftPost.start.X, leftPost.start.Y), Point(leftPost.end.X, leftPost.end.Y), LEFT_POST_COLOR[againstWhiteBackground], 4, CV_AA);
    }

    // label the right goal post
    if(foundRightPost)
    {
        line(canvas, Point(rightPost.start.X, rightPost.start.Y), Point(rightPost.end.X, rightPost.end.Y), RIGHT_POST_COLOR[againstWhiteBackground], 4, CV_AA);
    }

    // label the crossbar, if found
    if(foundRightPost && foundLeftPost)
    {
        line(canvas, Point(rightPost.start.X, rightPost.start.Y), Point(leftPost.start.X, leftPost.start.Y), CROSSBAR_COLOR, 4, CV_AA);
    }

    // label any sample points for detecting background whiteness
    for(i = samplePoints.begin(); i != samplePoints.end(); i ++)
    {
        circle(canvas, *i, 1, Scalar(0, 0, 255), 1);
    }

    // draw the bounding box around the goal
    boundingBox.Draw(canvas, markColour, 1);

}

void GoalTarget::Print()
{
    cout << "Goal Target:" << endl <<
            "\t Left Post: (" << leftPost.start.X << " , " << leftPost.start.Y << ") -- (" << leftPost.end.X << " , " << leftPost.end.Y << ")" << endl <<
            "\tRight Post: (" << rightPost.start.X << " , " << rightPost.start.Y << ") -- (" << rightPost.end.X << " , " << rightPost.end.Y << ")" << endl <<
            endl;
}

void GoalTarget::ProcessCandidates(vector<BoundingBox*> &candidates)
{
    (void)candidates;
    // do nothing
}


void GoalTarget::solveTriangle()
{
    /* Given the following (awful ASCII art) triangle:
     *
     *      X       X
     * A--------B--------C
     * |       /        /
     * |       /       /
     * |      /       /
     * |      /      /
     * |     /      /
     * |     /     /
     * |    /     /
     * |    /    /
     * |   /    /
     * |   /   /
     * |  /   /
     * |  /  /
     * | /  /
     * | / /
     * |/ /
     * |//
     * D
     *
     * A, B, C are co-linear and evenly separated by X
     * The robot is at D
     * For reference, known values below are in CAPS, unknowns are in lowercase
     *
     * Known values: angles ADB and BDC, lengths AB, BC
     *
     * Need to solve for: lengths ad, bd, cd
     *
     * Given standard Trig identities we know the following:
     * 1: ADB + bad + abd = 180
     * 2: BDC + cbd + bcd = 180
     * 3: abd + cdb = 180
     * 4: ADB + BDC + bad + bcd = 180
     * 5: sin(ABD)/X = sin(abd)/ad = sin(bad)/bd
     * 6: sin(BDC/X = sin(cbd)/cd = sin(cbd)/bd
     *
     * From these equations we can do some algebra and come up with the following equations:
     * 7:  bd = X * sin(180-ADB)/sin(ADB)
     * 8:  bcd = asin(bd * sin(BDC) / X)                                                = asin(sin(180-ADB)/sin(ADB) * sin(BDC))
     * 9:  cbd = 180 - BDC - bcd                                                        = 180 - BCD - asin(sin(180-ADB)/sin(ADB) * sin(BDC))
     * 10: abd = 180 - cbd                                                              = BDC + asin(sin(180-ADB)/sin(ADB) * sin(BDC))
     * 11: ad = X * sin(abd)/sin(ABD)                                                   = X * sin(BCD + asin(sin(180-ADB)/sin(ADB) * sin(BDC))) / sin(ADB)
     * 12: cd = X * sin(cbd)/sin(BDC)                                                   = X * sin(180 - BDC - asin(sin(180-ADB)/sin(ADB) * sin(BDC)) / sin(BDC)
     *
     * If we draw circles of radii ad, cd, and bd around points A, B, and C then the point where
     * all three circles intersect is the robot's current position
     *
     * There may be multiple solutions, so we'll need to check all of them (filtering out any negative or imaginary solutions)
     */

    // the following is implemented entirely using RADIANS and MILLIMETERS!!!

    const double ADB = deg2rad(GetLeftPostAngle() - GetAngle());
    const double BDC = deg2rad(GetAngle() - GetRightPostAngle());
    const double X = realWidth/2;

    double bd = X * sin(PI-ADB)/sin(ADB);
    double bcd = asin(bd * sin(BDC) / X);
    double cbd = PI - BDC - bcd;
    double abd = PI - cbd;
    double ad = X * sin(abd)/sin(ADB);
    double cd = X * sin(cbd)/sin(BDC);

#ifdef DEBUG
    cerr << "ADB = " << rad2deg(ADB) << endl <<
            "BDC = " << rad2deg(BDC) << endl <<
            "  X = " << X << endl <<
            " bd = " << bd << endl <<
            "bcd = " << rad2deg(bcd) << endl <<
            "cbd = " << rad2deg(cbd) << endl <<
            "abd = " << rad2deg(abd) << endl <<
            " ad = " << ad << endl <<
            " cd = " << cd << endl <<
            endl;
#endif

    leftPostRange = ad;
    rightPostRange = cd;
    centerRange = bd;
}
