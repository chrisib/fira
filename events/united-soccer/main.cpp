#include <SoccerPlayer.h>
#include <pthread.h>
#include <iostream>
#include <SocketMonitor.h>
#include <cstring>

using namespace std;

int main(int argc, char *argv[])
{
    SoccerPlayer *player = SoccerPlayer::Create(argc, argv);

    cout << "*** Set up complete ***" << endl;
    int result = player->Execute();
    delete player;
    return result;
}

