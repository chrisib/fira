#ifndef UNITEDSOCCER_H
#define UNITEDSOCCER_H

// if this is uncommented then the team owning a found goal is
// determined 100% on the background colour
// presence of goalies, orientation data, etc... are IGNORED
// this is a bit of a dirty RoboCup 2013 hack that leverages unique environmental conditions
// (namely white walls separating the fields at one end only)
#define RELY_ON_WHITE_BACKGROUND

#include <darwin/framework/Target.h>
#include <darwin/framework/MultiBlob.h>
#include <darwin/framework/SingleBlob.h>
#include <darwin/framework/BallFollower.h>
#include <darwin/framework/BallTracker.h>
#include <vector>
#include "SocketMonitor.h"
#include "VisionMap.h"
#include "FrameRateMonitor.h"
#include "GoalTarget.h"
#include "SoccerField.h"
#include "PlayerTarget.h"
#include "BallInfo.h"
#include <darwin/framework/minIni.h>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <pthread.h>
#include <ThreadPauser.h>
#include <inttypes.h>

//#define DEBUG

#define PASS_FORWARD_LEFT_MOTION         13
#define PASS_FORWARD_RIGHT_MOTION        12
#define KICK_LEFT_MOTION                 61
#define KICK_RIGHT_MOTION                60

#ifdef USE_TIAGOS_LATERAL_KICKS
#define PASS_BALL_RIGHT_MOTION           70
#define PASS_BALL_LEFT_MOTION            71
#else
#define PASS_BALL_RIGHT_MOTION           180
#define PASS_BALL_LEFT_MOTION            190
#endif

#define PASS_BALL_OUTSIDE_RIGHT_MOTION  200
#define PASS_BALL_OUTSIDE_LEFT_MOTION   210

//TODO: move these to config.ini?
/*
#define LEFT_FOOT_RIGHT_THRESHOLD 0
#define LEFT_FOOT_LEFT_THRESHOLD 25
#define RIGHT_FOOT_RIGHT_THRESHOLD -30
#define RIGHT_FOOT_LEFT_THRESHOLD -5
#define LEFT_FORWARD_LEFT_THRESHOLD 13.0
#define LEFT_FORWARD_RIGHT_THRESHOLD 0.0
#define RIGHT_FORWARD_LEFT_THRESHOLD -4.5
#define RIGHT_FORWARD_RIGHT_THRESHOLD -17.5
#define LEFT_INSIDE_LEFT_THRESHOLD 1.5
#define LEFT_INSIDE_RIGHT_THRESHOLD -12.0
#define RIGHT_INSIDE_LEFT_THRESHOLD 6.5
#define RIGHT_INSIDE_RIGHT_THRESHOLD -6.0
#define LEFT_OUTSIDE_LEFT_THRESHOLD 26.5
#define LEFT_OUTSIDE_RIGHT_THRESHOLD 17.0
#define RIGHT_OUTSIDE_LEFT_THRESHOLD -21.0
#define RIGHT_OUTSIDE_RIGHT_THRESHOLD -27.0
*/
#define DEGREES_PER_PIVOT_STEP 10
#define MIN_SCAN_TILT -15
#define MAX_SCAN_TILT 40
#define MIN_SCAN_PAN -80
#define MAX_SCAN_PAN 80
#define LOW_SCAN_TILT -10
#define HIGH_SCAN_TILT 30
#define HEAD_DAMPING_FACTOR 0.33333
//#define LOST_FRAMES_BEFORE_SCAN 4
#define FRAMES_AFTER_SCAN 5

class SoccerPlayer
{
public:
    static SoccerPlayer *Create(int argc, char *argv[]);
    SoccerPlayer(minIni *ini, SocketMonitor *sockMon); // use UnitedSoccerPlayer::Create, not the constructor, unless you know what you're doing!!
    virtual ~SoccerPlayer();

    // run process() in an infinite loop
    virtual int Execute();

    // initialize the robot and cameras
    virtual void Initialize();

    // analyze the current world-state and act accordingly
    virtual void Process();

    // handle a frame of video on a timer interrupt
    virtual void HandleVideo();
    virtual void AnnotateAndRecordVideo();

    enum TEAM_COLOUR {
        TEAM_RED = 1,
        TEAM_BLUE = 0
    };

    enum POSITION {
        POSITION_FIELD = 0,
        POSITION_GOALIE = 1
    };

    enum PLAYER_STATE {
        STATE_WAITING = 0,
        STATE_READY,
        STATE_WALKING,
        STATE_PLAYING,
        STATE_HW_INTERRUPT,

        NUM_STATES
    };

    enum PLAYING_SUB_STATE {
        SUBSTATE_FINDING_BALL = 0,
        SUBSTATE_CHASING_BALL,
        SUBSTATE_FINAL_APPROACH,
        SUBSTATE_KICKING,


        NUM_SUBSTATES
    };


    // accessors needed by socket monitor classes
    int getId(){return myId;}
    int getCurrentState(){return myState;}
    int getPrevState(){return myLastState;}
    int getSubstate(){return playSubstate;}
    SoccerField &getFieldMap(){return *field;}
    VisionMap &getVisionMap(){return map;}
    SocketMonitor &getReferee(){return *socketMonitor;}
    int getPacketSequence(){return numPacketsSent;}
    bool isKeeper(){return isGoalie;}

    static bool DEBUG_PRINT;


protected:
    enum {
        KICK_DIRECTION_FORWARD,
        KICK_DIRECTION_LEFT,
        KICK_DIRECTION_RIGHT
    };

    // configuration parameters from command-line
    bool showVideo;
    bool recordVideo;
    bool visionTest;
    bool noNetwork;
    bool networkTest;
    bool isGoalie;
    bool isDefender;
    bool penaltyKick;
    bool testKick;
    bool ignoreGoals;
    int subsample;

    // game-related config settings
    int myId;       // the robot's network ID
    int myState;    // the robot's internal state
    int myLastState; // the robot's last internal state
    int playSubstate;
    int chosenKick;
    int refState;   // the robot's state as allowed by the referee
    int myColour;   // is the robot on the red or the blue team?
    int theirColour;
    int myPosition; // the robot's position (currently goalie or non-goalie)
    int gameState;  // current state of the game as reported by the (volatile) referee
    int lastGameState; //last state of the game
    bool newGameState; //true if this game state is different from last frame's
    int lastPlaySubstate;
    bool newPlaySubstate;
    bool onPenalty; // am i currently on penalty?
    bool myGoalWhiteBackgroundInFirstHalf;

    double RUN_UP_DISTANCE; // the range at which we stop caring about the angle to the ball/goal and just go straight for the ball and kick it
    int MIN_CONFIDENCE_FOR_AIMED_KICK; // the minimum confidence in our position required for an aimed kick; otherwise we'll just blast the ball forward

    int LOST_FRAMES_BEFORE_SCAN;		// how many frames it takes before it determines that the ball is not really in front of the robot
    double LEFT_FOOT_RIGHT_THRESHOLD;
    double LEFT_FOOT_LEFT_THRESHOLD;
    double RIGHT_FOOT_RIGHT_THRESHOLD;
    double RIGHT_FOOT_LEFT_THRESHOLD;
    double LEFT_FORWARD_LEFT_THRESHOLD;
    double LEFT_FORWARD_RIGHT_THRESHOLD;
    double RIGHT_FORWARD_LEFT_THRESHOLD;
    double RIGHT_FORWARD_RIGHT_THRESHOLD;
    double LEFT_INSIDE_LEFT_THRESHOLD;
    double LEFT_INSIDE_RIGHT_THRESHOLD;
    double RIGHT_INSIDE_LEFT_THRESHOLD;
    double RIGHT_INSIDE_RIGHT_THRESHOLD;
    double LEFT_OUTSIDE_LEFT_THRESHOLD;
    double LEFT_OUTSIDE_RIGHT_THRESHOLD;
    double RIGHT_OUTSIDE_LEFT_THRESHOLD;
    double RIGHT_OUTSIDE_RIGHT_THRESHOLD;

    double SCAN_AMPLITUDE;

    Robot::BallTracker tracker;
    Robot::BallFollower follower;

    // vision parameters
    Robot::CameraPosition cameraPosition;
    int noBallFrames;
    int maxNoBallFrames;

    VisionMap map;
    SoccerField *field;
    FrameRateMonitor *frMonitor;
    BallInfo *lastBallInfo;

    // pivot/turn walking parameters
    double maxKickRange;

    pthread_t videoThreadID;
    //pthread_t buttonThreadID;
    minIni *ini;

    ThreadPauser pauser;

    // images for display/video stream
    cv::Mat rgbImage;
    cv::VideoWriter webcamStream;
    cv::VideoWriter debugStream;
    cv::VideoWriter mapStream;

    // UDP Tx packet information
    int lastUpdateSentAt;
    uint16_t numPacketsSent;

    // UDP Rx packet information
    SocketMonitor *socketMonitor;

    // timestamps of when we last processed a message from each player
    int lastMessageAt[SocketMonitor::MAX_PLAYERS_PER_TEAM * 2];

    // all targets collected into a vector for easy processing
    std::vector<Robot::BlobTarget*> targets;

    // head & ball tracking controls
    bool alreadyTracked;
    double headTrackingPan, headTrackingTilt;
    double dPan, dTilt;
    void handleLostBall();    // we've lost the ball and need to start looking around

    bool refSaysWeCanPlay;

    // check the packets from the referee and see what we are allowed to do
    void checkRefStatus();

    // read config.ini
    void loadIniSettings();

    // look around to see if we can see the goal and/or ball
    int lookCycles;
    void lookAround();

    //variables related to scan method
    double headPan;
    double headTilt;
    int scanDirection;
    bool firstSweep;
    bool headMoving;
    int scanFrames;
    int lostFrames;

    //scan moves head; scanAround checks for the ball and calls lookAt if found, scan if not
    void scan(bool scanLow);
    void quickScan(); //does a quick scan, gathering as much information as it can
    bool foundGoalDuringQuickScan;

    // handle walking to our goal position
    // positions are in (cm,cm,deg)
    Robot::Point3D homePosition;
    Robot::Point3D walkingDestination;
    std::vector<Robot::Point3D> penaltyEntrances;
    Robot::Point3D kickoffPosition;
    bool takesKickoff;
    void processWalking();
    void walkTo(Robot::Point3D &point);

    std::list<Robot::Point3D> waypoints;
    void setSearchWaypoints();

    // handle mid-game substates
    void choosePlayerState(bool prevOnPenalty);
    void processFindBall();
    void processChaseBall();
    void processFinalApproach();
    void processKick();

    Robot::Point3D calculateChaseBallDestination();
    int chooseKickDirection();
    int chooseKick(int direction);

    void updateOdometry();
    int stepCounter;

    // returns -1/0/1 if the ball is right/ok/left of the necessary position
    int ballInFrontOfRightFoot();
    int ballInFrontOfLeftFoot();
    int ballBesideLeftFoot();
    int ballBesideRightFoot();
    int ballPosnForKick();

    // outside passes
    void passOutsideLeft();
    void passOutsideRight();

    void sitDown();
    void standUp();

    // check if we've fallen over and stand up if necessary
    bool handleFall();

    // check the button state and deal with it accordingly
    bool handleButton();

    // transmit our current status as a UDP packet
    static const unsigned int BROADCAST_INTERVAL = 100;
    unsigned long int lastBroadcastTime;
    virtual void broadcastPacket();

    // can we play right now?
    bool canPlay();

    //transmit status packets, if it is time to do so
    void transmitStatus();

    // are we in a testing/debug mode?
    bool inTestMode();

    //finds the current game state
    int findGameState();

    // arg is the player
    static void* videoThread(void* arg);
    static void* buttonThread(void* arg);
    static void yieldThread();

    // dynamically change the robot's hip pitch offset based on the walking amplitudes
    void adjustHipPitch();

    static bool pointInArc(Robot::Point2D &p, Robot::Point2D &arcCentre, double arcRadius, double theta1, double theta2);

};

#endif // UNITEDSOCCER_H
