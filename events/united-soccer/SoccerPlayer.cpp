#include "SoccerPlayer.h"
#include <pthread.h>
#include <LinuxDARwIn.h>
#include <iostream>
#include <darwin/framework/Target.h>
#include <darwin/linux/CvCamera.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/Voice.h>
#include <ctime>
#include <cstring>
#include <cstdio>
#include <darwin/framework/Kinematics.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "FiraSocketMonitor.h"
#include "RoboCupSocketMonitor.h"
#include "Goalie.h"
#include "RoboCupField.h"
#include "FiraSoccerField.h"

using namespace std;
using namespace Robot;
using namespace cv;

bool SoccerPlayer::DEBUG_PRINT = false;

SoccerPlayer *SoccerPlayer::Create(int argc, char *argv[])
{
    bool isGoalie = false;
    bool showVideo = false;
    bool visionTest = false;
    bool recordVideo = false;
    bool noNetwork = false;
    bool networkTest = false;
    bool noDives = false;
    bool executePenalty = false;
    bool testKick = false;
    bool ignoreGoals = false;
    bool isDefender = false;
    int chosenKick = KICK_RIGHT_MOTION;
    int subsample = 2;
    const char* configPath = "config.ini";

    for(int i=1; i<argc; i++)
    {
        if(!strcmp(argv[i], "--goalie"))
            isGoalie = true;
        else if(!strcmp(argv[i], "--defender"))
            isDefender = true;
        else if(!strcmp(argv[i], "--penalty"))
            executePenalty = true;
        else if(!strcmp(argv[i], "--show-video"))
            showVideo = true;
        else if(!strcmp(argv[i], "--vision-test"))
            visionTest = true;
        else if(!strcmp(argv[i], "--record-video"))
            recordVideo = true;
        else if(!strcmp(argv[i],"--no-network"))
            noNetwork = true;
        else if(!strcmp(argv[i], "--network-test"))
            networkTest = true;
        else if(!strcmp(argv[i], "--no-dives"))
            noDives = true;
        else if(!strcmp(argv[i], "--ignore-goals"))
            ignoreGoals = true;
        else if(!strcmp(argv[i],"--debug"))
            DEBUG_PRINT = true;
        else if(!strcmp(argv[i], "--subsample"))
        {
            i++;
            if(i >= argc)
            {
                cerr << "ERROR: no subsample provided" << endl;
                exit(1);
            }

            subsample = atoi(argv[i]);

            if(subsample < 1)
            {
                cerr << "ERROR: subsample cannot be " << subsample << endl;
                exit(1);
            }
        }
        else if(!strcmp(argv[i],"--kick"))
        {
            testKick = true;
            if(!strcmp(argv[i+1],"l"))
            {
                chosenKick = PASS_BALL_LEFT_MOTION;
                i++;
            }
            else if(!strcmp(argv[i+1],"r"))
            {
                chosenKick = PASS_BALL_RIGHT_MOTION;
                i++;
            }
            else if(!strcmp(argv[i+1],"fl"))
            {
                chosenKick = KICK_LEFT_MOTION;
                i++;
            }
            else if(!strcmp(argv[i+1],"fr"))
            {
                chosenKick = KICK_RIGHT_MOTION;
                i++;
            }
            else
            {
                cerr << "ERROR: unknown kick to test: " << argv[i+1] << endl;
                exit(1);
            }

        }
        else if(!strcmp(argv[i],"-c") || !strcmp(argv[i],"--config"))
        {
            i++;
            if(argc >= i)
            {
                cerr << "ERROR: no config file provided" << endl;
                exit(1);
            }

            configPath = argv[i];
        }
        else
        {
            cerr << endl <<
                    "Usage:" << endl <<
                    "\tunited-soccer [options]" << endl << endl <<
                    "Argument summaries:" << endl <<
                    "\t--goalie       this player is playing as the goalie" << endl <<
                    "\t--defender     this player is a defender; kick forward and sit down" << endl <<
                    "\t--penalty      execute penalty kick behaviour instead of regular play" << endl <<
                    "\t--show-video   display vision in an X-window" << endl <<
                    "\t--record-video record the video streams to .avi files for debugging" << endl <<
                    "\t--no-network   do not listen for the referee on the network" << endl <<
                    "\t--subsample    specify the scaline subsample (default 2)" << endl <<
                    "\t--vision-test  do not power the motors; simply process the vision [does not show windows]" << endl <<
                    "\t--kick [dir]   kick the ball in the direction (fl|fr|l|r)" << endl <<
                    "\t--ignore-goals ignore the goal posts, don't orient, and kick blindly forwards all the time" << endl <<
                    "\t-c|--config    set the path to the configuration file (default config.ini)" << endl <<
                    "\t--debug        enable extra debug printing" << endl <<
                    "\t--help         show this message" << endl;
            exit(0);

        }
    }

    // create the ini loader
    cout << "Opening configuration file " << configPath << endl;
    minIni *ini = new minIni(configPath);

    SoccerField *field;
    if(ini->geti("Soccer","RoboCup",0))
    {
        field = new RoboCupField();
    }
    else
    {
        field = new FiraSoccerField();
    }

    // create the socket monitor
    cout << "Configuring Networking" << endl;
    SocketMonitor *sockMon;
    if(ini->geti("Soccer","RoboCup",0))
    {
        cout << "Using RoboCup network protocol" << endl;
        sockMon = new RoboCupSocketMonitor(ini->geti("Network","Port", SocketMonitor::DEFAULT_PORT),*field);
    }
    else
    {
        cout << "Using FIRA network protocol" << endl;
        sockMon = new FiraSocketMonitor(ini->geti("Network","Port", SocketMonitor::DEFAULT_PORT),*field);
    }
    sockMon->DEBUG_PRINT = networkTest;

    cout << "Done. Listening on port " << ini->geti("Network","Port") << endl;

    SoccerPlayer *player;

    if(isGoalie)
    {
        cout << "Creating Goalie" << endl;
        player = new Goalie(ini, sockMon, noDives);
        player->myPosition = SoccerPlayer::POSITION_GOALIE;
    }
    else
    {
        cout << "Creating Standard Player" << endl;
        player = new SoccerPlayer(ini, sockMon);
        player->myPosition = SoccerPlayer::POSITION_FIELD;
    }

    player->showVideo = showVideo;
    player->visionTest = visionTest;
    player->noNetwork = noNetwork && !networkTest;
    player->networkTest = networkTest;
    player->recordVideo = recordVideo;
    player->subsample = subsample;
    player->isGoalie = isGoalie;
    player->isDefender = isDefender;
    player->penaltyKick = executePenalty;
    player->lastGameState = -1;
    player->myLastState = -1;
    player->lastPlaySubstate = -1;
    player->chosenKick = chosenKick;
    player->testKick = testKick;
    player->ignoreGoals = ignoreGoals;
    player->field = field;
    player->Initialize();
    cout << "Done Creating Player" << endl;

    return player;
}

SoccerPlayer::SoccerPlayer(minIni *ini, SocketMonitor *sockMon)
{
    this->ini = ini;
    this->frMonitor = new FrameRateMonitor();
    this->lastBallInfo = new BallInfo();
    showVideo = true;
    recordVideo = false;
    visionTest = false;
    subsample = 2;
    noBallFrames = 0;

    headPan = 0;
    headTilt = 0;
    scanDirection = 0;
    firstSweep = true;
    headMoving = true;
    scanFrames = 0;
    lostFrames = 0;

    lookCycles = 0;
    dPan = 1;
    dTilt = 1;

    rgbImage = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);

    socketMonitor = sockMon;

    lastUpdateSentAt = 0;
    numPacketsSent = 0;
    refSaysWeCanPlay = false;
    onPenalty = false;

    for(int i=0; i<SocketMonitor::MAX_PLAYERS_PER_TEAM*2; i++)
        lastMessageAt[i] = -1;

    // initialize the camera
    if(!CvCamera::GetInstance()->Initialize(ini->geti("Camera","Device",0)))
    {
        cerr << "Failed to initialize camera" << endl;
        exit(-1);
    }

    CvCamera::GetInstance()->LoadINISettings(ini);
}

SoccerPlayer::~SoccerPlayer()
{
    delete ini;
    delete socketMonitor;
    delete field;
    delete[] frMonitor;
    delete[] lastBallInfo;
}

void SoccerPlayer::Initialize()
{
    map.setSubsample(subsample);

    if(!visionTest && !networkTest)
    {
        if(!LinuxDARwIn::Initialize(ini->gets("Files","MotionFile").c_str(), Action::DEFAULT_MOTION_SIT_DOWN))
        {
            cerr << "Failed to initialize" << endl;
            exit(-1);
        }
        //LinuxDARwIn::Initialize();

        // disable modules before adding them
        Walking::GetInstance()->m_Joint.SetEnableBody(false);
        Head::GetInstance()->m_Joint.SetEnableBody(false);
        LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
        RightArm::GetInstance()->m_Joint.SetEnableBody(false);

        // insert the new modules
        MotionManager::GetInstance()->AddModule(Walking::GetInstance());
        MotionManager::GetInstance()->AddModule(Head::GetInstance());
        MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
        MotionManager::GetInstance()->AddModule(RightArm::GetInstance());

        Walking::GetInstance()->LoadINISettings(ini);

        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

        // relax the hands
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();
        sitDown();

        // remove the Action module
        //MotionManager::GetInstance()->RemoveModule(Action::GetInstance());
    }

    field->Initialize(&map,*ini);
    loadIniSettings();

    if(myPosition == POSITION_GOALIE)
    {
        takesKickoff = false;
        kickoffPosition.X = homePosition.X = field->myGoalCenter.X;
        kickoffPosition.Y = homePosition.Y = field->myGoalCenter.Y + 30;
        kickoffPosition.Z = homePosition.Z = 0;
    }
    else
    {
        if(takesKickoff)
        {
            // the robot that takes the kickoff plays further up the field
            kickoffPosition.X = homePosition.X = field->GetFieldWidthCM()/2;
            kickoffPosition.Y = homePosition.Y = field->GetFieldLengthCM()/2 - field->GetCenterRadiusCM();
            kickoffPosition.Z = homePosition.Z = 0;
        }
        else
        {
            // the robot that does not take penalty kicks is further back and to the left
            kickoffPosition.X = homePosition.X = field->GetFieldWidthCM()/2;
            kickoffPosition.Y = homePosition.Y = field->GetCreaseDepthCM();
            kickoffPosition.Z = homePosition.Z = 0;
        }
    }
    cout << "Home Position: " << homePosition.X << " " << homePosition.Y << " " << homePosition.Z << endl;
    field->SetPosition(homePosition);

    // center the head & look upwards (so we can see the field)
    Head::GetInstance() -> MoveByAngle(0, 30);
    Voice::Initialize("default");

    myState = STATE_WAITING;

    if(showVideo || visionTest)
    {
        namedWindow("Field", CV_WINDOW_AUTOSIZE);
        namedWindow("Webcam", CV_WINDOW_AUTOSIZE);
        namedWindow("Scanline", CV_WINDOW_AUTOSIZE);
        namedWindow("Map", CV_WINDOW_AUTOSIZE);

        cvMoveWindow("Webcam",0,0);
        cvMoveWindow("Scanline",0,280);
        cvMoveWindow("Map",320,0);
        cvMoveWindow("Field",320 + map.mapImage.cols + 12, 0);

        imshow("Field",field->fieldImage);
        imshow("Map",map.mapImage);
        imshow("Webcam", rgbImage);
        imshow("Scanline", map.dbgImage);

        cvWaitKey(1);
    }

    Voice::Speak("Initialization complete");
}

void SoccerPlayer::loadIniSettings()
{
    if(recordVideo)
    {
        cout << "Video streams will be recorded to " <<
                ini->gets("Files","WebcamStream") << ", " <<
                ini->gets("Files","ScanlineStream") << " and " <<
                ini->gets("Files","MapStream") << endl;

        webcamStream = VideoWriter (ini->gets("Files","WebcamStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        debugStream = VideoWriter (ini->gets("Files","ScanlineStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        mapStream = VideoWriter(ini->gets("Files","MapStream"), CV_FOURCC('D','I','V','X'), 30, cvSize(VisionMap::IMAGE_SIZE, VisionMap::IMAGE_SIZE),true);
    }

    Walking::GetInstance()->LoadINISettings(ini);
    maxNoBallFrames = ini->geti("Soccer","MaxNoBallFrames");
    myId = ini->geti("Soccer","ID");

    maxKickRange = ini->getd("Kicks","MaxKickRange");
    LOST_FRAMES_BEFORE_SCAN = ini -> getd("Kicks", "LostFramesBeforeScan");
    LEFT_FOOT_RIGHT_THRESHOLD = ini -> getd("Kicks", "LeftFootRightThreshold");
    LEFT_FOOT_LEFT_THRESHOLD = ini -> getd("Kicks", "LeftFootLeftThreshold");
    RIGHT_FOOT_RIGHT_THRESHOLD = ini -> getd("Kicks", "RightFootRightThreshold");
    RIGHT_FOOT_LEFT_THRESHOLD = ini -> getd("Kicks", "RightFootLeftThreshold");
    LEFT_FORWARD_LEFT_THRESHOLD = ini -> getd("Kicks", "LeftForwardLeftThreshold");
    LEFT_FORWARD_RIGHT_THRESHOLD = ini -> getd("Kicks", "LeftForwardRightThreshold");
    RIGHT_FORWARD_LEFT_THRESHOLD = ini -> getd("Kicks", "RightForwardLeftThreshold");
    RIGHT_FORWARD_RIGHT_THRESHOLD = ini -> getd("Kicks", "RightForwardRightThreshold");
    LEFT_INSIDE_LEFT_THRESHOLD = ini -> getd("Kicks", "LeftInsideLeftThreshold");
    LEFT_INSIDE_RIGHT_THRESHOLD = ini -> getd("Kicks", "LeftInsideRightThreshold");
    RIGHT_INSIDE_LEFT_THRESHOLD = ini -> getd("Kicks", "RightInsideLeftThreshold");
    RIGHT_INSIDE_RIGHT_THRESHOLD = ini -> getd("Kicks", "RightInsideRightThreshold");
    LEFT_OUTSIDE_LEFT_THRESHOLD = ini -> getd("Kicks", "LeftOutsideLeftThreshold");
    LEFT_OUTSIDE_RIGHT_THRESHOLD = ini -> getd("Kicks", "LeftOutsideRightThreshold");
    RIGHT_OUTSIDE_LEFT_THRESHOLD = ini -> getd("Kicks", "RightOutsideLeftThreshold");
    RIGHT_OUTSIDE_RIGHT_THRESHOLD = ini -> getd("Kicks", "RightOutsideRightThreshold");

    SCAN_AMPLITUDE = ini -> getd("Soccer","ScanAmplitude",5.0);

    myColour = (strcmp(ini->gets("Soccer","Colour").c_str(),"red") == 0) ? TEAM_RED : TEAM_BLUE;
    if(myColour == TEAM_RED)
        theirColour = TEAM_BLUE;
    else
        theirColour = TEAM_RED;
    takesKickoff = ini->geti("Soccer","TakesKickoff");
    myGoalWhiteBackgroundInFirstHalf = ini->geti("Soccer","OurGoalWhiteInFirstHalf");

    int myEyeColour = ini->geti("Soccer", "EyeColour"); // 0 for off, 1 for team colour, -1 for default eye colour
    if( myEyeColour == 0 && !visionTest && !networkTest) {
        MotionManager::GetInstance()->SetEyeColour(0, 0, 0);
        MotionManager::GetInstance()->SetForeheadColour(0, 0, 0);
    }

    // there are 2 penalty entrances; one on each side of the field
    // create both and push them into the vector that stores them
    int tmp = fabs(ini->geti("Soccer","PenaltyEntranceX"));
    Point3D penaltyEntrance;
    penaltyEntrance.X = -tmp;
    penaltyEntrance.Y = ini->getd("Soccer","PenaltyEntranceY");
    penaltyEntrance.Z = -90;
    penaltyEntrances.push_back(penaltyEntrance);

    penaltyEntrance.X = field->GetFieldWidthCM() + tmp;
    penaltyEntrance.Y = ini->getd("Soccer","PenaltyEntranceY");
    penaltyEntrance.Z = 90;
    penaltyEntrances.push_back(penaltyEntrance);

    RUN_UP_DISTANCE = ini->getd("Soccer","RunUpDistance");
    MIN_CONFIDENCE_FOR_AIMED_KICK = ini->geti("Soccer","MinConfidenceForAimedKick",128);

    map.LoadIniSettings(ini,"Polar Map");

    socketMonitor->loadIniSettings(ini);

    cout << "On team " << (myColour==TEAM_RED ? "red" : (myColour==TEAM_BLUE ? "blue" : "unknown")) << " player ID " << myId << endl;
}

int SoccerPlayer::Execute()
{
    cout << "Starting video thread..." << endl;
    pthread_create(&videoThreadID, NULL, videoThread, this);
    //pthread_create(&buttonThreadID, NULL, buttonThread, this);
    cout << "done" << endl;

    // perform specific tests if necessary
    bool exitImmediately = false;
    if(visionTest)
    {
        cout << "Performing vision test" << endl;
        field->UnknownPosition();
    }
    else if(networkTest)
    {
        cout << "Performing network test" << endl;
    }
    else if(testKick)
    {
        Action::GetInstance()->m_Joint.SetEnableBody(true,true);
        Action::GetInstance()->Start(Action::DEFAULT_MOTION_WALKREADY);
        Action::GetInstance()->Finish();
        MotionManager::GetInstance()->msleep(5000);
        cout << "Executing kick test" << endl;
        processKick();
        cout << "Done" << endl;

        exitImmediately = true;
    }

    if(exitImmediately)
        return 0;

    for(;;)
        Process();

    return 0;
}

bool SoccerPlayer::inTestMode()
{
    return visionTest || networkTest || testKick;
}

void SoccerPlayer::Process()
{
    //bool prevCanPlay = canPlay();
    bool prevOnPenalty = onPenalty;
    bool newState = false;
    checkRefStatus();

    // transmit status packets at regular intervals (if we're using the network)
    transmitStatus();

    handleButton();

    if(!inTestMode() && refSaysWeCanPlay)
    {
        handleFall();

        adjustHipPitch();

        // figure out what we need to be doing right now
        findGameState();
        choosePlayerState(prevOnPenalty);

        newState = myLastState != myState;  // is this a new player state?
        myLastState = myState;

        // stand up if we were just on penalty
        if(prevOnPenalty && myState != STATE_HW_INTERRUPT && myState != STATE_WAITING)
        {
            cout << "[info] Leaving Penalty State" << endl;
            Head::GetInstance()->MoveByAngle(0,30); // look straight ahead and angled up so we can see goal posts for orientation
            standUp();
        }

        switch(myState)
        {
        case STATE_PLAYING:
            newPlaySubstate = lastPlaySubstate != playSubstate;
            lastPlaySubstate = playSubstate;

            switch(playSubstate)
            {
            case SUBSTATE_CHASING_BALL:
                if(newState || newPlaySubstate)
                    cout << "[state] PLAYING :: CHASING" << endl;
                processChaseBall();
                break;

            case SUBSTATE_FINAL_APPROACH:
                if(newState || newPlaySubstate)
                    cout << "[state] PLAYING :: FINAL APPROACH" << endl;
                processFinalApproach();
                break;

            case SUBSTATE_KICKING:
                if(newState || newPlaySubstate)
                    cout << "[state] PLAYING :: KICKING" << endl;
                processKick();
                break;

            case SUBSTATE_FINDING_BALL:
                if(newState || newPlaySubstate)
                {
                    cout << "[state] PLAYING :: SEARCH FOR BALL" << endl;
                    setSearchWaypoints();
                    handleLostBall();
                    Walking::GetInstance()->Stop();
                    Walking::GetInstance()->Finish();
                }
                processFindBall();
                break;

            default:
                if(newState || newPlaySubstate)
                    cout << "[state] PLAYING :: unk. (fallback to search)" << endl;
                playSubstate = SUBSTATE_FINDING_BALL;
                break;
            }
            break;

        case STATE_WALKING:
            /*if(newState) {
                field.SetPosition(Point3D(0,300,180));
            }*/

            if(newState)
                cout << "[state] WALKING (" << field->presentPosition.X << " , " << field->presentPosition.Y << " , " << field->presentPosition.Z << ") --> (" <<
                    walkingDestination.X << " , " << walkingDestination.Y << " , " << walkingDestination.Z << ")" << endl;
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            if(!Walking::GetInstance()->IsRunning())
            {
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->Start();
            }
            processWalking();
            break;

        case STATE_READY:
            if(newState)
                cout << "[state] READY" << endl;
            if(newState)
            {
                standUp();
            }
            break;

        case STATE_WAITING:
        case STATE_HW_INTERRUPT:
        default:
            if(newState)
                cout << "[state] IDLE" << endl;
            sitDown();
            break;
        }
    }
    else    // we're either on penalty or in a test mode
    {
        if(newState)
            cout << "Waiting" << endl;

        // just sit down and wait
        sitDown();
    }
}

void SoccerPlayer::choosePlayerState(bool prevOnPenalty)
{
    switch(gameState)
    {
    case SocketMonitor::STATE_READY:
        /*
        if(newGameState)
        {
            if(myState != STATE_WALKING)
            {
                standUp();
            }
            myState = STATE_WALKING;

            if(takesKickoff && socketMonitor->getKickoffTeam() == myColour)
            {
                walkTo(kickoffPosition);
            }
            else
            {
                walkTo(homePosition);
            }
        }*/

        break;

    case SocketMonitor::STATE_SET:
        myState = STATE_READY;

        // STOP MOVING!!!
        if(Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->Stop();
            standUp();

            // CIB
            // set the home position since we're manually positioning the robots
            // this ensures that the robot's orientation is mostly-correct at the beginning of the game
            field->SetPosition(homePosition);
        }
        break;


    case SocketMonitor::STATE_PLAY:
        if(prevOnPenalty && !onPenalty && !noNetwork)   // only worry about this if we actually are listening to the referee
        {
            // just came off penalty; walk  back to our starting position
            cout << "[info] Re-entering field from penalty" << endl;
            field->SetCandidatePositions(penaltyEntrances);

            // this doesn't work, for whatever reason, so we just take some steps forward
            /*
            walkTo(homePosition);
            */

            Voice::Speak("Entering from penalty");
            standUp();
            Walking::GetInstance()->m_Joint.SetEnableBody(false);
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Start();
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 20;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;

            int stepCount = Walking::GetInstance()->GetStepCount();
            const int NUM_STEPS = 12;	// completely arbitrary
            while(Walking::GetInstance()->GetStepCount() - stepCount < NUM_STEPS)
            {
                handleFall();
            }
        }
        else if(!onPenalty)
        {
            if(myState == STATE_READY || myState == STATE_WAITING) // only re-enter the playing state if we're ready to
            {
                standUp();
                myState = STATE_PLAYING;
            }
        }
        break;

    case SocketMonitor::STATE_INITIAL:
    case SocketMonitor::STATE_FINISHED:
    default:
        // no action going on, so just sit down and wait
        myState = STATE_WAITING;
        break;
    }
}

void SoccerPlayer::updateOdometry()
{
    int newCount = Walking::GetInstance()->GetStepCount();

    // removed; we deal with the particle filter inside the vision-processing thread -- CIB
#if 0
    if(newCount != 0 && newCount != stepCounter)
    {
        // update the particle filter
        field->Update(Walking::GetInstance()->X_MOVE_AMPLITUDE, Walking::GetInstance()->Y_MOVE_AMPLITUDE, Walking::GetInstance()->A_MOVE_AMPLITUDE);
    }
#endif
    stepCounter = newCount;
}

void SoccerPlayer::processChaseBall()
{
    if(isDefender)
    {
        if(Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->Stop();
            Walking::GetInstance()->Finish();

            standUp();
            sitDown();
        }

        if(handleFall())
            sitDown();

        if(map.ballPosition.X > 0 &&               // we actually see the ball AND
           ((map.ballPosition.X < RUN_UP_DISTANCE && RUN_UP_DISTANCE > 0 ) || // the ball is close and we care about the run-up distance or...
            RUN_UP_DISTANCE <= 1) //... we just don't care about the run-up distance
        )
        {
            // stand up and get set to kick the ball
            standUp();
            playSubstate = SUBSTATE_FINAL_APPROACH;
        }
        else
        {
            const int MAX_PAN = 60;
            const int MIN_PAN = -60;
            const int MAX_TILT = 30;
            const int MIN_TILT = -40;

            // swivel the head back and forth
            if(headTrackingPan < MIN_PAN)
                headTrackingPan = MIN_PAN;
            else if(headTrackingPan> MAX_PAN)
                headTrackingPan = MAX_PAN;

            if(headTrackingTilt < MIN_TILT)
                headTrackingTilt = MIN_TILT;
            else if(headTrackingTilt> MAX_TILT)
                headTrackingTilt = MAX_TILT;

            pauser.threadWaitHead();

            if((headTrackingPan <= MIN_PAN && dPan < 0) || (headTrackingPan >= MAX_PAN && dPan > 0))
            {
                dPan *= -1;

                headTrackingTilt += dTilt;
                if((headTrackingTilt <= MIN_TILT && dTilt < 0) || (headTrackingTilt >= MAX_TILT && dTilt >0))
                {
                    dTilt *= -1;
                }
            }

            Head::GetInstance()->MoveByAngle(headTrackingPan,headTrackingTilt);
            pauser.headHasReacted();
        }
    }
    else
    {
        // make sure the walking module is up and running
        if(!Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Start();
        }
        updateOdometry();

        // if we know where the ball is then walk towards it following an arcing path that will take us behind it
        if(map.ballPosition.X > 0) // we can actually see the ball
        {
            walkingDestination = calculateChaseBallDestination();

            // if it turns out we don't need to walk anywhere then just go straight to the aiming phase
            if(walkingDestination == field->presentPosition || walkingDestination == Point3D(0,0,0))
                playSubstate = SUBSTATE_FINAL_APPROACH;
            else
            {
                processWalking();
                if(myState == STATE_READY) // we've reached the destination
                {
                    myState = STATE_PLAYING;
                    playSubstate = SUBSTATE_FINAL_APPROACH;
                }
            }

            pauser.threadWaitHead();
            Head::GetInstance()->LookAt(*(map.getBall()));
            pauser.headHasReacted();
        }
        else if(field->ballConfidence > 0) // we can't see the ball but think we know where it is anyway
        {
            walkingDestination = calculateChaseBallDestination();
            processWalking();
            if(myState == STATE_READY) // we've reached the destination
            {
                myState = STATE_PLAYING;
                playSubstate = SUBSTATE_FINAL_APPROACH;
            }
            else
            {
                // look at where we think the ball is
                double lateralAngle = field->GetAngleToPoint(field->ballPosition);
                double ballRange = field->GetRangeToPoint(field->ballPosition);

                cameraPosition.RecalculatePosition();
                double downAngle = rad2deg(atan2(-cameraPosition.GetCameraHeight(),ballRange*10));

                pauser.threadWaitHead();
                Head::GetInstance()->MoveByAngle(lateralAngle,downAngle);
                pauser.headHasReacted();
            }
        }
        else
        {
            // we have absolutely no idea where the ball is
            // begin a search pattern to try to find it
            playSubstate = SUBSTATE_FINDING_BALL;
        }
    }
}

Point3D SoccerPlayer::calculateChaseBallDestination() // figure out where we want to be in order to kick the ball into their goal
{
    // TODO: right now we aim to get directly behind the ball.  this WILL cause problems if a straight line to our destination will
    // take us through the ball
    // also we're ignoring the fact that we have lateral kicks.
    // ideally we should either
    // 1- plan a more intricate, arcing path to our destination
    // 2- allow approaching the ball from the side and kicking it laterally
    // 3- special cases for facing our own goal and clearing the ball out of bounds

    // if we're ignoring the goals and orientation entirely then simply return zero and jump straight into
    // the aim-kick phase
    if(ignoreGoals || RUN_UP_DISTANCE <= 0)
        return Point3D(0,0,0);

    // figure out the desired orientation. this is any angle that will put the ball between the opposing goal posts
    /*
      --------------X------------X-----------------
                    .           .
                    .          .
                    .         .
                    .        .
                    .       .
                    .      .
                    .     .
                    .    .
                    .   .
                    .  .
                    . .
                    ..
                    o
                 x
                  * x  <-- arc of valid destination positions; an arc of angular size phi (= theta_max - theta_min) and length l where l is our run-up length

       e.g. theta in range [-22.5, 0]; any angle in that range will put the ball in the goal
            so take the average
     */

    // note that x and y are reversed because the zero angle runs along the positive y axis in our coordinate system!
    double angleToLeftPost  = rad2deg(atan2(field->ballPosition.X - field->theirGoalLeftPost.X, field->ballPosition.Y - field->theirGoalLeftPost.Y));
    double angleToRightPost = rad2deg(atan2(field->ballPosition.X - field->theirGoalRightPost.X, field->ballPosition.Y - field->theirGoalRightPost.Y));
    double targetAngle = (angleToLeftPost + angleToRightPost)/2.0;

    Point2D targetPoint(
                field->ballPosition.X + RUN_UP_DISTANCE * deg2rad(targetAngle), // TODO: check this math: the sign of the angle might mess things up
                field->ballPosition.Y + RUN_UP_DISTANCE * deg2rad(targetAngle)  // TODO: see above
    );

    Point2D presentPosition(field->presentPosition.X, field->presentPosition.Y);


    // if we're already somewhere within our valid arc simply return our present position
    if(pointInArc(presentPosition, field->ballPosition, RUN_UP_DISTANCE, field->normalizeAngle(180+angleToLeftPost), field->normalizeAngle(180+angleToRightPost)))
    {
        cout << "[debug] already standing in destination arc" << endl;
        targetPoint = presentPosition;

        // if out present angle is within the valid range then don't bother turning either
        if(field->presentPosition.Z >= angleToRightPost && field->presentPosition.Z <= angleToLeftPost)
            targetAngle = field->presentPosition.Z;
    }

    cout << "[debug] Calculated kicking destination position: (" << targetPoint.X << " , " << targetPoint.Y << ") @ " << targetAngle << endl;


    return Point3D(targetPoint.X,targetPoint.Y,targetAngle);
}

bool SoccerPlayer::pointInArc(Point2D &p, Point2D &arcCentre, double arcRadius, double theta1, double theta2)
{
    double theta = rad2deg(atan2(arcCentre.X - p.X, arcCentre.Y - p.Y));
    if(theta < theta1 || theta > theta2)
    {
        return false;
    }

    double range = (arcCentre - p).Magnitude();
    if(range > arcRadius)
    {
        return false;
    }

    return true;
}

void SoccerPlayer::processFindBall()
{
#if 0
    if(!Walking::GetInstance()->IsRunning())
    {
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        Walking::GetInstance()->Start();
    }

    walkingDestination = waypoints.front();
    processWalking();

    // TODO: instead of zeroing the head have it swivel side-to-side
    Head::GetInstance()->MoveByAngle(0,0);

    if(map.ballPosition.X > 0) // we've found the ball!
    {
        cout << "Found ball" << endl;
        Voice::Speak("Found ball");
        playSubstate = SUBSTATE_CHASING_BALL;
        waypoints.clear();

        Head::GetInstance()->LookAt(*(map.getBall()));
    }
    else if(myState == STATE_READY) // reached the waypoint; go the the next
    {
        Voice::Speak("Reached waypoint");
        cout << "Reached waypoint" << endl;
        waypoints.pop_front();

        if(waypoints.empty())
        {
            Voice::Speak("Finished search pattern");
            cout << "Exhausted search pattern.  Trying again" << endl;
            setSearchWaypoints();
        }
    }
#else
    lookAround();
#endif
}

void SoccerPlayer::setSearchWaypoints()
{
    // create a search pattern that goes around the field looking for the ball
    /* ASCII ART INCOMING
    The search area should look something like this: (waypoints are marked as * symbols)

    +----------------------------------------+----------------------------------------+
    |                                        |                                        |
    |          *           *           *     |    *          *           *            |
    |                                        |                                        |
    |      *         *           *           *         *           *           *      |
    |-----+                                  |                                  +-----|
    |     |                               ---+---                               |     |
    |     |                             .    |   .                              |     |
    |     |                            /     |     \                            |     |
    |     |*       *    +        *    |      *      |    *       +   *         *|     |
    |     |                            \     |     /                            |     |
    |     |                              .   |    .                             |     |
    |     |                               ---+---                               |     |
    |-----+                                  |                                  +-----|
    |      *         *           *           *         *           *           *      |
    |                                        |                                        |
    |          *           *           *     |    *          *           *            |
    |                                        |                                        |
    +----------------------------------------+----------------------------------------+

    Where the robot walks a zig-zag path around the field and though the centre.  Alternatively the robot
    can circle its own half
    */

    waypoints.clear();

    // FIRST DRAFT
    // just use the 4 corner waypoints in some permutation
    Point3D backLeft(50,50,-45);
    Point3D backRight(field->GetFieldWidthCM()-50,50,45);
    Point3D frontLeft(50,field->GetFieldLengthCM()-50,-135);
    Point3D frontRight(field->GetFieldWidthCM()-40,field->GetFieldLengthCM()-50,135);

    // just set 5 waypoints; somewhere near each corner and the centre.  Walk an hour-glass shape through the field
    switch(field->GetCurrentQuadrant())
    {
    case SoccerField::QUADRANT_DEFENCE_LEFT:
        switch(field->GetQuadrantFacing())
        {
        case SoccerField::QUADRANT_DEFENCE_LEFT:
            waypoints.push_back(backLeft);
            waypoints.push_back(backRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(frontRight);
            break;

        case SoccerField::QUADRANT_DEFENCE_RIGHT:
            waypoints.push_back(backRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(frontRight);
            waypoints.push_back(backLeft);
            break;

        case SoccerField::QUADRANT_OFFENCE_LEFT:
            waypoints.push_back(frontLeft);
            waypoints.push_back(backRight);
            waypoints.push_back(backLeft);
            waypoints.push_back(frontRight);
            break;

        case SoccerField::QUADRANT_OFFENCE_RIGHT:
        default:
            waypoints.push_back(backRight);
            waypoints.push_back(frontRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(backLeft);
            break;
        }
        break;

    case SoccerField::QUADRANT_DEFENCE_RIGHT:
        switch(field->GetQuadrantFacing())
        {
        case SoccerField::QUADRANT_DEFENCE_LEFT:
            waypoints.push_back(backLeft);
            waypoints.push_back(frontRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(backRight);
            break;

        case SoccerField::QUADRANT_DEFENCE_RIGHT:
            waypoints.push_back(backRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(frontRight);
            waypoints.push_back(backLeft);
            break;

        case SoccerField::QUADRANT_OFFENCE_LEFT:
            waypoints.push_back(frontLeft);
            waypoints.push_back(backRight);
            waypoints.push_back(backLeft);
            waypoints.push_back(frontRight);
            break;

        case SoccerField::QUADRANT_OFFENCE_RIGHT:
        default:
            waypoints.push_back(frontRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(backRight);
            waypoints.push_back(backLeft);
            break;
        }
        break;

    case SoccerField::QUADRANT_OFFENCE_LEFT:
        switch(field->GetQuadrantFacing())
        {
        case SoccerField::QUADRANT_DEFENCE_LEFT:
            waypoints.push_back(frontLeft);
            waypoints.push_back(frontRight);
            waypoints.push_back(backLeft);
            waypoints.push_back(backRight);
            break;

        case SoccerField::QUADRANT_DEFENCE_RIGHT:
            waypoints.push_back(frontRight);
            waypoints.push_back(backRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(backLeft);
            break;

        case SoccerField::QUADRANT_OFFENCE_LEFT:
            waypoints.push_back(frontLeft);
            waypoints.push_back(backRight);
            waypoints.push_back(backLeft);
            waypoints.push_back(frontRight);
            break;

        case SoccerField::QUADRANT_OFFENCE_RIGHT:
        default:
            waypoints.push_back(backRight);
            waypoints.push_back(frontRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(backLeft);
            break;
        }
        break;

    case SoccerField::QUADRANT_OFFENCE_RIGHT:
    default:
        switch(field->GetQuadrantFacing())
        {
        case SoccerField::QUADRANT_DEFENCE_LEFT:
            waypoints.push_back(frontLeft);
            waypoints.push_back(backLeft);
            waypoints.push_back(frontRight);
            waypoints.push_back(backRight);
            break;

        case SoccerField::QUADRANT_DEFENCE_RIGHT:
            waypoints.push_back(frontRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(backRight);
            waypoints.push_back(backLeft);
            break;

        case SoccerField::QUADRANT_OFFENCE_LEFT:
            waypoints.push_back(frontLeft);
            waypoints.push_back(backRight);
            waypoints.push_back(backLeft);
            waypoints.push_back(frontRight);
            break;

        case SoccerField::QUADRANT_OFFENCE_RIGHT:
        default:
            waypoints.push_back(frontRight);
            waypoints.push_back(frontRight);
            waypoints.push_back(frontLeft);
            waypoints.push_back(backLeft);
            break;
        }
        break;
    }

}

int SoccerPlayer::ballInFrontOfLeftFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle > LEFT_FOOT_RIGHT_THRESHOLD && angle < LEFT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(angle >= LEFT_FOOT_RIGHT_THRESHOLD)
        ans = 1;
    else
        ans = -1;

    return ans;
}

int SoccerPlayer::ballInFrontOfRightFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle > RIGHT_FOOT_RIGHT_THRESHOLD && angle < RIGHT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(angle >= RIGHT_FOOT_RIGHT_THRESHOLD)
        ans = 1;
    else
        ans = -1;

    return ans;
}

int SoccerPlayer::ballBesideLeftFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle > LEFT_FOOT_LEFT_THRESHOLD && angle < 1.5*LEFT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(angle >= 1.5*LEFT_FOOT_RIGHT_THRESHOLD)
        ans = 1;
    else
        ans = -1;

    return ans;
}

int SoccerPlayer::ballBesideRightFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle < RIGHT_FOOT_RIGHT_THRESHOLD && angle > 1.5*RIGHT_FOOT_RIGHT_THRESHOLD)
        ans = 0;
    else if(angle <= 1.5*RIGHT_FOOT_RIGHT_THRESHOLD)
        ans = -1;
    else
        ans = 1;

    return ans;
}

int SoccerPlayer::ballPosnForKick()
{
    int ans;
    double angle = map.ballPosition.Y;

    switch(chosenKick)
    {
        case PASS_FORWARD_LEFT_MOTION:
        case KICK_LEFT_MOTION:
            if(angle < LEFT_FORWARD_LEFT_THRESHOLD && angle > LEFT_FORWARD_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= LEFT_FORWARD_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_FORWARD_RIGHT_MOTION:
        case KICK_RIGHT_MOTION:
            if(angle < RIGHT_FORWARD_LEFT_THRESHOLD && angle > RIGHT_FORWARD_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= RIGHT_FORWARD_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_LEFT_MOTION:
            if(angle < LEFT_INSIDE_LEFT_THRESHOLD && angle > LEFT_INSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= LEFT_INSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_RIGHT_MOTION:
            if(angle < RIGHT_INSIDE_LEFT_THRESHOLD && angle > RIGHT_INSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= RIGHT_INSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_OUTSIDE_LEFT_MOTION:
            if(angle < LEFT_OUTSIDE_LEFT_THRESHOLD && angle > LEFT_OUTSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= LEFT_OUTSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_OUTSIDE_RIGHT_MOTION:
            if(angle < RIGHT_OUTSIDE_LEFT_THRESHOLD && angle > RIGHT_OUTSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= RIGHT_OUTSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        default:
            cout<<"INVALID KICK CHOSEN!"<<endl;
            ans = 0;
            break;
    }

    return ans;
}

void SoccerPlayer::processFinalApproach()
{
    // this is the old chase-ball behaviour; we just walk straight to the ball and kick it


    static int ballFrames = 0;
    static int noBallFrames = 0;
    const int RACE_TO = 5;

    updateOdometry();

    if(map.ballPosition.X < 0)
    {
        if(!Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Start();
        }

        // we've temporarily lost sight of the ball
        // check our confidence in the ball's last-known position and use that to plan our movement
        if(field->ballConfidence > 0)
        {
            Point2D pos2d(field->presentPosition.X,field->presentPosition.Y);

            cameraPosition.RecalculatePosition();
            double ballRange = (field->ballPosition - pos2d).Magnitude();
            double ballAngle = field->GetAngleToPoint(field->ballPosition);

            double downAngle = atan2(cameraPosition.GetCameraHeight(),ballRange);
            Point2D ballAngles = Point2D(downAngle, ballAngle);
            follower.Process(ballAngles);

            pauser.threadWaitHead();
            Head::GetInstance()->MoveByAngle(ballAngle, downAngle);
            pauser.headHasReacted();
        }
        else
        {
            if(DEBUG_PRINT)
                cout << "[info] Lost sight of the ball" << endl;
            Walking::GetInstance()->Stop();
            ballFrames = 0;
            noBallFrames = 0;
            handleLostBall();
        }
    }
    else if(map.ballPosition.X > maxKickRange || map.ballPosition.Y > LEFT_FOOT_LEFT_THRESHOLD || map.ballPosition.Y < RIGHT_FOOT_RIGHT_THRESHOLD)
    {
        if(!Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Start();
        }

        // we can see the ball right now but it's not in position (either too far away or too far to either side)
        // walk towards it
        pauser.threadWaitHead();
        tracker.Process(map.getBall()->GetBoundingBox()->center);
        pauser.headHasReacted();
        follower.Process(tracker.ball_position);

        if(DEBUG_PRINT)
        {
            cout << "[info] ball too far away or off to the side" << endl <<
                    "       " << map.ballPosition.X << " <? " << maxKickRange << endl <<
                    "       " << map.ballPosition.Y << " in [" << RIGHT_FOOT_RIGHT_THRESHOLD << " " << LEFT_FOOT_LEFT_THRESHOLD << "]" << endl;
        }

        // chasing, so set these to zero (they're only used when preparing to aim)
        ballFrames = 0;
        noBallFrames = 0;
    }
    else if(map.ballPosition.X <= maxKickRange && map.ballPosition.Y >= RIGHT_FOOT_RIGHT_THRESHOLD && map.ballPosition.Y <= LEFT_FOOT_LEFT_THRESHOLD)
    {
        if(DEBUG_PRINT)
        {
            cout << "[info] Ball might be in position to aim kick (score: Y-" << ballFrames << " N-" << noBallFrames << ")" << endl;
            cout << "       " << map.ballPosition.X << " <? " << maxKickRange << endl <<
                   "       " << map.ballPosition.Y << " in [" << RIGHT_FOOT_RIGHT_THRESHOLD << " " << LEFT_FOOT_LEFT_THRESHOLD << "]" << endl;
        }

        Walking::GetInstance()->Stop();
        if(!Walking::GetInstance()->IsRunning())
        {
            // do a sanity check by tilting the head down and making sure we can see the ball
            pauser.threadWaitHead();
            Head::GetInstance()->MoveByAngle(map.ballPosition.Y, -45);
            pauser.headHasReacted();

            // see if we can see the ball for 10 frames
            // if we do (before we hit 10 no-ball frames) then
            // we can safely assume the ball is at our feet
            if(map.getBall()->WasFound())
                ballFrames++;
            else
                noBallFrames++;

            if(ballFrames >= RACE_TO)
            {
                if(DEBUG_PRINT)
                    cout << "[info] Can begin aiming" << endl;
                playSubstate = SUBSTATE_KICKING;
            }
            else if(noBallFrames >= RACE_TO)
            {
                if(DEBUG_PRINT)
                    cout << "[info] Ball not in position to kick. Falling back to chasing" << endl;
                handleLostBall();
            }
        }
    }
}

// do whatever final positioning we need and then kick the ball
// the choice of what kick we use depends on what the field class tells us our orientation is
// in order of priority we will...
// 1. kick forward toward their goal
// 2. kick laterally toward their goal
// 3. kick forward away from our goal
// 4. kick laterally away from our goal
void SoccerPlayer::processKick()
{
    bool inPosition = false;
    int chosenDirection = KICK_DIRECTION_FORWARD;
    int shuffleDirection;

    if(map.ballPosition.X > 0)
    {
        chosenDirection = chooseKickDirection();
    }
    else
    {
        handleLostBall();
        return;
    }

    while(!inPosition)
    {
        if(handleFall())
        {
            // give up and fall back to the looking around state to re-orient ourselves
            // TODO: we could try something less drastic here, but for now I think this will suffice - CIB
            cout << "[info] Aborting kick attempt; fallen over" << endl;
            chosenKick = -1;
            handleLostBall();
            return;
        }
        pauser.threadWaitHead();
        if(map.ballPosition.X>0 && map.ballPosition.X<=maxKickRange)
        {
            Head::GetInstance()->LookAt(*(map.getBall()));
            MotionManager::Sync();

            chosenKick = chooseKick(chosenDirection);
            shuffleDirection = ballPosnForKick();

            cout << "Stepping " << shuffleDirection << endl;
#if 0
            if(shuffleDirection == 0)
            {
                inPosition = true;
            }
            else if(shuffleDirection > 0)
            {
                /*
                Action::GetInstance()->m_Joint.SetEnableBody(false);
                Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Action::GetInstance()->Start(SHUFFLE_LEFT);
                Action::GetInstance()->Finish();
                standUp();*/
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->Y_MOVE_AMPLITUDE = 16;
                Walking::GetInstance()->Start(1);
                Walking::GetInstance()->Finish();
            }
            else
            {
                /*
                Action::GetInstance()->m_Joint.SetEnableBody(false);
                Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Action::GetInstance()->Start(SHUFFLE_RIGHT);
                Action::GetInstance()->Finish();
                standUp();*/
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->Y_MOVE_AMPLITUDE = -16;
                Walking::GetInstance()->Start(1);
                Walking::GetInstance()->Finish();
            }
#else
            inPosition = true;
#endif

            if(handleFall())
            {
                // give up and fall back to the looking around state to re-orient ourselves
                // TODO: we could try something less drastic here, but for now I think this will suffice - CIB
                cout << "[info] Aborting kick attempt; fallen over" << endl;
                chosenKick = -1;
                handleLostBall();
            }
        }
        else if(map.ballPosition.X > 0)
        {
            cout << "[info] Aborting kick attempt; ball too far away" << endl;
            Voice::Speak("Ball too far");
            playSubstate = SUBSTATE_CHASING_BALL;
            return; //the ball is now far away, so exit this function and go back to chasing it
        }
        else
        {
            // give up and fall back to the looking around state to re-orient ourselves
            // TODO: we could try something less drastic here, but for now I think this will suffice - CIB
            cout << "[info] Aborting kick attempt; lost sight of the ball" << endl;
            chosenKick = -1;
            handleLostBall();
            return;
        }
        pauser.headHasReacted();
    }

    cout << "[info] Aiming complete" << endl;

    // execute the kick
    if(chosenKick > 0)
    {
        Action::GetInstance()->m_Joint.SetEnableBody(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        Action::GetInstance()->Start(chosenKick);
        cout << "Kicking Left" << endl;
        Action::GetInstance()->Finish();
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
        standUp();

#if 1
        // go back to chasing down the ball
        playSubstate = SUBSTATE_CHASING_BALL;
#else
        // we kicked the ball, but have no real idea where it may have gone, so just look for it again
        if(chosenKick == KICK_LEFT_MOTION || chosenKick == KICK_RIGHT_MOTION ||
           chosenKick == PASS_FORWARD_LEFT_MOTION || chosenKick == PASS_FORWARD_RIGHT_MOTION)
        {
            // kicked forward, so start walking forward
            // start walking forward
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Start();
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 15;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
        }
        else if(chosenKick == PASS_BALL_OUTSIDE_LEFT_MOTION || chosenKick == PASS_BALL_LEFT_MOTION)
        {
            // kicked left; turn to face
        }

        handleLostBall();
#endif
        //chosenKick = -1;
    }
    else
    {
        cout << "[error] Unknown Kicking Motion: " << chosenKick << endl;
        handleLostBall();
    }
}

int SoccerPlayer::chooseKickDirection()
{
    int chosenDirection = KICK_DIRECTION_FORWARD;

    if(penaltyKick)
    {
        // bypass EVERYTHING and just kick the ball straight forward
        Voice::Speak("Taking penalty kick");
        cout << "Executing penalty kick" << endl;

        chosenDirection = KICK_DIRECTION_FORWARD;
    }
    else if(ignoreGoals)
    {
        cout << "Kicking forward" << endl;
        Voice::Speak("Kicking forward");

        chosenDirection = KICK_DIRECTION_FORWARD;
    }
    else if(field->positionConfidence >= MIN_CONFIDENCE_FOR_AIMED_KICK) // at least 50% confidence in our position
    {
        // calculate angles to the various goal posts, choose the optimal kick
        double thetaMyLeftPost = field->GetAngleToPoint(field->myGoalLeftPost);
        double thetaMyRightPost = field->GetAngleToPoint(field->myGoalRightPost);
        double thetaTheirLeftPost = field->GetAngleToPoint(field->theirGoalLeftPost);
        double thetaTheirRightPost = field->GetAngleToPoint(field->theirGoalRightPost);

        double thetaMyGoalCenter = (thetaMyLeftPost + thetaMyRightPost)/2.0;
        double thetaTheirGoalCenter = (thetaTheirLeftPost + thetaTheirRightPost)/2.0;

        if((thetaTheirLeftPost > 0 && thetaTheirRightPost < 0) || // we're facing somewhere between their goal posts
                (thetaTheirGoalCenter/2.0 <= 45 && thetaTheirGoalCenter/2.0 >=-45)) //their goal's centre is somewhere in our front-90
        {
            Voice::Speak("Kicking toward their goal");
            cout << "Forward kick toward their goal" << endl;
            chosenDirection = KICK_DIRECTION_FORWARD;
        }
        else if(thetaMyGoalCenter >= 135 || thetaMyGoalCenter <= -135) // our goal's centre is somwhere in our back-90
        {
            Voice::Speak("Kicking away from my goal");
            cout << "Forward kick away from my goal" << endl;
            chosenDirection = KICK_DIRECTION_FORWARD;
        }
        else if(thetaTheirGoalCenter > 45) // their goal's centre is somewhere to our left
        {
            Voice::Speak("Their goal left");
            cout << "Left kick toward their goal" << endl;
            chosenDirection = KICK_DIRECTION_LEFT;
        }
        else if(thetaTheirGoalCenter < -45) // their goal's centre is somewhere to our right
        {
            Voice::Speak("Their goal right");
            cout << "Right kick toward their goal" << endl;
            chosenDirection = KICK_DIRECTION_RIGHT;
        }
        else if(thetaMyGoalCenter > 45) // our goal is somewhere to our left
        {
            Voice::Speak("My goal left");
            cout << "Right kick away from my goal" << endl;
            chosenDirection = KICK_DIRECTION_RIGHT;
        }
        else if(thetaMyGoalCenter < -45) // our goal is somewhere to our right
        {
            Voice::Speak("My goal right");
            cout << "Left kick away from my goal" << endl;
            chosenDirection = KICK_DIRECTION_LEFT;
        }
        else
        {
            Voice::Speak("unhandled case");
            cout << "Unhandled case; kicking forward" << endl;
            chosenDirection = KICK_DIRECTION_FORWARD;
        }
    }
    else
    {
        // orientation is unknown; just kick the ball straight ahead
        Voice::Speak("Kicking blind");
        cout << "Kicking blind" << endl;
        chosenDirection = KICK_DIRECTION_FORWARD;
    }

    return chosenDirection;
}

int SoccerPlayer::chooseKick(int direction)
{
    int chosenKick;

    // decide what kick we want to use based on where the ball is right now
    switch(direction)
    {
    case KICK_DIRECTION_LEFT:
        /*if(map.ballPosition.Y < 0)      // ball is in front of the right foot; this is good
        {
            chosenKick = PASS_BALL_LEFT_MOTION;
        }
        else // use the outside left kick (get the ball beside the left foot
        {
            chosenKick = PASS_BALL_OUTSIDE_LEFT_MOTION;
        }*/
        chosenKick = PASS_BALL_LEFT_MOTION;
        break;

    case KICK_DIRECTION_RIGHT:
        /*if(map.ballPosition.Y > 0)      // ball is in fromt of the left foot
        {
            chosenKick = PASS_BALL_RIGHT_MOTION;
        }
        else
        {
            chosenKick = PASS_BALL_OUTSIDE_RIGHT_MOTION;
        }*/
        chosenKick = PASS_BALL_RIGHT_MOTION;
        break;

    case KICK_DIRECTION_FORWARD:
    default:
#if 0
        // TODO: this is a bit hacky; should be agnostic to the actual competition type
        int refState;
        if(ini->geti("Soccer","RoboCup",0))
            refState = ((FiraSocketMonitor*)socketMonitor)->getGameControlData()->State();
        else
            refState = RefereeObject::STATE_PLAY;

        if(map.ballPosition.Y > 0)
        {
            if(refState == RefereeObject::STATE_PLAY_KICKOFF_BLUE || refState == RefereeObject::STATE_PLAY_KICKOFF_RED)
                chosenKick = PASS_FORWARD_LEFT;
            else
                chosenKick = KICK_LEFT_MOTION;
        }
        else
        {
            if(refState == RefereeObject::STATE_PLAY_KICKOFF_BLUE || refState == RefereeObject::STATE_PLAY_KICKOFF_RED)
                chosenKick = PASS_FORWARD_RIGHT;
            else
                chosenKick = KICK_RIGHT_MOTION;
        }
#else
        if(map.ballPosition.Y > 0)
            chosenKick = KICK_LEFT_MOTION;
        else
            chosenKick = KICK_RIGHT_MOTION;
#endif
        break;
    }

    return chosenKick;
}

bool SoccerPlayer::canPlay()
{
    return (refSaysWeCanPlay /*&& myState != STATE_WAITING && myState != STATE_HW_INTERRUPT*/) || noNetwork;
}

void SoccerPlayer::checkRefStatus()
{
    //int oldState = myState;
    //bool oldRefSaysWeCanPlay = refSaysWeCanPlay;
    //int oldGameState = gameState;

    if(noNetwork)
    {
        refSaysWeCanPlay = true;
    }
    else
    {
        refSaysWeCanPlay = socketMonitor->canRobotPlay(myColour, myId);
        onPenalty = socketMonitor->isRobotOnPenalty(myColour, myId);
    }
}

void SoccerPlayer::walkTo(Point3D &point)
{
    this->walkingDestination.X = point.X;
    this->walkingDestination.Y = point.Y;
    this->walkingDestination.Z = point.Z;

    myState = STATE_WALKING;
}

void SoccerPlayer::processWalking()
{
    const double MAX_ERROR_DISTANCE = 15;   // as long as we're within 30cm it's close enough
    const double MAX_ERROR_ANGLE = 7.5;       // orientation can be +/- 15 degrees and still be good enough
    const double MAX_WALKING_ANGLE = 20;     // The robot must be at least this close to the correct angle before walking forwards

    double xError = fabs(walkingDestination.X - field->presentPosition.X);
    double yError = fabs(walkingDestination.Y - field->presentPosition.Y);
    double zError = fabs(walkingDestination.Z - field->presentPosition.Z);

    if(DEBUG_PRINT)
        cout << "[info] Walking to destination. (" << walkingDestination.X << " , " << walkingDestination.Y << " , " << walkingDestination.Z << ")" << endl;

    // TODO: place-holder
    // right now the particle filter is updated from inside the vision thread, but we may want to change that
    // to increase the camera's framerate
    //updateOdometry();

    // if we are not in the x/y position yet then turn to face the correct location
    if(xError > MAX_ERROR_DISTANCE || yError > MAX_ERROR_DISTANCE)
    {
        if(DEBUG_PRINT)
        {
            cout << "       Correcting X/Y/Z error of " << xError << " " << yError << " " << zError << endl;
            cout << "       Current position: " <<field->presentPosition.X << " " << field->presentPosition.Y << " " << field->presentPosition.Z << endl;
        }

        // calculate the angle we should be facing to reach our destination
        double toFace = rad2deg(atan2(fabs(walkingDestination.X - field->presentPosition.X), fabs(walkingDestination.Y - field->presentPosition.Y)));
        if(field->presentPosition.X <= walkingDestination.X && field->presentPosition.Y <= walkingDestination.Y)
        {
            // walking toward first quadrant (which is to the front-right)
            toFace = -toFace;
        }
        else if(field->presentPosition.X <= walkingDestination.X && field->presentPosition.Y > walkingDestination.Y)
        {
            // walking toward the fourth quadrant (which is to the back-right)
            toFace = -180+toFace;
        }
        else if(field->presentPosition.X > walkingDestination.X && field->presentPosition.Y <= walkingDestination.Y)
        {
            // walking toward the second quadrant (which is to the front-left)
            // no change to toFace
        }
        else
        {
            // walking toward the third quadrant (which is to the back-left)
            toFace = 180-toFace;
        }

        // normalize the angle to be in the [-180,180] range
        toFace = field->normalizeAngle(toFace);

        // we know where we need to go, so now figure out how much we need to turn to get there
        double toTurn = field->normalizeAngle(toFace - field->presentPosition.Z);

        if(fabs(toTurn) < MAX_WALKING_ANGLE)
        {
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 20.0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = toTurn/10.0;
        }
        else
        {
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = -2.0;
            if(toTurn<0) {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = -10.0;
            } else {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 10.0;
            }
        }

        if(DEBUG_PRINT)
            cout << "       Turning " << toTurn << endl;
        //Walking::GetInstance()->A_MOVE_AMPLITUDE = -toTurn/10.0;

    }
    else if(zError > MAX_ERROR_ANGLE)
    {
        if(DEBUG_PRINT)
            cout << "       Correcting Z error of " << zError << endl;

        // just turn until we are facing the right way
        double toTurn = walkingDestination.Z - field->presentPosition.Z;
        toTurn = field->normalizeAngle(toTurn);
        Walking::GetInstance()->X_MOVE_AMPLITUDE = -2.0;
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
        //Walking::GetInstance()->A_MOVE_AMPLITUDE = -toTurn/10;
        if(toTurn<0) {
            Walking::GetInstance()->A_MOVE_AMPLITUDE = -10.0;
        } else {
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 10.0;
        }
    }
    else
    {
        if(DEBUG_PRINT)
            cout << "[info] Reached destination" << endl;

        Walking::GetInstance()->X_MOVE_AMPLITUDE = 0.0;
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0.0;
        Walking::GetInstance()->Stop();
        Walking::GetInstance()->Finish();

        if(isGoalie)
            ((Goalie*)this)->doneWalking();
        else
            myState = STATE_READY;
    }
}

bool SoccerPlayer::handleFall()
{
    bool didFall = false;
    MotionModule *inControl = Action::GetInstance();

    if(MotionStatus::FALLEN != STANDUP)
    {
        didFall = true;

        if(Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->Stop();
            Walking::GetInstance()->Finish();

            inControl = Walking::GetInstance();
        }

        //MotionManager::GetInstance()->AddModule(Action::GetInstance());
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);

        if(MotionStatus::FALLEN == FORWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_F_UP);   // FORWARD GETUP
        else if(MotionStatus::FALLEN == BACKWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_B_UP);   // BACKWARD GETUP

        Action::GetInstance()->Finish();

        inControl->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
    }

    return didFall;
}

void *SoccerPlayer::videoThread(void *arg)
{
    SoccerPlayer *instance = (SoccerPlayer*)arg;
    for(;;)
    {
        instance->HandleVideo();
        pthread_yield();
    }
    return NULL;
}

void SoccerPlayer::yieldThread()
{
    pthread_yield();
}

bool SoccerPlayer::handleButton()
{
    static int lastButton = 0;
    int currentButton;

    currentButton = MotionStatus::BUTTON;

    if(currentButton != lastButton)
    {
        // if we're on penalty then the left/middle button is used to say if we're re-entering
        // play from the left/right
        if(this->onPenalty)
        {
            if(currentButton == CM730::LEFT_BUTTON)
            {
                cout << "[button] Left button pressed; setting penalty entrance to left" << endl;
                //this->penaltyEntrance.X = -50;
            }
            else if(currentButton == CM730::MIDDLE_BUTTON)
            {
                cout << "[button] Middle button pressed; setting penalty entrance to left" << endl;
                //this->penaltyEntrance.X = 50;
            }
        }
        else if(this->myState != STATE_HW_INTERRUPT && currentButton == CM730::LEFT_BUTTON)
        {
            cout << "[button] HW Interrupt event detected" << endl;
            this->myState = STATE_HW_INTERRUPT;
            Walking::GetInstance()->Stop();
        }
        else if(this->myState == STATE_HW_INTERRUPT && currentButton == CM730::MIDDLE_BUTTON)
        {
            cout << "[button] Leaving HW Interrupt state" << endl;
            this->myState = STATE_READY;
        }

        lastButton = currentButton;
    }

    return currentButton != 0;
}

void *SoccerPlayer::buttonThread(void *arg)
{
    SoccerPlayer *instance = (SoccerPlayer*)arg;
    for(;;)
    {
        instance->handleButton();
        instance->pauser.threadWait();
    }
    return NULL;
}

void SoccerPlayer::HandleVideo()
{
    if(!CvCamera::GetInstance()->CaptureFrame())
    {
        cerr << "Failed co capture frame" << endl;
        return;
    }

    if(showVideo || recordVideo || visionTest) // clear the debugging frame if necessary
        rectangle(map.dbgImage,Point(0,0),Point(map.dbgImage.cols,map.dbgImage.rows),Scalar(0,0,0),-1);

    map.ProcessFrame(CvCamera::GetInstance()->yuvFrame);

    // update the position estimate for the field
    field->Update(Walking::GetInstance()->X_MOVE_AMPLITUDE, Walking::GetInstance()->Y_MOVE_AMPLITUDE, Walking::GetInstance()->A_MOVE_AMPLITUDE);

    // deal with displaying/annotating/recording the video frames if necessary
    AnnotateAndRecordVideo();

    // wait broadcast that we have a new frame so motion-planning can be updated
    pauser.newFrame();
    pauser.threadBroadcast();
}

void SoccerPlayer::AnnotateAndRecordVideo()
{
    // annotate the images if necessary (i.e. the images are actually used)
    if(recordVideo || showVideo || visionTest)
    {
        // make a local copy of the RGB frame that we can annotate
        cvtColor(CvCamera::GetInstance()->yuvFrame, rgbImage, CV_YCrCb2RGB);

        std::list<cv::Mat> frames;
        frames.push_back(rgbImage);
        frames.push_back(map.dbgImage);
        map.AnnotateFrames(frames,myState);

        map.Draw();
        field->Draw();
    }


    if(showVideo || visionTest)
    {
        //cout << "displaying frames" << endl;
        imshow("Map", map.mapImage);
        imshow("Field",field->fieldImage);
        imshow("Webcam", rgbImage);
        imshow("Scanline", map.dbgImage);
        //imshow("Webcam", YuvCamera::GetInstance()->img);
        cvWaitKey(1);
        //cout << "done" << endl;
    }
    if(recordVideo)
    {
        webcamStream << rgbImage;
        debugStream << map.dbgImage;
        mapStream << map.mapImage;
    }
}

void SoccerPlayer::handleLostBall()
{
    lostFrames++;
    if(lostFrames >= LOST_FRAMES_BEFORE_SCAN)
    {

        switch(chosenKick)
        {
        // kicked left; the ball is probably left so turn that way
        case PASS_BALL_LEFT_MOTION:
        case PASS_BALL_OUTSIDE_LEFT_MOTION:
            dPan = 1;
            dTilt = 1;
            break;

        // kicked right; the ball is probably right so turn that way
        case PASS_BALL_RIGHT_MOTION:
        case PASS_BALL_OUTSIDE_RIGHT_MOTION:
            dPan = -1;
            dTilt = 1;
            break;

        // did not just kick, or kicked forward
        // pick a random direction to look
        case KICK_LEFT_MOTION:
        case KICK_RIGHT_MOTION:
        case PASS_FORWARD_LEFT_MOTION:
        case PASS_FORWARD_RIGHT_MOTION:
        case -1:
        default:
            dPan = 5 * ((rand() % 2 == 0) ? 1 : -1);
            dTilt = 5 * ((rand() % 2 == 0) ? 1 : -1);
            break;
        }

        headTrackingPan = Head::GetInstance()->GetPanAngle();
        headTrackingTilt = Head::GetInstance()->GetTiltAngle();
        lookCycles = 0;

        chosenKick = -1;
        playSubstate = SUBSTATE_FINDING_BALL;
    }
}

void SoccerPlayer::scan(bool scanLow)
{
    int tilt;
    double speedAmplitude;
    double xSpeed = lastBallInfo->getXSpeed();
    double ySpeed = lastBallInfo->getYSpeed();
    bool panValid, tiltValid;

    if(firstSweep && lastBallInfo->getXSpeed() != -1)
    {
        speedAmplitude = sqrt(xSpeed*xSpeed + ySpeed*ySpeed);
        panValid = headPan-xSpeed/speedAmplitude*SCAN_AMPLITUDE<=MAX_SCAN_PAN && headPan+xSpeed/speedAmplitude*SCAN_AMPLITUDE>=MIN_SCAN_PAN;
        tiltValid = headTilt-ySpeed/speedAmplitude*SCAN_AMPLITUDE<=MAX_SCAN_TILT && headTilt-ySpeed/speedAmplitude*SCAN_AMPLITUDE>=MIN_SCAN_TILT;

        if(panValid && tiltValid)
        {
            headPan -= xSpeed/speedAmplitude*SCAN_AMPLITUDE;
            headTilt -= ySpeed/speedAmplitude*SCAN_AMPLITUDE;
            Head::GetInstance()->MoveByAngle(headPan,headTilt);
        }
        else if(panValid)
        {
            if(xSpeed > 0)
            {
                panValid = headPan-SCAN_AMPLITUDE<=MAX_SCAN_PAN && headPan-SCAN_AMPLITUDE>=MIN_SCAN_PAN;
                if(panValid)
                {
                    headPan -= SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
            else
            {
                panValid = headPan+SCAN_AMPLITUDE<=MAX_SCAN_PAN && headPan+SCAN_AMPLITUDE>=MIN_SCAN_PAN;
                if(panValid)
                {
                    headPan += SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
        }
        else if(tiltValid)
        {
            if(ySpeed > 0)
            {
                tiltValid = headTilt-SCAN_AMPLITUDE<=MAX_SCAN_TILT && headTilt-SCAN_AMPLITUDE>=MIN_SCAN_TILT;
                if(tiltValid)
                {
                    headTilt -= SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
            else
            {
                tiltValid = headTilt+SCAN_AMPLITUDE<=MAX_SCAN_TILT && headTilt+SCAN_AMPLITUDE>=MIN_SCAN_TILT;
                if(tiltValid)
                {
                    headTilt += SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
        }
        else
        {
            firstSweep = false;
        }

    }
    else
    {
        if(scanDirection == 0)
        {
            if(lastBallInfo->getTheta() > headPan)
                scanDirection = 1;
            else
                scanDirection = -1;
        }

        if(scanDirection == 1 && scanLow)
            tilt = LOW_SCAN_TILT;
        else
            tilt = HIGH_SCAN_TILT;

        if((scanDirection<0 || headPan+SCAN_AMPLITUDE*scanDirection<=MAX_SCAN_PAN) && (scanDirection>0 || headPan+SCAN_AMPLITUDE*scanDirection>=MIN_SCAN_PAN))
        {
            headPan+=SCAN_AMPLITUDE*scanDirection;
            headTilt = tilt;
            Head::GetInstance()->MoveByAngle(headPan,headTilt);
        }
        else
        {
            scanDirection *= -1;
            lookCycles++;
        }
    }
}

void SoccerPlayer::quickScan()
{
    // quick head-flick up and back down again

    Head *head = Head::GetInstance();   // cached reference to our head
    int oldHeadTilt;                    // stored head angles
    int oldHeadPan;

    // save our old head orientation
    oldHeadTilt = headTilt;
    oldHeadPan = headPan;

    // begin waiting for a frame
    pauser.threadWaitHead();

    // move to look high and center
    standUp();
    head -> MoveByAngle(0, 60);

    for(int i=0; i<2; i++)
    {
        pauser.waitAMoment();

        if(map.getGoal()->WasFound())
            foundGoalDuringQuickScan = true;
    }

    // restore our old head position
    head -> MoveByAngle(oldHeadPan, oldHeadTilt);
    pauser.waitAMoment();
    pauser.headHasReacted();
}

// swivel the head around until we find the ball
void SoccerPlayer::lookAround()
{
    const int MAX_PAN = 60;
    const int MIN_PAN = -60;
    const int MAX_TILT = 30;
    const int MIN_TILT = -40;

    //cout << "\tLooking around (" << headTrackingPan << " , " << headTrackingTilt << ")" << endl;

    pauser.threadWaitHead();

    if(map.getBall()->WasFound())
    {
        //cout << "\tFound ball!" << endl;
        Head::GetInstance()->LookAt(*map.getBall());
        headTrackingPan = Head::GetInstance()->GetPanAngle();
        headTrackingTilt = Head::GetInstance()->GetTiltAngle();

        myState = STATE_PLAYING;
        playSubstate = SUBSTATE_CHASING_BALL;
    }
    else
    {
        //cout << "\tNo ball (" << dPan << " , " << dTilt << ")" << endl;

        // only pan the head if we're not walking
        //if(!Walking::GetInstance()->IsRunning())
        //{
        //    headTrackingPan += dPan;
        //}
        //else
        {
            headTrackingPan = 0;
        }

        if(headTrackingPan < MIN_PAN)
            headTrackingPan = MIN_PAN;
        else if(headTrackingPan> MAX_PAN)
            headTrackingPan = MAX_PAN;

        if(headTrackingTilt < MIN_TILT)
            headTrackingTilt = MIN_TILT;
        else if(headTrackingTilt> MAX_TILT)
            headTrackingTilt = MAX_TILT;

        //if((headTrackingPan <= MIN_PAN && dPan < 0) || (headTrackingPan >= MAX_PAN && dPan > 0))
        {
            //dPan *= -1;

            headTrackingTilt += dTilt;
            if((headTrackingTilt <= MIN_TILT && dTilt < 0) || (headTrackingTilt >= MAX_TILT && dTilt >0))
            {
                dTilt *= -1;
                lookCycles++;
            }
        }

        // we've swiveled the head through the entire range of motion
        if(!isGoalie && !Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->Start();
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = dPan * 2;  // start pivoting on the spot
        }
    }

    Head::GetInstance()->MoveByAngle(headTrackingPan,headTrackingTilt);
    pauser.headHasReacted();
}

// send a packet describing the robot's current state
void SoccerPlayer::broadcastPacket()
{
    socketMonitor->transmitState(*this);
}

void SoccerPlayer::sitDown()
{
    // stop walking and wait until we can act again
    //Head::GetInstance()->MoveByAngle(0,10);
    Walking::GetInstance()->Stop();
    Walking::GetInstance()->Finish();


    // sit down
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_SIT_DOWN);
    Action::GetInstance()->Finish();
}

void SoccerPlayer::standUp()
{
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_WALKREADY);
    Action::GetInstance()->Finish();
    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
}

void SoccerPlayer::transmitStatus()
{
    if(!noNetwork)
    {
        struct timespec time;
        clock_gettime(CLOCK_MONOTONIC, &time);
        unsigned long int ms = (time.tv_sec * 1000000000 + time.tv_nsec) / 1000000;

        if(ms - lastBroadcastTime >= BROADCAST_INTERVAL)
        {
            lastBroadcastTime = ms;
            broadcastPacket();
            numPacketsSent++;
        }
    }
}

int SoccerPlayer::findGameState()
{
    if(!noNetwork)
        gameState = socketMonitor->getGameState();
    else
        gameState = SocketMonitor::STATE_PLAY;

    newGameState = lastGameState != gameState;
    lastGameState = gameState;

    return gameState;
}

void SoccerPlayer::adjustHipPitch()
{
    if(Walking::GetInstance()->IsRunning())
    {
        if(Walking::GetInstance()->X_MOVE_AMPLITUDE < 0)
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Walking Config","hip_pitch_offset_lo");
        }
        else if(Walking::GetInstance()->X_MOVE_AMPLITUDE > 10)
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Walking Config","hip_pitch_offset_hi");
        }
        else
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Walking Config","hip_pitch_offset");
        }
#ifdef DEBUG
        cout << Walking::GetInstance()->X_MOVE_AMPLITUDE << " " <<
                Walking::GetInstance()->HIP_PITCH_OFFSET << endl;
#endif
    }
}
