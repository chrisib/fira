#include "PlayerTarget.h"
#include <vector>
#include <cmath>

using namespace std;
using namespace Robot;

PlayerTarget::PlayerTarget() : MultiBlob("Team Target")
{
}

PlayerTarget::~PlayerTarget()
{
    ClearAllTargets();
}

void PlayerTarget::LoadIniSettings(minIni &ini, const string &section)
{
    MultiBlob::LoadIniSettings(ini,section);
    groupingThreshold = ini.geti("Players","GroupingThreshold");
}

// do a two-step filter to identify likely candidates for players on the field
// first remove bounding boxes that are likely noise (too small, too sparse)
// then group bounding boxes together if they are close enough to form a single larger box
void PlayerTarget::ProcessCandidates(vector<BoundingBox*> &candidates)
{
    /********************************************************************************************/
    // COPIED FROM MULTI-TARGET
    ClearAllTargets();

    BoundingBox *current;
    int votes;

    double targetRatio = (double)realHeight/(double)realWidth;
    double currentRatio;

    vector<BoundingBox*> tmpTargets;

    unsigned int numCandidates = candidates.size();
//    cout << "Analyzing " << numCandidates <<" candidates" << endl;
    for(unsigned int i=0; i<numCandidates; i++)
    {
        current = candidates[i];

        // ignore anything smaller than a certain threshold
        if(current->height >= minPixelHeight && current->width >= minPixelWidth)
        {
            votes = 0;

            currentRatio = (double) current->height / (double) current->width;

            if(!proportionsDisabled)
            {
                // proportions within [0.75,1.25] times the correct size && colours that match
                if((currentRatio < targetRatio && currentRatio * 1.25 >= targetRatio) || (currentRatio > targetRatio && currentRatio * 0.75 <= targetRatio))
                    votes++;

                // proportions are WAY off
                if((currentRatio < targetRatio && currentRatio * 1.25 < targetRatio) || (currentRatio > targetRatio && currentRatio * 0.75 > targetRatio))
                    votes--;
            }

            // each colour channel is acceptable
            if(!yRangeDisabled && yRange.Check(current->avgY))
                votes++;
            if(!uRangeDisabled && uRange.Check(current->avgU))
                votes++;
            if(!vRangeDisabled && vRange.Check(current->avgV))
                votes++;

            // extra vote if the key channel range is good
            if(!keyRangeDisabled)
            {
                switch(keyChannel)
                {
                    case 2:
                        if(keyChannelRange->Check(current->avgV))
                            votes++;
                        break;
                    case 1:
                        if(keyChannelRange->Check(current->avgU))
                            votes++;
                        break;
                    case 0:
                    default:
                    if(keyChannelRange->Check(current->avgY))
                            votes++;
                        break;
                }
            }

            if(!fillDisabled)
            {
                // fill is sufficient
                if(current->fill >= 0.5)
                    votes++;
                if(current->fill >= 0.6)
                    votes++;
                if(current->fill >= 0.7)
                    votes++;

                // fill is insufficient
                if(current->fill <0.5)
                    votes--;
                if(current->fill <0.4)
                    votes--;
                if(current->fill <0.3)
                    votes--;
            }

            //cerr << "candidate got " << votes << " votes" << endl;

            // found a decent match; add this to the list of candidates
            // MODIFIED FROM MULTI-TARGET TO REMOVE NESTED/INTERSECTION FILTERING
            if(votes >= GetMaxVotes()/2)
            {
                //cerr << "Candidate queued for second pass" << endl;
                //if(filterNested || filterIntersections)
                    tmpTargets.push_back(new BoundingBox(*current));
                //else
                //    allTargets->push_back(new BoundingBox(*current));
            }
        }
        //else
        //{
        //    cerr << "Candidate is too small; noise-filtered" << endl;
        //}
    }
    // COPIED FROM MULTI-TARGET
    /********************************************************************************************/

    //cerr << "Starting second pass on " << tmpTargets.size() << " candidates" << endl;

    // do a second pass to group nearby bounding boxes together
    BoundingBox*bucket;
    numCandidates = tmpTargets.size();
    vector<BoundingBox*> buckets;
    for(unsigned int i=0; i<numCandidates; i++)
    {
        current = tmpTargets[i];
        //current->Print();

        int numBuckets = buckets.size();
        bool addedToBucket = false;
        for(int b=0; b<numBuckets && !addedToBucket; b++)
        {
            bucket = buckets[b];
            double distance = sqrt(pow(current->center.X - bucket->center.X,2.0) + pow(current->center.Y - bucket->center.Y,2.0));

            //cerr << "Candidate " << i << " @ distance " << distance << " from bucket " << b << endl;
            if(distance <= groupingThreshold)
            {
                //cerr << "Adding to existing bucket" << endl;

                addedToBucket = true;

                int left = bucket->center.X - bucket->width/2;
                int right = bucket->center.X + bucket->width/2;
                int top = bucket->center.Y - bucket->height/2;
                int bottom = bucket->center.Y + bucket->height/2;

                if(current->center.X - current->width/2 < left)
                {
                    left = current->center.X - current->width/2;
                }

                if(current->center.X + current->width/2 > right)
                {
                    right = current->center.X + current->width/2;
                }

                if(current->center.Y - current->height/2 < top)
                {
                    top = current->center.Y - current->height/2;
                }

                if(current->center.Y + current->height/2 > bottom)
                {
                    bottom = current->center.Y + current->height/2;
                }

                bucket->width = right-left;
                bucket->height = bottom-top;

                bucket->center.X = (left+right)/2;
                bucket->center.Y = (top+bottom)/2;

                //cerr << "Bucket " << b << " new dimensions: " << bucket->width << " x " << bucket->height << endl <<
                //        "       new position: " << bucket->center.X << " , " << bucket->center.Y << endl;
            }
        }

        if(!addedToBucket)
        {
            //cerr << "Creating new bucket" << endl;
            buckets.push_back(new BoundingBox(*current));
        }

        // clean up our garbage
        delete current;
    }

    // merge overlapping & nested bounding boxes
    // sort the candidates in ascending order of size
    sort(buckets.begin(), buckets.end(), BoundingBox::CompareBySize);

    for(unsigned int i=0; i<buckets.size(); i++)
    {
        bool distinct = true;
        for(unsigned int j=buckets.size()-1; distinct && j>i; j--)
        {
            if( (filterIntersections && buckets[j]->Intersects(*(buckets[i]))) ||
                 (filterNested && buckets[j]->Contains(*(buckets[i]))) )
            {
                distinct = false;
                //cerr << "Candidate " << i << " is contained by candidate " << j << endl;
            }
        }

        if(distinct)
            allTargets.push_back(buckets[i]);
        else
            delete buckets[i];
    }

    //cerr << "Found a total of " << allTargets->size() << " players in the field on this team" << endl;
}
