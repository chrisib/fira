#include "SocketMonitor.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <SoccerPlayer.h>
#include <darwin/framework/Math.h>
#include <cstdio>
#include "FiraPackets.h"

using namespace std;

SocketMonitor::SocketMonitor(SoccerField &field)
{
    DEBUG_PRINT = false;

    this->field = &field;
}

SocketMonitor::SocketMonitor(int portNo,SoccerField &field)
{
    DEBUG_PRINT = false;
    this->setPort(portNo);

    this->field = &field;
}

SocketMonitor::~SocketMonitor()
{
    // TODO: anything?
}

void SocketMonitor::loadIniSettings(minIni *ini)
{
    myTeam = (strcmp(ini->gets("Soccer","Colour").c_str(),"red") == 0) ? SoccerPlayer::TEAM_RED : SoccerPlayer::TEAM_BLUE;
}

void SocketMonitor::setPort(int portNo)
{
    this->portNo = portNo;

#ifndef USE_Q_SOCKET
    int yes = 1;

    // create the C socket we need to listen on
    sockFD = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(sockFD < 0)
        cerr << "[error] Could not create socket" << endl;

    bzero((char *) &sockAddr, sizeof(sockAddr));
    sockAddr.sin_family = AF_INET;
    sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);//INADDR_BROADCAST);//
    sockAddr.sin_port = htons(portNo);

    // set broadcast permissions
    if(setsockopt(sockFD, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) < 0)
        cerr << "[error] Could not set broadcast permissions for socket" << endl;

    if (bind(sockFD, (struct sockaddr *) &sockAddr, sizeof(sockAddr)) < 0)
          cerr << "[error] Could not bind socket" << endl;

#else
    qSocket.bind(this->portNo, QUdpSocket::ShareAddress);
#endif

    cerr << "Creating socket monitor thread..." << endl;
    pthread_mutex_init(&socketBufferLock, NULL);
    pthread_create(&monitorThreadId, NULL, SocketMonitor::listenThread, this);
    cerr << "done" << endl;
}

void *SocketMonitor::listenThread(void* arg)
{
    SocketMonitor *instance = (SocketMonitor*)arg;

    for(;;)
    {
#ifndef USE_Q_SOCKET
        //pthread_mutex_lock(&instance->socketBufferLock);
        instance->bufferLength = recv(instance->sockFD, instance->sockBuffer, SocketMonitor::BUFFER_LENGTH, 0);
        if(instance->bufferLength > 0)
        {
            instance->processUdpDatagram(instance->sockBuffer, instance->bufferLength);
            if(instance->DEBUG_PRINT)
            {
                cout << "[Packet Debug] Last Packet: ";
                instance->printLastPacket();
                cout << "Time remaining: " << instance->getTimeRemaining() << endl <<
                        "    Game state: " << gameState2string(instance->getGameState()) << " (" << instance->getGameState() << ")" << endl <<
                        endl;
            }
        }
#else
        // read all datagrams until we get to the latest one
        while(instance->qSocket.hasPendingDatagrams())
        {
            instance->qSocket.readDatagram(instance->sockBuffer, SocketMonitor::BUFFER_LENGTH);
        }
        instance->processUdpDatagram();
#endif
        pthread_yield();
    }
    return NULL;
}

void SocketMonitor::printLastPacket()
{
    if(bufferLength > 0)
    {
        for(int i=0; i<bufferLength; i++)
            printf("%02x ", sockBuffer[i] & 0xff);
        cout << endl;
    }
}

void SocketMonitor::transmitState(SoccerPlayer &player)
{
    SoccerField &field = player.getFieldMap();
    VisionMap &polarMap = player.getVisionMap();

    vector<PacketObject> toTransmit;

    // the robot's current state
    toTransmit.push_back(UserDefStateObject(player.getCurrentState(),player.getSubstate()));

    // the robot's position
    uint16_t x,y,z;
    Point2Packet(player.getFieldMap().presentPosition,x,y,z,field);
    toTransmit.push_back(StateObject(StateObject::TYPE_OWN_POSITION,x,y,z,field.positionConfidence & 0xff));

    // the robot's current intent
    if(player.isKeeper())
    {
        toTransmit.push_back(IntentObject(IntentObject::TYPE_PLAY_GOALIE,x,y,z,0));
    }
    else
    {
        switch(player.getCurrentState())
        {
        case SoccerPlayer::STATE_WAITING:
        case SoccerPlayer::STATE_HW_INTERRUPT:
        case SoccerPlayer::STATE_READY:
            toTransmit.push_back(IntentObject(IntentObject::TYPE_UNDEFINED,x,y,z,128));
            break;

        case SoccerPlayer::STATE_WALKING:
            toTransmit.push_back(IntentObject(IntentObject::TYPE_MOVEMENT,x,y,z,128));
            break;

        case SoccerPlayer::STATE_PLAYING:
            {
                switch(player.getSubstate())
                {
                case SoccerPlayer::SUBSTATE_KICKING:
                case SoccerPlayer::SUBSTATE_FINAL_APPROACH:
                    toTransmit.push_back(IntentObject(IntentObject::TYPE_SHOT_ON_GOAL,x,y,z,255));
                    break;

                case SoccerPlayer::SUBSTATE_CHASING_BALL:
                case SoccerPlayer::SUBSTATE_FINDING_BALL:
                    toTransmit.push_back(IntentObject(IntentObject::TYPE_MOVEMENT,x,y,z,128));
                    break;

                default:
                    break;
                }
            }
            break;

        default:
            break;
        }
    }

    // ball position
    Point2Packet(field.ballPosition,x,y,field);
    toTransmit.push_back(StateObject(StateObject::TYPE_BALL,x,y,0,field.ballConfidence & 0xff));

    // objects we're currently looking at (inc. field lines, goal posts, balls, etc...)
    // the endpoints are passed as user-defined objects with adjacent packet objects being points
    for(unsigned int i=0; i<polarMap.lines.size(); i++)
    {
        x = Range2Packet(polarMap.lines[i].start.X);
        y = Angle2Packet(polarMap.lines[i].start.Y);
        toTransmit.push_back(UserDefLineObject(x,y,0,PerceptionObject::TYPE_FIELD_LINE));   // field line start

        x = Range2Packet(polarMap.lines[i].end.X);
        y = Angle2Packet(polarMap.lines[i].end.Y);
        toTransmit.push_back(UserDefLineObject(x,y,1,PerceptionObject::TYPE_FIELD_LINE));   // field line end
    }
    x = Range2Packet(polarMap.ballPosition.X);
    y = Angle2Packet(polarMap.ballPosition.Y);
    toTransmit.push_back(UserDefPointObject(x,y,0,PerceptionObject::TYPE_BALL));    // ball

    // goal
    x = Range2Packet(polarMap.goal.start.X);
    y = Angle2Packet(polarMap.goal.start.Y);
    toTransmit.push_back(UserDefPointObject(x,y,0,PerceptionObject::TYPE_TEAM_GOAL)); // goal left post
    x = Range2Packet(polarMap.goal.end.X);
    y = Angle2Packet(polarMap.goal.end.Y);
    toTransmit.push_back(UserDefPointObject(x,y,1,PerceptionObject::TYPE_TEAM_GOAL)); // goal left post

    // my team
    for(unsigned int i=0; i<polarMap.myTeam.size(); i++)
    {
        x = Range2Packet(polarMap.myTeam[i].X);
        y = Angle2Packet(polarMap.myTeam[i].Y);
        toTransmit.push_back(UserDefPointObject(x,y,0,PerceptionObject::TYPE_TEAM_ROBOT));
    }

    // their team
    for(unsigned int i=0; i<polarMap.myTeam.size(); i++)
    {
        x = Range2Packet(polarMap.theirTeam[i].X);
        y = Angle2Packet(polarMap.theirTeam[i].Y);
        toTransmit.push_back(UserDefPointObject(x,y,0,PerceptionObject::TYPE_OPPOSING_ROBOT));
    }

    // the ball(if it was seen)
    if(polarMap.ballPosition.X > 0)
    {
        x = Range2Packet(polarMap.ballPosition.X);
        y = Angle2Packet(polarMap.ballPosition.Y);
        toTransmit.push_back(UserDefPointObject(x,y,0,PerceptionObject::TYPE_BALL));
    }

    if(DEBUG_PRINT)
        cout << toTransmit.size() << " objects created for transmission" << endl;


    // fill in the rest of the packet and send it
    SoccerPacket packet;
    packet.header = FIRA_PACKET_HEADER;
    packet.dummy = 0x00;
    packet.id = player.getId();
    packet.sequence = player.getPacketSequence();
    packet.numObjects = (uint8_t)toTransmit.size() & 0xff;
    for(uint8_t i=0; i<packet.numObjects; i++)
    {
        packet.objects[i] = toTransmit[i];
    }
    transmitPacket(packet);
}

void SocketMonitor::transmitPacket(SoccerPacket &packetData)
{
    uint8_t checksum = 0;
    uint16_t numBytesUsed = FIRA_PACKET_ADDITIONAL_BYTES + packetData.numObjects * NUM_BYTES_PER_OBJECT;
    uint8_t packet[PACKET_LENGTH];

    packet[0] = (packetData.header >> 8) & 0xff;
    packet[1] = packetData.header & 0xff;
    packet[2] = (numBytesUsed >> 8) & 0xff;
    packet[3] = numBytesUsed & 0xff;
    packet[4] = packetData.id & 0xff;
    packet[5] = (packetData.sequence >> 8) & 0xff;
    packet[6] = packetData.sequence & 0xff;
    packet[7] = packetData.numObjects;
    packet[numBytesUsed-2] = packetData.dummy;

    for(unsigned int i=0; i<packetData.numObjects; i++)
    {
        //packetData.objects[i].print();
        packet[8 + i*8] = packetData.objects[i].type & 0xff;

        for(int j=0; j<7; j++)
            packet[8 + i*8 + j+1] = packetData.objects[i].payload[j] & 0xff;
    }

    // calculate the checksum and put it at the end
    checksum = calculateChecksum(packet,numBytesUsed-1);
    packet[numBytesUsed-1] = checksum;


    // transmit the buffer
    //cout << "[info] Transmitting packet: " << numBytesUsed << " bytes" << endl;
    if(sendto(sockFD, packet, numBytesUsed, 0, (struct sockaddr*)&sockAddr/*(struct sockaddr*)&addr*/, sizeof(sockAddr)) < 0)
    {
        //cout << "[error] Failed to transmit packet" << endl;
    }
    //send(sockfd, packet, UOFM_NUM_BYTES, 0);
}

uint8_t SocketMonitor::calculateChecksum(uint8_t *data, int numBytes)
{
    uint8_t checksum = 0;
    for(int i=0; i<numBytes; i++)
        checksum = (checksum + data[i]) & 0xff;
    return checksum;
}

void SocketMonitor::updatePlayerData(char *data, int numBytes)
{
    // make sure we've got the right header for one of our packets
    if((uint8_t)data[0] != (FIRA_PACKET_HEADER >> 8) || (uint8_t)data[1] != (FIRA_PACKET_HEADER & 0xff))
    {
        //cerr << "Wrong packet header" << endl;
        return;
    }

    // validate the length of the packet
    int length = ((data[2] << 8) & 0xff) | (data[3] & 0xff);
    if(numBytes != length)
    {
        //cerr << "Wrong number of bytes; received " << numBytes << " but expected " << length << endl;
        return;
    }

    // one last check
    int numObjects = data[7] & 0xff;
    if(numObjects * NUM_BYTES_PER_OBJECT + FIRA_PACKET_ADDITIONAL_BYTES != numBytes)
    {
        //cerr << "Size mismatch: received " << numObjects << " objects; should be " << (numObjects * NUM_BYTES_PER_OBJECT + FIRA_PACKET_ADDITIONAL_BYTES) <<
        //        "b, not " << length << endl;
        return;
    }


    // validate the checksum
    uint8_t checksum = data[numBytes-1];
    if(checksum != calculateChecksum((uint8_t*)data,numBytes-1))
    {
        //cerr << "Checksum failed" << endl;
        return;
    }

    uint8_t id = (uint8_t)(data[FIRA_PACKET_ID_INDEX]);

    // get and clear the old data
    RobotStateInformation &playerData = getTeamData(id);
    playerData.lastPacketData.clear();
    playerData.myTeam.clear();
    playerData.theirTeam.clear();
    playerData.observedLines.clear();

    playerData.sequence = ((data[5] & 0xff) << 8) | (data[6] & 0xff);

    Robot::Point2D p;
    Robot::Line2D l;
    for(uint8_t i=0; i<numObjects; i++)
    {
        uint8_t objectStart = 8+NUM_BYTES_PER_OBJECT*i;

        uint8_t objectType = data[objectStart] & 0xff;

        //cout << "Object of type " << (int)objectType << endl;

        uint16_t payload1 = ((data[objectStart+1] & 0xff) << 8) | (data[objectStart+2] & 0xff);
        uint16_t payload2 = ((data[objectStart+3] & 0xff) << 8) | (data[objectStart+4] & 0xff);
        uint16_t payload3 = ((data[objectStart+5] & 0xff) << 8) | (data[objectStart+6] & 0xff);
        uint8_t payload4 = data[objectStart+7] & 0xff;

        switch(objectType)
        {
        case StateObject::TYPE_OWN_POSITION:
            playerData.lastPacketData.push_back(StateObject(objectType,(uint8_t*)data+objectStart+1));
            Packet2Point(payload1,payload2,payload3,playerData.presentPosition,*field);
            playerData.positionConfidence = payload4;
            break;

        case StateObject::TYPE_BALL:
            playerData.lastPacketData.push_back(StateObject(objectType,(uint8_t*)data+objectStart+1));
            Packet2Point(payload1,payload2,playerData.ballPosition,*field);
            playerData.ballConfidence = payload4;
            break;

        case StateObject::TYPE_TEAM_ROBOT:
        case StateObject::TYPE_OPPOSING_ROBOT:
        case StateObject::TYPE_TEAM_GOAL:
        case StateObject::TYPE_OPPOSING_GOAL:
        case StateObject::TYPE_UNDEFINED:
            playerData.lastPacketData.push_back(StateObject(objectType,(uint8_t*)data+objectStart+1));
            break;

        case PerceptionObject::TYPE_BALL:
        case PerceptionObject::TYPE_TEAM_ROBOT:
        case PerceptionObject::TYPE_OPPOSING_ROBOT:
        case PerceptionObject::TYPE_OWN_POSITION:
        case PerceptionObject::TYPE_TEAM_GOAL:
        case PerceptionObject::TYPE_OPPONENT_GOAL:
        case PerceptionObject::TYPE_FIELD_LINE:
        case PerceptionObject::TYPE_CENTER_ARC:
        case PerceptionObject::TYPE_UNDEFINED:
            playerData.lastPacketData.push_back(PerceptionObject(objectType,(uint8_t*)data+objectStart+1));
            break;

        case IntentObject::TYPE_SHOT_ON_GOAL:
        case IntentObject::TYPE_PLAY_GOALIE:
        case IntentObject::TYPE_MOVEMENT:
        case IntentObject::TYPE_UNDEFINED:
            playerData.lastPacketData.push_back(IntentObject(objectType,(uint8_t*)data+objectStart+1));
            break;

        case RefereeObject::TYPE_GAME_STATE:
        case RefereeObject::TYPE_UNDEFINED:
            playerData.lastPacketData.push_back(RefereeObject(objectType,(uint8_t*)data+objectStart+1));
            break;

        case UserDefObject::TYPE_USER_DEFINED:
        {
            switch(payload4)
            {
            case PerceptionObject::TYPE_FIELD_LINE:
                playerData.lastPacketData.push_back(UserDefLineObject(payload1,payload2,payload3,payload4));
                if(payload3==0)
                {
                    l.start.X = Packet2Range(payload1);
                    l.start.Y = Packet2Angle(payload2);
                }
                else
                {
                    l.end.X = Packet2Range(payload1);
                    l.end.Y = Packet2Angle(payload2);

                    playerData.observedLines.push_back(l);
                }
                break;

            case PerceptionObject::TYPE_TEAM_GOAL:
                playerData.lastPacketData.push_back(UserDefPointObject(payload1,payload2,payload3,payload4));
                if(payload3==0)
                {
                    playerData.observedGoal.start.X = Packet2Range(payload1);
                    playerData.observedGoal.start.Y = Packet2Angle(payload2);
                }
                else
                {
                    playerData.observedGoal.end.X = Packet2Range(payload1);
                    playerData.observedGoal.end.Y = Packet2Angle(payload2);
                }
                break;

            case PerceptionObject::TYPE_BALL:
                playerData.lastPacketData.push_back(UserDefPointObject(payload1,payload2,payload3,payload4));
                playerData.observedBall.X = Packet2Range(payload1);
                playerData.observedBall.Y = Packet2Angle(payload2);
                break;

            case PerceptionObject::TYPE_TEAM_ROBOT:
                playerData.lastPacketData.push_back(UserDefPointObject(payload1,payload2,payload3,payload4));
                p.X = Packet2Range(payload1);
                p.Y = Packet2Angle(payload2);
                playerData.myTeam.push_back(p);
                break;

            case PerceptionObject::TYPE_OPPOSING_ROBOT:
                playerData.lastPacketData.push_back(UserDefPointObject(payload1,payload2,payload3,payload4));
                p.X = Packet2Range(payload1);
                p.Y = Packet2Angle(payload2);
                playerData.theirTeam.push_back(p);
                break;

            case PerceptionObject::TYPE_OWN_POSITION:
                playerData.lastPacketData.push_back(UserDefStateObject(payload1,payload2));
                playerData.state = payload1;
                playerData.substate = payload2;
                break;

            default:
                playerData.lastPacketData.push_back(UserDefObject(UserDefObject::TYPE_USER_DEFINED,payload1,payload2,payload3,payload4));
                break;
            }
        }
        default:
            playerData.lastPacketData.push_back(PacketObject(objectType,(uint8_t*)data+objectStart+1));
            break;
        }
    }

    if(DEBUG_PRINT)
        playerData.print();
}

SocketMonitor::RobotStateInformation &SocketMonitor::getTeamData(int id)
{
    return teamData[id];
}

void SocketMonitor::Point2Packet(Robot::Point3D &position, uint16_t &positionX, uint16_t &positionY, uint16_t &orientation, SoccerField &field)
{
    orientation = Angle2Packet(position.Z);

    // network protocol defines X as the distance along the touch line
    // and y as the distance from the goal
    // positive Y points to our goal, positive X follows the right-hand-rule
    // all ranges are in mm
    positionX = (field.GetFieldWidthCM()-position.Y) * 10.0;
    positionY = (field.GetFieldLengthCM()-position.X) * 10.0;
}

void SocketMonitor::Packet2Point(uint16_t &positionX, uint16_t &positionY, uint16_t &orientation, Robot::Point3D &position, SoccerField &field)
{
    position.Z = Packet2Angle(orientation);

    // network protocol defines X as the distance along the touch line
    // and y as the distance from the goal
    // positive Y points to our goal, positive X follows the right-hand-rule
    // all ranges are in mm
    position.X = field.GetFieldLengthCM() - positionY / 10.0;
    position.Y = field.GetFieldWidthCM() - positionX / 10.0;
}

void SocketMonitor::Point2Packet(Robot::Point2D &position, uint16_t &positionX, uint16_t &positionY, SoccerField &field)
{
    // network protocol defines X as the distance along the touch line
    // and y as the distance from the goal
    // positive Y points to our goal, positive X follows the right-hand-rule
    // all ranges are in mm
    positionX = (field.GetFieldWidthCM()-position.Y) * 10.0;
    positionY = (field.GetFieldLengthCM()-position.X) * 10.0;
}

void SocketMonitor::Packet2Point(uint16_t &positionX, uint16_t &positionY, Robot::Point2D &position, SoccerField &field)
{
    // network protocol defines X as the distance along the touch line
    // and y as the distance from the goal
    // positive Y points to our goal, positive X follows the right-hand-rule
    // all ranges are in mm
    position.X = field.GetFieldLengthCM() - positionY / 10.0;
    position.Y = field.GetFieldWidthCM() - positionX / 10.0;
}

uint16_t SocketMonitor::Angle2Packet(double angle)
{
    angle = SoccerField::normalizeAngle(angle);

    // the zero angle points to our left and rotates anti-clockwise for United Soccer
    // therefore our zero corresponds to the network protocol's 270
    angle += 270;
    while(angle > 360)
        angle -=360;
    while(angle < 0)
        angle += 360;

    angle = deg2rad(angle);
    angle *= PACKET_DEGREE_PRECISION;

    int tmp = (int)angle;

    uint16_t result = (uint16_t)tmp;
    //cerr << "Angle: " << angle << " " << tmp << " " << result << endl;

    return result;
}

double SocketMonitor::Packet2Angle(uint16_t angle)
{
    double result = (double) angle;
    result /= PACKET_DEGREE_PRECISION;
    result = rad2deg(result);

    // the zero angle points to our left and rotates anti-clockwise for United Soccer
    // therefore our zero corresponds to the network protocol's 270
    result -= 270;
    result = SoccerField::normalizeAngle(result);
    return result;
}

uint16_t SocketMonitor::Range2Packet(double range)
{
    if(range < 0)
        return 0xffff;
    else
        return (uint16_t)range;
}

double SocketMonitor::Packet2Range(uint16_t range)
{
    if(range == 0xffff)
        return -1;
    else
        return (double)range;
}

string SocketMonitor::gameState2string(int gameState)
{
    switch(gameState)
    {
    case STATE_INITIAL:
        return "Initial";
    case STATE_READY:
        return "Ready";
    case STATE_SET:
        return "Set";
    case STATE_PLAY:
        return "Play";
    case STATE_FINISHED:
        return "Finished";
    case STATE_DEFEND:
        return "Defend";
    default:
        return "unknown";
    }
}

void SocketMonitor::RobotStateInformation::print()
{
    cout << "++++ Robot State Information ++++" << endl <<
            "     Sequence: " << sequence << endl <<
            "Current State: ";

    switch(state)
    {
    case SoccerPlayer::STATE_READY:
        cout << "Ready";
        break;
    case SoccerPlayer::STATE_WALKING:
        cout << "Walking";
        break;
    case SoccerPlayer::STATE_WAITING:
        cout << "Waiting";
        break;
    case SoccerPlayer::STATE_PLAYING:
        cout << "Playing";
        break;
    case SoccerPlayer::STATE_HW_INTERRUPT:
        cout << "HW Int.";
        break;
    default:
        cout << "Unknown";
        break;
    }
    cout << " [";

    switch(substate)
    {
    case SoccerPlayer::SUBSTATE_CHASING_BALL:
        cout << "Chasing";
        break;
    case SoccerPlayer::SUBSTATE_FINAL_APPROACH:
        cout << "Aiming";
        break;
    case SoccerPlayer::SUBSTATE_KICKING:
        cout << "Kicking";
        break;
    case SoccerPlayer::SUBSTATE_FINDING_BALL:
        cout << "Finding Ball";
        break;
    default:
        cout << "Unknown";
        break;
    }
    cout << "]" << endl;

    cout << "     Position: " << presentPosition.X << " " << presentPosition.Y << " @ " << presentPosition.Z << " [" << positionConfidence << "]" << endl <<
            "Ball Position: " << ballPosition.X << " " << ballPosition.Y << " [" << ballConfidence << "]" << endl <<
            "               " << observedBall.X << " @ " << observedBall.Y << endl <<
            "     Observed: " << observedLines.size() << " lines" << endl <<
            "               " << myTeam.size() << " players from my team" << endl <<
            "               " << theirTeam.size() << " players from their team" << endl <<
            " Objects Sent: " << lastPacketData.size() << endl <<
            endl;
}

void PacketObject::print()
{
    cout << "Packet Object: " <<
            (int)type << " ";

    for(int i=0; i<7; i++)
        cout << (int)payload[i] << " ";
    cout << endl;
}
