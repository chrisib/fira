#ifndef BALLINFO_H
#define BALLINFO_H

using namespace std;

class BallInfo {
	private:
		double x;
		double y;
		double xSpeed;
		double ySpeed;
		double r;
		double theta;
		double rSpeed;
		double thetaSpeed;
	public:
		~BallInfo();
		BallInfo();
		double getX();
		double getY();
		double getXSpeed();
		double getYSpeed();
		double getR();
		double getTheta();
		double getRSpeed();
		double getThetaSpeed();
		void setX(double newX);
		void setY(double newY);
		void setXSpeed(double newXSpeed);
		void setYSpeed(double newYSpeed);
		void setR(double newR);
		void setTheta(double newTheta);
		void setRSpeed(double newRSpeed);
		void setThetaSpeed(double newThetaSpeed);
		void reset();
		void update(double newX, double newY, double newR, double newTheta, double frameRate);
		double calculateForwardSpeed();
		double calculateLateralSpeed();
		double calculateForwardPosition();
		double calculateLateralPosition();
		double calculateLateralCrossingPosition();
		double calculateCrossingTime();
		string toString();
};

#endif
