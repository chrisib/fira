#include <cstdlib>
#include <time.h>
#include <sys/time.h>
#include "FrameRateMonitor.h"
using namespace std;

//destructor
FrameRateMonitor::~FrameRateMonitor() {
	for(int i=0;i<FRAME_TIMES_KEPT;i++) {
		free(lastFrameTimes[i]);
	}
	delete[] lastFrameTimes;
}

//constructor
FrameRateMonitor::FrameRateMonitor() {
	minIni *ini = new minIni("config.ini");

	FRAME_RATE_GUESS = ini->getd("Frame Rate","FrameRateGuess");
	FRAME_TIMES_KEPT = ini->getd("Frame Rate","FrameTimesKept");
	frameRate = FRAME_RATE_GUESS;
	mostRecentFrameRate = FRAME_RATE_GUESS;
	lastFrameTimes = new tspec*[FRAME_TIMES_KEPT];
	for(int i=0;i<FRAME_TIMES_KEPT;i++) {
		lastFrameTimes[i] = (tspec*)malloc(sizeof(tspec));
		lastFrameTimes[i]->tv_sec = 0;
		lastFrameTimes[i]->tv_nsec = 0;
	}
}

//accessors
double FrameRateMonitor::getFrameRate() {
	return frameRate;
}

double FrameRateMonitor::getMostRecentFrameRate() {
	return mostRecentFrameRate;
}

//updates frame rate variables using system time functions
void FrameRateMonitor::updateFrameRate() {
	double average = 0;
	double lastInterval = 0;

	for(int i=FRAME_TIMES_KEPT-1;i>0;i--) {
		lastFrameTimes[i]->tv_sec = lastFrameTimes[i-1]->tv_sec;
		lastFrameTimes[i]->tv_nsec = lastFrameTimes[i-1]->tv_nsec;
	}
	clock_gettime(CLOCK_REALTIME, lastFrameTimes[0]);

	if(lastFrameTimes[FRAME_TIMES_KEPT-1]->tv_sec != 0) {
		for(int i=0;i<FRAME_TIMES_KEPT-1;i++) {
			average += (lastFrameTimes[i]->tv_sec-lastFrameTimes[i+1]->tv_sec)*1000000000.0 + (lastFrameTimes[i]->tv_nsec-lastFrameTimes[i+1]->tv_nsec);
			if(i == FRAME_TIMES_KEPT-2)
				lastInterval = (lastFrameTimes[i]->tv_sec-lastFrameTimes[i+1]->tv_sec)*1000000000.0 + (lastFrameTimes[i]->tv_nsec-lastFrameTimes[i+1]->tv_nsec);
		}
		average = average/(FRAME_TIMES_KEPT-1);

		frameRate = 1.0/(average/1000000000.0);
		mostRecentFrameRate = 1.0/(lastInterval/1000000000.0);
	}
}
