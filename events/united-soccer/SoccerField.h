#ifndef SOCCERFIELD_H
#define SOCCERFIELD_H

#include <VisionMap.h>
#include <opencv2/core/core.hpp>
#include <vector>

// represents the robot's absolute position on the soccer field
// allows us to designate "home" positions
class SoccerField
{
public:
    /*
      FOR REFERENCE:

    (0,0)  +Y -------->
        +----------------------------------------+----------------------------------------+
     +X |                                        |                                        |
      | |                                        |                                        |
      | |        theta:  x------> 0              |                                        |
      | |                |                       |                                        |
      | |-----+          |                       |                                  +-----|
      V |     |          |                    ---+---                               |     |
        |     |          V                  .    |    .                             |  T  |
        |  O  |         -90                /     |     \                            |  H  |
        |  U  |             +             |      |      |            +              |  E  |
        |  R  |                            \     |     /                            |  I  |
        |  S  |                              .   |    .                             |  R  |
        |     |                               ---+---                               |  S  |
        |-----+                                  |                                  +-----|
        |                                        |                                        |
        |                                        |                                        |
        |                                        |                                        |
        |                                        |                                        |
        +----------------------------------------+----------------------------------------+

        The coordinate system is in cm and degrees, with the angles rotated 90 degrees from
        your standard cartesean system (to preserve the "facing their goal = 0" mechanic)

        Otherwise the coordinate system is standard cartesean (when the field is drawn with our goal at the bottom anyway....)
    */

    SoccerField();
    virtual ~SoccerField();
    virtual void Initialize(VisionMap *polarMap, minIni &ini);

    // given the robot's speed move the particle cloud around and recalculate where we likely are on the field
    virtual void Update(double xAmplitude, double yAmplitude, double aAmplitude);

    // override the robot's current position with whatever we tell it
    // this sets all of the particles to the specified position
    virtual void SetPosition(Robot::Point3D position);

    // we have a list of candidate positions, but don't know which is correct
    // e.g. coming in from penalty; are we entering from the left or the right?
    // the particles will be randomly positioned at the candidates (even distribution)
    virtual void SetCandidatePositions(std::vector<Robot::Point3D> &possiblePositions);

    // we have no idea where we are; scatter the particles randomly
    virtual void UnknownPosition();

    // we haven't seen the ball in a while; consider its position stale and mark it as unknown
    virtual void LostBall();

    // draw an OpenCV image of the field and objects' positions on it
    virtual void Draw(bool hideParticles=false);

    // get the range and angle from the robot's present position to point on the field
    // angle is in degrees: 0 is directly ahead, positive is to the left, negative to the right
    // range is in cm
    // parameters must use these same units
    virtual double GetAngleToPoint(Robot::Point2D point);
    virtual double GetAngleToPoint(Robot::Point2D point, Robot::Point3D position);
    virtual double GetRangeToPoint(Robot::Point2D point);
    virtual double GetRangeToPoint(Robot::Point2D point, Robot::Point3D position);

    // the robot's current position on the field relative to our back left corner (in cm) (Z = orientation in degrees)
    // X measured parallel to the goal line, Y measured parallel to the sideline
    Robot::Point3D presentPosition;

    // confidence level for our position, ball position
    // 255 = maximal confidence, 0 = no confidence
    int positionConfidence;
    int ballConfidence;

    // absolute position of the mobile objects on the field
    // if we can't see the ball then we assume it's stationary
    // position is in cm, X measured parallel to the goal line, Y measured parallel to the sideline
    Robot::Point2D ballPosition;
    std::vector<Robot::Point2D> myTeam;
    std::vector<Robot::Point2D> theirTeam;

    cv::Mat fieldImage;

    // coordinates for fixed points on the field
    // all values are in cm
    // NOTE: this is different than most other classes that use mm
    // these should be treated as read-only values
    // (they're not const because their actual values are loaded from the config file)
    Robot::Point2D myGoalLeftPost;
    Robot::Point2D myGoalRightPost;
    Robot::Point2D myGoalCenter;
    Robot::Point2D theirGoalLeftPost;
    Robot::Point2D theirGoalRightPost;
    Robot::Point2D theirGoalCenter;
    Robot::Point2D myPenaltyMark;
    Robot::Point2D theirPenaltyMark;
    Robot::Point2D centre;
    Robot::Point2D kickoffPosition;

    static const int UNKNOWN = -99999;  // arbitrary; used to indicate unknown X/Y/theta positions for objects on the field

    virtual int GetFieldLengthCM(){return fieldLength;}
    virtual int GetFieldWidthCM(){return fieldWidth;}
    virtual int GetCreaseWidthCM(){return creaseWidth;}
    virtual int GetCreaseDepthCM(){return creaseLength;}
    virtual int GetCenterRadiusCM(){return centreRadius;}
    virtual int GetGoalWidthCM(){return goalWidth;}

    virtual int GetCurrentQuadrant(); // what quadrant are we in right now?
    virtual int GetQuadrantFacing();  // what quadrant are we facing (if this returns the same quadrant as the one we're in it's because we're facing toward the sideline/base line)
    // return one of...
    enum {
        QUADRANT_DEFENCE_LEFT,
        QUADRANT_DEFENCE_RIGHT,
        QUADRANT_OFFENCE_LEFT,
        QUADRANT_OFFENCE_RIGHT
    };

    // convert an angle so that it lies within a standard [-180,180] range
    static double normalizeAngle(double degrees);

    static const cv::Scalar WHITE;
    static const cv::Scalar BLACK;
    static const cv::Scalar GREY;
    static const cv::Scalar BLUE;
    static const cv::Scalar RED;
    static const cv::Scalar YELLOW;
    static const cv::Scalar GREEN;
    static const cv::Scalar ORANGE;
    static const cv::Scalar CYAN;
    static const cv::Scalar MAGENTA;
    static const cv::Scalar DK_GREEN;

    // is a line currently in the robot's field of view, taking into account the camera's pan angle?
    // (tilt is ignored; we assume infinite depth of vision)
    bool IsVisible(Robot::Line2D &line, Robot::Point3D &position);
    bool IsVisible(Robot::Line2D &line, Robot::Point2D &position, Robot::Point2D &orientationVector);

    bool IsVisible(Robot::Point2D &point, Robot::Point2D &position, Robot::Point2D &orientationVector);

protected:
    class Particle {
    public:
        Robot::Point3D position;
        double weight;
    };

    VisionMap *polarMap;

    // field dimensions in cm
    int fieldLength;
    int fieldWidth;
    int goalWidth;
    int goalDepth;
    int creaseWidth;
    int creaseLength;
    int centreRadius;
    int marginWidth;

    double goalWeightScaling;
    double lineWeightScaling;

    double ballConfidenceDecay;

    // how many sides to we estimate a circle to have?
    int circleApprox;

    // minimum range for the particles to wander
    double particleXwander;
    double particleYwander;
    double particleAwander;

    virtual void LoadIniSettings(minIni &ini, const char* section);

    int NUM_PARTICLES;
    static const int DEFAULT_NUM_PARTICLES = 100;
    std::vector<Particle> particles;
    double xGainX, xGainY, xGainA;
    double yGainX, yGainY, yGainA;
    double aGainX, aGainY, aGainA;

    // move a particle given the robot's walking amplitudes
    virtual void MoveParticle(Particle &particle, double xAmplitude, double yAmplitude, double aAmplitude);
    virtual double WeighParticle(Particle &particle);

    // the set of all lines on the field
    std::vector<Robot::Line2D> fieldLines;
    virtual void MakeFieldLines();

    static const double MIN_WEIGHT = 0.01;
    static const double MAX_WEIGHT = 0.99;

    int imageScaleFactor;

    // calculate the minimum distance between a line and a point [projecting the line to infinity in each direction]
    double MinDistance(Robot::Line2D &line, Robot::Point2D &point);


    double WeighLines(std::vector<Robot::Line2D> &seenLines, std::vector<Robot::Line2D> &visibleLines, Robot::Point3D &position);
    double WeighLinesByExplanation(std::vector<Robot::Line2D> &visibleFieldLines, std::vector<Robot::Line2D> &observedLines, Robot::Point3D &position);
    double WeighLinesByObservation(std::vector<Robot::Line2D> &visibleFieldLines, std::vector<Robot::Line2D> &observedLines, Robot::Point3D &position);

    double WeighGoalPost(Robot::Point2D &polarPost, Robot::Point2D& myPost, Robot::Point2D& theirPost, Robot::Point2D &position, Robot::Point2D &orientationVector);
    double WeighGoalPost(Robot::Point2D &seenPost, Robot::Point2D visiblePost, Robot::Point2D &position, Robot::Point2D &orientationVector);

    // calculate the confidence in our position
    void CalculateConfidence();
};

#endif // SOCCERFIELD_H
