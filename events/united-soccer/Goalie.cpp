#include <iostream>
#include <darwin/framework/Voice.h>
#include <darwin/linux/CvCamera.h>
#include <cstring>
#include "opencv2/core/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include "Goalie.h"

using namespace Robot;
using namespace std;
using namespace cv;

//"constants" set with config.ini
int Goalie::FRAMES_BEFORE_DIVE;
int Goalie::FRAMES_BEFORE_ACTIVE;
int Goalie::FRAMES_BEFORE_PASSIVE;
int Goalie::FRAMES_BEFORE_SHUFFLE;
int Goalie::FRAMES_AFTER_SHUFFLE;
int Goalie::FRAMES_BEFORE_TILT;
int Goalie::FRAMES_BEFORE_DONE_REORIENTING;
int Goalie::DIVE_STEPS;
double Goalie::CROUCH_TOLERANCE;
double Goalie::PASSIVE_RANGE;
double Goalie::CROSS_TIME_THRESHOLD;
double Goalie::WALKING_CROSS_TIME_THRESHOLD;
double Goalie::SHUFFLE_TOLERANCE;
double Goalie::PIVOT_X_OFFSET;
double Goalie::PIVOT_DEGREES;
double Goalie::FORWARD_STEP_LENGTH;
int Goalie::POSITIONING_STEPS;
double Goalie::GOAL_LINE_ANGLE_THRESHOLD;
double Goalie::LOW_TILT_GOAL_LINE_CLOSE;
double Goalie::LOW_TILT_GOAL_LINE_FAR;
double Goalie::HIGH_TILT_GOAL_LINE_FAR;
double Goalie::BACKWARDS_HIP_PITCH;

//destructor
Goalie::~Goalie(){}

//constructor
Goalie::Goalie(minIni *ini, SocketMonitor *sockMon, bool noDives) : SoccerPlayer(ini, sockMon)
{
    const string SECTION_NAME = "Goalie";

    FRAMES_BEFORE_DIVE =                ini->getd(SECTION_NAME,"FramesBeforeDive");
    FRAMES_BEFORE_ACTIVE =              ini->getd(SECTION_NAME,"FramesBeforeActive");
    FRAMES_BEFORE_PASSIVE =             ini->getd(SECTION_NAME,"FramesBeforePassive");
    FRAMES_AFTER_SHUFFLE =              ini->getd(SECTION_NAME,"FramesAfterShuffle");
    FRAMES_BEFORE_SHUFFLE =             ini->getd(SECTION_NAME,"FramesBeforeShuffle");
    FRAMES_BEFORE_TILT =                ini->geti(SECTION_NAME,"FramesBeforeTilt",5); //5;
    FRAMES_BEFORE_DONE_REORIENTING =    ini->geti(SECTION_NAME,"FramesBeforeDoneReorienting",3); //3;
    DIVE_STEPS =                        ini->getd(SECTION_NAME,"DiveSteps");
    CROUCH_TOLERANCE =                  ini->getd(SECTION_NAME,"CrouchTolerance");
    PASSIVE_RANGE =                     ini->getd(SECTION_NAME,"PassiveRange");
    CROSS_TIME_THRESHOLD =              ini->getd(SECTION_NAME,"CrossTimeThreshold");
    WALKING_CROSS_TIME_THRESHOLD =      ini->getd(SECTION_NAME,"WalkingCrossTimeThreshold");
    SHUFFLE_TOLERANCE =                 ini->getd(SECTION_NAME,"ShuffleTolerance");
    PIVOT_X_OFFSET =                    ini->getd(SECTION_NAME,"PivotXOffset",-4); //-4;
    PIVOT_DEGREES =                     ini->getd(SECTION_NAME,"PivotDegrees",15); //15;
    FORWARD_STEP_LENGTH =               ini->getd(SECTION_NAME,"ForwardStepLength",10); //10;
    POSITIONING_STEPS =                 ini->geti(SECTION_NAME,"PositioningSteps",2); //2;
    GOAL_LINE_ANGLE_THRESHOLD =         ini->getd(SECTION_NAME,"GoalLineAngleThreshold",7); //7;
    LOW_TILT_GOAL_LINE_CLOSE =          ini->getd(SECTION_NAME,"LowTiltGoalLineClose",138); //138;
    LOW_TILT_GOAL_LINE_FAR =            ini->getd(SECTION_NAME,"LowTiltGoalLineFar",26); //26;
    HIGH_TILT_GOAL_LINE_FAR =           ini->getd(SECTION_NAME,"HighTiltGoalLineFar",222); //222;
    BACKWARDS_HIP_PITCH =               ini->getd(SECTION_NAME,"BackwardsHipPitch",17); //17;

    this->noDives = noDives;
    newState = false;
    oneFrameOffset = false;
    walkingStarted = false;
    diveFrames = 0;
    activePassiveFrames = 0;
    framesBeforeShuffle = 0;
    framesAfterShuffle = FRAMES_AFTER_SHUFFLE;
    mode = MODE_NORMAL;
    myState = STATE_IDLE;
    myLastState = STATE_IDLE;

    finder = GoalieFinder::create();

    lastDiveLeft = true;
    expectedLateralDistance = 0;
    expectedCrossingTime = 0;
    goalLineAngle = 0;

    target = map.getBall();
    target->SetMinPixelHeight(ini->getd(ini->gets("Soccer","BallColour","Orange").c_str(),"MinPixelHeight"));
    target->SetMinPixelWidth(ini->getd(ini->gets("Soccer","BallColour","Orange").c_str(),"MinPixelWidth"));

    if(visionTest)
        mode = MODE_VIS_TEST;
}

//calls SoccerPlayer::handleVideo(), could be expanded later
void Goalie::HandleVideo()
{
    Mat dbg = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);

    //SoccerPlayer::HandleVideo();

    if(myState == STATE_REORIENTING)
    {
        CvCamera::GetInstance()->RGB_ENABLE = true;
    }
    else
    {
        CvCamera::GetInstance()->RGB_ENABLE = false;
    }

    if(CvCamera::GetInstance()->CaptureFrame())
    {
        Mat *output = (showVideo ? &dbg : NULL);
        map.getBall()->FindInFrame(CvCamera::GetInstance()->yuvFrame,output);

        BoundingBox *targetBox;

        targetBox = map.getBall()->GetBoundingBox();
        range = map.getBall()->GetRange(0);
        angle = map.getBall()->GetAngle(0);
        lastBallInfo->update(targetBox->center.X, targetBox->center.Y, range, angle, frMonitor->getMostRecentFrameRate());

        if(DEBUG_PRINT)
        {
            cout << "Ball will cross the line in " << lastBallInfo->calculateCrossingTime() << endl;
            cout << "Ball X position: " << lastBallInfo->getX() << endl;
        }

        if(showVideo)
        {
            finder->openTrackingWindows();
        }

        if(myState == STATE_REORIENTING)
        {
            finder->update(CvCamera::GetInstance()->rgbFrame);
            finder->findGoalLine();
        }
        else
        {
            finder->clearLastLine();
        }

        if(showVideo)
        {
            if(map.getBall()->WasFound())
                map.getBall()->Draw(dbg);
            imshow("Ball",dbg);
            cvWaitKey(1);
        }
    }

    pauser.newFrame();
    pauser.threadBroadcast();
}

//function that the video thread lives in
void* Goalie::videoLoop(void* self)
{
    Goalie* s = (Goalie*)self;
    for(;;)
    {
        s->frMonitor->updateFrameRate();
        s->HandleVideo();
    }

    pthread_exit(NULL);
    return NULL; // Unreachable
}

//function that the head movement thread lives in
void* Goalie::headLoop(void* self)
{
    Goalie* s = (Goalie*)self;
    while(true)
        s->scanAround();

    pthread_exit(NULL);
}

//checks whether or not the goalie should dive
//also sets the expectedLateralDistance and expectedCrossingTime members
bool Goalie::checkDiveCriteria()
{
    bool ans = false;

    if(lastBallInfo->getX() > 0)
    {
        expectedLateralDistance = lastBallInfo->calculateLateralCrossingPosition();
        expectedCrossingTime = lastBallInfo->calculateCrossingTime();

        if(DEBUG_PRINT)
        {
            cout << "Tracking ball: estimated lateral distance: " << expectedLateralDistance << endl <<
                    "               estimated crossing time: " << expectedCrossingTime << endl;
        }

        if(expectedCrossingTime <= CROSS_TIME_THRESHOLD)//(!walkingStarted && expectedCrossingTime <= CROSS_TIME_THRESHOLD) || (walkingStarted && expectedCrossingTime <= WALKING_CROSS_TIME_THRESHOLD))
        {
            if(scanFrames >= FRAMES_AFTER_SCAN)
                diveFrames++;

            if(diveFrames >= FRAMES_BEFORE_DIVE)
            {
                cout << "Preparing to dive" << endl;
                ans = true;
                diveFrames = 0;
                scanFrames = 0;
            }
        }
        else
        {
            diveFrames = 0;
        }
    }
    else
    {
        diveFrames = 0;
    }

    return ans;
}

//initializes threads and then calls process in an infinite loop
int Goalie::Execute()
{
    pthread_create(&videoThreadId, NULL, &Goalie::videoLoop, this);
    pthread_create(&headThreadId, NULL, &Goalie::headLoop, this);

    pthread_setname_np(pthread_self(), "mainThread");
    pthread_setname_np(videoThreadId, "videoThread");
    pthread_setname_np(headThreadId, "headThread");

    //finder->openTrackingWindows();

    finder->loadIniSettings(ini);

    cout << "Executing goalie loop" << endl;

    if(mode==MODE_VIS_TEST)
    {
        cout << "Vision test mode" << endl;
        myState = STATE_ALERT;
        for(;;)
            HandleVideo();
    }
    else
    {
        //cout << "Press the middle button to start" << endl;
        //Voice::Speak("Press the middle button.");
        //MotionManager::GetInstance()->WaitButton(CM730::MIDDLE_BUTTON);
        //myState = STATE_ENTER_GOAL;
        myState = STATE_PASSIVE;

        //field.SetPosition(Point3D(0,300,180));

        for(;;)
            Process();
    }

    return 0;
}

//contains the main goalie logic
//should be called in an infinite loop
void Goalie::Process() {
    bool prevOnPenalty = onPenalty;
    checkRefStatus();

    //cout << penaltyEntrance.X << " " << penaltyEntrance.Y << " " << penaltyEntrance.Z << endl;

    // transmit status packets at regular intervals (if we're using the network)
    transmitStatus();

    SoccerPlayer::handleButton();

    // if we're in test mode just run the vision and/or networking and don't do anything else
    if(!inTestMode() && refSaysWeCanPlay)
    {
        //handleButton();

        if(myState!=STATE_DIVING && myState!=STATE_CROUCHING && myState!=STATE_LYING_DOWN)
        {
            if(SoccerPlayer::handleFall())
            {
                Action::GetInstance()->m_Joint.SetEnableBody(false);
                Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                if(myState!=STATE_ENTER_GOAL)
                    myState = STATE_REORIENTING;
            }
        }

        // actual game logic
        findGameState();

        switch(gameState)
        {
        case SocketMonitor::STATE_READY:
            if(newGameState)
            {
                if(myState != STATE_ENTER_GOAL)
                {
                    standUp();
                }
                myState = STATE_ENTER_GOAL;
                walkTo(homePosition);
            }
            break;

        case SocketMonitor::STATE_SET:
            myState = STATE_IDLE;
            break;


        case SocketMonitor::STATE_PLAY:
            if(prevOnPenalty && !onPenalty && !noNetwork)   // only worry about this if we actually are listening to the referee
            {
                // just came off penalty; walk  back to our starting position

                cout << "[info] Re-entering field from penalty" << endl;
                field->SetCandidatePositions(penaltyEntrances);
                walkTo(homePosition);
                myState = STATE_ENTER_GOAL;
            }
            else if(!onPenalty)
            {
                if(myState == STATE_IDLE) // only re-enter the playing state if we're ready to
                {
                    standUp();
                    myState = STATE_REORIENTING;
                }
            }
            break;

        case SocketMonitor::STATE_INITIAL:
        case SocketMonitor::STATE_FINISHED:
        default:
            // no action going on, so just sit down and wait
            myState = STATE_IDLE;
            break;
        }

        processGoalieState();
    }
    else
    {
        //cerr << "Waiting" << endl;

        // just sit down and wait
        sitDown();
    }
}

void Goalie::processGoalieState()
{
    double lateralPosn, goalLineY, originalHipOffset;
    int fallDirection;
    bool foundLine;
    Vec4i goalLine;

    newState = myLastState != myState;
    myLastState = myState;

    pauser.threadWaitBody();

    if(showVideo)
        finder->openTrackingWindows();

    switch(myState)
    {
    case STATE_IDLE:
        if(newState)
            cout << "[info] entering state IDLE" << endl;

        headMoving = false;
        sitDown();
        break;

    case STATE_ENTER_GOAL:
        if(newState)
        {
            cout << "[info] entering state ENTER_GOAL" << endl;

            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Action::GetInstance()->Start(READY);
            Action::GetInstance()->Finish();
            walkTo(homePosition);
        }

        if(!Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Start();
        }

        processWalking();
        break;

    case STATE_REORIENTING:
        if(newState)
        {
            cout << "[info] entering state REORIENTING" << endl;

            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Action::GetInstance()->Start(READY);
            Action::GetInstance()->Finish();

            headMoving = false;
            headPan = 0;
            headTilt = LOW_SCAN_TILT;
            MotionManager::Sync();
            Head::GetInstance()->MoveByAngle(0, headTilt);
            MotionManager::Sync();
            framesBeforeTilt = FRAMES_BEFORE_TILT;
            framesBeforeDoneReorienting = FRAMES_BEFORE_DONE_REORIENTING;
        }

        goalLineAngle = 999999;

        pauser.bodyHasReacted();
        pauser.threadWaitBody();

#if 0
        finder->update(CvCamera::GetInstance()->rgbFrame);
        foundLine = finder->findGoalLine();
#else
        foundLine = finder->goalLineWasFound();
#endif
        //waitKey(1);
        if(foundLine)
        {
            framesBeforeTilt = FRAMES_BEFORE_TILT;
            goalLineAngle = finder->getLastGoalLineAngle();
            Walking::GetInstance()->m_Joint.SetEnableBody(false);
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;

            goalLine = finder->getLastGoalLine();
            goalLineY = (double)(goalLine[1] + goalLine[3])/2.0;

            if(goalLineAngle > GOAL_LINE_ANGLE_THRESHOLD)
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = -PIVOT_DEGREES;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
            }
            else if (goalLineAngle < -GOAL_LINE_ANGLE_THRESHOLD)
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = PIVOT_DEGREES;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
            }
            else if (abs(headTilt-LOW_SCAN_TILT)<TOLERANCE && goalLineY>LOW_TILT_GOAL_LINE_CLOSE && !penaltyKick)   // do not back up if we're defending a penalty kick
            {
                originalHipOffset = Walking::GetInstance()->HIP_PITCH_OFFSET;
                Walking::GetInstance()->HIP_PITCH_OFFSET = BACKWARDS_HIP_PITCH;
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -FORWARD_STEP_LENGTH;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
                Walking::GetInstance()->HIP_PITCH_OFFSET = originalHipOffset;
            }
            else if (((abs(headTilt-LOW_SCAN_TILT)<TOLERANCE && goalLineY<LOW_TILT_GOAL_LINE_FAR) || (abs(headTilt-HIGH_SCAN_TILT)<TOLERANCE && goalLineY < HIGH_TILT_GOAL_LINE_FAR && goalLineY > 120))
                     && !penaltyKick)   // do not walk forwards if we're defending a penalty kick
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = FORWARD_STEP_LENGTH;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
            }
            else
            {
                framesBeforeDoneReorienting--;
                if(framesBeforeDoneReorienting <= 0) {
                    headMoving = true;
                    myState = STATE_ALERT;
                }
            }
        }
        else
        {
            framesBeforeDoneReorienting = FRAMES_BEFORE_DONE_REORIENTING;
            framesBeforeTilt--;
            if(abs(headTilt - LOW_SCAN_TILT) < TOLERANCE) {
                if(framesBeforeTilt <= 0) {
                    Walking::GetInstance()->m_Joint.SetEnableBody(false);
                    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                    originalHipOffset = Walking::GetInstance()->HIP_PITCH_OFFSET;
                    Walking::GetInstance()->HIP_PITCH_OFFSET = BACKWARDS_HIP_PITCH;
                    Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                    Walking::GetInstance()->X_MOVE_AMPLITUDE = -FORWARD_STEP_LENGTH;
                    Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
                    Walking::GetInstance()->Start(POSITIONING_STEPS);
                    Walking::GetInstance()->Finish();
                    Walking::GetInstance()->HIP_PITCH_OFFSET = originalHipOffset;
                    /*framesBeforeTilt = 5;
                    headTilt = HIGH_SCAN_TILT;
                    Head::GetInstance()->MoveByAngle(0, headTilt);
                    MotionManager::Sync();*/
                }
            } else {
                if(framesBeforeTilt <= 0) {
                    framesBeforeTilt = 5;
                    headTilt = LOW_SCAN_TILT;
                    Head::GetInstance()->MoveByAngle(0, headTilt);
                    MotionManager::Sync();
                }
            }
        }

        break;

    case STATE_PASSIVE:
        if(newState)
        {
            cout << "[info] entering state PASSIVE" << endl;

            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Action::GetInstance()->Start(SLOW_CROUCH);
            Action::GetInstance()->Finish();
            activePassiveFrames = 0;
            oneFrameOffset = false;
        }

        if(target->WasFound())
        {
            oneFrameOffset = false;
            if(checkDiveCriteria())
                myState = STATE_CROUCH_DIVING;
            else
            {
                activePassiveFrames++;
                if(activePassiveFrames >= FRAMES_BEFORE_ACTIVE)
                    myState = STATE_ALERT;
            }
        }
        else
        {
            diveFrames = 0;
            if(oneFrameOffset)
                activePassiveFrames = 0;
            else
                oneFrameOffset = true;
        }

        break;

    case STATE_ALERT:
        if(newState)
        {
            cout << "[info] entering state ALERT" << endl;

            for(int i=0; i<JointData::NUMBER_OF_JOINTS; i++)
            {
                Action::GetInstance()->m_Joint.SetAngle(i,MotionStatus::m_CurrentJoints.GetAngle(i));
            }

            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Action::GetInstance()->Start(READY);
            Action::GetInstance()->Finish();
            activePassiveFrames = 0;
            framesBeforeShuffle = 0;
            //ThreadPauser::milisleep(500);
            pauser.waitAMoment();
        }

        if(target->WasFound())
        {
            activePassiveFrames = 0;
            if(checkDiveCriteria())
                myState = STATE_DIVING;
            else
            {
                lateralPosn = lastBallInfo->calculateLateralPosition();
                if(abs(lateralPosn) > SHUFFLE_TOLERANCE)
                {
                    framesBeforeShuffle++;
                    if(framesBeforeShuffle >= FRAMES_BEFORE_SHUFFLE)
                        myState = STATE_SHUFFLING;
                }
                else
                    framesBeforeShuffle = 0;
            }
        }
        else
        {
            diveFrames = 0;
            activePassiveFrames++;
            if((lostFrames>=LOST_FRAMES_BEFORE_SCAN && map.getBall()->GetBoundingBox()->center.X >= PASSIVE_RANGE) || activePassiveFrames >= FRAMES_BEFORE_PASSIVE)
                myState = STATE_PASSIVE;
        }

        break;

    case STATE_SHUFFLING:
        if(newState)
            cout << "[info] entering state SHUFFLING" << endl;

        if(penaltyKick)
        {
            if(newState)
            {
                Action::GetInstance()->m_Joint.SetEnableBody(false);
                Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                framesAfterShuffle = FRAMES_AFTER_SHUFFLE;
            }

            if(target->WasFound())
            {
                lateralPosn = lastBallInfo->calculateLateralPosition();
                if(abs(lateralPosn) > SHUFFLE_TOLERANCE)
                {
                    if(lateralPosn > 0)
                    {
                        framesAfterShuffle = -1;
    #ifdef USE_SHUFFLE
                        Action::GetInstance()->Start(SHUFFLE_LEFT);
                        Action::GetInstance()->Finish();
    #else
                        standUp();
                        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 30;
                        Walking::GetInstance()->X_MOVE_AMPLITUDE = -2;
                        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                        Walking::GetInstance()->Start(2);
                        Walking::GetInstance()->Finish();
    #endif
                    }
                    else
                    {
                        framesAfterShuffle = -1;
    #ifdef USE_SHUFFLE
                        Action::GetInstance()->Start(SHUFFLE_RIGHT);
                        Action::GetInstance()->Finish();
    #else
                        standUp();
                        Walking::GetInstance()->Y_MOVE_AMPLITUDE = -30;
                        Walking::GetInstance()->X_MOVE_AMPLITUDE = -2;
                        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                        Walking::GetInstance()->Start(2);
                        Walking::GetInstance()->Finish();
    #endif
                    }
                }
                else {
                    framesAfterShuffle++;
                    if(framesAfterShuffle >= FRAMES_AFTER_SHUFFLE)
                        myState = STATE_ALERT;
                }
            }
            else
            {
                framesAfterShuffle++;
                if(framesAfterShuffle >= FRAMES_AFTER_SHUFFLE)
                    myState = STATE_ALERT;
            }
        }
        else
        {
            myState = STATE_ALERT;
        }
        break;

    case STATE_DIVING:
        if(newState)
        {
            cout << "[info] entering state DIVING" << endl;

            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        }

        if(abs(expectedLateralDistance) < CROUCH_TOLERANCE || noDives)
        {
            Voice::Speak("Crouching");
            cout << "Crouching" << endl;
            Action::GetInstance()->Start(CROUCH_BLOCK);
            Action::GetInstance()->Finish();
            myState = STATE_CROUCHING;
        }
        else if(expectedLateralDistance > 0)
        {
            Voice::Speak("Diving left");
            cout << "Diving left" << endl;
            headMoving = false;
            lastDiveLeft = true;
            Action::GetInstance()->Start(DIVE_LEFT);
            Action::GetInstance()->Finish();
            myState = STATE_LYING_DOWN;
        }
        else
        {
            Voice::Speak("Diving right");
            cout << "Diving right" << endl;
            headMoving = false;
            lastDiveLeft = false;
            Action::GetInstance()->Start(DIVE_RIGHT);
            Action::GetInstance()->Finish();
            myState = STATE_LYING_DOWN;
        }
        Action::GetInstance()->Finish();
        cout << "done" << endl;
        break;

    case STATE_CROUCH_DIVING:
        if(newState)
        {
            cout << "[info] entering state CROUCH_DIVING" << endl;

            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        }

        if(abs(expectedLateralDistance) < CROUCH_TOLERANCE || noDives)
        {
            Voice::Speak("Still Crouching");
            cout << "Still Crouching" << endl;
            myState = STATE_CROUCHING;
        }
        else if(expectedLateralDistance > 0)
        {
            Voice::Speak("Crouch Diving left");
            cout << "Crouch Diving left" << endl;
            headMoving = false;
            lastDiveLeft = true;
            Action::GetInstance()->Start(CROUCH_DIVE_LEFT);
            Action::GetInstance()->Finish();
            myState = STATE_LYING_DOWN;
        }
        else
        {
            Voice::Speak("Crouch Diving right");
            cout << "Crouch Diving right" << endl;
            headMoving = false;
            lastDiveLeft = false;
            Action::GetInstance()->Start(CROUCH_DIVE_RIGHT);
            Action::GetInstance()->Finish();
            myState = STATE_LYING_DOWN;
        }

        Action::GetInstance()->Finish();
        cout << "done" << endl;
        break;

    case STATE_CROUCHING:
        if(newState)
            cout << "[info] entering state CROUCHING" << endl;

        Action::GetInstance()->Start(CROUCH_WAIT);
        Action::GetInstance()->Finish();
        Action::GetInstance()->Start(READY);
        Action::GetInstance()->Finish();
        myState = STATE_ALERT;
        break;

    case STATE_LYING_DOWN:
        if(newState)
            cout << "[info] entering state LYING_DOWN" << endl;

        Action::GetInstance()->Start(DIVE_WAIT);
        Action::GetInstance()->Finish();
        Action::GetInstance()->Start(READY);
        Action::GetInstance()->Finish();
        fallDirection = MotionStatus::FALLEN;
        SoccerPlayer::handleFall();

        // turn 90 degrees
        //headMoving = true;
        Walking::GetInstance()->m_Joint.SetEnableBody(false);
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        Walking::GetInstance()->Start(DIVE_STEPS);
        Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Goalie","PivotHipPitch",17);
        if((lastDiveLeft &&  fallDirection == BACKWARD) || (!lastDiveLeft && fallDirection == FORWARD))
        {
            cout << "Dove left and fell backward OR dove right and fell forward -- turning left" << endl;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = PIVOT_DEGREES;
        }
        else
        {
            cout << "Dove left and fell forward OR dove right and fell backward -- turning right" << endl;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = -PIVOT_DEGREES;
        }
        Walking::GetInstance()->Finish();

        for(int i=0; i<JointData::NUMBER_OF_JOINTS; i++)
        {
            Action::GetInstance()->m_Joint.SetAngle(i,MotionStatus::m_CurrentJoints.GetAngle(i));
        }
        Action::GetInstance()->m_Joint.SetEnableBody(false);
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        myState = STATE_REORIENTING;
        break;

    default:
        if(newState)
            cout << "[info] entering state (unknown)" << endl;
        break;
    }// switch

    pauser.bodyHasReacted();
}

//changes state to STATE_REORIENTING if current state is STATE_ENTER_GOAL
void Goalie::doneWalking() {
    if(myState == STATE_ENTER_GOAL && gameState == SocketMonitor::STATE_PLAY)
        myState = STATE_REORIENTING;
    else if(myState == STATE_ENTER_GOAL)
        myState = STATE_IDLE;
}

void Goalie::scanAround()
{
    while(pauser.getHeadReacted() || !headMoving)
    {
        pauser.threadWait();
    }
    if(map.getBall()->WasFound())
    {
        if(scanFrames >= FRAMES_AFTER_SCAN)
            Head::GetInstance()->LookAt(*map.getBall());
        else
            Head::GetInstance()->LookAt(*map.getBall(), HEAD_DAMPING_FACTOR);
        MotionManager::Sync();
        headPan = Head::GetInstance()->GetPanAngle();
        headTilt = Head::GetInstance()->GetTiltAngle();

        scanFrames++;
        scanDirection = 0;
        firstSweep = true;
        lostFrames = 0;
        lookCycles = 0;

        if(!isGoalie)
            myState = STATE_PLAYING;
    }
    else
    {
        scanFrames = 0;
        if(lostFrames>=LOST_FRAMES_BEFORE_SCAN)
        {
            if(isGoalie)
                scan(this->myState != STATE_PASSIVE);
            else
                scan(true);
        }
        else
        {
            lostFrames++;
        }

        if(!isGoalie && lookCycles >= 4 && !Walking::GetInstance()->IsRunning())
        {
            cout << "[DEBUG] " << lookCycles << endl;
            if(lookCycles <= 16)
            {
                // start pivoting on the spot
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
                if(map.ballPosition.Y > 0)
                    Walking::GetInstance()->A_MOVE_AMPLITUDE = 14;
                else
                    Walking::GetInstance()->A_MOVE_AMPLITUDE = -14;
                Walking::GetInstance()->Start();
            }
            else
            {
                // Go somewhere else and look for the ball
                Point3D myPosition( field->myPenaltyMark.X, field->myPenaltyMark.Y, field->presentPosition.Z);
                Point3D theirPosition( field->theirPenaltyMark.X, field->theirPenaltyMark.Y, field->presentPosition.Z);
                const float myDistance = Point3D::Distance(field->presentPosition, myPosition);
                const float theirDistance = Point3D::Distance(field->presentPosition, theirPosition);
                if( myDistance < 10 || theirDistance < myDistance ) {
                    walkTo(theirPosition);
                } else {
                    walkTo(myPosition);
                }
                lookCycles = 4;
            }
        }
    }
    pauser.headHasReacted();
}
