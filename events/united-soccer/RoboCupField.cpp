#include "RoboCupField.h"

#include <darwin/framework/Point.h>
#include <vector>
#include <iostream>
#include <darwin/framework/Math.h>

using namespace std;
using namespace Robot;

RoboCupField::RoboCupField()
{
}

RoboCupField::~RoboCupField()
{
}

void RoboCupField::Initialize(VisionMap *polarMap, minIni &ini)
{
    myPenaltyMark = Point2D(fieldWidth/2, penaltyMarkDistance);
    theirPenaltyMark = Point2D(fieldWidth/2, fieldLength - penaltyMarkDistance);

    SoccerField::Initialize(polarMap,ini);
}

void RoboCupField::LoadIniSettings(minIni &ini, const char *section)
{
    SoccerField::LoadIniSettings(ini,section);
    penaltyMarkDistance = ini.geti(section,"PenaltyMarkDistance");
    penaltyMarkSize = ini.geti(section,"PenaltyMarkSize");
}

void RoboCupField::MakeFieldLines()
{
    SoccerField::MakeFieldLines();

    // our penalty mark
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2-penaltyMarkSize/2,penaltyMarkDistance),
                             Point2D(fieldWidth/2+penaltyMarkSize/2,penaltyMarkDistance)
    ));
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2,penaltyMarkDistance-penaltyMarkSize/2),
                             Point2D(fieldWidth/2,penaltyMarkDistance+penaltyMarkSize/2)
    ));

    // their penalty mark
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2-penaltyMarkSize/2,fieldLength-penaltyMarkDistance),
                             Point2D(fieldWidth/2+penaltyMarkSize/2,fieldLength-penaltyMarkDistance)
    ));
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2,fieldLength-(penaltyMarkDistance-penaltyMarkSize/2)),
                             Point2D(fieldWidth/2,fieldLength-(penaltyMarkDistance+penaltyMarkSize/2))
    ));
}
