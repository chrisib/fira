#ifndef ROBOCUPFIELD_H
#define ROBOCUPFIELD_H

#include <SoccerField.h>

class RoboCupField : public SoccerField
{
public:
    RoboCupField();
    virtual ~RoboCupField();

    virtual void Initialize(VisionMap *polarMap, minIni &ini);
    virtual void LoadIniSettings(minIni &ini, const char *section);

    Robot::Point2D myPenaltyMark, theirPenaltyMark;

protected:
    int penaltyMarkDistance;
    int penaltyMarkSize;
    virtual void MakeFieldLines();
};

#endif // ROBOCUPFIELD_H
