#-------------------------------------------------
#
# Project created by QtCreator 2012-07-09T13:42:10
#
#-------------------------------------------------

QT       -= gui
QT       += core
QT       += network

QMAKE_CXXFLAGS += -g

MOC_DIR = .moc/
OBJECTS_DIR = .build/

TARGET = united-soccer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += \
        -ldl \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lespeak \
        -ljpeg \
        -lpthread \
        -ldarwin \

SOURCES += main.cpp \
    Goalie.cpp \
    SocketMonitor.cpp \
    SoccerPlayer.cpp \
    BallInfo.cpp \
    FrameRateMonitor.cpp \
    ThreadPauser.cpp \
    GoalTarget.cpp \
    SoccerField.cpp \
    PlayerTarget.cpp \
    goaliefinder.cpp \
    util.cpp \
    FiraSocketMonitor.cpp \
    RoboCupSocketMonitor.cpp \
    VisionMap.cpp \
    RoboCupField.cpp \
    FiraSoccerField.cpp

HEADERS += \
    Goalie.h \
    SocketMonitor.h \
    SoccerPlayer.h \
    BallInfo.h \
    FrameRateMonitor.h \
    ThreadPauser.h \
    GoalTarget.h \
    SoccerField.h \
    PlayerTarget.h \
    goaliefinder.h \
    util.h \
    goaliefinder.h \
    FiraSocketMonitor.h \
    RoboCupSocketMonitor.h \
    FiraPackets.h \
    VisionMap.h \
    RoboCupField.h \
    FiraSoccerField.h

OTHER_FILES += \
    motion.bin \
    config.ini
