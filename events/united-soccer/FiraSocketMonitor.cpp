#include "FiraSocketMonitor.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <SoccerPlayer.h>
#include <darwin/framework/Math.h>
#include <cstdio>
#include <FiraPackets.h>

using namespace std;

FiraSocketMonitor::FiraSocketMonitor(SoccerField &field) : SocketMonitor(field)
{
    packetsSent = 0;
    gameControlData = SoccerPacket();
    gameControlData.objects[0] = RefereeObject(RefereeObject::TYPE_GAME_STATE,0x0000,0x0000,0xffff,RefereeObject::STATE_STOPPED);
}

FiraSocketMonitor::FiraSocketMonitor(int portNo,SoccerField &field) : SocketMonitor(portNo,field)
{
    packetsSent = 0;
    gameControlData.objects[0] = RefereeObject(RefereeObject::TYPE_GAME_STATE,0x0000,0x0000,0xffff,RefereeObject::STATE_STOPPED);
}

FiraSocketMonitor::~FiraSocketMonitor()
{
    // TODO: anything?
}

bool FiraSocketMonitor::isRobotOnPenalty(int team, int id)
{
    if(team != SoccerPlayer::TEAM_BLUE && team != SoccerPlayer::TEAM_RED)
    {
        cerr << "[warning] Unknown team colour: " << team << endl;
        return false;
    }
    else if(id < 0 || id >= MAX_PLAYERS_PER_TEAM*2)
    {
        cerr << "[warning] Invalid robot ID" << id << endl;
        return false;
    }
    else
    {
        bool onPenalty;
        if(team == SoccerPlayer::TEAM_BLUE)
        {
            id -= TEAM_BLUE_START_ID;
            onPenalty = (getGameControlData()->TeamBlueBitmask() & (1 << id)) == 0;
        }
        else
        {
            id -= TEAM_RED_START_ID;
            onPenalty = (getGameControlData()->TeamRedBitmask() & (1 << id)) == 0;
        }

        return onPenalty;
    }
}

bool FiraSocketMonitor::canRobotPlay(int team, int id)
{
    bool onPenalty = isRobotOnPenalty(team,id);

    // TODO: check state of play?

    return !onPenalty;
}

void FiraSocketMonitor::processUdpDatagram(char *src, int numBytes)
{
    //cerr << "reading " << numBytes << " bytes" << endl;

    // make a local copy of all the buffered data to keep things thread-safe
    char data[numBytes];
    for(int i=0; i<numBytes; i++)
        data[i] = src[i] & 0xff;

    uint8_t senderId = data[SoccerPacket::ID_INDEX];
    if(senderId == REFEREE_ID)
        updateControlData(data,numBytes);
    else
        updatePlayerData(data,numBytes);
}

void FiraSocketMonitor::updateControlData(char *data, int numBytes)
{
#ifdef SOCKET_DEBUG
    for(int i=0; i<numBytes; i++)
        printf("%02x ", data[i] & 0xff);
    cout << endl;
#else
    (void)numBytes;
#endif

    (void) numBytes;
    uint8_t id = (uint8_t)(data[4]);

    SoccerPacket *packet = &gameControlData;

    packet->header = (data[0] << 8) | data[1];
    packet->length = (data[2] << 8) | data[3];
    packet->id = id;
    packet->sequence = (data[5] << 8) | data[6];
    packet->numObjects = data[7];
    packet->dummy = 0x00;
    packet->checksum = data[packet->length-1];

    for(unsigned int i=0; i<packet->numObjects; i++)
    {
        packet->objects[i].type = data[8 + 8*i];

        if(packet->objects[i].type == RefereeObject::TYPE_GAME_STATE)
        {
            for(int j=0; j<7; j++)
            {
                packet->objects[i].payload[j] = data[9 + 8*i + j];
                //printf("%02x ", data[8 + 8*i + j]);
            }
        }
        else
        {
            for(int j=0; j<7; j++)
            {
                packet->objects[0].payload[j] = data[9 + 8*i + j];
                //printf("%02x ", data[8 + 8*i + j]);
            }
        }
    }
    //cout << endl;

    //cout << "Updated game control data. Time remaining: " << getTimeRemaining() << endl;
    //printf("  B: %02x  R: %02x\n", getGameControlData()->TeamBlueBitmask(), getGameControlData()->TeamRedBitmask());
}

int FiraSocketMonitor::getGameState()
{
    uint8_t gameState = getGameControlData()->State();

    switch(gameState)
    {
    case RefereeObject::STATE_PLAY:
        return STATE_PLAY;

    case RefereeObject::STATE_STOPPED:
        return STATE_INITIAL;

    case RefereeObject::STATE_SET_KICKOFF_BLUE:
    case RefereeObject::STATE_SET_KICKOFF_RED:
        return STATE_SET;

    case RefereeObject::STATE_PLAY_KICKOFF_BLUE:
        if(myTeam == SoccerPlayer::TEAM_BLUE)
            return STATE_PLAY;
        else
            return STATE_DEFEND;

    case RefereeObject::STATE_PLAY_KICKOFF_RED:
        if(myTeam == SoccerPlayer::TEAM_RED)
            return STATE_PLAY;
        else
            return STATE_DEFEND;

    default:
        cerr << "[warning] Unknown referee game state: " << gameState << endl;
        return STATE_PLAY;
    }
}

int FiraSocketMonitor::getTimeRemaining()
{
    return (int)getGameControlData()->Time();
}

int FiraSocketMonitor::getKickoffTeam()
{
    uint8_t gameState = getGameState();
    if(gameState == RefereeObject::STATE_SET_KICKOFF_RED || gameState == RefereeObject::STATE_PLAY_KICKOFF_RED)
        return SoccerPlayer::TEAM_RED;
    else
        return SoccerPlayer::TEAM_BLUE;
}

RefereeObject *FiraSocketMonitor::getGameControlData()
{
    return (RefereeObject*)(&gameControlData.objects[0]);
}
