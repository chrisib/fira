#include "FiraSoccerField.h"

#include <iostream>
#include <darwin/framework/Point.h>
#include <darwin/framework/Math.h>

using namespace std;
using namespace Robot;

FiraSoccerField::FiraSoccerField()
{
}

FiraSoccerField::~FiraSoccerField()
{
}

void FiraSoccerField::LoadIniSettings(minIni &ini, const char *section)
{
    shootingAreaLength = ini.geti(section,"ShootingAreaLength");
    shootingAreaWidth = ini.geti(section,"ShootingAreaWidth");
    shootingAreaArc = ini.geti(section,"ShootingAreaArc");

    SoccerField::LoadIniSettings(ini,section);
}

void FiraSoccerField::MakeFieldLines()
{
    SoccerField::MakeFieldLines();

    // our shooting area
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2-shootingAreaWidth/2,0),
                             Point2D(fieldWidth/2-shootingAreaWidth/2,shootingAreaLength)
    ));
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2+shootingAreaWidth/2,0),
                             Point2D(fieldWidth/2+shootingAreaWidth/2,shootingAreaLength)
    ));
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2-shootingAreaWidth/2,shootingAreaLength),
                             Point2D(fieldWidth/2+shootingAreaWidth/2,shootingAreaLength)
    ));

    // their shooting area
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2-shootingAreaWidth/2,fieldLength),
                             Point2D(fieldWidth/2-shootingAreaWidth/2,fieldLength-shootingAreaLength)
    ));
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2+shootingAreaWidth/2,fieldLength),
                             Point2D(fieldWidth/2+shootingAreaWidth/2,fieldLength-shootingAreaLength)
    ));
    fieldLines.push_back(Line2D(
                             Point2D(fieldWidth/2-shootingAreaWidth/2,fieldLength-shootingAreaLength),
                             Point2D(fieldWidth/2+shootingAreaWidth/2,fieldLength-shootingAreaLength)
    ));

    // the arc at the top of our shooting area
    Point2D ctr(fieldWidth/2,shootingAreaLength);
    Point2D a,b;
    double startAngle,endAngle;
    startAngle = 0;
    endAngle = 360.0/circleApprox;
    for(int i=0; i<circleApprox/2; i++)
    {
        a = Point2D(ctr.X + cos(deg2rad(startAngle))*shootingAreaArc,
                    ctr.Y + sin(deg2rad(startAngle))*shootingAreaArc);
        b = Point2D(ctr.X + cos(deg2rad(endAngle))*shootingAreaArc,
                    ctr.Y + sin(deg2rad(endAngle))*shootingAreaArc);

        fieldLines.push_back(Line2D(a,b));

        startAngle = endAngle;
        endAngle += 360.0/circleApprox;
    }

    // the arc at the top of their shooting area
    ctr.Y = fieldLength - shootingAreaLength;
    startAngle = 0;
    endAngle = -360.0/circleApprox;
    for(int i=0; i<circleApprox/2; i++)
    {
        a = Point2D(ctr.X + cos(deg2rad(startAngle))*shootingAreaArc,
                    ctr.Y + sin(deg2rad(startAngle))*shootingAreaArc);
        b = Point2D(ctr.X + cos(deg2rad(endAngle))*shootingAreaArc,
                    ctr.Y + sin(deg2rad(endAngle))*shootingAreaArc);

        fieldLines.push_back(Line2D(a,b));

        startAngle = endAngle;
        endAngle -= 360.0/circleApprox;
    }
}
