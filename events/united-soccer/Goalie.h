#ifndef GOALIE_H
#define GOALIE_H

#include <LinuxDARwIn.h>
#include <darwin/framework/SingleBlob.h>
#include "BallInfo.h"
#include "ThreadPauser.h"
#include "SocketMonitor.h"
#include "SoccerPlayer.h"
#include "goaliefinder.h"

using namespace Robot;
using namespace std;

//#define ROLL_OVER 11

#define READY Action::DEFAULT_MOTION_STAND_UP
#define SLOW_CROUCH 138
#define CROUCH_BLOCK 140
#define CROUCH_WAIT 143
#define CROUCH_DIVE_LEFT 145
#define CROUCH_DIVE_RIGHT 146
#define DIVE_LEFT 150
#define DIVE_RIGHT 160
#define DIVE_WAIT 170
#define SHUFFLE_RIGHT 80
#define SHUFFLE_LEFT  90

#define TOLERANCE 0.000001

typedef enum ARG_STATUS_T {
    ARGS_OK,
    ARGS_BAD
} ArgStatus;

typedef enum MODE_T {
    MODE_NORMAL,
    MODE_VIS_TEST
} ProgramMode;

typedef enum STATE_T {
    STATE_IDLE,
    STATE_ENTER_GOAL,
    STATE_REORIENTING,
    STATE_PASSIVE,
    STATE_ALERT,
    STATE_SHUFFLING,
    STATE_DIVING,
    STATE_CROUCH_DIVING,
    STATE_CROUCHING,
    STATE_LYING_DOWN
} ProgramState;

class Goalie : public SoccerPlayer {
	private:
		//"constants"
		static int FRAMES_BEFORE_DIVE;
		static int FRAMES_BEFORE_ACTIVE;
		static int FRAMES_BEFORE_PASSIVE;
		static int FRAMES_BEFORE_SHUFFLE;
		static int FRAMES_AFTER_SHUFFLE;
		static int FRAMES_BEFORE_TILT;
		static int FRAMES_BEFORE_DONE_REORIENTING;
		static int DIVE_STEPS;
		static double CROUCH_TOLERANCE;
		static double PASSIVE_RANGE;
		static double CROSS_TIME_THRESHOLD;
		static double WALKING_CROSS_TIME_THRESHOLD;
		static double SHUFFLE_TOLERANCE;
		static double PIVOT_X_OFFSET;
		static double PIVOT_DEGREES;
		static double FORWARD_STEP_LENGTH;
		static int POSITIONING_STEPS;
		static double GOAL_LINE_ANGLE_THRESHOLD;
		static double LOW_TILT_GOAL_LINE_CLOSE;
		static double LOW_TILT_GOAL_LINE_FAR;
		static double HIGH_TILT_GOAL_LINE_FAR;
		static double BACKWARDS_HIP_PITCH;

		//instance variables
        SingleBlob* target;
		GoalieFinder* finder;

		bool noDives;
		bool newState;
		bool oneFrameOffset;
		bool walkingStarted;
		int diveFrames;
		int activePassiveFrames;
		int framesBeforeShuffle;
		int framesAfterShuffle;
		int framesBeforeTilt;
		int framesBeforeDoneReorienting;
        int mode;
		pthread_t videoThreadId;
		pthread_t headThreadId;
		double range;
		double angle;
		bool lastDiveLeft;
		double expectedLateralDistance;
		double expectedCrossingTime;
		double goalLineAngle;
	public:
		~Goalie();
		Goalie(minIni *ini, SocketMonitor *sockMon, bool noDives);

		//initializes threads and then calls process in an infinite loop
		virtual int Execute();

		//contains the main goalie logic
		//should be called in an infinite loop
		virtual void Process();

		//performs goalie-specific state logic
		void processGoalieState();

		//calls SoccerPlayer::handleVideo(), could be expanded later
    	virtual void HandleVideo();

		//checks whether or not the goalie should dive
		//also sets the expectedLateralDistance and expectedCrossingTime members
		bool checkDiveCriteria();

        void scanAround();

		//function that the video thread lives in
		static void* videoLoop(void* self);

		//function that the head movement thread lives in
		static void* headLoop(void* self);

		//returns current state
		ProgramState getState();

		//changes state to STATE_REORIENTING if current state is STATE_ENTER_GOAL
		void doneWalking();
};

#endif
