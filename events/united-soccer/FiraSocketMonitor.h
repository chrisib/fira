#ifndef FIRASOCKETMONITOR_H
#define FIRASOCKETMONITOR_H

#include <SocketMonitor.h>
#include <FiraPackets.h>
#include <SoccerPlayer.h>

class FiraSocketMonitor : public SocketMonitor
{
public:
    FiraSocketMonitor(SoccerField &field);
    FiraSocketMonitor(int portNo,SoccerField &field);
    virtual ~FiraSocketMonitor();

    static const int TEAM_RED_START_ID = 0;
    static const int TEAM_RED_END_ID = 10;
    static const int TEAM_BLUE_START_ID = 16;
    static const int TEAM_BLUE_END_ID = 26;
    static const int REFEREE_ID = 32;

    static const double PACKET_ANGLE_PRECISION = 1000.0;

    // headers so we can identify packets
    static const uint16_t PACKET_HEADER = 0xADDE;

    // abstract-ish game states
    // same as RoboCup, used internally for compatibility
    enum {
        STATE_INITIAL   = 0,
        STATE_READY     = 1,
        STATE_SET       = 2,
        STATE_PLAY      = 3,
        STATE_FINISHED  = 4

    };

    // can a given robot play? (i.e. will the referee allow it?)
    virtual bool canRobotPlay(int team, int id);
    virtual bool isRobotOnPenalty(int team, int id);

    virtual int getGameState();
    virtual int getTimeRemaining();
    virtual int getKickoffTeam();

    virtual RefereeObject *getGameControlData();

    virtual void processUdpDatagram(char *data, int numBytes);
    virtual void updateControlData(char *packetData, int numBytes);

protected:
    int packetsSent;

    SoccerPacket gameControlData;                   // packet data from the referee
};

#endif // FIRASOCKETMONITOR_H
