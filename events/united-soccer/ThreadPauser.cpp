#include <time.h>
#include "ThreadPauser.h"
using namespace std;

//destructor
ThreadPauser::~ThreadPauser() {}

//constructor
ThreadPauser::ThreadPauser()
{
	pthread_mutex_init(&condMutex, NULL);
	pthread_cond_init(&condVar, NULL);
	headReacted = true;
	bodyReacted = true;
}

//accessors
bool ThreadPauser::getHeadReacted()
{
	return headReacted;
}

bool ThreadPauser::getBodyReacted()
{
	return bodyReacted;
}

//called when there is a new frame ready
void ThreadPauser::newFrame()
{
	headReacted = false;
	bodyReacted = false;
}

//called when the head has reacted to the current frame
void ThreadPauser::headHasReacted()
{
	headReacted = true;
}

//called when the rest of the body has reacted to the current frame
void ThreadPauser::bodyHasReacted()
{
	bodyReacted = true;
}

//wait for a new frame
void ThreadPauser::threadWait()
{
	pthread_mutex_lock(&condMutex);
	pthread_cond_wait(&condVar, &condMutex);
	pthread_mutex_unlock(&condMutex);
}

//wait for a new frame if the body has already reacted
void ThreadPauser::threadWaitBody()
{
    while(bodyReacted)
    {
		threadWait();
	}
}

//wait for a new frame if the head has already reacted
void ThreadPauser::threadWaitHead()
{
    while(headReacted)
    {
		threadWait();
	}
}

//called when a new frame is available - unblocks all waiting threads
void ThreadPauser::threadBroadcast()
{
	pthread_mutex_lock(&condMutex);
	pthread_cond_broadcast(&condVar);
	pthread_mutex_unlock(&condMutex);
}

//just calls pthread_yield()
void ThreadPauser::pauseThread()
{
	pthread_yield();
}

void ThreadPauser::waitAMoment()
{
	volatile int j;
	for(int i=0;i<20000000;i++){j++;}
}

#if 0
//sleep some number of miliseconds
void ThreadPauser::milisleep(int milisec)
{
	struct timespec req = {0};
	req.tv_sec = 0;
	req.tv_nsec = milisec * 1000000L;
	nanosleep(&req, (struct timespec *)NULL);
}
#endif
