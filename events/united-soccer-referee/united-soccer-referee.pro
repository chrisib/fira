#-------------------------------------------------
#
# Project created by QtCreator 2012-07-11T14:54:14
#
#-------------------------------------------------

QT       += core gui network

MOC_DIR = .moc/
OBJECTS_DIR = .build/

TARGET = united-soccer-referee
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
