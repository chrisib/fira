#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCheckBox>
#include <inttypes.h>
#include <QUdpSocket>
#include <QTimer>

#define PLAYERS_PER_TEAM 5

class QCheckBox;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;

    QCheckBox *redTeam[PLAYERS_PER_TEAM];
    QCheckBox *blueTeam[PLAYERS_PER_TEAM];
    QUdpSocket *socket;

    enum {
        GAME_STOPPED = 0,
        SET_KICKOFF_RED = 1,
        SET_KICKOFF_BLUE = 2,
        PLAY_KICKOFF_RED = 3,
        PLAY_KICKOFF_BLUE = 4,
        PLAY_ALL = 5
    };

    enum {
        UNDEFINED = 96,
        GAME_STATE = 97
    };

    static const char HEADER_0 = 0xAD;
    static const char HEADER_1 = 0xDE;
    static const char REFEREE_ID = 32;

    uint16_t timeRemaining;
    uint16_t sequenceNum;
    int gameState;

    QTimer gameTimer;

    uint16_t getRedBitmask();
    uint16_t getBlueBitmask();

    char calculateChecksum(char packet[], int packetLength);

    void broadcastMessage(char gameState, uint16_t redPlayers, uint16_t bluePlayers, uint16_t secondsRemaining);

public slots:
    void setKickoffRed();
    void setKickoffBlue();
    void playKickoffRed();
    void playKickoffBlue();
    void playAll();
    void stopAll();
    void updateTime();
    void setTime();
    void toggleTimer();
    void resetSequence();
};

#endif // MAINWINDOW_H
