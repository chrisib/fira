#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cstdio>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    redTeam[0] = ui->chk_red0;
    redTeam[1] = ui->chk_red1;
    redTeam[2] = ui->chk_red2;
    redTeam[3] = ui->chk_red3;
    redTeam[4] = ui->chk_red4;

    blueTeam[0] = ui->chk_blu0;
    blueTeam[1] = ui->chk_blu1;
    blueTeam[2] = ui->chk_blu2;
    blueTeam[3] = ui->chk_blu3;
    blueTeam[4] = ui->chk_blu4;

    sequenceNum = 0;

    socket = new QUdpSocket(this);
    timeRemaining = 120 * 25;   // 120 seconds & 25 ticks per second

    gameState = GAME_STOPPED;
\
    gameTimer.setInterval(40);
    gameTimer.connect(&gameTimer,SIGNAL(timeout()),this,SLOT(updateTime()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete socket;
}

void MainWindow::broadcastMessage(char gameState, uint16_t redPlayers, uint16_t bluePlayers, uint16_t secondsRemaining)
{
    char numObjects = 1;

    const uint16_t packetLength = 10 + 8;
    char packet[packetLength];

    packet[0] = (char)HEADER_0;
    packet[1] = (char)HEADER_1;
    packet[2] = (char)((packetLength >> 8) & 0xff);
    packet[3] = (char)(packetLength & 0xff);
    packet[4] = (char)REFEREE_ID;
    packet[5] = (char)((sequenceNum >> 8) & 0xff);
    packet[6] = (char)(sequenceNum & 0xff);
    packet[7] = (char)numObjects;

    // referee object
    packet[8] = (char)GAME_STATE;
    packet[9] = (char)((redPlayers >> 8) & 0xff);
    packet[10] = (char)(redPlayers & 0xff);
    packet[11] = (char)((bluePlayers >> 8) & 0xff);
    packet[12] = (char)bluePlayers & 0xff;
    packet[13] = (char)((secondsRemaining >> 8) & 0xff);
    packet[14] = (char)(secondsRemaining & 0xff);
    packet[15] = (char)gameState;


    packet[16] = (char)0x00;
    packet[17] = (char)calculateChecksum(packet, packetLength);

    printf("Transmitting packet:\n");
    for(int i=0; i<packetLength; i++)
        printf("%02x ", packet[i] & 0xff);
    printf("\n");
    fflush(stdout);

    QByteArray datagram(packet, packetLength);
    socket->writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, ui->sbPortNum->value());
    printf("Sent %d bytes over port %d (UDP Broadcast)\n\n", datagram.size(), ui->sbPortNum->value());
    sequenceNum++;
}

char MainWindow::calculateChecksum(char packet[], int packetLength)
{
    int sum = 0;
    for(int i=0; i < packetLength - 1; i++)
    {
        sum = (sum + packet[i]) & 0xff;
    }
    return (char)sum;
}

void MainWindow::setKickoffRed()
{
    gameState = SET_KICKOFF_RED;
    gameTimer.start();
}

void MainWindow::setKickoffBlue()
{
   gameState = SET_KICKOFF_BLUE;
   gameTimer.start();
}

void MainWindow::playKickoffRed()
{
    gameState = PLAY_KICKOFF_RED;
    gameTimer.start();
}

void MainWindow::playKickoffBlue()
{
    gameState = PLAY_KICKOFF_BLUE;
    gameTimer.start();
}

void MainWindow::playAll()
{
    gameState = PLAY_ALL;
    gameTimer.start();
}

void MainWindow::stopAll()
{
    gameState = GAME_STOPPED;
    gameTimer.start();
}

uint16_t MainWindow::getRedBitmask()
{
    uint16_t bitmask = 0x00;

    for(int i=0; i<PLAYERS_PER_TEAM; i++)
    {
        if(redTeam[i]->isChecked())
            bitmask = bitmask | (1 << i);
    }

    return bitmask;
}

uint16_t MainWindow::getBlueBitmask()
{
    uint16_t bitmask = 0x00;

    for(int i=0; i<PLAYERS_PER_TEAM; i++)
    {
        if(blueTeam[i]->isChecked())
            bitmask = bitmask | (1 << i);
    }

    return bitmask;
}

void MainWindow::updateTime()
{
    timeRemaining--;
    this->ui->sbTime->setValue(timeRemaining/25);

    if(timeRemaining==0)
        gameState = GAME_STOPPED;

    switch(gameState)
    {
    case SET_KICKOFF_RED:
        this->ui->lblState->setText("Set Red");
        break;
    case SET_KICKOFF_BLUE:
        this->ui->lblState->setText("Set Blue");
        break;
    case PLAY_KICKOFF_RED:
        this->ui->lblState->setText("Play Red");
        break;
    case PLAY_KICKOFF_BLUE:
        this->ui->lblState->setText("Play Blue");
        break;
    case PLAY_ALL:
        this->ui->lblState->setText("Play All");
        break;
    case GAME_STOPPED:
        this->ui->lblState->setText("Stopped");
        break;

    }

    broadcastMessage(gameState, getRedBitmask(), getBlueBitmask(), timeRemaining);
}

void MainWindow::setTime()
{
    timeRemaining=this->ui->sbTime->value() * 25;
}

void MainWindow::toggleTimer()
{
    if(gameTimer.isActive())
        gameTimer.stop();
    else
        gameTimer.start();
}

void MainWindow::resetSequence()
{
    this->sequenceNum = 0;
}
