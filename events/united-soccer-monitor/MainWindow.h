#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <SocketMonitor.h>
#include <VisionMap.h>
#include <SoccerField.h>
#include <pthread.h>
#include <QUdpSocket>
#include <fstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onPortChanged(int newPort);
    void onRobotIdChanged(int newId);
    void onTimeout();
    void onPacketReceived();
    void onEventChanged(int index);
    
private:
    Ui::MainWindow *ui;

    bool windowsShown;

    QTimer timer;
    SocketMonitor *socketMonitor;
    VisionMap map;
    SoccerField *field;
    QUdpSocket socket;

    pthread_mutex_t dataLock;

    std::ofstream refereeLog;
    std::ofstream playerLogs[3];

    void updateUi();
    void updateImages();
    void logDatagram(char *data, int numBytes);
    void logRefereeDatagram(char *data, int numBytes);
};

#endif // MAINWINDOW_H
