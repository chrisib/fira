#-------------------------------------------------
#
# Project created by QtCreator 2013-06-03T13:34:20
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

MOC_DIR = .moc/
OBJECTS_DIR = .build/

TARGET = PlayerMonitor
TEMPLATE = app

INCLUDEPATH += ../united-soccer

LIBS += \
        -ldl \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lespeak \
        -ljpeg \
        -lpthread \
        -ldarwin \

SOURCES += main.cpp\
        MainWindow.cpp \
    ../united-soccer/SocketMonitor.cpp \
    ../united-soccer/FiraSocketMonitor.cpp \
    ../united-soccer/RoboCupSocketMonitor.cpp \
    ../united-soccer/VisionMap.cpp \
    ../united-soccer/SoccerField.cpp \
    ../united-soccer/FiraSoccerField.cpp \
    ../united-soccer/RoboCupField.cpp \
    ../united-soccer/PlayerTarget.cpp \
    ../united-soccer/GoalTarget.cpp \
    ../united-soccer/util.cpp \

HEADERS  += MainWindow.h \
    ../united-soccer/SocketMonitor.h \
    ../united-soccer/FiraSocketMonitor.h \
    ../united-soccer/RoboCupSocketMonitor.h \
    ../united-soccer/VisionMap.h \
    ../united-soccer/SoccerField.h \
    ../united-soccer/FiraSoccerField.h \
    ../united-soccer/RoboCupField.h \
    ../united-soccer/PlayerTarget.h \
    ../united-soccer/GoalTarget.h \
    ../united-soccer/util.h \

FORMS    += MainWindow.ui
