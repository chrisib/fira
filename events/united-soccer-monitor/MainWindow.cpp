#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <SoccerPlayer.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <darwin/framework/minIni.h>
#include <pthread.h>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <RoboCupField.h>
#include <RoboCupSocketMonitor.h>
#include <FiraSoccerField.h>
#include <FiraSocketMonitor.h>

using namespace cv;
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    pthread_mutex_init(&dataLock,0);

    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));

    if(!socket.bind(QHostAddress::Any,3838, QUdpSocket::ShareAddress))
        cout << "[error] Failed to bind to socket" << endl;
    connect(&socket,SIGNAL(readyRead()),this,SLOT(onPacketReceived()));

    //socketMonitor = new SocketMonitor(ui->sb_portNo->value());
    field = new RoboCupField();
    socketMonitor = new RoboCupSocketMonitor(*field);


    minIni ini("config.ini");
    field->Initialize(&map, ini);

    for(int i=0; i<3; i++)
    {
        char buffer[255];
        sprintf(buffer, "Robocup_Player_%02d.log", i+1);
        playerLogs[i].open(buffer);
    }
    refereeLog.open("Robocup_Referee.log");

    // set a timeout every 500ms to to refresh the graphics
    timer.setInterval(500);
    timer.setSingleShot(false);
    timer.start();
}

MainWindow::~MainWindow()
{
    for(int i=0; i<3; i++)
    {
        playerLogs[i].close();
    }
    refereeLog.close();

    delete ui;
    delete socketMonitor;
}

void MainWindow::onPortChanged(int newPort)
{
    socket.close();
    socket.bind(newPort, QUdpSocket::ShareAddress);
    //delete socketMonitor;
    //socketMonitor = new SocketMonitor(newPort);
}

void MainWindow::onRobotIdChanged(int newId)
{
    (void) newId;
}

void MainWindow::onEventChanged(int index)
{
    SoccerField *oldField = field;
    SocketMonitor *oldMonitor = socketMonitor;

    if(index == 0)
    {
        field = new RoboCupField();
        socketMonitor = new RoboCupSocketMonitor(*field);

        delete oldField;
        delete oldMonitor;
    }
    else if(index == 1)
    {
        field = new FiraSoccerField();
        socketMonitor = new FiraSocketMonitor(*field);

        delete oldField;
        delete oldMonitor;
    }

}

void MainWindow::onTimeout()
{
    //pthread_mutex_lock(&dataLock);
    updateImages();
    //pthread_mutex_unlock(&dataLock);
}

void MainWindow::onPacketReceived()
{
    //pthread_mutex_lock(&dataLock);
    while (socket.hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(socket.pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        socket.readDatagram(datagram.data(), datagram.size(),
                            &sender, &senderPort);

        //for(int i=0; i<datagram.size(); i++)
        //{
        //    cout << hex << ((int)datagram.data()[i] & 0xff) << " ";
        //}
        //cout << endl;

        socketMonitor->processUdpDatagram(datagram.data(), datagram.size());

        if((uint8_t)datagram.data()[0] == (uint8_t)(FIRA_PACKET_HEADER >> 8) && (uint8_t)datagram.data()[1] == (uint8_t)(FIRA_PACKET_HEADER * 0xff))
        {
            logDatagram(datagram.data(), datagram.size());
        }
        else if(datagram.data()[0] == RoboCupSocketMonitor::REFEREE_HEADER[0] && datagram.data()[1] == RoboCupSocketMonitor::REFEREE_HEADER[1] &&
                datagram.data()[2] == RoboCupSocketMonitor::REFEREE_HEADER[2] && datagram.data()[3] == RoboCupSocketMonitor::REFEREE_HEADER[3] )
        {
            logRefereeDatagram(datagram.data(), datagram.size());
        }
        updateUi();
    }
    //pthread_mutex_unlock(&dataLock);
}

void MainWindow::logDatagram(char *data, int numBytes)
{
    int id = data[6] - 1;
    //cout << "received packet from ID " << id << " length " << numBytes << endl;
    //
    //for(int i=0; i<numBytes; i++)
    //{
    //    cout << hex << (data[i] & 0xff) << " ";
    //}
    //cout << endl;

    for(int i=0; i<numBytes; i++)
    {
        playerLogs[id] << hex << ((int)data[i] & 0xff) << " ";
    }
    playerLogs[id] << endl;
}

void MainWindow::logRefereeDatagram(char *data, int numBytes)
{
    for(int i=0; i<numBytes; i++)
    {
        refereeLog << hex << ((int)data[i] & 0xff) << " ";
    }
    refereeLog << endl;
}

void MainWindow::updateUi()
{
    // update the UI with the latest packet information
    ui->lcd_time->display(socketMonitor->getTimeRemaining());
    switch(socketMonitor->getGameState())
    {
    case SocketMonitor::STATE_INITIAL:
        ui->lbl_refState->setText("Initial");
        break;
    case SocketMonitor::STATE_READY:
        ui->lbl_refState->setText("Ready");
        break;
    case SocketMonitor::STATE_SET:
        ui->lbl_refState->setText("Set");
        break;
    case SocketMonitor::STATE_PLAY:
        ui->lbl_refState->setText("Play");
        break;
    case SocketMonitor::STATE_FINISHED:
        ui->lbl_refState->setText("Finished");
        break;
    default:
        ui->lbl_refState->setText("Unknown");
        break;
    }

    switch(socketMonitor->getKickoffTeam())
    {
    case SoccerPlayer::TEAM_BLUE:
        ui->lbl_kickoffTeam->setText("Blue");
        break;

    case SoccerPlayer::TEAM_RED:
        ui->lbl_kickoffTeam->setText("Red");
        break;

    default:
        ui->lbl_kickoffTeam->setText("Unknown");
        break;
    }
    SocketMonitor::RobotStateInformation &robotState = socketMonitor->getTeamData(ui->sb_listenTo->value());

    switch(robotState.state)
    {
    case SoccerPlayer::STATE_READY:
        ui->lbl_playerState->setText("Ready");
        break;
    case SoccerPlayer::STATE_WALKING:
        ui->lbl_playerState->setText("Walking");
        break;
    case SoccerPlayer::STATE_WAITING:
        ui->lbl_playerState->setText("Waiting");
        break;
    case SoccerPlayer::STATE_PLAYING:
        ui->lbl_playerState->setText("Playing");
        break;
    case SoccerPlayer::STATE_HW_INTERRUPT:
        ui->lbl_playerState->setText("HW Int.");
        break;
    default:
        ui->lbl_playerState->setText("Unknown");
        break;
    }

    switch(robotState.substate)
    {
    case SoccerPlayer::SUBSTATE_CHASING_BALL:
        ui->lbl_playerSubstate->setText("Chasing");
        break;
    case SoccerPlayer::SUBSTATE_FINAL_APPROACH:
        ui->lbl_playerSubstate->setText("Approach");
        break;
    case SoccerPlayer::SUBSTATE_KICKING:
        ui->lbl_playerSubstate->setText("Kicking");
        break;
    case SoccerPlayer::SUBSTATE_FINDING_BALL:
        ui->lbl_playerSubstate->setText("Finding Ball");
        break;
    default:
        ui->lbl_playerSubstate->setText("Unknown");
        break;
    }

    int sequence = 0x0000 | (robotState.sequence);
    ui->lcd_sequence->display(sequence);


    // FIELD OBJECTS
    field->ballPosition.X = robotState.ballPosition.X;
    field->ballPosition.Y = robotState.ballPosition.Y;
    field->ballConfidence = robotState.ballConfidence;
    //cout << field->ballPosition.X << " " << field->ballPosition.Y << endl;

    for(unsigned int i=0; i<map.myTeam.size(); i++)
    {
        field->myTeam[i].X = robotState.myTeam[i].X;
        field->myTeam[i].Y = robotState.myTeam[i].Y;
    }

    for(unsigned int i=0; i<map.theirTeam.size(); i++)
    {
        field->theirTeam[i].X = robotState.theirTeam[i].X;
        field->theirTeam[i].Y = robotState.theirTeam[i].Y;
    }

    field->presentPosition = robotState.presentPosition;
    field->positionConfidence = robotState.positionConfidence;


    // POLAR MAP OBJECTS

    map.goal.start.X = robotState.observedGoal.start.X;
    map.goal.start.Y = robotState.observedGoal.start.Y;
    map.goal.end.X = robotState.observedGoal.end.X;
    map.goal.end.Y = robotState.observedGoal.end.Y;

    map.lines.clear();
    for(unsigned int i=0; i<robotState.observedLines.size(); i++)
    {
        map.lines.push_back(robotState.observedLines[i]);
    }

    map.ballPosition = robotState.observedBall;
    map.goal = robotState.observedGoal;


}

void MainWindow::updateImages()
{
    if(ui->chk_showImages->isChecked())
    {
        if(!windowsShown)
        {
            namedWindow("Polar Map");
            namedWindow("Field");
            windowsShown = true;
        }

        map.Draw();
        field->Draw(true);

        imshow("Polar Map", map.mapImage);
        imshow("Field",field->fieldImage);
        cvWaitKey(1);
    }
}
