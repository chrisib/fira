#ifndef UNITEDSOCCEROBJECT_H
#define UNITEDSOCCEROBJECT_H

#include <QtCore/QCoreApplication>

class UnitedSoccerObject
{
public:
    explicit UnitedSoccerObject(int type) {this->type = type;}

    int type;

    static const int MIN_PACKET_LENGTH = 10;
    static const int MAX_NUM_OBJECTS = 2048;
    static const int BYTES_PER_OBJECT = 8;
    static const int PACKET_HEADER_0 = 0xAD;
    static const int PACKET_HEADER_1 = 0xDE;

    enum OBJECT_TYPE {
        STATE_TYPE = 6,
        PERCEPTION_TYPE = 40,
        INTENT_TYPE = 67,
        REFEREE_TYPE = 97,
        EXTENSION_TYPE = 255
    };
};

class OrientationObject : public UnitedSoccerObject
{
public:
    int locationX;
    int locationY;
    int orientation;

    OrientationObject(int type, int locationX, int locationY, int orientation) :
        UnitedSoccerObject(type)
    {
        this->locationX = locationX;
        this->locationY = locationY;
        this->orientation = orientation;
    }

    virtual double orientationDegrees(){return (double)orientation / 10000 * 180/3.14159;}
};

class StateObject : public OrientationObject
{
public:
    enum TYPE
    {
        UNDEFINED = 0,
        OWN_POSITION,
        BALL,
        TEAM_ROBOT,
        OPPONENT_ROBOT,
        TEAM_GOAL,
        OPPONENT_GOAL
    };


    StateObject(int type, int locationX, int locationY, int orientation, int confidence) :
        OrientationObject(type, locationX, locationY, orientation)
    {
        this->confidence = confidence;
    }

    int confidence;
};

class PerceptionObject : public OrientationObject
{
public:
    enum TYPE
    {
        UNDEFINED = 32,
        OWN_POSITION,
        BALL,
        TEAM_ROBOT,
        OPPONENT_ROBOT,
        TEAM_GOAL,
        OPPONENT_GOAL,
        FIELD_LINE,
        FIELD_CENTER
    };

    PerceptionObject(int type, int locationX, int locationY, int orientation, int confidence) :
        OrientationObject(type, locationX, locationY, orientation)
    {
        this->confidence = confidence;
    }

    int confidence;
};

class IntentObject : public OrientationObject
{
public:
    enum TYPE
    {
        UNDEFINED = 64,
        SHOT_ON_GOAL,
        PLAY_GOAL_KEEPER,
        MOVEMENT
    };

    IntentObject(int type, int locationX, int locationY, int orientation, int offence) :
        OrientationObject(type, locationX, locationY, orientation)
    {
        this->offence = offence;
    }

    int offence;
};

class RefereeObject : public UnitedSoccerObject
{
public:
    enum TYPE
    {
        UNDEFINED = 96,
        GAME_STATE = 97
    };

    enum GAME_STATE
    {
        GAME_STOPPED = 0,
        SET_KICKOFF_RED,
        SET_KICKOFF_BLUE,
        PLAY_KICKOFF_RED,
        PLAY_KICKOFF_BLUE,
        BALL_IN_PLAY
    };

    RefereeObject(int type, int teamRed, int teamBlue, int time, int state) :
        UnitedSoccerObject(type)
    {
        this->teamRed = teamRed;
        this->teamBlue = teamBlue;
        this->time = time;
        this->state = state;
    }

    int teamRed;
    int teamBlue;
    int time;
    int state;
};

class ExtensionObject : public UnitedSoccerObject
{
public:
    ExtensionObject(int type, int userDef1, int userDef2, int userDef3, int userDef4) :
        UnitedSoccerObject(type)
    {
        this->userDef1 = userDef1;
        this->userDef2 = userDef2;
        this->userDef3 = userDef3;
        this->userDef4 = userDef4;
    }

    int userDef1;
    int userDef2;
    int userDef3;
    int userDef4;
};

#endif // UNITEDSOCCEROBJECT_H
