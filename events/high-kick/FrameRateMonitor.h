#ifndef FRAMERATEMONITOR_H
#define FRAMERATEMONITOR_H

#include <LinuxDARwIn.h>
using namespace std;

typedef struct timespec tspec;

class FrameRateMonitor {
	private:
		double FRAME_RATE_GUESS;
		int FRAME_TIMES_KEPT;
		double frameRate;
		double mostRecentFrameRate;
		tspec** lastFrameTimes;
		
		// Disable copying
		FrameRateMonitor(FrameRateMonitor&);
		FrameRateMonitor& operator=(FrameRateMonitor&);
	public:
		~FrameRateMonitor();
		FrameRateMonitor();
		double getFrameRate();
		double getMostRecentFrameRate();
		void updateFrameRate();
};

#endif
