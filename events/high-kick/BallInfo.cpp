#include <string>
#include <sstream>
#include <math.h>
#include <darwin/framework/Math.h>
#include "BallInfo.h"
using namespace std;

//destructor
BallInfo::~BallInfo() {}

//constructor
BallInfo::BallInfo() {
	xSpeed = 0;
	ySpeed = 0;
	reset();
}

//accessors
double BallInfo::getX() {
	return x;
}

double BallInfo::getY() {
	return y;
}

double BallInfo::getXSpeed() {
	return xSpeed;
}

double BallInfo::getYSpeed() {
	return ySpeed;
}

double BallInfo::getR() {
	return r;
}

double BallInfo::getTheta() {
	return theta;
}

double BallInfo::getRSpeed() {
	return rSpeed;
}

double BallInfo::getThetaSpeed() {
	return thetaSpeed;
}

//setters
void BallInfo::setX(double newX) {
	x = newX;
}

void BallInfo::setY(double newY) {
	y = newY;
}

void BallInfo::setXSpeed(double newXSpeed) {
	xSpeed = newXSpeed;
}

void BallInfo::setYSpeed(double newYSpeed) {
	ySpeed = newYSpeed;
}

void BallInfo::setR(double newR) {
	r = newR;
}

void BallInfo::setTheta(double newTheta) {
	theta = newTheta;
}

void BallInfo::setRSpeed(double newRSpeed) {
	rSpeed = newRSpeed;
}

void BallInfo::setThetaSpeed(double newThetaSpeed) {
	thetaSpeed = newThetaSpeed;
}

//Resets values to default
void BallInfo::reset() {
	x = -1;
	y = -1;
	r = -1;
	theta = 0;
	rSpeed = 0;
	thetaSpeed = 0;
}

void BallInfo::update(double newX, double newY, double newR, double newTheta, double frameRate) {
	if(x > 0) {
		xSpeed = (newX-x)*frameRate;
		ySpeed = (newY-y)*frameRate;
		rSpeed = (newR-r)*frameRate;
		thetaSpeed = (newTheta-theta)*frameRate;
	} else {
		xSpeed = 0;
		ySpeed = 0;
		rSpeed = 0;
		thetaSpeed = 0;
	}
	x = newX;
	y = newY;
	r = newR;
	theta = newTheta;
}

//for all calculations, suppose the robot is standing at the origin of a Cartesian plane, looking down the positive y-axis
//also, the x-axis runs from right-to-left to be consistent with the camera

//calculates the speed of the ball in the y-direction
double BallInfo::calculateForwardSpeed() {
	return rSpeed*cosf(deg2rad(theta)) - r*deg2rad(thetaSpeed)*sinf(deg2rad(theta));
}

//calculates the speed of the ball in the x-direction
double BallInfo::calculateLateralSpeed() {
	return rSpeed*sinf(deg2rad(theta)) + r*deg2rad(thetaSpeed)*cosf(deg2rad(theta));
}

//calculates the y-component of the ball's position
double BallInfo::calculateForwardPosition() {
	return r*cosf(deg2rad(theta));
}

//calculates the x-component of the ball's position
double BallInfo::calculateLateralPosition() {
	return r*sinf(deg2rad(theta));
}

//calculates the expected x-component of the ball when it crosses the x-axis
double BallInfo::calculateLateralCrossingPosition() {
	return calculateLateralPosition()-calculateLateralSpeed()*calculateForwardPosition()/calculateForwardSpeed();
}

//calculates how long it will be before the ball crosses the x-axis
double BallInfo::calculateCrossingTime() {
	double ans = 999999999;
	double forwardSpeed = calculateForwardSpeed();
	if(forwardSpeed < 0)
		ans = calculateForwardPosition()/(-forwardSpeed);
	return ans;
}

//Produces a string representing the current state of BallInfo
string BallInfo::toString() {
	ostringstream s;
	s << "x:" << x << ", y:" << y << ", xSpeed:" << xSpeed << ", ySpeed:" << ySpeed << ", r:" << r << ", theta:" << theta << ", rSpeed:" << rSpeed << ", thetaSpeed:" << thetaSpeed;
	return s.str();
}
