#include "SoccerPlayer.h"
#include <pthread.h>
#include <LinuxDARwIn.h>
#include <iostream>
#include <darwin/framework/Target.h>
#include <darwin/linux/CvCamera.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/Voice.h>
#include <ctime>
#include <PacketObject.h>
#include <PlayerPacket.h>
#include <cstring>
#include <cstdio>
#include <darwin/framework/Kinematics.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "SocketMonitor.h"
#include "Goalie.h"

using namespace std;
using namespace Robot;
using namespace cv;

SoccerPlayer *SoccerPlayer::Create(int argc, char *argv[])
{
    bool isGoalie = false;
    bool showVideo = false;
    bool visionTest = false;
    bool recordVideo = false;
    bool noNetwork = true;      // ignore network for high-kick
    bool networkTest = false;
    bool noDives = false;
    bool executePenalty = true; // high kick is always a penalty-style event
    bool noKick = false;
    int subsample = 2;
    const char* configPath = "config.ini";

    for(int i=1; i<argc; i++)
    {
        if(!strcmp(argv[i], "--goalie"))
            isGoalie = true;
        else if(!strcmp(argv[i], "--penalty"))
            executePenalty = true;
        else if(!strcmp(argv[i], "--show-video"))
            showVideo = true;
        else if(!strcmp(argv[i], "--vision-test"))
            visionTest = true;
        else if(!strcmp(argv[i], "--record-video"))
            recordVideo = true;
        else if(!strcmp(argv[i],"--no-network"))
            noNetwork = true;
        else if(!strcmp(argv[i], "--network-test"))
            networkTest = true;
        else if(!strcmp(argv[i], "--no-dives"))
            noDives = true;
        else if(!strcmp(argv[i],"--no-kick"))
            noKick = true;
        else if(!strcmp(argv[i], "--subsample"))
        {
            i++;
            if(i >= argc)
            {
                cerr << "ERROR: no subsample provided" << endl;
                exit(1);
            }

            subsample = atoi(argv[i]);

            if(subsample < 1)
            {
                cerr << "ERROR: subsample cannot be " << subsample << endl;
                exit(1);
            }
        }
        else if(!strcmp(argv[i],"-c") || !strcmp(argv[i],"--config"))
        {
            i++;
            if(argc >= i)
            {
                cerr << "ERROR: no config file provided" << endl;
                exit(1);
            }

            configPath = argv[i];
        }
        else
        {
            cerr << endl <<
                    "Usage:" << endl <<
                    "\thigh-kick [--goalie] [--penalty] [--show-video] [--record-video] [--no-network] [--subsample INT] [--vision-test] [-c|--config FILE] [--help]" << endl << endl <<
                    "Argument summaries:" << endl <<
                    "\t--goalie       this player is playing as the goalie" << endl <<
                    "\t--penalty      execute penalty kick behaviour instead of regular play" << endl <<
                    "\t--show-video   display vision in an X-window" << endl <<
                    "\t--record-video record the video streams to .avi files for debugging" << endl <<
                    "\t--no-network   do not listen for the referee on the network" << endl <<
                    "\t--subsample    specify the scaline subsample (default 2)" << endl <<
                    "\t--vision-test  do not power the motors; simply process the vision" << endl <<
                    "\t-c|--config    set the path to the configuration file (default config.ini)" << endl <<
                    "\t--help         show this message" << endl;
            exit(0);

        }
    }

    // create the ini loader
    cout << "Opening configuration file " << configPath << endl;
    minIni *ini = new minIni(configPath);

    // create the socket monitor
    cout << "Configuring Networking" << endl;
    SocketMonitor *sockMon = new SocketMonitor(ini->geti("Network","Port", SocketMonitor::DEFAULT_PORT));
    cout << "Done. Listening on port " << ini->geti("Network","Port") << endl;

    SoccerPlayer *player;

    if(isGoalie)
    {
        cout << "Creating Goalie" << endl;
        player = new Goalie(ini, sockMon, noDives);
        player->myPosition = SoccerPlayer::POSITION_GOALIE;
    }
    else
    {
        cout << "Creating Standard Player" << endl;
        player = new SoccerPlayer(ini, sockMon);
        player->myPosition = SoccerPlayer::POSITION_FIELD;
    }

    player->showVideo = showVideo || visionTest;
    player->visionTest = visionTest;
    player->noNetwork = noNetwork && !networkTest;
    player->networkTest = networkTest;
    player->recordVideo = recordVideo;
    player->subsample = subsample;
    player->isGoalie = isGoalie;
    player->penaltyKick = executePenalty;
    player->lastGameState = -1;
    player->myLastState = -1;
    player->lastPlaySubstate = -1;
    player->noKick = noKick;
    player->initialize();
    cout << "Done Creating Player" << endl;

    return player;
}

SoccerPlayer::SoccerPlayer(minIni *ini, SocketMonitor *sockMon)
{
    this->ini = ini;
    this->frMonitor = new FrameRateMonitor();
    this->lastBallInfo = new BallInfo();
    showVideo = true;
    recordVideo = false;
    visionTest = false;
    subsample = 2;
    noBallFrames = 0;

    headPan = 0;
    headTilt = 0;
    scanDirection = 0;
    firstSweep = true;
    headMoving = true;
    scanFrames = 0;
    lostFrames = 0;

    dbgImage = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);
    rgbImage = Mat(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);

    socketMonitor = sockMon;

    lastUpdateSentAt = 0;
    numPacketsSent = 0;
    refSaysWeCanPlay = false;
    onPenalty = false;

    for(int i=0; i<SocketMonitor::MAX_NUM_PLAYERS; i++)
        lastMessageAt[i] = -1;

    // initialize the camera
    CvCamera::GetInstance()->Initialize(ini->geti("Camera","Device",0));
    CvCamera::GetInstance()->LoadINISettings(ini);
}

SoccerPlayer::~SoccerPlayer()
{
    delete ini;
    delete socketMonitor;
    delete[] frMonitor;
    delete[] lastBallInfo;
}

void SoccerPlayer::initialize()
{
    field.Initialize(&(this->map), *ini);

    if(showVideo)
    {
        namedWindow("Field", CV_WINDOW_AUTOSIZE);
        namedWindow("Webcam", CV_WINDOW_AUTOSIZE);
        namedWindow("Scanline", CV_WINDOW_AUTOSIZE);
        namedWindow("Map", CV_WINDOW_AUTOSIZE);

        cvMoveWindow("Webcam",0,0);
        cvMoveWindow("Scanline",0,280);
        cvMoveWindow("Map",320,0);
        cvMoveWindow("Field",320 + map.mapImage.cols + 12, 0);

        imshow("Field",field.fieldImage);
        imshow("Map",map.mapImage);
        imshow("Webcam", rgbImage);
        imshow("Scanline", dbgImage);

        cvWaitKey(1);
    }

    if(!visionTest && !networkTest)
    {
        LinuxDARwIn::Initialize(ini->gets("Files","MotionFile").c_str(), Action::DEFAULT_MOTION_SIT_DOWN);
        //LinuxDARwIn::Initialize();

        // disable modules before adding them
        Walking::GetInstance()->m_Joint.SetEnableBody(false);
        Head::GetInstance()->m_Joint.SetEnableBody(false);
        LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
        RightArm::GetInstance()->m_Joint.SetEnableBody(false);

        // insert the new modules
        MotionManager::GetInstance()->AddModule(Walking::GetInstance());
        MotionManager::GetInstance()->AddModule(Head::GetInstance());
        MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
        MotionManager::GetInstance()->AddModule(RightArm::GetInstance());

        Walking::GetInstance()->LoadINISettings(ini);

        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

        // relax the hands
        LeftArm::GetInstance()->RelaxHand();
        RightArm::GetInstance()->RelaxHand();
        sitDown();

        // remove the Action module
        //MotionManager::GetInstance()->RemoveModule(Action::GetInstance());
    }

    //targets.push_back(&myGoal);
    //targets.push_back(&theirGoal);
    //targets.push_back(&robotFeet);
    //targets.push_back(&myTeamPlayers);
    //targets.push_back(&theirTeamPlayers);
    targets.push_back(&ball);

    loadIniSettings();

    if(myPosition == POSITION_GOALIE)
    {
        takesKickoff = false;
        homePosition.X = field.myGoalCenter.X;
        homePosition.Y = field.myGoalCenter.Y + 30;
        homePosition.Z = 0;

        kickoffPosition.X = field.myGoalCenter.X;
        kickoffPosition.Y = field.myGoalCenter.Y + 30;
        kickoffPosition.Z = 0;
    }
    else
    {
        if(takesKickoff)
        {
            // the robot that takes the kickoff plays further up the field
            homePosition.X = field.myPenaltyMark.X;
            homePosition.Y = field.myPenaltyMark.Y;
            homePosition.Z = 0;

            kickoffPosition.X = field.myPenaltyMark.X; //field.kickoffPosition.X;
            kickoffPosition.Y = field.myPenaltyMark.Y; //field.kickoffPosition.Y;
            kickoffPosition.Z = 0;
        }
        else
        {
            // the robot that does not take penalty kicks is further back and to the left
            homePosition.X = field.myPenaltyMark.X/2.0;
            homePosition.Y = (field.myPenaltyMark.Y + field.GetCreaseDepthCM())/2.0;
            homePosition.Z = 0;

            kickoffPosition.X = field.myPenaltyMark.X / 2.0;
            kickoffPosition.Y = field.myPenaltyMark.Y;
            kickoffPosition.Z = 0;
        }
    }
    field.SetPosition(homePosition);

    // center the head & look upwards (so we can see the field)
    Head::GetInstance() -> MoveByAngle(0, 30);
    Voice::Initialize("default");

    myState = STATE_WAITING;

    Voice::Speak("Initialization complete");
}

void SoccerPlayer::loadIniSettings()
{
    if(recordVideo)
    {
        cout << "Video streams will be recorded to " <<
                ini->gets("Files","WebcamStream") << ", " <<
                ini->gets("Files","ScanlineStream") << " and " <<
                ini->gets("Files","MapStream") << endl;

        webcamStream = VideoWriter (ini->gets("Files","WebcamStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        debugStream = VideoWriter (ini->gets("Files","ScanlineStream"), CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(Camera::WIDTH,Camera::HEIGHT),true);
        mapStream = VideoWriter(ini->gets("Files","MapStream"), CV_FOURCC('D','I','V','X'), 30, cvSize(SoccerMap::IMAGE_SIZE, SoccerMap::IMAGE_SIZE),true);
    }

    Walking::GetInstance()->LoadINISettings(ini);
    maxNoBallFrames = ini->geti("Soccer","MaxNoBallFrames");
    myId = ini->geti("Soccer","ID");
    maxKickRange = ini->getd("Soccer","MaxKickRange");
    myColour = (strcmp(ini->gets("Soccer","Colour").c_str(),"red") == 0) ? TEAM_RED : TEAM_BLUE;
    theirColour = myColour == TEAM_RED ? TEAM_BLUE : TEAM_RED;
    takesKickoff = ini->geti("Soccer","TakesKickoff");

    myGoalWhiteBackgroundInFirstHalf = ini->geti("Soccer","OurGoalWhiteInFirstHalf");

    numKicksBeforeHighKick = ini->geti("High Kick","NumKicks",3);

    int myEyeColour = ini->geti("Soccer", "EyeColour"); // 0 for off, 1 for team colour, -1 for default eye colour
    if( myEyeColour == 0 && !visionTest && !networkTest) {
        MotionManager::GetInstance()->SetEyeColour(0, 0, 0);
        MotionManager::GetInstance()->SetForeheadColour(0, 0, 0);
    }

    int tmp = ini->geti("Soccer","PenaltyEntranceX");
    if(tmp < 0)
        penaltyEntrance.X = tmp;
    else
        penaltyEntrance.X = field.GetFieldWidthCM() + tmp;

    penaltyEntrance.Y = ini->getd("Soccer","PenaltyEntranceY");

    // the robot starts facing perpendicular to whatever field line it enters from
    if(penaltyEntrance.X < 0)
        penaltyEntrance.Z = -90;
    else if(penaltyEntrance.X > field.GetFieldWidthCM())
        penaltyEntrance.Z = 90;
    else if(penaltyEntrance.Y < 0)
        penaltyEntrance.Z = 0;
    else
        penaltyEntrance.Z = 180;

    if(myColour == TEAM_RED)
    {
        myTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","RedTeam","Red"));
        theirTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","BlueTeam","Blue"));

        if( myEyeColour == 1 && !visionTest && !networkTest ) {
            MotionManager::GetInstance()->SetEyeColour(255,0,255);
            MotionManager::GetInstance()->SetForeheadColour(255,0,255);
        }
    }
    else
    {
        theirTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","RedTeam","Red"));
        myTeamPlayers.LoadIniSettings(*ini,ini->gets("Players","BlueTeam","Blue"));

        if( myEyeColour == 1 && !visionTest && !networkTest ) {
            MotionManager::GetInstance()->SetEyeColour(0,255,255);
            MotionManager::GetInstance()->SetForeheadColour(0,255,255);
        }
    }

    ball.LoadIniSettings(*ini, ini->gets("Soccer","BallColour").c_str());
    goal.LoadIniSettings(*ini,"Goal");

    map.LoadIniSettings(*ini,"Polar Map");

    cout << "On team " << (myColour==TEAM_RED ? "red" : (myColour==TEAM_BLUE ? "blue" : "unknown")) << " player ID " << myId << endl;
}

int SoccerPlayer::exec()
{
    cout << "Starting video thread..." << endl;
    pthread_create(&videoThreadID, NULL, videoThread, this);
    cout << "Starting button thread..." << endl;
    pthread_create(&buttonThreadID, NULL, buttonThread, this);

    Voice::Speak("Press the middle button");
    cout << "Press the middle button" << endl;

    myState = STATE_HW_INTERRUPT;

    for(;;)
        process();

    return 0;
}

void SoccerPlayer::process()
{
#ifdef DEBUG
    cout << "My angle is: " << field.presentPosition.Z << "; Angle to my goal: " << map.GetAngleToMyGoal() << "; Angle to their goal: " << map.GetAngleToTheirGoal() << endl;
    cout << "My field position is: " << field.presentPosition.X << " " << field.presentPosition.Y << endl;
#endif

    /*Head::GetInstance() -> MoveByAngle(0, -15);
    standUp();
    while(true)
    {
        pauser.threadWaitHead();
        cout << map.ballPosition.X << " " << map.ballPosition.Y << endl;
        pauser.headHasReacted();
    }*/

    //bool prevCanPlay = canPlay();
    bool prevOnPenalty = onPenalty;
    bool newState;
    checkRefStatus();

    // transmit status packets at regular intervals (if we're using the network)
    transmitStatus();

    handleButton();

    if(!handleTests() && refSaysWeCanPlay)
    {
        handleFall();

        adjustHipPitch();

        // actual game logic
        findGameState();

        switch(gameState)
        {
        case SocketMonitor::STATE_READY:
            if(newGameState)
            {
                if(myState != STATE_WALKING)
                {
                    standUp();
                }
                myState = STATE_WALKING;

                if(takesKickoff && socketMonitor->getKickoffTeam() == myColour)
                {
                    walkTo(kickoffPosition);
                }
                else
                {
                    walkTo(homePosition);
                }

            }

            break;

        case SocketMonitor::STATE_SET:
            myState = STATE_READY;

            // STOP MOVING!!!
            if(Walking::GetInstance()->IsRunning())
            {
                Walking::GetInstance()->Stop();
                standUp();

                // CIB
                // set the home position since we're manually positioning the robots
                // this ensures that the robot's orientation is mostly-correct at the beginning of the game
                field.SetPosition(homePosition);
            }
            break;


        case SocketMonitor::STATE_PLAY:
            if(prevOnPenalty && !onPenalty && !noNetwork)   // only worry about this if we actually are listening to the referee
            {
                // just came off penalty; walk  back to our starting position
                cout << "[info] Re-entering field from penalty" << endl;
                field.SetPosition(penaltyEntrance);

                // this doesn't work, for whatever reason, so we just take some steps forward
                /*
                walkTo(homePosition);
                */

                standUp();
                Walking::GetInstance()->m_Joint.SetEnableBody(false);
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 20;
                Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->Start(6);       // magic number
                Walking::GetInstance()->Finish();
            }
            else if(!onPenalty)
            {
                if(myState == STATE_READY || myState == STATE_WAITING) // only re-enter the playing state if we're ready to
                {
                    standUp();
                    myState = STATE_PLAYING;
                }
            }
            break;

        case SocketMonitor::STATE_INITIAL:
        case SocketMonitor::STATE_FINISHED:
        default:
            // no action going on, so just sit down and wait
            myState = STATE_WAITING;
            break;
        }
#ifdef DEBUG
        cout << "Game State: " << gameState << endl <<
                "  My State: " << myState << " [" << playSubstate << "]" << endl;
#endif

        newState = myLastState != myState;

        // stand up if we were just on penalty
        if(prevOnPenalty && myState != STATE_HW_INTERRUPT && myState != STATE_WAITING)
        {
            cout << "[info] Leaving Penalty State" << endl;
            Head::GetInstance()->MoveByAngle(0,30); // look straight ahead and angled up so we can see goal posts for orientation
            standUp();
        }

        switch(myState)
        {
        case STATE_LOOKING:
            // look around and orient ourselves
            //lookAround();
            if(newState)
                cout << "[state] LOOKING AROUND" << endl;
            scanAround();
            break;

        case STATE_PLAYING:
            newPlaySubstate = lastPlaySubstate != playSubstate;
            lastPlaySubstate = playSubstate;

            switch(playSubstate)
            {
            case SUBSTATE_CHASING_BALL:
                if(newState)
                {
                    cout << "[state] PLAYING :: CHASING" << endl;
                    if(myLastState == STATE_HW_INTERRUPT)
                        standUp();
                }
                processChaseBall();
                break;

            case SUBSTATE_AIMING_KICK:
                if(newState)
                {
                    cout << "[state] PLAYING :: AIMING" << endl;
                    if(myLastState == STATE_HW_INTERRUPT)
                        standUp();
                }
                processAimKick();
                break;

            case SUBSTATE_KICKING:
                if(newState)
                {
                    cout << "[state] PLAYING :: KICKING" << endl;
                    if(myLastState == STATE_HW_INTERRUPT)
                        standUp();
                }
                processKick();
                break;

            default:
                if(newState)
                {
                    cout << "[state] PLAYING :: unk." << endl;
                    if(myLastState == STATE_HW_INTERRUPT)
                        standUp();
                }
                playSubstate = SUBSTATE_CHASING_BALL;
                break;
            }

            break;

        case STATE_WALKING:
            /*if(newState) {
                field.SetPosition(Point3D(0,300,180));
            }*/

            if(newState)
            {
                cout << "[state] WALKING (" << field.presentPosition.X << " , " << field.presentPosition.Y << " , " << field.presentPosition.Z << ") --> (" <<
                    walkingDestination.X << " , " << walkingDestination.Y << " , " << walkingDestination.Z << ")" << endl;
                if(myLastState == STATE_HW_INTERRUPT)
                    standUp();
            }
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            if(!Walking::GetInstance()->IsRunning())
            {
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->Start();
            }
            processWalking();
            break;

        case STATE_READY:
            if(newState)
                cout << "[state] READY" << endl;
            if(newState)
            {
                standUp();
            }
            break;

        case STATE_WAITING:
        case STATE_HW_INTERRUPT:
        default:
            if(newState)
            {
                cout << "[state] IDLE" << endl;
                sitDown();
                Head::GetInstance()->MoveByAngle(0,0);

                numKicksTaken = 0;
            }
            break;
        }
    }
    else
    {
        if(newState)
            cout << "Waiting" << endl;

        // just sit down and wait
        sitDown();
    }

    myLastState = myState;
}

void SoccerPlayer::updateOdometry()
{
    int newCount = Walking::GetInstance()->GetStepCount();

    if(newCount != 0 && newCount != stepCounter)
    {
        int diff = newCount - stepCounter;
        double x = Walking::GetInstance()->X_MOVE_AMPLITUDE;
        double y = Walking::GetInstance()->Y_MOVE_AMPLITUDE;
        double a = Walking::GetInstance()->A_MOVE_AMPLITUDE;
        if(fabs(x) < 3)
            x = 0;

#ifdef DEBUG
        cout << "Odometry updated with: " << diff << " steps, " << x << " X-amp, " << y << " Y-amp, " << a << " A-amp" << endl;
#endif

        map.UpdateOdometry(diff, x, y, a);
    }
    stepCounter = newCount;
}

void SoccerPlayer::processChaseBall()
{
    static int ballFrames = 0;
    static int noBallFrames = 0;
    const int RACE_TO = 2;

    if(!Walking::GetInstance()->IsRunning())
    {
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        Walking::GetInstance()->Start();
    }

    updateOdometry();

    if(map.ballPosition.X < 0)
    {
        Walking::GetInstance()->Stop();
        cout << "[info] lost sight of the ball" << endl;
        //Voice::Speak("Lost the ball");
        handleLostBall();

        ballFrames = 0;
        noBallFrames = 0;
    }
    else if(map.ballPosition.X > maxKickRange || map.ballPosition.Y > LEFT_FOOT_LEFT_THRESHOLD || map.ballPosition.Y < RIGHT_FOOT_RIGHT_THRESHOLD)
    {
        if(!ball.WasFound()) // use the polar map to plan our course to the ball
        {
            // if the ball lies in an arc to the front then walk forwards as well
            cameraPosition.RecalculatePosition();
            double downAngle = atan2(cameraPosition.GetCameraHeight(),map.ballPosition.X);
            Point2D ballAngles = Point2D(downAngle, map.ballPosition.Y);
            follower.Process(ballAngles);

            pauser.threadWaitHead();
            Head::GetInstance()->MoveByAngle(map.ballPosition.Y, downAngle);
            pauser.headHasReacted();
        }
        else // use vision to plan our course to the ball
        {
            pauser.threadWaitHead();
            tracker.Process(ball.GetBoundingBox()->center);
            pauser.headHasReacted();
            follower.Process(tracker.ball_position);
        }

        // chasing, so set these to zero (they're only used when preparing to aim)s
        ballFrames = 0;
        noBallFrames = 0;
    }
    else if(map.ballPosition.X <= maxKickRange && map.ballPosition.Y >= RIGHT_FOOT_RIGHT_THRESHOLD && map.ballPosition.Y <= LEFT_FOOT_LEFT_THRESHOLD)
    {
        if(!noKick)
        {
            Walking::GetInstance()->Stop();
            if(!Walking::GetInstance()->IsRunning())
            {
                // do a sanity check by tilting the head down and making sure we can see the ball
                pauser.threadWaitHead();
                Head::GetInstance()->MoveByAngle(map.ballPosition.Y, -45);
                pauser.headHasReacted();

                // see if we can see the ball for 10 frames
                // if we do (before we hit 10 no-ball frames) then
                // we can safely assume the ball is at our feet
                if(ball.WasFound())
                    ballFrames++;
                else
                    noBallFrames++;

                cout << "[info] Ball might be in position to aim kick (score: Y-" << ballFrames << " N-" << noBallFrames << ")" << endl;

                if(ballFrames >= RACE_TO)
                {
                    cout << "[info] Can begin aiming" << endl;
                    if(!noKick)
                        playSubstate = SUBSTATE_AIMING_KICK;
                }
                else
                {
                    cout << "[info] Ball not in position to kick. Falling back to chasing" << endl;
                    //handleLostBall();
                    cout << "[info] Ball might be occluded by knee.  Trying to kick anyway" << endl;
                    if(!noKick)
                        playSubstate = SUBSTATE_AIMING_KICK;
                }
            }
        }
    }
}

int SoccerPlayer::ballInFrontOfLeftFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle > LEFT_FOOT_RIGHT_THRESHOLD && angle < LEFT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(angle >= LEFT_FOOT_RIGHT_THRESHOLD)
        ans = 1;
    else
        ans = -1;

    return ans;
}

int SoccerPlayer::ballInFrontOfRightFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle > RIGHT_FOOT_RIGHT_THRESHOLD && angle < RIGHT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(angle >= RIGHT_FOOT_RIGHT_THRESHOLD)
        ans = 1;
    else
        ans = -1;

    return ans;
}

int SoccerPlayer::ballBesideLeftFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle > LEFT_FOOT_LEFT_THRESHOLD && angle < 1.5*LEFT_FOOT_LEFT_THRESHOLD)
        ans = 0;
    else if(angle >= 1.5*LEFT_FOOT_RIGHT_THRESHOLD)
        ans = 1;
    else
        ans = -1;

    return ans;
}

int SoccerPlayer::ballBesideRightFoot()
{
    int ans;
    double angle = map.ballPosition.Y;

    if(angle < RIGHT_FOOT_RIGHT_THRESHOLD && angle > 1.5*RIGHT_FOOT_RIGHT_THRESHOLD)
        ans = 0;
    else if(angle <= 1.5*RIGHT_FOOT_RIGHT_THRESHOLD)
        ans = -1;
    else
        ans = 1;

    return ans;
}

int SoccerPlayer::ballPosnForKick()
{
    int ans;
    double angle = map.ballPosition.Y;

    switch(chosenKick)
    {
        case PASS_FORWARD_LEFT_MOTION:
        case HIGH_KICK_LEFT_MOTION:
            if(angle < LEFT_FORWARD_LEFT_THRESHOLD && angle > LEFT_FORWARD_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= LEFT_FORWARD_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_FORWARD_RIGHT_MOTION:
        case HIGH_KICK_RIGHT_MOTION:
            if(angle < RIGHT_FORWARD_LEFT_THRESHOLD && angle > RIGHT_FORWARD_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= RIGHT_FORWARD_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_LEFT_MOTION:
            if(angle < LEFT_INSIDE_LEFT_THRESHOLD && angle > LEFT_INSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= LEFT_INSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_RIGHT_MOTION:
            if(angle < RIGHT_INSIDE_LEFT_THRESHOLD && angle > RIGHT_INSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= RIGHT_INSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_OUTSIDE_LEFT_MOTION:
            if(angle < LEFT_OUTSIDE_LEFT_THRESHOLD && angle > LEFT_OUTSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= LEFT_OUTSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        case PASS_BALL_OUTSIDE_RIGHT_MOTION:
            if(angle < RIGHT_OUTSIDE_LEFT_THRESHOLD && angle > RIGHT_OUTSIDE_RIGHT_THRESHOLD)
                ans = 0;
            else if(angle <= RIGHT_OUTSIDE_RIGHT_THRESHOLD)
                ans = -1;
            else
            ans = 1;
            break;

        default:
            cout<<"INVALID KICK CHOSEN!"<<endl;
            ans = 0;
            break;
    }

    return ans;
}

void SoccerPlayer::processAimKick()
{
    int numSteps, shuffleDirection;

    enum {
        DIRECTION_FORWARD,
        DIRECTION_LEFT,
        DIRECTION_RIGHT
    };

    const double FORWARD_KICK_CUTOFF_ANGLE = 60;

    double deltaAngle = 9999999;
    double turn, currTilt, currPan;
    bool inPosition = false;

    int chosenDirection = DIRECTION_FORWARD;

    if(newPlaySubstate) //if we just arrived in this state, do a quick scan to see if we can find a goal
    {
        standUp();
        currPan = Head::GetInstance()->GetPanAngle();
        currTilt = Head::GetInstance()->GetTiltAngle();
        //foundGoalDuringQuickScan = false;
        //quickScan();
        //Head::GetInstance()->MoveByAngle(currPan, currTilt);
        //MotionManager::Sync();
        //pauser.waitAMoment();
    }

    if(ball.WasFound())
    {
        if(penaltyKick)
        {
            // bypass EVERYTHING and just kick the ball straight forward
            Voice::Speak("Taking penalty kick");
            cout << "Executing penalty kick" << endl;

            deltaAngle = 0;
            chosenDirection = DIRECTION_FORWARD;
        }
        else if(foundGoalDuringQuickScan)
        {
            cout << "Found goal during head-flick" << endl;

            if(map.GetRangeToTheirGoal() > 0) // kick towards their goal (with maximal aiming)
            {
                Voice::Speak("Kicking towards their goal");

                double angleToTheirGoal = map.GetAngleToTheirGoal();

                if(map.theirGoalPosition[SoccerMap::LEFT].Y > 0 && map.theirGoalPosition[SoccerMap::RIGHT].Y < 0)
                {
                    // we're facing between their posts
                    // just kick forward
                    cout << "[info] their goal is directly ahead" << endl;
                    angleToTheirGoal = 0;
                }
                else if(map.theirGoalPosition[SoccerMap::LEFT].Y < 0)
                {
                    // their entire goal is off to the right
                    // aim for the the inside of the left post
                    cout << "[info] their goal is to the right" << endl;
                    angleToTheirGoal = (map.theirGoalPosition[SoccerMap::LEFT].Y * 3 + map.theirGoalPosition[SoccerMap::RIGHT].Y)/4.0;
                }
                else if(map.theirGoalPosition[SoccerMap::RIGHT].Y > 0)
                {
                    // their entire goal is off to the left
                    // aim for the inside of the right post
                    cout << "[info] their goal is to the left" << endl;
                    angleToTheirGoal = (map.theirGoalPosition[SoccerMap::RIGHT].Y * 3 + map.theirGoalPosition[SoccerMap::LEFT].Y)/4.0;
                }

                //deltaAngle = angleToTheirGoal;
                deltaAngle = 0;
                chosenDirection = DIRECTION_FORWARD;
            }
            else if(map.GetRangeToMyGoal() > 0) // kick away from our goal at best speed (i.e. minimal turning; this is rapid defense)
            {
                Voice::Speak("Kicking away from my goal");
                deltaAngle = 0;

                double angleToMyGoal = map.GetAngleToMyGoal();

                if(angleToMyGoal > 90 || angleToMyGoal < -90)
                {
                    // our goal is WAY behind us; kick forward and hope for the best
                    chosenDirection = DIRECTION_FORWARD;
                }
                else if(angleToMyGoal > 0 )
                {
                    chosenDirection = DIRECTION_RIGHT;
                }
                else if(angleToMyGoal < 0)
                {
                    chosenDirection = DIRECTION_LEFT;
                }
                else
                {
                    // our goal is somwhere in our front-90 arc; kick laterally to get the ball out of play
                    if(angleToMyGoal > 0)
                        chosenDirection = DIRECTION_RIGHT;
                    else
                        chosenDirection = DIRECTION_LEFT;
                }
            }
            else
            {
                chosenDirection = DIRECTION_FORWARD;
                deltaAngle = 0;
            }
        }
        else if(map.GetRangeToTheirGoal() > 0)
        {
            cout << "Kicking towards their goal" << endl;
            Voice::Speak("Kicking towards their goal");

            // use polar map data for their goal
            if(map.GetAngleToTheirGoal() < -90)
            {
                // their goal is WAY right; side-kick right
                chosenDirection = DIRECTION_RIGHT;
                deltaAngle = 0;
            }
            else if(map.GetAngleToTheirGoal() > 90)
            {
                // their goal is WAY left; side-kick left
                chosenDirection = DIRECTION_LEFT;
                deltaAngle = 0;
            }
            else if(map.theirGoalPosition[SoccerMap::RIGHT].Y > 0)
            {
                // facing right of their goal
                chosenDirection = DIRECTION_FORWARD;
                //deltaAngle = (map.theirGoalPosition[SoccerMap::RIGHT].Y * 3 + map.theirGoalPosition[SoccerMap::LEFT].Y)/4.0;
                deltaAngle = 0;
            }
            else if(map.theirGoalPosition[SoccerMap::LEFT].Y < 0)
            {
                // facing left of their goal
                chosenDirection = DIRECTION_FORWARD;
                //deltaAngle = (map.theirGoalPosition[SoccerMap::LEFT].Y * 3 + map.theirGoalPosition[SoccerMap::RIGHT].Y)/4.0;
                deltaAngle = 0;
            }
            else //if(map.theirGoalPosition[SoccerMap::LEFT].Y > 0 && map.theirGoalPosition[SoccerMap::RIGHT].Y < 0)
            {
                // facing their goal directly
                // blast the ball forward
                chosenDirection = DIRECTION_FORWARD;
                deltaAngle = 0;
            }
        }
        else if(map.GetRangeToMyGoal() > 0)
        {
            cout << "Kicking away from my goal" << endl;
            Voice::Speak("Kicking away from my goal");

            // use polar map data for our goal
            if(map.GetAngleToMyGoal() < -90 || map.GetAngleToMyGoal() > 90)
            {
                // our goal is way off to a side or behind us; blast forward
                chosenDirection = DIRECTION_FORWARD;
                deltaAngle = 0;
            }
            else if(map.myGoalPosition[SoccerMap::RIGHT].Y > 0)
            {
                // our goal is to our left; side-kick right
                chosenDirection = DIRECTION_RIGHT;
                deltaAngle = 0;
            }
            else if(map.myGoalPosition[SoccerMap::LEFT].Y < 0)
            {
                // our goal is to our right; side-kick left
                chosenDirection = DIRECTION_LEFT;
                deltaAngle = 0;
            }
            else //if(map.theirGoalPosition[SoccerMap::LEFT].Y > 0 && map.theirGoalPosition[SoccerMap::RIGHT].Y < 0)
            {
                // worst-case; our goal is directly ahead
                // just side-tap as best we can
                if(map.GetAngleToMyGoal() > 0)
                {
                    chosenDirection = DIRECTION_RIGHT;
                    deltaAngle = 0;
                }
                else
                {
                    chosenDirection = DIRECTION_LEFT;
                    deltaAngle = 0;
                }
            }
        }
        else
        {
            // orientation is unknown; just kick the ball straight ahead
            Voice::Speak("Kicking blind");
            cout << "Kicking blind" << endl;
            deltaAngle = 0;
            chosenDirection = DIRECTION_FORWARD;
        }

        if(deltaAngle > 0)
            turn = -DEGREES_PER_PIVOT_STEP;
        else
            turn = DEGREES_PER_PIVOT_STEP;

        numSteps = abs((int)(deltaAngle/(turn/2.0 * map.getAGain())));

        // pivot so that we are facing the right direction (note: this may put us out of position laterally, or too far from the ball)
        cout << "Taking " << numSteps << " for a rotation of " << deltaAngle << endl;
        if(numSteps >= 1)
        {
            Walking::GetInstance()->m_Joint.SetEnableBody(false);
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->A_MOVE_AMPLITUDE = turn;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = -2;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->Start(numSteps);
            while(Walking::GetInstance()->IsRunning())
            {
                pauser.threadWaitHead();
                Head::GetInstance()->LookAt(ball);
                pauser.headHasReacted();
                if(handleFall())
                {
                    // give up and fall back to the looking around state to re-orient ourselves
                    // TODO: we could try something less drastic here, but for now I think this will suffice - CIB
                    cout << "[info] Aborting kick attempt; fallen over" << endl;
                    handleLostBall();
                    return;
                }
            }
            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_WALKREADY);
            Action::GetInstance()->Finish();
        }

        updateOdometry();
    }
    else
    {
        handleLostBall();
        return;
    }

    while(!inPosition)
    {
        cout << chosenDirection << " " << shuffleDirection << endl;
        if(handleFall())
        {
            // give up and fall back to the looking around state to re-orient ourselves
            // TODO: we could try something less drastic here, but for now I think this will suffice - CIB
            cout << "[info] Aborting kick attempt; fallen over" << endl;
            handleLostBall();
            return;
        }
        pauser.threadWaitHead();
        if(ball.WasFound() && map.ballPosition.X<=maxKickRange)
        {
            Head::GetInstance()->LookAt(ball);
            MotionManager::Sync();

            // decide what kick we want to use based on where the ball is right now
            switch(chosenDirection)
            {
            case DIRECTION_LEFT:
                /*if(map.ballPosition.Y < 0)      // ball is in front of the right foot; this is good
                {
                    chosenKick = PASS_BALL_LEFT_MOTION;
                }
                else // use the outside left kick (get the ball beside the left foot
                {
                    chosenKick = PASS_BALL_OUTSIDE_LEFT_MOTION;
                }*/
                chosenKick = PASS_BALL_LEFT_MOTION;
                break;

            case DIRECTION_RIGHT:
                /*if(map.ballPosition.Y > 0)      // ball is in fromt of the left foot
                {
                    chosenKick = PASS_BALL_RIGHT_MOTION;
                }
                else
                {
                    chosenKick = PASS_BALL_OUTSIDE_RIGHT_MOTION;
                }*/
                chosenKick = PASS_BALL_RIGHT_MOTION;
                break;

            case DIRECTION_FORWARD:
            default:
                if(map.ballPosition.Y > 0)
                {
                    if(numKicksTaken < numKicksBeforeHighKick)
                        chosenKick = PASS_FORWARD_LEFT_MOTION;
                    else
                        chosenKick = HIGH_KICK_LEFT_MOTION;
                }
                else
                {
                    if(numKicksTaken < numKicksBeforeHighKick)
                        chosenKick = PASS_FORWARD_RIGHT_MOTION;
                    else
                        chosenKick = HIGH_KICK_RIGHT_MOTION;
                }
                break;
            }
            shuffleDirection = ballPosnForKick();

            cout << "Stepping " << shuffleDirection << endl;

            if(shuffleDirection == 0)
            {
                inPosition = true;
            }
            else if(shuffleDirection > 0)
            {
                /*
                Action::GetInstance()->m_Joint.SetEnableBody(false);
                Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Action::GetInstance()->Start(SHUFFLE_LEFT);
                Action::GetInstance()->Finish();
                standUp();*/
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->Y_MOVE_AMPLITUDE = 16;
                Walking::GetInstance()->Start(1);
                Walking::GetInstance()->Finish();
                cout << "XX" << endl;
            }
            else
            {
                /*
                Action::GetInstance()->m_Joint.SetEnableBody(false);
                Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Action::GetInstance()->Start(SHUFFLE_RIGHT);
                Action::GetInstance()->Finish();
                standUp();*/
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->Y_MOVE_AMPLITUDE = -16;
                Walking::GetInstance()->Start(1);
                Walking::GetInstance()->Finish();
                cout << "YY" << endl;
            }

            if(handleFall())
            {
                // give up and fall back to the looking around state to re-orient ourselves
                // TODO: we could try something less drastic here, but for now I think this will suffice - CIB
                cout << "[info] Aborting kick attempt; fallen over" << endl;
                handleLostBall();
            }
        }
        else if(ball.WasFound())
        {
            cout << "[info] Aborting kick attempt; ball too far away" << endl;
            playSubstate = SUBSTATE_CHASING_BALL;
            return; //the ball is now far away, so exit this function and go back to chasing it
        }
        else
        {
            // give up and fall back to the looking around state to re-orient ourselves
            // TODO: we could try something less drastic here, but for now I think this will suffice - CIB
            cout << "[info] Aborting kick attempt; lost sight of the ball" << endl;
            handleLostBall();
            return;
        }
        pauser.headHasReacted();
    }

    cout << "[info] Aiming complete" << endl;
    playSubstate = SUBSTATE_KICKING;
}

// kick with whatever kick we have chosen
void SoccerPlayer::processKick()
{
    if(chosenKick > 0)
    {
        if(chosenKick == HIGH_KICK_LEFT_MOTION || chosenKick == HIGH_KICK_RIGHT_MOTION)
        {
            // take two steps forward to get the ball closer to the foot
            standUp();
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 10;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->Start(1);
            Walking::GetInstance()->Finish();
        }

        Action::GetInstance()->m_Joint.SetEnableBody(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
        Action::GetInstance()->Start(chosenKick);
        cout << "Kicking Now" << endl;
        Action::GetInstance()->Finish();
        numKicksTaken++;
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
        standUp();
        //playSubstate = SUBSTATE_CHASING_BALL;

        // we kicked the ball, but have no real idea where it may have gone, so just look for it again
        handleLostBall();

        chosenKick = -1;
    }
    else
    {
        cout << "[error] Unknown Kicking Motion: " << chosenKick << endl;
        handleLostBall();
    }
}

bool SoccerPlayer::canPlay()
{
    return (refSaysWeCanPlay /*&& myState != STATE_WAITING && myState != STATE_HW_INTERRUPT*/) || noNetwork;
}

void SoccerPlayer::checkRefStatus()
{
    //int oldState = myState;
    //bool oldRefSaysWeCanPlay = refSaysWeCanPlay;
    //int oldGameState = gameState;

    if(noNetwork)
    {
        refSaysWeCanPlay = true;
    }
    else
    {
        refSaysWeCanPlay = socketMonitor->canRobotPlay(myColour, myId);
        onPenalty = socketMonitor->isRobotOnPenalty(myColour, myId);
    }
}

void SoccerPlayer::walkTo(Point3D &point)
{
    this->walkingDestination.X = point.X;
    this->walkingDestination.Y = point.Y;
    this->walkingDestination.Z = point.Z;

    myState = STATE_WALKING;
}

void SoccerPlayer::processWalking()
{
    const double MAX_ERROR_DISTANCE = 15;   // as long as we're within 30cm it's close enough
    const double MAX_ERROR_ANGLE = 7.5;       // orientation can be +/- 15 degrees and still be good enough
    const double MAX_WALKING_ANGLE = 20;     // The robot must be at least this close to the correct angle before walking forwards

    double xError = fabs(walkingDestination.X - field.presentPosition.X);
    double yError = fabs(walkingDestination.Y - field.presentPosition.Y);
    double zError = fabs(walkingDestination.Z - field.presentPosition.Z);

    cout << "[info] Walking to destination. (" << walkingDestination.X << " , " << walkingDestination.Y << " , " << walkingDestination.Z << ")" << endl;
    updateOdometry();

    // if we are not in the x/y position yet then turn to face the correct location
    if(xError > MAX_ERROR_DISTANCE || yError > MAX_ERROR_DISTANCE)
    {
        cout << "       Correcting X/Y/Z error of " << xError << " " << yError << " " << zError << endl;
        cout << "       Current position: " <<field.presentPosition.X << " " << field.presentPosition.Y << " " << field.presentPosition.Z << endl;

        // calculate the angle we should be facing to reach our destination
        double toFace = rad2deg(atan2(fabs(walkingDestination.X - field.presentPosition.X), fabs(walkingDestination.Y - field.presentPosition.Y)));
        if(field.presentPosition.X <= walkingDestination.X && field.presentPosition.Y <= walkingDestination.Y)
        {
            // walking toward first quadrant (which is to the front-right)
            toFace = -toFace;
        }
        else if(field.presentPosition.X <= walkingDestination.X && field.presentPosition.Y > walkingDestination.Y)
        {
            // walking toward the fourth quadrant (which is to the back-right)
            toFace = -180+toFace;
        }
        else if(field.presentPosition.X > walkingDestination.X && field.presentPosition.Y <= walkingDestination.Y)
        {
            // walking toward the second quadrant (which is to the front-left)
            // no change to toFace
        }
        else
        {
            // walking toward the third quadrant (which ios to the back-left)
            toFace = 180-toFace;
        }

        // normalize the angle to be in the [-180,180] range
        toFace = field.normalizeAngle(toFace);

        // we know where we need to go, so now figure out how much we need to turn to get there
        double toTurn = field.normalizeAngle(toFace - field.presentPosition.Z);

        if(fabs(toTurn) < MAX_WALKING_ANGLE)
        {
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 20.0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = toTurn/10.0;
        }
        else
        {
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
            Walking::GetInstance()->X_MOVE_AMPLITUDE = -2.0;
            if(toTurn<0) {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = -10.0;
            } else {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 10.0;
            }
        }

        cout << "       Turning " << toTurn << endl;
        //Walking::GetInstance()->A_MOVE_AMPLITUDE = -toTurn/10.0;

    }
    else if(zError > MAX_ERROR_ANGLE)
    {
        cout << "       Correcting Z error of " << zError << endl;

        // just turn until we are facing the right way
        double toTurn = walkingDestination.Z - field.presentPosition.Z;
        toTurn = field.normalizeAngle(toTurn);
        Walking::GetInstance()->X_MOVE_AMPLITUDE = -2.0;
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
        //Walking::GetInstance()->A_MOVE_AMPLITUDE = -toTurn/10;
        if(toTurn<0) {
            Walking::GetInstance()->A_MOVE_AMPLITUDE = -10.0;
        } else {
            Walking::GetInstance()->A_MOVE_AMPLITUDE = 10.0;
        }
    }
    else
    {
        cout << "[info] Reached destination" << endl;

        Walking::GetInstance()->X_MOVE_AMPLITUDE = 0.0;
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0.0;
        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0.0;
        Walking::GetInstance()->Stop();
        Walking::GetInstance()->Finish();

        if(isGoalie)
            ((Goalie*)this)->doneWalking();
        else
            myState = STATE_READY;
    }
}

bool SoccerPlayer::handleFall()
{
    bool didFall = false;
    MotionModule *inControl = Action::GetInstance();

    if(MotionStatus::FALLEN != STANDUP)
    {
        didFall = true;

        if(Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->Stop();
            Walking::GetInstance()->Finish();

            inControl = Walking::GetInstance();
        }

        //MotionManager::GetInstance()->AddModule(Action::GetInstance());
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);

        if(MotionStatus::FALLEN == FORWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_F_UP);   // FORWARD GETUP
        else if(MotionStatus::FALLEN == BACKWARD)
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_B_UP);   // BACKWARD GETUP

        Action::GetInstance()->Finish();

        inControl->m_Joint.SetEnableBodyWithoutHead(true, true);
        LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
        RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
    }

    return didFall;
}

void *SoccerPlayer::videoThread(void *arg)
{
    SoccerPlayer *instance = (SoccerPlayer*)arg;
    for(;;)
    {
        instance->handleVideo();
        pthread_yield();
    }
    return NULL;
}

void SoccerPlayer::yieldThread()
{
    pthread_yield();
}

bool SoccerPlayer::handleButton()
{
    static int lastButton = 0;
    int currentButton;

    currentButton = MotionStatus::BUTTON;

    if(currentButton != lastButton)
    {
        if(myState == STATE_HW_INTERRUPT && currentButton == CM730::MIDDLE_BUTTON)
            myState = STATE_PLAYING;
        else if(myState == STATE_PLAYING && currentButton == CM730::LEFT_BUTTON)
            myState = STATE_HW_INTERRUPT;

        lastButton = currentButton;
    }

    return currentButton != 0;
}

void *SoccerPlayer::buttonThread(void *arg)
{
    SoccerPlayer *instance = (SoccerPlayer*)arg;
    for(;;)
    {
        instance->handleButton();
        instance->pauser.threadWait();
    }
    return NULL;
}

void SoccerPlayer::handleVideo()
{
    // update where the robot's head is based on its body position
    cameraPosition.RecalculatePosition();

    if(showVideo || recordVideo) // clear the debugging frame if necessary
        rectangle(dbgImage,Point(0,0),Point(dbgImage.cols,dbgImage.rows),Scalar(0,0,0),-1);

    //double pan = cameraPosition.GetCameraPanOffset() + Head::GetInstance()->GetPanAngle();
    double tilt = cameraPosition.GetCameraTiltOffset() + Head::GetInstance()->GetTiltAngle() - Kinematics::EYE_TILT_OFFSET_ANGLE;

    // set the ignoreTop value so we're not looking for the ball higher than perfectly horizontal
    double cameraTop = tilt + Camera::VIEW_V_ANGLE/2;
    double horizontal = 0;
    if(cameraTop > 0)
    {
        double degreesPerPixel = Camera::VIEW_V_ANGLE/Camera::HEIGHT;
        horizontal = cameraTop / degreesPerPixel;
    }
    ball.SetIgnoreTop(horizontal);

    // grab the next frame from the camera
    CvCamera::GetInstance()->CaptureFrame();

    // find the objects in the frame
    BlobTarget::FindTargets(targets, CvCamera::GetInstance()->yuvFrame, &dbgImage,subsample);

    // if we find the ball then update its position on the map
    if(ball.WasFound())
    {
        alreadyTracked = false;
        map.FoundBall(ball);
        noBallFrames = 0;
        BoundingBox *targetBox;

        targetBox = ball.GetBoundingBox();
        range = ball.GetRange(0);
        angle = ball.GetAngle(0);
        lastBallInfo->update(targetBox->center.X, targetBox->center.Y, range, angle, frMonitor->getMostRecentFrameRate());
    }
    else
    {
        noBallFrames++;
        lastBallInfo->reset();

        if(noBallFrames >= maxNoBallFrames)
            map.LostBall();
    }

    // let the map know where players on the field are
    map.FoundRobots(myTeamPlayers.GetBoundingBoxes(), theirTeamPlayers.GetBoundingBoxes());

    // look for the goal posts if we are in the playing state
    // otherwise false-positives can REALLY throw us off
    if(myState == STATE_PLAYING || visionTest)
    {
        goal.FindInFrame(CvCamera::GetInstance()->yuvFrame,NULL);

        if(goal.WasFound())
        {
            if(goal.IsAgainstWhiteBackground())
            {
                if((socketMonitor->isFirstHalf() && myGoalWhiteBackgroundInFirstHalf) ||
                   (socketMonitor->isSecondtHalf() && !myGoalWhiteBackgroundInFirstHalf))
                {
                    // the goal on the white background is ours in the first half
                    map.FoundGoal(goal, myColour);
                }
                else
                {
                    // the goal not on the white background is theirs in the first half
                    map.FoundGoal(goal, theirColour);
                }

            }
            else
            {
#ifndef RELY_ON_WHITE_BACKGROUND
                // we found a goal but are not certain what colour it is
                map.FoundGoal(goal);
#else
                if((socketMonitor->isFirstHalf() && myGoalWhiteBackgroundInFirstHalf) ||
                   (socketMonitor->isSecondtHalf() && !myGoalWhiteBackgroundInFirstHalf))
                {
                    // the goal is not on a white background is theirs in the first half
                    map.FoundGoal(goal, theirColour);
                }
                else
                {
                    // the goal not on the white background is ours in the second half
                    map.FoundGoal(goal, myColour);
                }
#endif
            }
        }
    }

    field.Update();

    // deal with displaying/annotating/recording the video frames if necessary
    annotateAndRecordVideo();

    // wait broadcast that we have a new frame so motion-planning can be updated
    pauser.newFrame();
    pauser.threadBroadcast();
}

void SoccerPlayer::annotateAndRecordVideo()
{
    double pan = cameraPosition.GetCameraPanOffset() + Head::GetInstance()->GetPanAngle();
    double tilt = cameraPosition.GetCameraTiltOffset() + Head::GetInstance()->GetTiltAngle() - Kinematics::EYE_TILT_OFFSET_ANGLE;

    // annotate the images if necessary (i.e. the images are actually used)
    if(recordVideo || showVideo)
    {
        // make a local copy of the RGB frame that we can annotate
        cvtColor(CvCamera::GetInstance()->yuvFrame, rgbImage, CV_YCrCb2RGB);

        ball.DrawScanRange(rgbImage);
        ball.DrawScanRange(dbgImage);

        //myGoal.Draw(rgbImage);
        //myGoal.Draw(dbgImage);
        //theirGoal.Draw(rgbImage);
        //theirGoal.Draw(dbgImage);

        // draw some diagnostic data on the image
        char buffer[255];
        sprintf(buffer,"Ball: (%0.2f,%0.2f) State: %d Tilt: %0.1f", map.ballPosition.X, map.ballPosition.X, myState, tilt);
        cv::Scalar colour = cv::Scalar(255,255,255);
        cv::Point pt = cv::Point(0,Camera::HEIGHT-4);
        cv::putText(rgbImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        cv::putText(dbgImage, buffer,  pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

        // write the ranges/angles of the objects we can see
        if(ball.WasFound())
        {
            ball.Draw(rgbImage);
            ball.Draw(dbgImage);

            sprintf(buffer,"%0.2f", ball.GetRange()/10.0);
            pt = cv::Point(ball.GetBoundingBox()->center.X, ball.GetBoundingBox()->center.Y-6);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

            sprintf(buffer,"%0.2f", ball.GetAngle());
            pt = cv::Point(ball.GetBoundingBox()->center.X, ball.GetBoundingBox()->center.Y+6);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        }

        if(goal.NumFound() > 0)
        {
            goal.Draw(rgbImage);
            goal.Draw(dbgImage);

            sprintf(buffer,"%0.2f", goal.GetLeftPostRange()/10.0);
            pt = cv::Point(goal.leftPost.end.X-20, goal.leftPost.end.Y);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            sprintf(buffer,"%0.2f", goal.GetLeftPostAngle());
            pt = cv::Point(goal.leftPost.end.X-20, goal.leftPost.end.Y+12);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

            sprintf(buffer,"%0.2f", goal.GetRightPostRange()/10.0);
            pt = cv::Point(goal.rightPost.end.X-20, goal.rightPost.end.Y);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            sprintf(buffer,"%0.2f", goal.GetRightPostAngle());
            pt = cv::Point(goal.rightPost.end.X-20, goal.rightPost.end.Y+12);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

            sprintf(buffer,"%0.2f", goal.GetRange()/10.0);
            pt = cv::Point((goal.leftPost.end.X + goal.rightPost.end.X)/2-20, (goal.leftPost.end.Y + goal.rightPost.end.Y)/2);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            sprintf(buffer,"%0.2f", goal.GetAngle());
            pt = cv::Point((goal.leftPost.end.X + goal.rightPost.end.X)/2-20, (goal.leftPost.end.Y + goal.rightPost.end.Y)/2+12);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        }

        // draw where players on the field are
        BoundingBox* bbox;
        for(vector<BoundingBox*>::iterator it=myTeamPlayers.GetBoundingBoxes()->begin(); it!=myTeamPlayers.GetBoundingBoxes()->end(); it++)
        {
            bbox = ((BoundingBox*)(*it));
            bbox->Draw(rgbImage,*(myTeamPlayers.GetMarkColour()),1);
            bbox->Draw(dbgImage,*(myTeamPlayers.GetMarkColour()),1);

            sprintf(buffer,"%0.2f",cameraPosition.CalculateRange(*bbox)/10.0);
            pt = cv::Point(bbox->center.X, bbox->center.Y-6);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

            sprintf(buffer,"%0.2f",cameraPosition.CalculateAngle(*bbox));
            pt = cv::Point(bbox->center.X, bbox->center.Y+6);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        }
        for(vector<BoundingBox*>::iterator it=theirTeamPlayers.GetBoundingBoxes()->begin(); it!=theirTeamPlayers.GetBoundingBoxes()->end(); it++)
        {
            bbox = ((BoundingBox*)(*it));
            bbox->Draw(rgbImage,*(theirTeamPlayers.GetMarkColour()),1);
            bbox->Draw(dbgImage,*(theirTeamPlayers.GetMarkColour()),1);

            sprintf(buffer,"%0.2f",cameraPosition.CalculateRange(*bbox)/10.0);
            pt = cv::Point(bbox->center.X, bbox->center.Y-6);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);

            sprintf(buffer,"%0.2f",cameraPosition.CalculateAngle(*bbox));
            pt = cv::Point(bbox->center.X, bbox->center.Y+6);
            cv::putText(rgbImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
            cv::putText(dbgImage, buffer, pt, FONT_HERSHEY_PLAIN, 1.0, colour, 1);
        }


        // draw "crosshairs" that indicate where the head's zero position is
        pan = (pan + 70)/140;
        tilt = (tilt + 75)/150;

        pt = cv::Point(0, tilt * Camera::HEIGHT);
        cv::Point pt2 = cv::Point(Camera::WIDTH, tilt * Camera::HEIGHT);
        cv::line(rgbImage, pt, pt2, colour);
        cv::line(dbgImage, pt, pt2, colour);

        pt = cv::Point(pan * Camera::WIDTH, 0);
        pt2 = cv::Point(pan * Camera::WIDTH, Camera::HEIGHT);
        cv::line(rgbImage, pt, pt2, colour);
        cv::line(dbgImage, pt, pt2, colour);


        map.Draw();
        field.Draw();
    }


    if(showVideo)
    {
        //cout << "displaying frames" << endl;
        imshow("Map", map.mapImage);
        imshow("Field",field.fieldImage);
        imshow("Webcam", rgbImage);
        imshow("Scanline", dbgImage);
        //imshow("Webcam", YuvCamera::GetInstance()->img);
        cvWaitKey(1);
        //cout << "done" << endl;
    }
    if(recordVideo)
    {
        webcamStream << rgbImage;
        debugStream << dbgImage;
        mapStream << map.mapImage;
    }
}

void SoccerPlayer::handleLostBall()
{
    lostFrames++;
    if(lostFrames >= LOST_FRAMES_BEFORE_SCAN)
    {
        headTrackingPan = Head::GetInstance()->GetPanAngle();
        headTrackingTilt = Head::GetInstance()->GetTiltAngle();

        dPan = 5 * ((rand() % 2 == 0) ? 1 : -1);
        dTilt = 5 * ((rand() % 2 == 0) ? 1 : -1);
        lookCycles = 0;

        myState = STATE_LOOKING;
        playSubstate = SUBSTATE_CHASING_BALL;

        Walking::GetInstance()->Stop();
    }
}

void SoccerPlayer::scan(bool scanLow)
{
    int tilt;
    double speedAmplitude;
    double xSpeed = lastBallInfo->getXSpeed();
    double ySpeed = lastBallInfo->getYSpeed();
    bool panValid, tiltValid;

    if(firstSweep && lastBallInfo->getXSpeed() != -1)
    {
        speedAmplitude = sqrt(xSpeed*xSpeed + ySpeed*ySpeed);
        panValid = headPan-xSpeed/speedAmplitude*SCAN_AMPLITUDE<=MAX_SCAN_PAN && headPan+xSpeed/speedAmplitude*SCAN_AMPLITUDE>=MIN_SCAN_PAN;
        tiltValid = headTilt-ySpeed/speedAmplitude*SCAN_AMPLITUDE<=MAX_SCAN_TILT && headTilt-ySpeed/speedAmplitude*SCAN_AMPLITUDE>=MIN_SCAN_TILT;

        if(panValid && tiltValid)
        {
            headPan -= xSpeed/speedAmplitude*SCAN_AMPLITUDE;
            headTilt -= ySpeed/speedAmplitude*SCAN_AMPLITUDE;
            Head::GetInstance()->MoveByAngle(headPan,headTilt);
        }
        else if(panValid)
        {
            if(xSpeed > 0)
            {
                panValid = headPan-SCAN_AMPLITUDE<=MAX_SCAN_PAN && headPan-SCAN_AMPLITUDE>=MIN_SCAN_PAN;
                if(panValid)
                {
                    headPan -= SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
            else
            {
                panValid = headPan+SCAN_AMPLITUDE<=MAX_SCAN_PAN && headPan+SCAN_AMPLITUDE>=MIN_SCAN_PAN;
                if(panValid)
                {
                    headPan += SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
        }
        else if(tiltValid)
        {
            if(ySpeed > 0)
            {
                tiltValid = headTilt-SCAN_AMPLITUDE<=MAX_SCAN_TILT && headTilt-SCAN_AMPLITUDE>=MIN_SCAN_TILT;
                if(tiltValid)
                {
                    headTilt -= SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
            else
            {
                tiltValid = headTilt+SCAN_AMPLITUDE<=MAX_SCAN_TILT && headTilt+SCAN_AMPLITUDE>=MIN_SCAN_TILT;
                if(tiltValid)
                {
                    headTilt += SCAN_AMPLITUDE;
                    Head::GetInstance()->MoveByAngle(headPan,headTilt);
                }
                else
                    firstSweep = false;
            }
        }
        else
        {
            firstSweep = false;
        }

    }
    else
    {
        if(scanDirection == 0)
        {
            if(lastBallInfo->getTheta() > headPan)
                scanDirection = 1;
            else
                scanDirection = -1;
        }

        if(scanDirection == 1 && scanLow)
            tilt = LOW_SCAN_TILT;
        else
            tilt = HIGH_SCAN_TILT;

        if((scanDirection<0 || headPan+SCAN_AMPLITUDE*scanDirection<=MAX_SCAN_PAN) && (scanDirection>0 || headPan+SCAN_AMPLITUDE*scanDirection>=MIN_SCAN_PAN))
        {
            headPan+=SCAN_AMPLITUDE*scanDirection;
            headTilt = tilt;
            Head::GetInstance()->MoveByAngle(headPan,headTilt);
        }
        else
        {
            scanDirection *= -1;
            lookCycles++;
        }
    }
}

void SoccerPlayer::quickScan()
{

    Head *head = Head::GetInstance();   // cached reference to our head
    int oldHeadTilt;                    // stored head angles
    int oldHeadPan;

    // save our old head orientation
    oldHeadTilt = headTilt;
    oldHeadPan = headPan;

    // begin waiting for a frame
    pauser.threadWaitHead();

    // move to look high and center
    standUp();
    head -> MoveByAngle(0, 60);

    for(int i=0; i<2; i++)
    {
        pauser.waitAMoment();

        if(goal.WasFound())
            foundGoalDuringQuickScan = true;
    }

    // restore our old head position
    head -> MoveByAngle(oldHeadPan, oldHeadTilt);
    pauser.waitAMoment();
    pauser.headHasReacted();

    /*
    double tilt;

    lookCycles = 0;
    scanDirection = 0;
    firstSweep = false;

    while(headMoving && lookCycles<2)
    {
        pauser.threadWaitHead();
        if(scanDirection == 0)
        {
            if(lastBallInfo->getTheta() > headPan)
                scanDirection = 1;
            else
                scanDirection = -1;
        }

        tilt = Head::GetInstance()->GetTopLimitAngle();

        if((scanDirection<0 || headPan+SCAN_AMPLITUDE*scanDirection<=MAX_SCAN_PAN) && (scanDirection>0 || headPan+SCAN_AMPLITUDE*scanDirection>=MIN_SCAN_PAN))
        {
            headPan+=SCAN_AMPLITUDE*scanDirection;
            headTilt = tilt;
            Head::GetInstance()->MoveByAngle(headPan,headTilt);
        }
        else
        {
            scanDirection *= -1;
            lookCycles++;
        }
        pauser.headHasReacted();
    }
    */
}

void SoccerPlayer::scanAround()
{
    while(pauser.getHeadReacted() || !headMoving)
    {
        pauser.threadWait();
    }
    if(ball.WasFound())
    {
        if(scanFrames >= FRAMES_AFTER_SCAN)
            Head::GetInstance()->LookAt(ball);
        else
            Head::GetInstance()->LookAt(ball, HEAD_DAMPING_FACTOR);
        MotionManager::Sync();
        headPan = Head::GetInstance()->GetPanAngle();
        headTilt = Head::GetInstance()->GetTiltAngle();

        scanFrames++;
        scanDirection = 0;
        firstSweep = true;
        lostFrames = 0;
        lookCycles = 0;

        if(!isGoalie)
            myState = STATE_PLAYING;
    }
    else
    {
        scanFrames = 0;
        if(lostFrames>=LOST_FRAMES_BEFORE_SCAN)
        {
            if(isGoalie)
                scan(((Goalie*)this)->getState() != STATE_PASSIVE);
            else
                scan(true);
        }
        else
        {
            lostFrames++;
        }

        if(!isGoalie && lookCycles >= 4 && !Walking::GetInstance()->IsRunning())
        {
            cout << "[DEBUG] " << lookCycles << endl;
            if(lookCycles <= 16)
            {
                // start pivoting on the spot
                Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
                if(map.ballPosition.Y > 0)
                    Walking::GetInstance()->A_MOVE_AMPLITUDE = 14;
                else
                    Walking::GetInstance()->A_MOVE_AMPLITUDE = -14;
                Walking::GetInstance()->Start();
            }
            else
            {
                // Go somewhere else and look for the ball
                Point3D myPosition( field.myPenaltyMark.X, field.myPenaltyMark.Y, field.presentPosition.Z);
                Point3D theirPosition( field.theirPenaltyMark.X, field.theirPenaltyMark.Y, field.presentPosition.Z);
                const float myDistance = Point3D::Distance(field.presentPosition, myPosition);
                const float theirDistance = Point3D::Distance(field.presentPosition, theirPosition);
                if( myDistance < 10 || theirDistance < myDistance ) {
                    walkTo(theirPosition);
                } else {
                    walkTo(myPosition);
                }
                lookCycles = 4;
            }
        }
    }
    pauser.headHasReacted();
}

// swivel the head around until we find the ball
void SoccerPlayer::lookAround()
{
    const int MAX_PAN = 60;
    const int MIN_PAN = -60;
    const int MAX_TILT = 30;
    const int MIN_TILT = -40;

    //cout << "\tLooking around (" << headTrackingPan << " , " << headTrackingTilt << ")" << endl;

    pauser.threadWaitHead();
    Head::GetInstance()->MoveByAngle(headTrackingPan,headTrackingTilt);

    if(ball.WasFound())
    {
        //cout << "\tFound ball!" << endl;
        Head::GetInstance()->LookAt(ball);
        headTrackingPan = Head::GetInstance()->GetPanAngle();
        headTrackingTilt = Head::GetInstance()->GetTiltAngle();

        myState = STATE_PLAYING;
    }
    else
    {
        //cout << "\tNo ball (" << dPan << " , " << dTilt << ")" << endl;
        headTrackingPan += dPan;

        if(headTrackingPan < MIN_PAN)
            headTrackingPan = MIN_PAN;
        else if(headTrackingPan> MAX_PAN)
            headTrackingPan = MAX_PAN;

        if(headTrackingTilt < MIN_TILT)
            headTrackingTilt = MIN_TILT;
        else if(headTrackingTilt> MAX_TILT)
            headTrackingTilt = MAX_TILT;

        if((headTrackingPan <= MIN_PAN && dPan < 0) || (headTrackingPan >= MAX_PAN && dPan > 0))
        {
            dPan *= -1;

            headTrackingTilt += dTilt;
            if((headTrackingTilt <= MIN_TILT && dTilt < 0) || (headTrackingTilt >= MAX_TILT && dTilt >0))
            {
                dTilt *= -1;
                lookCycles++;
            }
        }

        // we've swiveled the head through the entire range of motion
        if(!isGoalie && lookCycles == 2 && !Walking::GetInstance()->IsRunning())
        {
            Walking::GetInstance()->Start();
            Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = dPan * 2;  // start pivoting on the spot
        }
    }

    pauser.headHasReacted();
}

// send a packet describing the robot's current state
void SoccerPlayer::broadcastPacket()
{
    static struct SocketMonitor::RobotStateInformation packetData;

    packetData.myId = myId;

    packetData.myPosition.x = SocketMonitor::Range2Packet(field.presentPosition.X);
    packetData.myPosition.y = SocketMonitor::Range2Packet(field.presentPosition.Y);
    packetData.myPosition.a = SocketMonitor::Angle2Packet(field.presentPosition.Z);

    packetData.ball.range = SocketMonitor::Range2Packet(map.ballPosition.X);
    packetData.ball.angle = SocketMonitor::Angle2Packet(map.ballPosition.Y);

    packetData.myGoal.leftPost.range = SocketMonitor::Range2Packet(map.myGoalPosition[SoccerMap::LEFT].X);
    packetData.myGoal.leftPost.angle = SocketMonitor::Angle2Packet(map.myGoalPosition[SoccerMap::LEFT].Y);
    packetData.myGoal.rightPost.range = SocketMonitor::Range2Packet(map.myGoalPosition[SoccerMap::RIGHT].X);
    packetData.myGoal.rightPost.angle = SocketMonitor::Angle2Packet(map.myGoalPosition[SoccerMap::RIGHT].Y);

    packetData.theirGoal.leftPost.range = SocketMonitor::Range2Packet(map.theirGoalPosition[SoccerMap::LEFT].X);
    packetData.theirGoal.leftPost.angle = SocketMonitor::Angle2Packet(map.theirGoalPosition[SoccerMap::LEFT].Y);
    packetData.theirGoal.rightPost.range = SocketMonitor::Range2Packet(map.theirGoalPosition[SoccerMap::RIGHT].X);
    packetData.theirGoal.rightPost.angle = SocketMonitor::Angle2Packet(map.theirGoalPosition[SoccerMap::RIGHT].Y);

    for(int i=0; i<2; i++)
    {
        packetData.myTeam[i].range = SocketMonitor::Range2Packet(map.myTeam[i].X);
        packetData.myTeam[i].angle = SocketMonitor::Angle2Packet(map.myTeam[i].Y);
    }

    for(int i=0; i<3; i++)
    {
        packetData.theirTeam[i].range = SocketMonitor::Range2Packet(map.theirTeam[i].X);
        packetData.theirTeam[i].angle = SocketMonitor::Angle2Packet(map.theirTeam[i].Y);
    }

    packetData.myState = myState;
    packetData.playSubstate = playSubstate;
    packetData.gameState = gameState;

    packetData.sequenceNum = numPacketsSent;

    //cout << "Transmitting packet #" << numPacketsSent << endl;
    socketMonitor->transmitPacket(packetData);

    numPacketsSent++;
}

void SoccerPlayer::sitDown()
{
    // stop walking and wait until we can act again
    //Head::GetInstance()->MoveByAngle(0,10);
    Walking::GetInstance()->Stop();
    Walking::GetInstance()->Finish();


    // sit down
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_SIT_DOWN);
    Action::GetInstance()->Finish();
}

void SoccerPlayer::standUp()
{
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_WALKREADY);
    Action::GetInstance()->Finish();
    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND, true, true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND, true, true);
}

void SoccerPlayer::transmitStatus()
{
    if(!noNetwork)
    {
        struct timespec time;
        clock_gettime(CLOCK_MONOTONIC, &time);
        unsigned long int ms = (time.tv_sec * 1000000000 + time.tv_nsec) / 1000000;

        if(ms - lastBroadcastTime >= BROADCAST_INTERVAL)
        {
            lastBroadcastTime = ms;
            broadcastPacket();
        }
    }
}

bool SoccerPlayer::handleTests()
{
    bool ans = false;

    if(visionTest)
    {
        map.UpdateOdometry(0,0,0,0);
        ans = true;
    }
    else if(networkTest)
    {
        cout << "Game State: " << socketMonitor->getGameState() << endl <<
                " Time left: " << socketMonitor->getTimeRemaining() << endl << endl;
        ans = true;
    }

    return ans;
}

int SoccerPlayer::findGameState()
{
    if(!noNetwork)
        gameState = socketMonitor->getGameState();
    else
        gameState = SocketMonitor::STATE_PLAY;

    newGameState = lastGameState != gameState;
    lastGameState = gameState;

    return gameState;
}

void SoccerPlayer::adjustHipPitch()
{
    if(Walking::GetInstance()->IsRunning())
    {
        if(Walking::GetInstance()->X_MOVE_AMPLITUDE < 0)
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Soccer","hip_pitch_offset_lo");
        }
        else if(Walking::GetInstance()->X_MOVE_AMPLITUDE > 10)
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Soccer","hip_pitch_offset_hi");
        }
        else
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd("Walking Config","hip_pitch_offset");
        }
#ifdef DEBUG
        cout << Walking::GetInstance()->X_MOVE_AMPLITUDE << " " <<
                Walking::GetInstance()->HIP_PITCH_OFFSET << endl;
#endif
    }
}
