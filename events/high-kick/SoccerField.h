#ifndef SOCCERFIELD_H
#define SOCCERFIELD_H

#include <SoccerMap.h>
#include <opencv2/core/core.hpp>

// represents the robot's absolute position on the soccer field
// allows us to designate "home" positions
class SoccerField
{
public:
    SoccerField();
    void Initialize(SoccerMap *polarMap, minIni &ini);
    virtual ~SoccerField();

    // update our position on the field (along with positions of everything else we think we know about)
    void Update();

    // the robot's current position on the field relative to our back left corner (in cm) (Z = orientation in degrees)
    // NOTE: confidence is not currently used
    Robot::Point3D presentPosition;
    bool positionKnown;
    bool orientationKnown;
    double positionConfidence;

    // override the robot's current position with whatever we tell it
    // this will clear everything in the polar map and reset it with hard, known values for fixed objects
    void SetPosition(Robot::Point3D position);

    // draw an OpenCV image of the field and objects' positions on it
    void Draw();
    cv::Mat fieldImage;

    // coordinates for fixed points on the field
    // all values are in cm
    // NOTE: this is different than most other classes that use mm
    Robot::Point2D myGoalLeftPost;
    Robot::Point2D myGoalRightPost;
    Robot::Point2D myGoalCenter;
    Robot::Point2D theirGoalLeftPost;
    Robot::Point2D theirGoalRightPost;
    Robot::Point2D theirGoalCenter;
    Robot::Point2D myPenaltyMark;
    Robot::Point2D theirPenaltyMark;
    Robot::Point2D centre;
    Robot::Point2D kickoffPosition;

    int GetFieldLengthCM(){return fieldLength;}
    int GetFieldWidthCM(){return fieldWidth;}
    int GetCreaseWidthCM(){return creaseWidth;}
    int GetCreaseDepthCM(){return creaseLength;}
    int GetCenterRadiusCM(){return centreRadius;}
    int GetPenaltyMarkDistanceCM(){return penaltyMarkDistance;}
    int GetGoalWidthCM(){return goalWidth;}

    // convert an angle so that it lies within a standard [-180,180] range
    static double normalizeAngle(double degrees);

private:
    SoccerMap *polarMap;

    // field dimensions in cm
    int fieldLength;
    int fieldWidth;
    int goalWidth;
    int goalDepth;
    int creaseWidth;
    int creaseLength;
    int centreRadius;
    int penaltyMarkDistance;
    int marginWidth;

    void LoadIniSettings(minIni &ini, const char* section);

    static const cv::Scalar WHITE;
    static const cv::Scalar BLACK;
    static const cv::Scalar BLUE;
    static const cv::Scalar RED;
    static const cv::Scalar YELLOW;
    static const cv::Scalar GREEN;
    static const cv::Scalar ORANGE;
    static const cv::Scalar CYAN;
    static const cv::Scalar MAGENTA;
    static const cv::Scalar DK_GREEN;



};

#endif // SOCCERFIELD_H
