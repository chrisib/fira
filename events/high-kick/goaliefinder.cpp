#include "goaliefinder.h"
#include "util.h"

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <vector>
#include <cmath>

using namespace cv;
using namespace std;

// names of the windows for training/tracking
const char *GoalieFinder::MAIN_WINDOW = "GF Main";
const char *GoalieFinder::THRESHOLD_WINDOW = "GF Threshold";

// singleton instance of the goal post tracker
GoalieFinder *GoalieFinder::trackerInstance = NULL;

// private, forcing us to use a singleton approach
GoalieFinder::GoalieFinder()
{

	fieldThreshold = 150;
	houghMinVotes = 50;//90;
	houghMinLength = 50;//150;//230;//64;//70;
	houghMaxGap = 170;//100;//50;
	windowsOpen = false;

}

// destructor
GoalieFinder::~GoalieFinder()
{
	cout << "GoalieFinder: Closing Tracking Windows" << endl;

	closeTrackingWindows();

	trackerInstance = NULL;
}

// factory method that returns our singleton instance
GoalieFinder *GoalieFinder::create()
{

	if(trackerInstance == NULL)
	{
		trackerInstance = new GoalieFinder();
	}

	return trackerInstance;
}

// pass in a frame for the high gui windows and the goal post detection
void GoalieFinder::update(Mat &frame)
{
	frame.copyTo(originalFrame);
}

// returns the last frame passed in
void GoalieFinder::getOriginalFrame(Mat &frame)
{
	originalFrame.copyTo(frame);
}

// attempts to locate the goal posts in the given image matrix; returns
// true if found, and updates the location of the posts internally, which
// can be accessed by the get() functions below
bool GoalieFinder::findGoalLine()
{

	const Scalar HOUGH_LINE_COLOR(0, 0, 255);		// red
	const Scalar MERGED_LINE_COLOR(0, 255, 255);	// yellow
	const Scalar LONGEST_LINE_COLOR(0, 255, 255);	// purple

	const int CANNY_THRESHOLD1 = 160;
	const int CANNY_THRESHOLD2 = 200;

	vector<Vec4i> lines;				// initial group of lines found by Hough line detector
	vector<Vec4i> merged;				// group of lines after a narrow merge pass
	Vec4i longest;						// longest line we've found is probably the goalie line

	unsigned int i;
	bool result = false;				// assume we didn't find the goal line

	// we only need a greyscale image
	cvtColor(originalFrame, thresholdFrame, CV_BGR2GRAY);

	// eliminate obvious noise and do a threshold based on our slider
	erode(thresholdFrame, thresholdFrame, Mat(), Point(-1, -1), 4);
	dilate(thresholdFrame, thresholdFrame, Mat(), Point(-1, -1), 4);
	threshold(thresholdFrame, thresholdFrame, fieldThreshold, 256, THRESH_BINARY);

	// perform canny edge detection on our thresholded image
	Canny(thresholdFrame, thresholdFrame, CANNY_THRESHOLD1, CANNY_THRESHOLD2, 3);

	// perform probabilistic hough-space line detection
	HoughLinesP(thresholdFrame, lines, 1, CV_PI/180, houghMinVotes, houghMinLength, houghMaxGap);

	// do a small pass over the lines to remove any closely-related duplicates
	mergeLines(lines, merged, 10, 0);

	// update the windows with the tracking data
	//if(windowsOpen)
	//{

		// draw the groups of lines we've detected
		/*
		for(i = 0; i < lines.size(); i++ )
		{
			line(originalFrame, Point(lines[i][0], lines[i][1]),
				 Point(lines[i][2], lines[i][3]), HOUGH_LINE_COLOR, 1, CV_AA);
		}*/

		for(i = 0; i < merged.size(); i++ )
		{
			line(originalFrame, Point(merged[i][0], merged[i][1]),
				 Point(merged[i][2], merged[i][3]), MERGED_LINE_COLOR, 3, CV_AA);
		}

		// if we've found at least one line
		if(merged.size() > 0)
		{

			// look for the longest line
			longest = Util::getClosestLongestLine(merged);

			// draw the longest line onscreen
			line(originalFrame, Point(longest[0], longest[1]),
				 Point(longest[2], longest[3]), MERGED_LINE_COLOR, 3, CV_AA);

			// get the angle of this line
			lastGoalLineAngle = Util::radToDeg(atan2(longest[3] - longest[1], longest[2] - longest[0]));
			lastGoalLine = longest;
			result = true;

			//cout << lastGoalLineAngle << endl;

		}

		// update window contents
		if(windowsOpen) {
			imshow(MAIN_WINDOW, originalFrame);
			imshow(THRESHOLD_WINDOW, thresholdFrame);
		}

	//}

	return result;

}

// opens training windows, and a window that labels the locations of the left and right goal posts
void GoalieFinder::openTrackingWindows()
{

	cout << "GoalieFinder: Opening Tracking Windows" << endl;

	namedWindow(MAIN_WINDOW, 1);
	namedWindow(THRESHOLD_WINDOW, 1);

	// only the first window is responsible for training
	cvCreateTrackbar("threshold", MAIN_WINDOW, &fieldThreshold, 254, NULL);
	cvCreateTrackbar("min votes", MAIN_WINDOW, &houghMinVotes, 250, NULL);
	cvCreateTrackbar("min length", MAIN_WINDOW, &houghMinLength, 250, NULL);
	cvCreateTrackbar("max gap", MAIN_WINDOW, &houghMaxGap, 250, NULL);

	windowsOpen = true;

}

// closes the training windows
void GoalieFinder::closeTrackingWindows()
{
	destroyWindow(MAIN_WINDOW);
	destroyWindow(THRESHOLD_WINDOW);
}

// maxDistance is the maxmimum distance of two lines' endpoints at which
// the lines will be merged into one; minVotes is the minimum number of lines
// that must be nearby for them to be considered a merged line and not discarded
void GoalieFinder::mergeLines(vector<Vec4i> &lines, vector<Vec4i> &merged, int maxDistance, int minVotes)
{

	vector<Vec4i>::iterator i;
	vector<Vec4i>::iterator j;
	vector<int>::iterator m;
	vector<int>::iterator n;
	vector<int> votes;

	int voteCount;
	unsigned int k;

	Vec4i curr1;
	Vec4i curr2;

	// each line has an initial merge vote of zero
	for(k = 0; k < lines.size(); k ++)
	{
		votes.push_back(0);
	}

	// begin traversing the lines and the vote count at the beginning
	i = lines.begin();
	m = votes.begin();

	while(i != lines.end())
	{

		// start at the line after this one, so we only consider line-line pairs once,
		// and also avoid comparing the same line
		j = i + 1;
		n = m + 1;

		while(j != lines.end())
		{

			curr1 = *i;
			curr2 = *j;

			// reorder the points of the two lines such that
			// their end points are closest
			Util::orderLinePointsTogether(curr1, curr2);

			// if the two lines' endpoints are sufficiently close,
			// merge the two lines into one
			if(Util::linePointsNearby(curr1, curr2, maxDistance))
			{

				// remove the lines that were used to make the merge;
				// this needs to occur before we add the merged lines
				// so that we don't invalidate the iterators by
				// reallocating space for the vector by adding first
				lines.erase(j);
				i = lines.erase(i);
				j = i + 1;

				// same as above, only with the votes; ensure that
				// we sum the votes for both lines, and add those
				// votes to the newly created merged line
				voteCount = (*m) + (*n);
				votes.erase(n);
				m = votes.erase(m);
				n = m + 1;

				// merge the two lines and increase the vote count of the merged line
				lines.push_back(Util::averageLines(curr1, curr2));
				votes.push_back(voteCount + 1);

			}
			else
			{
				// we only increment the inner counter if we don't merge anything,
				// so that we don't skip over elements when we delete lines
				j ++;
				n ++;
			}

		}

		i ++;
		m ++;

	}

	// now, add the merged lines that have a sufficiently high merge count
	// to the final merge line list
	i = lines.begin();
	m = votes.begin();

	while(i != lines.end())
	{
		if(*m >= minVotes)
		{
			merged.push_back(*i);
		}

		i ++;
		m ++;
	}

}

int GoalieFinder::getLastGoalLineAngle()
{
	return lastGoalLineAngle;
}

Vec4i GoalieFinder::getLastGoalLine()
{
	return lastGoalLine;
}
