#include "SoccerField.h"
#include <iostream>
#include <darwin/framework/Math.h>
#include <cmath>
#include <cstdlib>
#include <cstring>

using namespace Robot;
using namespace cv;
using namespace std;


const Scalar SoccerField::WHITE = Scalar(255,255,255);
const Scalar SoccerField::BLACK = Scalar(0,0,0);
const Scalar SoccerField::BLUE = Scalar(255,0,0);
const Scalar SoccerField::RED = Scalar(0,0,255);
const Scalar SoccerField::YELLOW = Scalar(0,255,255);
const Scalar SoccerField::GREEN = Scalar(0,255,0);
const Scalar SoccerField::ORANGE = Scalar(0,128,255);
const Scalar SoccerField::CYAN = Scalar(255,255,0);
const Scalar SoccerField::MAGENTA = Scalar(255,0,255);
const Scalar SoccerField::DK_GREEN = Scalar(0,128,0);

SoccerField::SoccerField()
{
}

SoccerField::~SoccerField()
{
}

void SoccerField::Initialize(SoccerMap *polarMap, minIni &ini)
{
    this->polarMap = polarMap;

    LoadIniSettings(ini,"Field");

    this->fieldImage = Mat::zeros(fieldLength + 2*goalDepth, fieldWidth + 2*marginWidth, CV_8UC3);

    myGoalLeftPost = Point2D(fieldWidth/2 - goalWidth/2, 0);
    myGoalRightPost = Point2D(fieldWidth/2 + goalWidth/2, 0);
    myGoalCenter = Point2D(fieldWidth/2, 0);
    myPenaltyMark = Point2D(fieldWidth/2, penaltyMarkDistance);
    centre = Point2D(fieldWidth/2, fieldLength/2);
    theirPenaltyMark = Point2D(fieldWidth/2, fieldLength - penaltyMarkDistance);
    theirGoalLeftPost = Point2D(fieldWidth/2 - goalWidth/2, fieldLength);
    theirGoalRightPost = Point2D(fieldWidth/2 + goalWidth/2, fieldLength);
    theirGoalCenter = Point2D(fieldWidth/2, fieldLength);

    kickoffPosition = Point2D(centre.X, centre.Y - 30.0);   // 30cm back from the center point
}

void SoccerField::SetPosition(Point3D position)
{
    polarMap->Clear();

    // now set the polar positions of the goal posts according to our position
    // NOTE: distances need to be converted to mm
    double distance, angle;

    //cout << position.X << " " << position.Y << " " << position.Z << endl;

    // our goal left post
    distance = sqrt(pow(position.X - myGoalLeftPost.X,2.0) + pow(position.Y - myGoalLeftPost.Y,2.0)) * 10;
    angle = 180 - rad2deg(atan2(position.X - myGoalLeftPost.X, position.Y - myGoalLeftPost.Y)) - position.Z;
    angle = normalizeAngle(angle);
    polarMap->myGoalPosition[SoccerMap::LEFT].X = distance;
    polarMap->myGoalPosition[SoccerMap::LEFT].Y = angle;
    //cout << distance << " " << angle << endl;

    // our goal right post
    distance = sqrt(pow(position.X - myGoalRightPost.X,2.0) + pow(position.Y - myGoalRightPost.Y,2.0)) * 10;
    angle = 180 - rad2deg(atan2(position.X - myGoalRightPost.X, position.Y - myGoalRightPost.Y)) - position.Z;
    angle = normalizeAngle(angle);
    polarMap->myGoalPosition[SoccerMap::RIGHT].X = distance;
    polarMap->myGoalPosition[SoccerMap::RIGHT].Y = angle;
    //cout << distance << " " << angle << endl;

    // their goal left post
    distance = sqrt(pow(position.X - theirGoalLeftPost.X,2.0) + pow(position.Y - theirGoalLeftPost.Y,2.0)) * 10;
    angle = 180- rad2deg(atan2(position.X - theirGoalLeftPost.X, position.Y - theirGoalLeftPost.Y)) - position.Z;
    angle = normalizeAngle(angle);
    polarMap->theirGoalPosition[SoccerMap::LEFT].X = distance;
    polarMap->theirGoalPosition[SoccerMap::LEFT].Y = angle;
    //cout << distance << " " << angle << endl;

    // their goal right post
    distance = sqrt(pow(position.X - theirGoalRightPost.X,2.0) + pow(position.Y - theirGoalRightPost.Y,2.0)) * 10;
    angle = 180 - rad2deg(atan2(position.X - theirGoalRightPost.X, position.Y - theirGoalRightPost.Y)) - position.Z;
    angle = normalizeAngle(angle);
    polarMap->theirGoalPosition[SoccerMap::RIGHT].X = distance;
    polarMap->theirGoalPosition[SoccerMap::RIGHT].Y = angle;
    //cout << distance << " " << angle << endl;

    Update();
}

void SoccerField::Draw()
{
    const Point ctr = Point(fieldImage.cols/2, fieldImage.rows/2);
    const Point ctrLeft = Point(marginWidth, fieldImage.rows/2);
    const Point ctrRight = Point(fieldWidth+marginWidth, fieldImage.rows/2);
    const Point tl = Point(marginWidth, goalDepth);
    const Point tr = Point(fieldWidth+marginWidth, goalDepth);
    const Point bl = Point(marginWidth, fieldLength + goalDepth);
    const Point br = Point(fieldWidth+marginWidth, fieldLength + goalDepth);

    Point me, pt;

    // clear the entire field
    rectangle(fieldImage,Point(0,0),Point(fieldImage.cols, fieldImage.rows),BLACK,-1);

    // draw the field lines & goal posts
    rectangle(fieldImage,tl,br,WHITE,5);            // touch lines
    line(fieldImage,ctrLeft,ctrRight,WHITE,5);      // centre line
    circle(fieldImage,ctr,centreRadius, WHITE,5);   // centre circle
    circle(fieldImage,ctr,10,WHITE,-1);

    // penalty marks
    circle(fieldImage,Point(ctr.x, br.y - penaltyMarkDistance), 10, WHITE, -1);
    circle(fieldImage,Point(ctr.x, tr.y + penaltyMarkDistance), 10, WHITE, -1);

    // creases
    rectangle(fieldImage,Point(ctr.x - creaseWidth/2, bl.y - creaseLength), Point(ctr.x + creaseWidth/2, bl.y), WHITE, 5);
    rectangle(fieldImage,Point(ctr.x - creaseWidth/2, tl.y + creaseLength), Point(ctr.x + creaseWidth/2, tl.y), WHITE, 5);

    // our goal (at the bottom)
    rectangle(fieldImage, Point(ctr.x - goalWidth/2, bl.y), Point(ctr.x + goalWidth/2, bl.y + goalDepth), GREEN, 5);

    // their goal (at the top)
    rectangle(fieldImage, Point(ctr.x - goalWidth/2, tl.y), Point(ctr.x + goalWidth/2, tl.y - goalDepth), RED, 5);

    // write our position in the corner
    char buffer[255];
    sprintf(buffer,"(%0.2f %0.2f) @ %0.2f",presentPosition.X, presentPosition.Y, presentPosition.Z);
    putText(fieldImage, buffer, Point(0, fieldImage.rows-12),FONT_HERSHEY_PLAIN, 1.0, WHITE);

    if(polarMap->myGoalPosition[SoccerMap::LEFT].X > 0)
        circle(fieldImage, Point(ctr.x - goalWidth/2, bl.y), polarMap->myGoalPosition[SoccerMap::LEFT].X/10,CYAN,1);

    if(polarMap->myGoalPosition[SoccerMap::RIGHT].X > 0)
        circle(fieldImage, Point(ctr.x + goalWidth/2, bl.y), polarMap->myGoalPosition[SoccerMap::RIGHT].X/10,CYAN,1);

    if(polarMap->theirGoalPosition[SoccerMap::LEFT].X > 0)
        circle(fieldImage, Point(ctr.x - goalWidth/2, tl.y), polarMap->theirGoalPosition[SoccerMap::LEFT].X/10,MAGENTA,1);

    if(polarMap->theirGoalPosition[SoccerMap::RIGHT].X > 0)
        circle(fieldImage, Point(ctr.x + goalWidth/2, tl.y), polarMap->theirGoalPosition[SoccerMap::RIGHT].X/10,MAGENTA,1);


    // our position on the field
    me = Point(bl.x + presentPosition.X, bl.y - presentPosition.Y);

    // draw us with a line indicating orientation
    pt = Point(me.x - sin(deg2rad(presentPosition.Z)) * 20, me.y - cos(deg2rad(presentPosition.Z))*20);
    circle(fieldImage, me, 15, GREEN, -1);
    line(fieldImage, me, pt, GREEN, 4);

    // mark our position with a dark ring to indicate the uncertainty
    if(!positionKnown)
    {
        circle(fieldImage, me, 15, DK_GREEN, 2);
    }

    // draw the other players on our team (if we know where they are)
    for(int i=0; i<SoccerMap::OTHER_PLAYERS_ON_MY_TEAM; i++)
    {
        if(polarMap->myTeam[i].X >0)
        {
            pt.x = me.x - sin(deg2rad(presentPosition.Z + polarMap->myTeam[i].Y)) * polarMap->myTeam[i].X/10;
            pt.y = me.y - cos(deg2rad(presentPosition.Z + polarMap->myTeam[i].Y)) * polarMap->myTeam[i].X/10;

            circle(fieldImage, pt, 10, GREEN, -1);
        }
    }

    // draw opposing players
    for(int i=0; i<SoccerMap::PLAYERS_ON_THEIR_TEAM; i++)
    {
        if(polarMap->myTeam[i].X >0)
        {
            pt.x = me.x - sin(deg2rad(presentPosition.Z + polarMap->theirTeam[i].Y)) * polarMap->theirTeam[i].X/10;
            pt.y = me.y - cos(deg2rad(presentPosition.Z + polarMap->theirTeam[i].Y)) * polarMap->theirTeam[i].X/10;

            circle(fieldImage, pt, 10, RED, -1);
        }
    }

    // draw the ball
    if(polarMap->ballPosition.X > 0)
    {
        pt.x = me.x - sin(deg2rad(presentPosition.Z + polarMap->ballPosition.Y)) * polarMap->ballPosition.X/10;
        pt.y = me.y - cos(deg2rad(presentPosition.Z + polarMap->ballPosition.Y)) * polarMap->ballPosition.X/10;

        circle(fieldImage, pt, 8, ORANGE, -1);
    }
}

void SoccerField::LoadIniSettings(minIni &ini, const char* section)
{
    fieldLength = ini.geti(section,"FieldLength");
    fieldWidth = ini.geti(section,"FieldWidth");
    goalWidth = ini.geti(section,"GoalWidth");
    goalDepth = ini.geti(section,"GoalDepth");
    centreRadius = ini.geti(section,"CentreRadius");
    creaseWidth = ini.geti(section,"CreaseWidth");
    creaseLength = ini.geti(section,"CreaseLength");
    penaltyMarkDistance = ini.geti(section,"PenaltyMarkDistance");
    marginWidth = ini.geti(section,"MarginWidth");
}

void SoccerField::Update()
{
    Point3D bestGuessPosition(0,0,0);
    int numGuesses = 0;

    // based on the position of our goal and/or their goal triangulate our position on the field
    if(polarMap->myGoalPosition[SoccerMap::LEFT].X > 0 && polarMap->myGoalPosition[SoccerMap::RIGHT].X > 0)
    {
        // let's assume that the left post is at (0,0) and that the right post is on the x-axis, offset to the right by the width of the goal
        // we need to find the point of intersection between the circles formed around each goal post (there will be 2, but one will be behind the goal)

        // get the radii in cm
        double leftRadius = polarMap->myGoalPosition[SoccerMap::LEFT].X/10.0;
        double rightRadius = polarMap->myGoalPosition[SoccerMap::RIGHT].X/10.0;

        // x position of the intersection
        double x = (pow(goalWidth,2.0) - pow(rightRadius,2.0) + pow(leftRadius,2.0)) / (2 * goalWidth);

        // substite x back into the equation for a circle...
        double y = pow(leftRadius,2.0) - pow(x,2.0);

        // avoid NaN errors with sqrt
        if(y >= 0)
        {
            y = sqrt(y);

            // offset the solved positions by the actual goal post position
            x += myGoalLeftPost.X;
            y += myGoalLeftPost.Y;

            //cerr << "(" << x <<" , " << y << ")" << endl;

            bestGuessPosition.X += x;
            bestGuessPosition.Y += y;

            numGuesses++;
        }
    }

    if(polarMap->theirGoalPosition[SoccerMap::LEFT].X > 0 && polarMap->theirGoalPosition[SoccerMap::RIGHT].X > 0)
    {
        // we can see their goal

        // let's assume that the left post is at (0,0) and that the right post is on the x-axis, offset to the right by the width of the goal
        // we need to find the point of intersection between the circles formed around each goal post (there will be 2, but one will be behind the goal)

        // get the radii in cm
        double leftRadius = polarMap->theirGoalPosition[SoccerMap::LEFT].X/10.0;
        double rightRadius = polarMap->theirGoalPosition[SoccerMap::RIGHT].X/10.0;

        // x position of the intersection
        double x = (pow(goalWidth,2.0) - pow(rightRadius,2.0) + pow(leftRadius,2.0)) / (2 * goalWidth);

        // substite x back into the equation for a circle...
        double y = pow(leftRadius,2.0) - pow(x,2.0);

        // avoid NaN errors with sqrt
        if(y >= 0)
        {
            y = -sqrt(y);

            // offset the solved positions by the actual goal post position
            x += theirGoalLeftPost.X;
            y += theirGoalLeftPost.Y;

            //cerr << "(" << x <<" , " << y << ")" << endl;

            bestGuessPosition.X += x;
            bestGuessPosition.Y += y;


            numGuesses++;
        }
    }

    if(numGuesses>0)
    {
        //cout << "[info] Triangulated our position with " << numGuesses << " guesses" << endl;
        bestGuessPosition.X /= numGuesses;
        bestGuessPosition.Y /= numGuesses;
        positionConfidence = 1.0;
        positionKnown = true;
    }
    else
    {
        //cout << "[info] Cannot determine X/Y position on the field.  Falling back to last known value" << endl;
        // if we really don't know where we are then just use our last known position
        bestGuessPosition.X = this->presentPosition.X;
        bestGuessPosition.Y = this->presentPosition.Y;
        positionConfidence = 0.0;
        positionKnown = false;
    }

    // assuming that we now have a rough guess of our X/Y position we can tackle the robot's orientation
    // this involves calculating the standard angle to each goal post from our present position and then
    // calculating the angular offset, resulting in a four-tab average
    numGuesses = 0;
    double x, y;

    if(polarMap->theirGoalPosition[SoccerMap::LEFT].X > 0)
    {
        x = theirGoalLeftPost.X - bestGuessPosition.X;
        y = theirGoalLeftPost.Y - bestGuessPosition.Y;
        double standardAngleTheirLeftPost = -normalizeAngle(rad2deg(atan2(x,y)));   // ahead-left
        double guessTheirLeftPost = normalizeAngle(-polarMap->theirGoalPosition[SoccerMap::LEFT].Y + standardAngleTheirLeftPost);

        bestGuessPosition.Z += guessTheirLeftPost;
        numGuesses++;
    }

    if(polarMap->theirGoalPosition[SoccerMap::RIGHT].X > 0)
    {
        x = theirGoalRightPost.X - bestGuessPosition.X;
        y = theirGoalRightPost.Y - bestGuessPosition.Y;
        double standardAngleTheirRightPost = -normalizeAngle(rad2deg(atan2(x,y)));   // ahead-right
        double guessTheirRightPost = normalizeAngle(-polarMap->theirGoalPosition[SoccerMap::RIGHT].Y + standardAngleTheirRightPost);

        bestGuessPosition.Z += guessTheirRightPost;
        numGuesses++;
    }

    if(polarMap->myGoalPosition[SoccerMap::LEFT].X > 0)
    {
        x = myGoalLeftPost.X - bestGuessPosition.X;
        y = myGoalLeftPost.Y - bestGuessPosition.Y;
        double standardAngleMyLeftPost = -normalizeAngle(rad2deg(atan2(x,y))); // back-left
        double guessMyLeftPost  = normalizeAngle(-polarMap->myGoalPosition[SoccerMap::LEFT].Y + standardAngleMyLeftPost);

        bestGuessPosition.Z += guessMyLeftPost;
        numGuesses++;
    }

    if(polarMap->myGoalPosition[SoccerMap::RIGHT].X > 0)
    {
        x = myGoalRightPost.X - bestGuessPosition.X;
        y = myGoalRightPost.Y - bestGuessPosition.Y;
        double standardAngleMyRightPost = -normalizeAngle(rad2deg(atan2(x,y)));   // back-right
        double guessMyRightPost  = normalizeAngle(-polarMap->myGoalPosition[SoccerMap::RIGHT].Y + standardAngleMyRightPost);

        bestGuessPosition.Z += guessMyRightPost;
        numGuesses++;
    }

    bestGuessPosition.Z = normalizeAngle(bestGuessPosition.Z/numGuesses);
    orientationKnown = true;

#if 0
    cout << standardAngleTheirLeftPost << " " <<
            standardAngleTheirRightPost << " " <<
            standardAngleMyLeftPost << " " <<
            standardAngleMyRightPost << " " <<
            bestGuessPosition.Z << endl <<
            guessTheirLeftPost << endl <<
            guessTheirRightPost << endl <<
            guessMyLeftPost << endl <<
            guessMyRightPost << endl <<
            endl;
#endif

    presentPosition.X = bestGuessPosition.X;
    presentPosition.Y = bestGuessPosition.Y;
    presentPosition.Z = bestGuessPosition.Z;
}

double SoccerField::normalizeAngle(double degrees)
{
    if(-180 <= degrees && 180 >= degrees)
        return degrees;

    while(degrees < -180)
        degrees += 360;
    while(degrees > 180)
        degrees -= 360;

    return degrees;
}
