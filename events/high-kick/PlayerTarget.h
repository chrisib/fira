#ifndef PLAYERTARGET_H
#define PLAYERTARGET_H

#include <darwin/framework/MultiBlob.h>
#include <vector>

class PlayerTarget : public Robot::MultiBlob
{
public:
    PlayerTarget();
    virtual ~PlayerTarget();

    virtual void LoadIniSettings(minIni &ini, const std::string &section);
    virtual void ProcessCandidates(std::vector<Robot::BoundingBox*> &candidates);


protected:
    int groupingThreshold;
    
private:
	PlayerTarget(PlayerTarget&);
	PlayerTarget& operator=(PlayerTarget&);
};

#endif // PLAYERTARGET_H
