#ifndef GOALPOSTTRACKER_H
#define GOALPOSTTRACKER_H

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include <vector>

class GoalPostTracker
{
public:

    // destructor
    ~GoalPostTracker();

    // this is a singleton class
    static GoalPostTracker *getInstance(){return uniqueInstance;}

    // widen the goal post color-tracking based on the passed color sample
    void train(cv::Vec3i color);

    // remove the color-tracking data for the goal post
    void unTrain();

    // pass in a frame for the high gui windows and the goal post detection
    void update(cv::Mat &frame);

    // returns the last frame passed in
    void getOriginalFrame(cv::Mat &frame);
    void getHSVFrame(cv::Mat &frame);

    // attempts to locate the goal posts in the given image matrix; returns
    // true if found, and updates the location of the posts internally, which
    // can be accessed by the get() functions below;
    // this method is also responsible for updating the 4 tracking windows,
    // if they have been opened by openTrackingWindow(), below
    bool findGoalPosts();

    // opens training windows, and a window that labels the locations of the left and right goal posts
    void openTrackingWindows();

    // closes the training windows, and the window that labels the locations of the left and right goal posts
    void closeTrackingWindows();

    // returns the vertices associated with the corners of the goal posts (ie,
    // top-left corner, etc.)
    // if findGoalPosts() was not successful, then these methods return the last
    // known location of the posts, even if this information is out-of-date
    // NOTE: these returned values are immediate; they are not averaged or weighted
    cv::Vec2i getTopLeft();
    cv::Vec2i getBottomLeft();
    cv::Vec2i getTopRight();
    cv::Vec2i getBottomRight();

    // returns the (top-down) lines associated with the goal posts
    // NOTE: these returned values are averages of the highest
    // GOAL_POST_AVERAGE_SIZE lengths in the leftPosts and rightPosts
    cv::Vec4i getLeftPostAverage();
    cv::Vec4i getRightPostAverage();

    // returns the lines associated with the goal posts, the result of which
    // is a 4-element vector that is accessed like this:
    // result[0] = topX
    // result[1] = topY;
    // result[2] = bottomX;
    // result[3] = bottomY;
    cv::Vec4i getLeft();
    cv::Vec4i getRight();


private:

    // names of the windows for training/tracking
    static const char *TRAINING_WINDOW;
    static const char *THRESHOLD_WINDOW;
    static const char *LINES_WINDOW;
    static const char *RESULT_WINDOW;

    // number maximum goal post positions to average
    static const int GOAL_POST_BUFFER_SIZE;
    static const int GOAL_POST_AVERAGE_SIZE;

    // we use this so we can refer to the GoalPostTracker from a static context
    static GoalPostTracker *uniqueInstance;

    // OpenCV mat frames
    cv::Mat originalFrame;
    cv::Mat hsv;
    cv::Mat thresholdFrame;
    cv::Mat houghFrame;
    cv::Mat finFrame;

    // top and bottom left vertices
    cv::Vec2i topLeft;
    cv::Vec2i bottomLeft;

    cv::Vec2i topRight;
    cv::Vec2i bottomRight;

    // saved (averaged) goal post lines
    cv::Vec4i *leftPosts;
    cv::Vec4i *rightPosts;
    int bufferPos;

    // minimum and maximum per-channel ranges for tracking the posts
    cv::Vec3i minTrackColor;
    cv::Vec3i maxTrackColor;

    // starting values for the Hough-space line detector
    int houghMinVotes;
    int houghMinLength;
    int houghMaxGap;

    // indicates if the training and tracking windows are open
    bool windowsOpen;

    // maxDistance is the maxmimum distance of two lines' endpoints at which
    // the lines will be merged into one; minVotes is the minimum number of lines
    // that must be nearby for them to be considered a merged line and not discarded
    void mergeLines(std::vector<cv::Vec4i> &lines, std::vector<cv::Vec4i> &merged, int maxDistance, int minVotes);

    // uses an upvote/downvote system to determine which pair of lines is most likely
    // the left and right goal posts; then, if goal posts are found, it reorders the
    // points that form the goal post so that the goal lines travel downwards
    bool voteOnGoalPosts(std::vector<cv::Vec4i> &lines, cv::Vec4i &left, cv::Vec4i &right, float &slopeLeft, float &slopeRight);

    // responds to mouse clicks to train the goal post tracker color range
    static void trainMouseEvent(int event, int x, int y, int flags, void *param);

    // averages out the GOAL_POST_AVERAGE_SIZE longest lines and returns it
    cv::Vec4i getGoalPostAverage(cv::Vec4i *postBuffer);

    // sorts a series of lines by non-ascending order of length
    void sortLines(cv::Vec4i *arr, int length);

    // private, forcing us to use a singleton approach
    GoalPostTracker();
/*
    // static utility class
    class Util
    {

        public:

            static int min(int, int);
            static int max(int, int);

            static cv::Point2f average(cv::Point2f, cv::Point2f);
            static cv::Point2f average(cv::Point2f, cv::Point2f, cv::Point2f);
            static cv::Point2f average(std::vector<cv::Point2f>);

            static float length(cv::Point2f);
            static float dist(cv::Point2f, cv::Point2f);
            static float dist(cv::Vec4i);
            static float angle(cv::Point2f);
            static cv::Point2f normalize(cv::Point2f);
            static cv::Point2f vecAtAngle(float);

            static void orderLinePointsTogether(cv::Vec4i&, cv::Vec4i&);
            static bool linePointsNearby(cv::Vec4i, cv::Vec4i, int);
            static cv::Vec4i averageLines(cv::Vec4i, cv::Vec4i);
            static float computeSlope(cv::Vec4i);

            static float wrapvalue(float);

            static cv::Vec3b matToVec3b(cv::Mat);
            static cv::Mat vec3bToMat(cv::Vec3b);
            static cv::Point2f keyPointToPoint(cv::KeyPoint);

            static int getFurthestFromAverage(std::vector<cv::Point2f>);

            static float areaToRadius(float);

            static void sortKeyPointsDownBySize(std::vector<cv::KeyPoint>&);

    };*/
};


#endif // GOALPOSTTRACKER_H
