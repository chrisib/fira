#include "SoccerMap.h"
#include "cmath"
#include <darwin/framework/Math.h>
#include <iostream>
#include "SoccerPlayer.h"

using namespace Robot;
using namespace std;
using namespace cv;

SoccerMap::SoccerMap()
{
    objects.push_back(&ballPosition);
    objects.push_back(myGoalPosition);
    objects.push_back(myGoalPosition+1);
    objects.push_back(theirGoalPosition);
    objects.push_back(theirGoalPosition+1);
    objects.push_back(myTeam);
    objects.push_back(myTeam+1);
    objects.push_back(theirTeam);
    objects.push_back(theirTeam+1);
    objects.push_back(theirTeam+2);

    // initially none of the objects are found so set their range to negative values
    for(list<Point2D*>::iterator it = objects.begin(); it != objects.end(); it++)
    {
        ((Point2D*)*it)->X = -1;
    }


    mapImage = Mat::zeros(IMAGE_SIZE,IMAGE_SIZE,CV_8UC3);

    xGain = 1.0;
    yGain = 1.0;
    aGain = 1.0;
}

SoccerMap::~SoccerMap()
{

}

void SoccerMap::LoadIniSettings(minIni &ini, string section)
{
    std::string colour = ini.gets("Soccer","Colour");

    if(colour.compare("red") == 0)
    {
        myTeamColour = SoccerPlayer::TEAM_RED;
        theirTeamColour = SoccerPlayer::TEAM_BLUE;
    }
    else
    {
        myTeamColour = SoccerPlayer::TEAM_BLUE;
        theirTeamColour = SoccerPlayer::TEAM_RED;
    }

    xGain = ini.getd(section,"XGain",1.0);
    yGain = ini.getd(section,"YGain",1.0);
    aGain = ini.getd(section,"AGain",1.0);

}

void SoccerMap::UpdateOdometry(int numSteps, double xAmplitude, double yAmplitude, double aAmplitude)
{
    // 2d (x,y) + orientation (z)
    Point3D newPosition = Point3D(0,0,0);

    // track the change in the robot's position on a cartesean plane
    for(int i=0; i<numSteps; i++)
    {
        newPosition.X += xAmplitude * xGain * cos(deg2rad(newPosition.Z)) + yAmplitude * yGain * sin(deg2rad(newPosition.Z));
        newPosition.Y += xAmplitude * xGain * sin(deg2rad(newPosition.Z)) + yAmplitude * yGain * cos(deg2rad(newPosition.Z));
        newPosition.Z += aAmplitude/2.0 * aGain;
    }

    ChangePosition(newPosition);
}

void SoccerMap::ChangePosition(Point3D &delta)
{
    // update the ranges to each object
    Point2D cartesean;
    for(list<Point2D*>::iterator it=objects.begin(); it!=objects.end(); it++)
    {
        // only bother updating if the point was actually found (i.e. has a positive range)
        if(((Point2D*)*it)->X > 0)
        {
            // convert the polar coordinate to cartesean
            cartesean.X = ((Point2D*)*it)->X * cos(deg2rad(((Point2D*)*it)->Y));
            cartesean.Y = ((Point2D*)*it)->X * sin(deg2rad(((Point2D*)*it)->Y));

            // offset by the change in position
            cartesean.X -= delta.X;
            cartesean.Y -= delta.Y;

            // convert the cartesean position back to polar
            ((Point2D*)*it)->X = sqrt(pow(cartesean.X,2.0) + pow(cartesean.Y,2.0));
            ((Point2D*)*it)->Y = -delta.Z + rad2deg(atan2(cartesean.Y, cartesean.X));

            //cout << newPosition.X << " " << newPosition.Y << " " << newPosition.Z << " " << rad2deg(atan2(cartesean.Y, cartesean.X)) << endl;
        }
    }
}

void SoccerMap::Clear()
{
    ballPosition.X = -1;
    myGoalPosition[LEFT].X = -1;
    myGoalPosition[RIGHT].X = -1;
    theirGoalPosition[LEFT].X = -1;
    theirGoalPosition[RIGHT].X = -1;

    for(int i=0; i<OTHER_PLAYERS_ON_MY_TEAM; i++)
        myTeam[i].X = -1;

    for(int i=0; i<PLAYERS_ON_THEIR_TEAM; i++)
        theirTeam[i].X = -1;
}

void SoccerMap::Draw()
{
    Point p,q;
    Scalar colour;

    // fade the old image out by 3/4 so we get a trail effect
    IplImage iplImg(mapImage);
    uint8_t *data = (uint8_t*)iplImg.imageData;
    int maxIndex = IMAGE_SIZE * IMAGE_SIZE * mapImage.channels();
    for(int i=0; i < maxIndex; i++)
    {
        data[i] = (uint8_t)(data[i] * 0.75);
    }

    // draw circles indicating 25cm range increments
    p = Point(IMAGE_SIZE/2, IMAGE_SIZE/2);
    colour = Scalar(255,255,255);
    for(int r=250; r<=IMAGE_SIZE/2*10; r+=250)
    {
        circle(mapImage, p, r/10, colour, 1);
    }

    // draw 45-degree lines
    p = Point(0,0);
    q = Point(IMAGE_SIZE,IMAGE_SIZE);
    line(mapImage,p,q,colour,1);
    p = Point(0, IMAGE_SIZE/2);
    q = Point(IMAGE_SIZE,IMAGE_SIZE/2);
    line(mapImage,p,q,colour,1);
    p = Point(IMAGE_SIZE, 0);
    q = Point(0,IMAGE_SIZE);
    line(mapImage,p,q,colour,1);
    p = Point(IMAGE_SIZE/2,0);
    q = Point(IMAGE_SIZE/2,IMAGE_SIZE);
    line(mapImage,p,q,colour,1);

    // mark the directions for reference
    p = Point(IMAGE_SIZE/2, 12);
    putText(mapImage, "Front",p,FONT_HERSHEY_PLAIN, 1.0, colour, 1);
    p = Point(IMAGE_SIZE/2,IMAGE_SIZE-12);
    putText(mapImage, "Back",p,FONT_HERSHEY_PLAIN, 1.0, colour, 1);
    p = Point(12,IMAGE_SIZE/2);
    putText(mapImage, "Left",p,FONT_HERSHEY_PLAIN, 1.0, colour, 1);
    p = Point(IMAGE_SIZE-48,IMAGE_SIZE/2);
    putText(mapImage, "Right",p,FONT_HERSHEY_PLAIN, 1.0, colour, 1);

    // mark our position in green
    colour = Scalar(0,255,0);
    p = Point(IMAGE_SIZE/2, IMAGE_SIZE/2);
    colour = Scalar(255,255,255);
    circle(mapImage, p, 3, colour, -1);

    // mark the ball in orange
    if(ballPosition.X > 0)
    {
        colour = Scalar(0,128,255);
        p = Point(IMAGE_SIZE/2 - ballPosition.X * sin(deg2rad(ballPosition.Y))/10, IMAGE_SIZE/2 - ballPosition.X * cos(deg2rad(ballPosition.Y))/10);
        circle(mapImage, p, 5,colour,-1);
    }

    // mark our goal posts as hollow green circles
    if(myGoalPosition[LEFT].X > 0)
    {
        colour = Scalar(0,255,0);
        p = Point(IMAGE_SIZE/2 - myGoalPosition[LEFT].X * sin(deg2rad(myGoalPosition[LEFT].Y))/10, IMAGE_SIZE/2 - myGoalPosition[LEFT].X * cos(deg2rad(myGoalPosition[LEFT].Y))/10);
        circle(mapImage,p,6,colour,2);
    }
    if(myGoalPosition[RIGHT].X > 0)
    {
        colour = Scalar(0,255,0);
        q = Point(IMAGE_SIZE/2 - myGoalPosition[RIGHT].X * sin(deg2rad(myGoalPosition[RIGHT].Y))/10, IMAGE_SIZE/2 - myGoalPosition[RIGHT].X * cos(deg2rad(myGoalPosition[RIGHT].Y))/10);
        circle(mapImage,q,6,colour,2);
    }

    // connect-the-dots to draw the plane of our goal
    if(myGoalPosition[0].X > 0 && myGoalPosition[1].X > 0)
    {
        line(mapImage, p, q, colour, 3);
    }


    // mark their goal as hollow red circles
    if(theirGoalPosition[LEFT].X > 0)
    {
        colour = Scalar(0,0,255);
        p = Point(IMAGE_SIZE/2 - theirGoalPosition[LEFT].X * sin(deg2rad(theirGoalPosition[LEFT].Y))/10, IMAGE_SIZE/2 - theirGoalPosition[LEFT].X * cos(deg2rad(theirGoalPosition[LEFT].Y))/10);
        circle(mapImage,p,6,colour,2);
    }
    if(theirGoalPosition[RIGHT].X > 0)
    {
        colour = Scalar(0,0,255);
        q = Point(IMAGE_SIZE/2 - theirGoalPosition[RIGHT].X * sin(deg2rad(theirGoalPosition[RIGHT].Y))/10, IMAGE_SIZE/2 - theirGoalPosition[RIGHT].X * cos(deg2rad(theirGoalPosition[RIGHT].Y))/10);
        circle(mapImage,q,6,colour,2);
    }

    // connect-the-dots to draw the plane of their goal
    if(theirGoalPosition[0].X > 0 && theirGoalPosition[1].X > 0)
    {
        line(mapImage, p, q, colour, 3);
    }


    // mark our players as solid green circles
    for(int i=0; i<OTHER_PLAYERS_ON_MY_TEAM; i++)
    {
        colour = Scalar(0,255,0);
        if(myTeam[i].X > 0)
        {
            p = Point(IMAGE_SIZE/2 - myTeam[i].X * sin(deg2rad(myTeam[i].Y))/10, IMAGE_SIZE/2 - myTeam[i].X * cos(deg2rad(myTeam[i].Y))/10);
            circle(mapImage, p, 4,colour,-1);
        }
    }

    // mark their players as solid red circles
    for(int i=0; i<PLAYERS_ON_THEIR_TEAM; i++)
    {
        colour = Scalar(0,0,255);
        if(theirTeam[i].X > 0)
        {
            p = Point(IMAGE_SIZE/2 - theirTeam[i].X * sin(deg2rad(theirTeam[i].Y))/10, IMAGE_SIZE/2 - theirTeam[i].X * cos(deg2rad(theirTeam[i].Y))/10);
            circle(mapImage, p, 4,colour,-1);
        }
    }
}

void SoccerMap::FoundBall(SingleBlob &ball)
{
    // simply copy the ball's position
    ballPosition.X = ball.GetRange();
    ballPosition.Y = ball.GetAngle();
}

void SoccerMap::LostBall()
{
    ballPosition.X = -1;
}


void SoccerMap::FoundRobots(vector<BoundingBox*> *myPlayers, vector<BoundingBox*> *theirPlayers)
{
    int i;
    Point2D polar;

    // clear ALL robots and start again from scratch
    // TODO: recycle information?
    // this is a LOT of work, since robots are highly dynamic and can move around a lot when we're not looking
    for(i=0; i<OTHER_PLAYERS_ON_MY_TEAM; i++)
        myTeam[i].X = 0;
    for(i=0; i<PLAYERS_ON_THEIR_TEAM; i++)
        theirTeam[i].X = 0;

    i=0;
    for(vector<BoundingBox*>::iterator it = myPlayers->begin(); it!=myPlayers->end() && i<OTHER_PLAYERS_ON_MY_TEAM; it++)
    {
        polar.X = ((BoundingBox*)*it)->GetRange();
        polar.Y = ((BoundingBox*)*it)->GetAngle();

        //cerr << "My Team[" << i << "] " << polar.X << " " << polar.Y << endl;

        if(polar.X > 0)
        {
            myTeam[i].X = polar.X;
            myTeam[i].Y = polar.Y;
            i++;
        }
    }

    i=0;
    for(vector<BoundingBox*>::iterator it = theirPlayers->begin(); it!=theirPlayers->end() && i < 3; it++)
    {
        polar.X = ((BoundingBox*)*it)->GetRange();
        polar.Y = ((BoundingBox*)*it)->GetAngle();

        //cerr << "Their Team[" << i << "] " << polar.X << " " << polar.Y << endl;

        if(polar.X > 0)
        {
            theirTeam[i].X = polar.X;
            theirTeam[i].Y = polar.Y;
            i++;
        }
    }
}

void SoccerMap::FoundGoal(GoalTarget &goal, int teamColour, bool clearOther)
{
    // figure out what team's goal was found if we weren't told
    if(teamColour!= SoccerPlayer::TEAM_BLUE && teamColour != SoccerPlayer::TEAM_RED)
    {
        // otherwise use democracy to decice what goal this *probably* is
        const double MAX_ERROR = 45;    // allow up to 45 degrees off angle-wise

        int myGoalVotes = 0;
        int theirGoalVotes = 0;

        // first off check if this goal has range & angle comparable to one of our existing goals' last known positions
        if(GetRangeToMyGoal() > 0)
        {
            double existingAngle, newAngle;
            existingAngle = GetAngleToMyGoal();
            newAngle = goal.GetAngle();

            // this is a likely candidate for our goal based on the position
            if(fabs(newAngle - existingAngle) <= MAX_ERROR)
            {
                myGoalVotes++;

                if(fabs(newAngle - existingAngle) <= MAX_ERROR/2)   // reward extra accuracy
                    myGoalVotes++;
            }
            else
            {
                myGoalVotes--;

                if(fabs(newAngle - existingAngle) >= MAX_ERROR*2)   // penalize extra inaccuracy
                    myGoalVotes--;
            }
        }

        if(GetRangeToTheirGoal() > 0)
        {
            double existingAngle, newAngle;
            existingAngle = GetAngleToTheirGoal();
            newAngle = goal.GetAngle();

            // this is a likely candidate for their goal based on the position
            if(fabs(newAngle - existingAngle) <= MAX_ERROR)
            {
                theirGoalVotes++;

                if(fabs(newAngle - existingAngle) <= MAX_ERROR/2)   // reward extra accuracy
                    theirGoalVotes++;
            }
            else
            {
                theirGoalVotes--;

                if(fabs(newAngle - existingAngle) >= MAX_ERROR*2)   // penalize extra inaccuracy
                    theirGoalVotes--;
            }
        }

        // next check to see if there is a goalie standing in the goal
        // go through all the robots and if one is standing between the pipes then vote for that team
        int myTeamVisible = 0;
        int theirTeamVisible = 0;
        for(int i=0; i<OTHER_PLAYERS_ON_MY_TEAM; i++)
        {
            if(myTeam[i].X > 0)
            {
                myTeamVisible++;
                double angle = myTeam[i].Y;
                if(angle > goal.GetRightPostAngle() && angle < goal.GetLeftPostAngle())
                    myGoalVotes++;
            }
        }
        for(int i=0; i<PLAYERS_ON_THEIR_TEAM; i++)
        {
            if(theirTeam[i].X > 0)
            {
                theirTeamVisible++;
                double angle = theirTeam[i].Y;
                if(angle > goal.GetRightPostAngle() && angle < goal.GetLeftPostAngle())
                    theirGoalVotes++;
            }
        }

        // if we can see all 3 members of their team give their goal +1 vote, our goal -1 vote
        if(theirTeamVisible == 3)
        {
            theirGoalVotes++;
            myGoalVotes--;
        }

        // if we can see both players on our team give our goal +1 vote, their goal -1 vote
        if(myTeamVisible == 2)
        {
            myGoalVotes++;
            theirGoalVotes--;
        }

        // whatever goal has more votes is the winner
        // [redacted] in case of a tie we assume we are facing our goal so as to avoid own goals
        // in case of a tie we assume we are facing their goal to force more forward kicks
        if(theirGoalVotes >= myGoalVotes)
        {
            teamColour = theirTeamColour;
        }
        else
        {
            teamColour = myTeamColour;
        }
    }

    if(teamColour == myTeamColour)
    {
        myGoalPosition[0].X = goal.GetLeftPostRange();
        myGoalPosition[0].Y = goal.GetLeftPostAngle();

        myGoalPosition[1].X = goal.GetRightPostRange();
        myGoalPosition[1].Y = goal.GetRightPostAngle();

        if(clearOther)
        {
            theirGoalPosition[0].X = -1;
            theirGoalPosition[0].Y = 0;

            theirGoalPosition[1].X = -1;
            theirGoalPosition[1].Y = 0;
        }
    }
    else if(teamColour == theirTeamColour)
    {
        theirGoalPosition[0].X = goal.GetLeftPostRange();
        theirGoalPosition[0].Y = goal.GetLeftPostAngle();

        theirGoalPosition[1].X = goal.GetRightPostRange();
        theirGoalPosition[1].Y = goal.GetRightPostAngle();

        if(clearOther)
        {
            myGoalPosition[0].X = -1;
            myGoalPosition[0].Y = 0;

            myGoalPosition[1].X = -1;
            myGoalPosition[1].Y = 0;
        }
    }
    else
    {
        cerr << "[warning] SoccerMap::FoundGoal -- Unknown team colour: " << teamColour << endl;
    }
}
