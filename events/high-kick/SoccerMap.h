#ifndef SOCCERMAP_H
#define SOCCERMAP_H

#include <darwin/framework/Point.h>
#include <list>
#include <opencv2/core/core.hpp>
#include <darwin/framework/SingleBlob.h>
#include <darwin/framework/MultiBlob.h>
#include <list>
#include <GoalTarget.h>

class SoccerMap
{
public:
    SoccerMap();
    virtual ~SoccerMap();
    static const int IMAGE_SIZE = 700;
    static const int LEFT = 0;
    static const int RIGHT = 1;
    static const int TOP = 0;
    static const int BOTTOM = 1;
    static const int OTHER_PLAYERS_ON_MY_TEAM = 2;
    static const int PLAYERS_ON_THEIR_TEAM = 3;

    void LoadIniSettings(minIni &ini, std::string section);

    // objects of interest in the scene
    // X is the range in mm (negative range = not found)
    // Y is the angle in degrees (0 = straight ahead, 90 = left, -90 = right)
    Robot::Point2D ballPosition;
    Robot::Point2D myGoalPosition[2];
    Robot::Point2D theirGoalPosition[2];

    // positions of other robots
    Robot::Point2D myTeam[OTHER_PLAYERS_ON_MY_TEAM];
    Robot::Point2D theirTeam[PLAYERS_ON_THEIR_TEAM];

    cv::Mat mapImage;

    // update the position of objects in the scene based on walking odometry
    void UpdateOdometry(int numSteps, double xAmplitude, double yAmplitude, double aAmplitude);

    // change the robot's position
    void ChangePosition(Robot::Point3D &delta);

    // remove EVERYTHING and reset
    void Clear();

    // TODO: update the position of objects in the scene based on vision
    //void UpdateVision()

    // update the mapImage with the objects' current positions
    // this is only necessary if the image is being streamed to a file or to an X window
    void Draw();

    // update the positions of objects based on vision
    void FoundBall(Robot::SingleBlob &ball);
    void LostBall();

    // found a robot belonging to a specific team
    // again, this does a best-guess based on the last known positions of all robots
    void FoundRobots(std::vector<Robot::BoundingBox*> *myPlayers, std::vector<Robot::BoundingBox*> *theirPlayers);

    // found a goal, but we don't know which one it is
    // based on our best-guess information determine if this is our goal or the opposing goal
    // optionally we can tell the map what colour the goal belongs to
    // if clearOther is true then the goal NOT found will be cleared
    // otherwise the data for the other goal is preserved (though it may be obsolete or wrong -- no corrections are made)
    void FoundGoal(GoalTarget &goal, int team = -1, bool clearOther = true);

    // return information about my goal and the opponents' goal
    double GetRangeToMyGoal(){return (myGoalPosition[0].X+myGoalPosition[1].X)/2.0;}
    double GetRangeToTheirGoal(){return (theirGoalPosition[0].X+theirGoalPosition[1].X)/2.0;}
    double GetAngleToMyGoal(){return (myGoalPosition[0].Y+myGoalPosition[1].Y)/2.0;}
    double GetAngleToTheirGoal(){return (theirGoalPosition[0].Y+theirGoalPosition[1].Y)/2.0;}

    //accessors for gain members
    double getXGain(){return xGain;}
    double getYGain(){return yGain;}
    double getAGain(){return aGain;}

private:
    std::list<Robot::Point2D*> objects;
    int myTeamColour;
    int theirTeamColour;

    double xGain, yGain, aGain;
};

#endif // SOCCERMAP_H
