#ifndef GOALTARGET_H
#define GOALTARGET_H

#include <darwin/framework/SingleBlob.h>
#include <darwin/framework/Point.h>

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include <vector>

// A RoboCup goal
class GoalTarget : public Robot::SingleBlob
{
public:
    
    static const int FOUND_NEITHER;
    static const int FOUND_LEFT;
    static const int FOUND_RIGHT;
    static const int FOUND_BOTH;
    
    GoalTarget();
    virtual ~GoalTarget();

    virtual void LoadIniSettings(minIni &ini, const std::string &section);

    // try to find the goal in a given YUV image
    virtual int FindInFrame(cv::Mat &image, cv::Mat *dest);

    // get the range and angle to the centre of the goal
    virtual double GetRange();
    virtual double GetAngle();

    // get the range and angle to each goal post
    virtual double GetLeftPostRange();
    virtual double GetLeftPostAngle();
    virtual double GetRightPostRange();
    virtual double GetRightPostAngle();

	// get whether or not the goal is against a white background
    virtual bool IsAgainstWhiteBackground();

    // draw the goal post lines on the canvas
    virtual void Draw(cv::Mat &canvas);

    virtual void Print();
    virtual void ProcessCandidates(std::vector<Robot::BoundingBox*> &candidates);

    virtual bool WasFound(){return postsFound != 0;}

    int NumFound(){return postsFound;}

    // pixel positions of the left and right posts inside the frame
    Robot::Line2D leftPost;
    Robot::Line2D rightPost;

private:
    
    // number of posts found in the last frame
    int postsFound;
    bool foundLeftPost;
    bool foundRightPost;

    // configuration parameters
    int houghMinVotes;
    int houghMinLength;
    int houghMaxGap;
    
    // white background threshold
    std::vector<cv::Point2f> samplePoints;
    int whiteThreshold;
    bool againstWhiteBackground;

    // the current camera position (used to calculate ranges and angles)
    Robot::CameraPosition camPos;
    
    // determine if the goal posts are against a white background
	virtual void determineBackgroundStatus(cv::Mat&);

    // maxDistance is the maxmimum distance of two lines' endpoints at which
    // the lines will be merged into one; minVotes is the minimum number of lines
    // that must be nearby for them to be considered a merged line and not discarded
    void mergeLines(std::vector<cv::Vec4i> &lines, std::vector<cv::Vec4i> &merged, int maxDistance, int minVotes);

    // uses an upvote/downvote system to determine which pair of lines is most likely
    // the left and right goal posts; then, if goal posts are found, it reorders the
    // points that form the goal post so that the goal lines travel downwards
    bool voteOnGoalPosts(std::vector<cv::Vec4i> &lines, cv::Vec4i &left, cv::Vec4i &right);

    // returns the list of corners detected (by slope and endpoint distance) of
    // the given list of lines
    void findCorners(std::vector<cv::Vec4i>&, std::vector<cv::Vec2i>&, std::vector<cv::Vec4i>&);
    void findVerticalLines(std::vector<cv::Vec4i>&, std::vector<cv::Vec4i>&, std::vector<cv::Vec4i>&);

    // do some serious trig to calculate the range to the centre of the goal and to each post
    double leftPostRange, centerRange, rightPostRange;
    bool needToReSolve;
    void solveTriangle();

    // calculate the distance to a goal post based on its top and bottom positions
    double calculateRange(Robot::Point2D &top, Robot::Point2D &bottom);
};

#endif // GOALTARGET_H
