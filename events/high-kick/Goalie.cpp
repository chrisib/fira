#include <iostream>
#include <darwin/framework/Voice.h>
#include <darwin/linux/CvCamera.h>
#include <cstring>
#include "opencv2/core/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include "Goalie.h"

using namespace Robot;
using namespace std;
using namespace cv;

//"constants" set with config.ini
int Goalie::FRAMES_BEFORE_DIVE;
int Goalie::FRAMES_BEFORE_ACTIVE;
int Goalie::FRAMES_BEFORE_PASSIVE;
int Goalie::FRAMES_BEFORE_SHUFFLE;
int Goalie::FRAMES_AFTER_SHUFFLE;
int Goalie::FRAMES_BEFORE_TILT;
int Goalie::FRAMES_BEFORE_DONE_REORIENTING;
int Goalie::DIVE_STEPS;
double Goalie::CROUCH_TOLERANCE;
double Goalie::PASSIVE_RANGE;
double Goalie::CROSS_TIME_THRESHOLD;
double Goalie::WALKING_CROSS_TIME_THRESHOLD;
double Goalie::SHUFFLE_TOLERANCE;
double Goalie::PIVOT_X_OFFSET;
double Goalie::PIVOT_DEGREES;
double Goalie::FORWARD_STEP_LENGTH;
int Goalie::POSITIONING_STEPS;
double Goalie::GOAL_LINE_ANGLE_THRESHOLD;
double Goalie::LOW_TILT_GOAL_LINE_CLOSE;
double Goalie::LOW_TILT_GOAL_LINE_FAR;
double Goalie::HIGH_TILT_GOAL_LINE_FAR;
double Goalie::BACKWARDS_HIP_PITCH;

//destructor
Goalie::~Goalie(){}

//constructor
Goalie::Goalie(minIni *ini, SocketMonitor *sockMon, bool noDives) : SoccerPlayer(ini, sockMon)
{
    const string SECTION_NAME = "Goalie";

    FRAMES_BEFORE_DIVE =                ini->getd(SECTION_NAME,"FramesBeforeDive");
    FRAMES_BEFORE_ACTIVE =              ini->getd(SECTION_NAME,"FramesBeforeActive");
    FRAMES_BEFORE_PASSIVE =             ini->getd(SECTION_NAME,"FramesBeforePassive");
    FRAMES_AFTER_SHUFFLE =              ini->getd(SECTION_NAME,"FramesAfterShuffle");
    FRAMES_BEFORE_SHUFFLE =             ini->getd(SECTION_NAME,"FramesBeforeShuffle");
    FRAMES_BEFORE_TILT =                ini->geti(SECTION_NAME,"FramesBeforeTilt",5); //5;
    FRAMES_BEFORE_DONE_REORIENTING =    ini->geti(SECTION_NAME,"FramesBeforeDoneReorienting",3); //3;
    DIVE_STEPS =                        ini->getd(SECTION_NAME,"DiveSteps");
    CROUCH_TOLERANCE =                  ini->getd(SECTION_NAME,"CrouchTolerance");
    PASSIVE_RANGE =                     ini->getd(SECTION_NAME,"PassiveRange");
    CROSS_TIME_THRESHOLD =              ini->getd(SECTION_NAME,"CrossTimeThreshold");
    WALKING_CROSS_TIME_THRESHOLD =      ini->getd(SECTION_NAME,"WalkingCrossTimeThreshold");
    SHUFFLE_TOLERANCE =                 ini->getd(SECTION_NAME,"ShuffleTolerance");
    PIVOT_X_OFFSET =                    ini->getd(SECTION_NAME,"PivotXOffset",-4); //-4;
    PIVOT_DEGREES =                     ini->getd(SECTION_NAME,"PivotDegrees",15); //15;
    FORWARD_STEP_LENGTH =               ini->getd(SECTION_NAME,"ForwardStepLength",10); //10;
    POSITIONING_STEPS =                 ini->geti(SECTION_NAME,"PositioningSteps",2); //2;
    GOAL_LINE_ANGLE_THRESHOLD =         ini->getd(SECTION_NAME,"GoalLineAngleThreshold",7); //7;
    LOW_TILT_GOAL_LINE_CLOSE =          ini->getd(SECTION_NAME,"LowTiltGoalLineClose",138); //138;
    LOW_TILT_GOAL_LINE_FAR =            ini->getd(SECTION_NAME,"LowTiltGoalLineFar",26); //26;
    HIGH_TILT_GOAL_LINE_FAR =           ini->getd(SECTION_NAME,"HighTiltGoalLineFar",222); //222;
    BACKWARDS_HIP_PITCH =               ini->getd(SECTION_NAME,"BackwardsHipPitch",17); //17;

    this->noDives = noDives;
    newState = false;
    oneFrameOffset = false;
    walkingStarted = false;
    diveFrames = 0;
    activePassiveFrames = 0;
    framesBeforeShuffle = 0;
    framesAfterShuffle = FRAMES_AFTER_SHUFFLE;
    mode = MODE_NORMAL;
    state = STATE_IDLE;
    lastState = STATE_IDLE;

    finder = GoalieFinder::create();

    lastDiveLeft = true;
    expectedLateralDistance = 0;
    expectedCrossingTime = 0;
    goalLineAngle = 0;

    target = &ball;
    target->SetMinPixelHeight(ini->getd("Yellow","MinPixelHeight"));
    target->SetMinPixelWidth(ini->getd("Yellow","MinPixelWidth"));
}

//calls SoccerPlayer::handleVideo(), could be expanded later
void Goalie::handleVideo()
{
    SoccerPlayer::handleVideo();
}

//function that the video thread lives in
void* Goalie::videoLoop(void* self)
{
    Goalie* s = (Goalie*)self;
    for(;;)
    {
        s->frMonitor->updateFrameRate();
        s->handleVideo();
    }

    pthread_exit(NULL);
    return NULL; // Unreachable
}

//function that the head movement thread lives in
void* Goalie::headLoop(void* self)
{
    Goalie* s = (Goalie*)self;
    while(true)
		s->scanAround();

    pthread_exit(NULL);
}

//checks whether or not the goalie should dive
//also sets the expectedLateralDistance and expectedCrossingTime members
bool Goalie::checkDiveCriteria()
{
    bool ans = false;

    if(lastBallInfo->getX() > 0)
    {
        expectedLateralDistance = lastBallInfo->calculateLateralCrossingPosition();
        expectedCrossingTime = lastBallInfo->calculateCrossingTime();

        if(expectedCrossingTime <= CROSS_TIME_THRESHOLD)//(!walkingStarted && expectedCrossingTime <= CROSS_TIME_THRESHOLD) || (walkingStarted && expectedCrossingTime <= WALKING_CROSS_TIME_THRESHOLD))
        {
            if(scanFrames >= FRAMES_AFTER_SCAN)
                diveFrames++;

            if(diveFrames >= FRAMES_BEFORE_DIVE)
            {
                cout << "Preparing to dive" << endl;
                ans = true;
                diveFrames = 0;
                scanFrames = 0;
            }
        }
        else
        {
            diveFrames = 0;
        }
    }
    else
    {
        diveFrames = 0;
    }

    return ans;
}

//initializes threads and then calls process in an infinite loop
int Goalie::exec()
{
    pthread_create(&videoThreadId, NULL, &Goalie::videoLoop, this);
    pthread_create(&headThreadId, NULL, &Goalie::headLoop, this);

    pthread_setname_np(pthread_self(), "mainThread");
    pthread_setname_np(videoThreadId, "videoThread");
    pthread_setname_np(headThreadId, "headThread");

    //finder->openTrackingWindows();

    if(mode==MODE_VIS_TEST)
    {
        for(;;)
            handleVideo();
    }
    else
    {
        //cout << "Press the middle button to start" << endl;
        //Voice::Speak("Press the middle button.");
        //MotionManager::GetInstance()->WaitButton(CM730::MIDDLE_BUTTON);
        state = STATE_ENTER_GOAL;

		//field.SetPosition(Point3D(0,300,180));

        for(;;)
            process();
    }

    return 0;
}

//contains the main goalie logic
//should be called in an infinite loop
void Goalie::process() {
	bool prevOnPenalty = onPenalty;
	checkRefStatus();

	//cout << penaltyEntrance.X << " " << penaltyEntrance.Y << " " << penaltyEntrance.Z << endl;

	// transmit status packets at regular intervals (if we're using the network)
	transmitStatus();

    SoccerPlayer::handleButton();

	// if we're in test mode just run the vision and/or networking and don't do anything else
	if(!handleTests() && refSaysWeCanPlay)
	{
		//handleButton();

		if(state!=STATE_DIVING && state!=STATE_CROUCHING && state!=STATE_LYING_DOWN)
		{
			if(SoccerPlayer::handleFall())
			{
				Action::GetInstance()->m_Joint.SetEnableBody(false);
				Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
				if(state!=STATE_ENTER_GOAL)
					state = STATE_REORIENTING;
			}
    	}

		// actual game logic
		findGameState();

		switch(gameState)
		{
		case SocketMonitor::STATE_READY:
			if(newGameState)
			{
				if(state != STATE_ENTER_GOAL)
				{
					standUp();
				}
				state = STATE_ENTER_GOAL;
				walkTo(homePosition);
			}
			break;

		case SocketMonitor::STATE_SET:
			state = STATE_IDLE;
			break;


		case SocketMonitor::STATE_PLAY:
			if(prevOnPenalty && !onPenalty && !noNetwork)   // only worry about this if we actually are listening to the referee
			{
				// just came off penalty; walk  back to our starting position

				cout << "[info] Re-entering field from penalty" << endl;
				field.SetPosition(penaltyEntrance);
				walkTo(homePosition);
				state = STATE_ENTER_GOAL;
			}
			else if(!onPenalty)
			{
				if(state == STATE_IDLE) // only re-enter the playing state if we're ready to
				{
					standUp();
					state = STATE_REORIENTING;
				}
			}
			break;

		case SocketMonitor::STATE_INITIAL:
		case SocketMonitor::STATE_FINISHED:
		default:
			// no action going on, so just sit down and wait
			state = STATE_IDLE;
			break;
		}

		processGoalieState();
	}
	else
	{
        //cerr << "Waiting" << endl;

		// just sit down and wait
		sitDown();
    }
}

void Goalie::processGoalieState()
{
	double lateralPosn, goalLineY, originalHipOffset;
	int fallDirection;
	bool foundLine;
	Vec4i goalLine;

    newState = lastState != state;
    lastState = state;

    pauser.threadWaitBody();

    switch(state)
    {
    case STATE_IDLE:
    	headMoving = false;
    	sitDown();
        break;

    case STATE_ENTER_GOAL:
		if(newState)
		{
			Action::GetInstance()->m_Joint.SetEnableBody(false);
			Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
			Action::GetInstance()->Start(READY);
			Action::GetInstance()->Finish();
			walkTo(homePosition);
		}

		if(!Walking::GetInstance()->IsRunning())
		{
			Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
			Walking::GetInstance()->Start();
		}

		processWalking();
    	break;

    case STATE_REORIENTING:
        if(newState)
        {
            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Action::GetInstance()->Start(READY);
            Action::GetInstance()->Finish();

            headMoving = false;
            headPan = 0;
            headTilt = LOW_SCAN_TILT;
            MotionManager::Sync();
            Head::GetInstance()->MoveByAngle(0, headTilt);
            MotionManager::Sync();
            framesBeforeTilt = FRAMES_BEFORE_TILT;
            framesBeforeDoneReorienting = FRAMES_BEFORE_DONE_REORIENTING;
        }

        goalLineAngle = 999999;

        pauser.bodyHasReacted();
        pauser.threadWaitBody();

        finder->update(CvCamera::GetInstance()->rgbFrame);
        foundLine = finder->findGoalLine();
        //waitKey(1);
        if(foundLine)
        {
            framesBeforeTilt = FRAMES_BEFORE_TILT;
            goalLineAngle = finder->getLastGoalLineAngle();
            Walking::GetInstance()->m_Joint.SetEnableBody(false);
            Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;

            goalLine = finder->getLastGoalLine();
            goalLineY = (double)(goalLine[1] + goalLine[3])/2.0;

            if(goalLineAngle > GOAL_LINE_ANGLE_THRESHOLD)
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = -PIVOT_DEGREES;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
            }
            else if (goalLineAngle < -GOAL_LINE_ANGLE_THRESHOLD)
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = PIVOT_DEGREES;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
            }
            else if (abs(headTilt-LOW_SCAN_TILT)<TOLERANCE && goalLineY>LOW_TILT_GOAL_LINE_CLOSE && !penaltyKick)   // do not back up if we're defending a penalty kick
            {
                originalHipOffset = Walking::GetInstance()->HIP_PITCH_OFFSET;
                Walking::GetInstance()->HIP_PITCH_OFFSET = BACKWARDS_HIP_PITCH;
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = -FORWARD_STEP_LENGTH;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
                Walking::GetInstance()->HIP_PITCH_OFFSET = originalHipOffset;
            }
            else if (((abs(headTilt-LOW_SCAN_TILT)<TOLERANCE && goalLineY<LOW_TILT_GOAL_LINE_FAR) || (abs(headTilt-HIGH_SCAN_TILT)<TOLERANCE && goalLineY < HIGH_TILT_GOAL_LINE_FAR && goalLineY > 120))
                     && !penaltyKick)   // do not walk forwards if we're defending a penalty kick
            {
                Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                Walking::GetInstance()->X_MOVE_AMPLITUDE = FORWARD_STEP_LENGTH;
                Walking::GetInstance()->Start(POSITIONING_STEPS);
                Walking::GetInstance()->Finish();
            }
            else
            {
                framesBeforeDoneReorienting--;
                if(framesBeforeDoneReorienting <= 0) {
                    headMoving = true;
                    state = STATE_ALERT;
                }
            }
        }
        else
        {
            framesBeforeDoneReorienting = FRAMES_BEFORE_DONE_REORIENTING;
            framesBeforeTilt--;
            if(abs(headTilt - LOW_SCAN_TILT) < TOLERANCE) {
                if(framesBeforeTilt <= 0) {
                    Walking::GetInstance()->m_Joint.SetEnableBody(false);
                    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                    originalHipOffset = Walking::GetInstance()->HIP_PITCH_OFFSET;
                    Walking::GetInstance()->HIP_PITCH_OFFSET = BACKWARDS_HIP_PITCH;
                    Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                    Walking::GetInstance()->X_MOVE_AMPLITUDE = -FORWARD_STEP_LENGTH;
                    Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
                    Walking::GetInstance()->Start(POSITIONING_STEPS);
                    Walking::GetInstance()->Finish();
                    Walking::GetInstance()->HIP_PITCH_OFFSET = originalHipOffset;
                    /*framesBeforeTilt = 5;
                    headTilt = HIGH_SCAN_TILT;
                    Head::GetInstance()->MoveByAngle(0, headTilt);
                    MotionManager::Sync();*/
                }
            } else {
                if(framesBeforeTilt <= 0) {
                    framesBeforeTilt = 5;
                    headTilt = LOW_SCAN_TILT;
                    Head::GetInstance()->MoveByAngle(0, headTilt);
                    MotionManager::Sync();
                }
            }
        }

    	break;

    case STATE_PASSIVE:
        if(newState)
        {
            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
            Action::GetInstance()->Start(SLOW_CROUCH);
            Action::GetInstance()->Finish();
            activePassiveFrames = 0;
            oneFrameOffset = false;
        }

        if(target->WasFound())
        {
            oneFrameOffset = false;
            if(checkDiveCriteria())
                state = STATE_CROUCH_DIVING;
            else
            {
                activePassiveFrames++;
                if(activePassiveFrames >= FRAMES_BEFORE_ACTIVE)
                    state = STATE_ALERT;
            }
        }
        else
        {
            diveFrames = 0;
            if(oneFrameOffset)
                activePassiveFrames = 0;
            else
                oneFrameOffset = true;
        }

        break;

    case STATE_ALERT:
        if(newState)
		{
			Action::GetInstance()->m_Joint.SetEnableBody(false);
			Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
			Action::GetInstance()->Start(READY);
			Action::GetInstance()->Finish();
			activePassiveFrames = 0;
			framesBeforeShuffle = 0;
			//ThreadPauser::milisleep(500);
			pauser.waitAMoment();
		}

		if(target->WasFound())
		{
			activePassiveFrames = 0;
			if(checkDiveCriteria())
				state = STATE_DIVING;
			else
			{
				lateralPosn = lastBallInfo->calculateLateralPosition();
                if(abs(lateralPosn) > SHUFFLE_TOLERANCE)
				{
					framesBeforeShuffle++;
					if(framesBeforeShuffle >= FRAMES_BEFORE_SHUFFLE)
						state = STATE_SHUFFLING;
				}
				else
					framesBeforeShuffle = 0;
			}
		}
        else
		{
			diveFrames = 0;
			activePassiveFrames++;
			if((lostFrames>=LOST_FRAMES_BEFORE_SCAN && map.ballPosition.X >= PASSIVE_RANGE) || activePassiveFrames >= FRAMES_BEFORE_PASSIVE)
				state = STATE_PASSIVE;
		}

        break;

    case STATE_SHUFFLING:
        if(penaltyKick)
        {
            if(newState)
            {
                Action::GetInstance()->m_Joint.SetEnableBody(false);
                Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
                framesAfterShuffle = FRAMES_AFTER_SHUFFLE;
            }

            if(target->WasFound())
            {
                lateralPosn = lastBallInfo->calculateLateralPosition();
                if(abs(lateralPosn) > SHUFFLE_TOLERANCE)
                {
                    if(lateralPosn > 0)
                    {
                        framesAfterShuffle = -1;
    #ifdef USE_SHUFFLE
                        Action::GetInstance()->Start(SHUFFLE_LEFT);
                        Action::GetInstance()->Finish();
    #else
                        standUp();
                        Walking::GetInstance()->Y_MOVE_AMPLITUDE = 30;
                        Walking::GetInstance()->X_MOVE_AMPLITUDE = -2;
                        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                        Walking::GetInstance()->Start(2);
                        Walking::GetInstance()->Finish();
    #endif
                    }
                    else
                    {
                        framesAfterShuffle = -1;
    #ifdef USE_SHUFFLE
                        Action::GetInstance()->Start(SHUFFLE_RIGHT);
                        Action::GetInstance()->Finish();
    #else
                        standUp();
                        Walking::GetInstance()->Y_MOVE_AMPLITUDE = -30;
                        Walking::GetInstance()->X_MOVE_AMPLITUDE = -2;
                        Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
                        Walking::GetInstance()->Start(2);
                        Walking::GetInstance()->Finish();
    #endif
                    }
                }
                else {
                    framesAfterShuffle++;
                    if(framesAfterShuffle >= FRAMES_AFTER_SHUFFLE)
                        state = STATE_ALERT;
                }
            }
            else
            {
                framesAfterShuffle++;
                if(framesAfterShuffle >= FRAMES_AFTER_SHUFFLE)
                    state = STATE_ALERT;
            }
        }
        else
        {
            state = STATE_ALERT;
        }
    	break;

    case STATE_DIVING:
        if(newState)
        {
            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        }

        if(abs(expectedLateralDistance) < CROUCH_TOLERANCE || noDives)
        {
            Voice::Speak("Crouching");
            cout << "Crouching" << endl;
            Action::GetInstance()->Start(CROUCH_BLOCK);
            Action::GetInstance()->Finish();
            state = STATE_CROUCHING;
        }
        else if(expectedLateralDistance > 0)
        {
            Voice::Speak("Diving left");
            cout << "Diving left" << endl;
            headMoving = false;
            lastDiveLeft = true;
            Action::GetInstance()->Start(DIVE_LEFT);
            Action::GetInstance()->Finish();
            state = STATE_LYING_DOWN;
        }
        else
        {
            Voice::Speak("Diving right");
            cout << "Diving right" << endl;
            headMoving = false;
            lastDiveLeft = false;
            Action::GetInstance()->Start(DIVE_RIGHT);
            Action::GetInstance()->Finish();
            state = STATE_LYING_DOWN;
        }
        Action::GetInstance()->Finish();
        cout << "done" << endl;
        break;

    case STATE_CROUCH_DIVING:
        if(newState)
        {
            Action::GetInstance()->m_Joint.SetEnableBody(false);
            Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        }

        if(abs(expectedLateralDistance) < CROUCH_TOLERANCE || noDives)
        {
            Voice::Speak("Still Crouching");
            cout << "Still Crouching" << endl;
            state = STATE_CROUCHING;
        }
        else if(expectedLateralDistance > 0)
        {
            Voice::Speak("Crouch Diving left");
            cout << "Crouch Diving left" << endl;
            headMoving = false;
            lastDiveLeft = true;
            Action::GetInstance()->Start(CROUCH_DIVE_LEFT);
            Action::GetInstance()->Finish();
            state = STATE_LYING_DOWN;
        }
        else
        {
            Voice::Speak("Crouch Diving right");
            cout << "Crouch Diving right" << endl;
            headMoving = false;
            lastDiveLeft = false;
            Action::GetInstance()->Start(CROUCH_DIVE_RIGHT);
            Action::GetInstance()->Finish();
            state = STATE_LYING_DOWN;
        }

        Action::GetInstance()->Finish();
        cout << "done" << endl;
        break;

    case STATE_CROUCHING:
        Action::GetInstance()->Start(CROUCH_WAIT);
        Action::GetInstance()->Finish();
        Action::GetInstance()->Start(READY);
        Action::GetInstance()->Finish();
        state = STATE_ALERT;
        break;

    case STATE_LYING_DOWN:
        Action::GetInstance()->Start(DIVE_WAIT);
        Action::GetInstance()->Finish();
        Action::GetInstance()->Start(READY);
        Action::GetInstance()->Finish();
        fallDirection = MotionStatus::FALLEN;
        SoccerPlayer::handleFall();

        // turn 90 degrees
        //headMoving = true;
        Walking::GetInstance()->m_Joint.SetEnableBody(false);
        Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        if((lastDiveLeft &&  fallDirection == BACKWARD) || (!lastDiveLeft && fallDirection == FORWARD))
        {
            Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = PIVOT_DEGREES;
        }
        else
        {
            Walking::GetInstance()->X_MOVE_AMPLITUDE = PIVOT_X_OFFSET;
            Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
            Walking::GetInstance()->A_MOVE_AMPLITUDE = -PIVOT_DEGREES;
        }
        Walking::GetInstance()->Start(DIVE_STEPS);
        Walking::GetInstance()->Finish();

        Action::GetInstance()->m_Joint.SetEnableBody(false);
        Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
        state = STATE_REORIENTING;
        break;
    }// switch

    pauser.bodyHasReacted();
}

//returns current state
ProgramState Goalie::getState() {
	return state;
}

//changes state to STATE_REORIENTING if current state is STATE_ENTER_GOAL
void Goalie::doneWalking() {
	if(state == STATE_ENTER_GOAL && gameState == SocketMonitor::STATE_PLAY)
		state = STATE_REORIENTING;
	else if(state == STATE_ENTER_GOAL)
		state = STATE_IDLE;
}
