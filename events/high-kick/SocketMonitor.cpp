#include "SocketMonitor.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <SoccerPlayer.h>

using namespace std;

const char* SocketMonitor::REFEREE_HEADER = "RGme";
const char* SocketMonitor::UOFM_HEADER = "UofM";

SocketMonitor::SocketMonitor()
{
    // do nothing
}

SocketMonitor::SocketMonitor(int portNo)
{
    this->setPort(portNo);
}

SocketMonitor::~SocketMonitor()
{
    // TODO: anything?
}

void SocketMonitor::setPort(int portNo)
{
    this->portNo = portNo;

#ifndef USE_Q_SOCKET
    int yes = 1;

    // create the C socket we need to listen on
    recvSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(recvSock < 0)
        cerr << "[error] Could not open Rx socket" << endl;

    sendSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(sendSock < 0)
        cerr << "[error] Could not open Tx socket" << endl;

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(portNo);

    bzero((char *) &cli_addr, sizeof(cli_addr));
    cli_addr.sin_family = AF_INET;
    cli_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    cli_addr.sin_port = htons(portNo);

    // set broadcast permissions
    setsockopt(recvSock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
    setsockopt(sendSock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
    //if (setsockopt(recvSock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0)
    //{
    //    cerr << "[error] Could not share socket" << endl;
    //}

    if (bind(recvSock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
          cerr << "[error] Could not bind Rx socket" << endl;

    if (bind(sendSock, (struct sockaddr *) &cli_addr, sizeof(cli_addr)) < 0)
        cerr << "[error] Could not bind Tx socket" << endl;

#else
    qSocket.bind(this->portNo, QUdpSocket::ShareAddress);
#endif

    pthread_mutex_init(&socketBufferLock, NULL);
    pthread_create(&monitorThreadId, NULL, SocketMonitor::listenThread, this);
}

void *SocketMonitor::listenThread(void* arg)
{
    SocketMonitor *instance = (SocketMonitor*)arg;

    for(;;)
    {
#ifndef USE_Q_SOCKET
        //pthread_mutex_lock(&instance->socketBufferLock);
        instance->bufferLength = recv(instance->recvSock, instance->sockBuffer, SocketMonitor::BUFFER_LENGTH, 0);
        instance->processUdpDatagram(instance->sockBuffer, instance->bufferLength);
#else
        // read all datagrams until we get to the latest one
        while(instance->qSocket.hasPendingDatagrams())
        {
            instance->qSocket.readDatagram(instance->sockBuffer, SocketMonitor::BUFFER_LENGTH);
        }
        instance->processUdpDatagram();
#endif
        pthread_yield();
    }
    return NULL;
}

void SocketMonitor::printLastPacket()
{
    if(bufferLength > 0)
    {
        for(int i=0; i<bufferLength; i++)
            printf("%02x ", sockBuffer[i] & 0xff);
        cout << endl;
    }
}

bool SocketMonitor::isRobotOnPenalty(int team, int id)
{
    if(team != SoccerPlayer::TEAM_BLUE && team != SoccerPlayer::TEAM_RED)
    {
        cerr << "[warning] Unknown team colour: " << team << endl;
        return false;
    }
    else if(id < 0 || id >= MAX_NUM_PLAYERS)
    {
        cerr << "[warning] Invalid robot ID" << id << endl;
        return false;
    }
    else
    {
        bool onPenalty = gameControlData.teams[team].players[id-1].penalty != NO_PENALTY;
        return onPenalty;
    }
}

bool SocketMonitor::canRobotPlay(int team, int id)
{
    bool onPenalty = isRobotOnPenalty(team,id);

    // TODO: check state of play?

    return !onPenalty;
}

void SocketMonitor::processUdpDatagram(char *src, int numBytes)
{
    //cerr << "reading " << numBytes << " bytes" << endl;

    // make a local copy of all the buffered data to keep things thread-safe
    char data[numBytes];
    for(int i=0; i<numBytes; i++)
        data[i] = src[i] & 0xff;

    // check the header to decide what to do with this packet
    if(data[0] == REFEREE_HEADER[0] &&
        data[1] == REFEREE_HEADER[1] &&
        data[2] == REFEREE_HEADER[2] &&
        data[3] == REFEREE_HEADER[3]
    )
    {
        // the game control data does not use a checksum, so we can only check the header & length
        if(numBytes == REFEREE_PACKET_LENGTH)
        {
            updateControlData(data, numBytes);
        }
#ifdef SOCKET_DEBUG
        else
            cerr << "[info] faulty referee packet ignored" << endl;
#endif

    }
    else if(data[0] == UOFM_HEADER[0] &&
            data[1] == UOFM_HEADER[1] &&
            data[2] == UOFM_HEADER[2] &&
            data[3] == UOFM_HEADER[3]
    )
    {
        // this is a UofM header; other team members are telling us things!
        parseUofMPacket(data, numBytes);
    }
#ifdef SOCKET_DEBUG
    else
    {
        cerr << "Unknown packet header: " << data[0] << data[1] << data[2] << data[3] << endl;
    }
#endif


}

void SocketMonitor::updateControlData(char *data, int numBytes)
{
    (void)numBytes;

    // header data
    for(int i=0; i<4; i++)
        gameControlData.header[i] = data[i];

    // little-endian 32-bit integer for version
    gameControlData.version = (data[4] & 0xff) | ((data[5] & 0xff) << 8) | ((data[6] & 0xff) << 16) | ((data[7] & 0xff) << 24);

    // players per team
    gameControlData.playersPerTeam = data[8] & 0xff;

    // state
    gameControlData.state = data[9] & 0xff;

    // first/second half
    gameControlData.firstHalf = data[10] & 0xff;

    // kickoff team
    gameControlData.kickOffTeam = data[11] & 0xff;

    // secondary state
    gameControlData.secondaryState = data[12] & 0xff;

    // drop in team & time
    gameControlData.dropInTeam = data[13] & 0xff;
    gameControlData.dropInTime = (data[14] & 0xff) | ((data[15] & 0xff) << 8);

    // time remaining (little-endian 32-bit integer)
    gameControlData.secsRemaining = (data[16] & 0xff) | ((data[17] & 0xff) << 8) | ((data[18] & 0xff) << 16) | ((data[19] & 0xff) << 24);

    // 2x team data
    for(int team=0; team<2; team++)
    {
        const int TEAM_START = 20 + team * TEAM_INFO_LENGTH;
        gameControlData.teams[team].teamNumber = data[TEAM_START + 0] & 0xff;
        gameControlData.teams[team].teamColour = data[TEAM_START + 1] & 0xff;
        gameControlData.teams[team].goalColour = data[TEAM_START + 2] & 0xff;
        gameControlData.teams[team].score = data[TEAM_START + 3] & 0xff;

        // n players
        for(int i=0; i<MAX_NUM_PLAYERS; i++)
        {
            const int PLAYER_START = TEAM_START + 4 + i*PLAYER_INFO_LENGTH;

            gameControlData.teams[team].players[i].penalty = (data[PLAYER_START + 0] & 0xff) | ((data[PLAYER_START + 1] & 0xff) << 8);
            gameControlData.teams[team].players[i].secsTillUnpenalised = (data[PLAYER_START + 2] & 0xff) | ((data[PLAYER_START + 3] & 0xff) << 8);

            //cout << "Team " << team << " player " << i << " Penalty: " << hex << gameControlData.teams[team].players[i].penalty <<
            //        " Secs Left: " << dec << (int)gameControlData.teams[team].players[i].secsTillUnpenalised << endl;
        }
    }
}

void SocketMonitor::parseUofMPacket(char *data, int numBytes)
{
    uint8_t checksum = 0;

    for(int i=0; i<numBytes-1; i++)
    {
        checksum = checksum | (uint8_t)data[i];
    }
    checksum &= 0xff;

    if(numBytes == UOFM_NUM_BYTES && checksum == (uint8_t)data[numBytes-1])
    {
        int playerId = data[6] - 1;    // 4 header bytes + 2 bytes sequence.  offset by 1 because IDs are 1+, not 0+

        // copy all of the data into the packet
        teamData[playerId].header[0] = data[0] & 0xff;
        teamData[playerId].header[1] = data[1] & 0xff;
        teamData[playerId].header[2] = data[2] & 0xff;
        teamData[playerId].header[3] = data[3] & 0xff;

        teamData[playerId].sequenceNum = (data[4] & 0xff) | ((data[5] & 0xff) << 8);
        teamData[playerId].myId = data[6] & 0xff;

        teamData[playerId].myPosition.x = (data[7] & 0xff) | ((data[8] & 0xff) << 8);
        teamData[playerId].myPosition.y = (data[9] & 0xff) | ((data[10] & 0xff) << 8);
        teamData[playerId].myPosition.a = (data[11] & 0xff) | ((data[12] & 0xff) << 8);

        teamData[playerId].ball.range = (data[13] & 0xff) | ((data[14] & 0xff) << 8);
        teamData[playerId].ball.angle = (data[15] & 0xff) | ((data[16] & 0xff) << 8);

        teamData[playerId].myGoal.leftPost.range = (data[17] & 0xff) | ((data[18] & 0xff) << 8);
        teamData[playerId].myGoal.leftPost.angle = (data[19] & 0xff) | ((data[20] & 0xff) << 8);
        teamData[playerId].myGoal.rightPost.range = (data[21] & 0xff) | ((data[22] & 0xff) << 8);
        teamData[playerId].myGoal.rightPost.angle = (data[23] & 0xff) | ((data[24] & 0xff) << 8);

        teamData[playerId].theirGoal.leftPost.range = (data[25] & 0xff) | ((data[26] & 0xff) << 8);
        teamData[playerId].theirGoal.leftPost.angle = (data[27] & 0xff) | ((data[28] & 0xff) << 8);
        teamData[playerId].theirGoal.rightPost.range = (data[29] & 0xff) | ((data[30] & 0xff) << 8);
        teamData[playerId].theirGoal.rightPost.angle = (data[31] & 0xff) | ((data[32] & 0xff) << 8);

        teamData[playerId].myTeam[0].range = (data[33] & 0xff) | ((data[34] & 0xff) << 8);
        teamData[playerId].myTeam[0].angle = (data[35] & 0xff) | ((data[36] & 0xff) << 8);
        teamData[playerId].myTeam[1].range = (data[37] & 0xff) | ((data[38] & 0xff) << 8);
        teamData[playerId].myTeam[1].angle = (data[39] & 0xff) | ((data[40] & 0xff) << 8);

        teamData[playerId].theirTeam[0].range = (data[41] & 0xff) | ((data[42] & 0xff) << 8);
        teamData[playerId].theirTeam[0].angle = (data[43] & 0xff) | ((data[44] & 0xff) << 8);
        teamData[playerId].theirTeam[1].range = (data[45] & 0xff) | ((data[46] & 0xff) << 8);
        teamData[playerId].theirTeam[1].angle = (data[47] & 0xff) | ((data[48] & 0xff) << 8);
        teamData[playerId].theirTeam[2].range = (data[49] & 0xff) | ((data[50] & 0xff) << 8);
        teamData[playerId].theirTeam[2].angle = (data[51] & 0xff) | ((data[52] & 0xff) << 8);

        teamData[playerId].myState = data[53] & 0xff;
        teamData[playerId].playSubstate = data[54] & 0xff;
        teamData[playerId].gameState = data[55] & 0xff;

        teamData[playerId].checksum = data[56] & 0xff;
    }
#ifdef SOCKET_DEBUG
    else
        cerr << "[info] invalid checksum in UofM packet" << endl;
#endif
}

// turn the state information into a single char array that we can broadcast
void SocketMonitor::transmitPacket(RobotStateInformation &packetData)
{
    uint8_t checksum = 0;

    uint8_t packet[UOFM_NUM_BYTES];

    packet[0] = UOFM_HEADER[0];
    packet[1] = UOFM_HEADER[1];
    packet[2] = UOFM_HEADER[2];
    packet[3] = UOFM_HEADER[3];

    packet[4] = packetData.sequenceNum & 0xff;
    packet[5] = (packetData.sequenceNum >> 8) & 0xff;

    packet[6] = packetData.myId;

    packet[7] = packetData.myPosition.x & 0xff;
    packet[8] = (packetData.myPosition.x >> 8) & 0xff;
    packet[9] = packetData.myPosition.y & 0xff;
    packet[10]= (packetData.myPosition.y >> 8) & 0xff;
    packet[11]= packetData.myPosition.a & 0xff;
    packet[12]= (packetData.myPosition.a >> 8) & 0xff;

    packet[13]= packetData.ball.range & 0xff;
    packet[14]= (packetData.ball.range >> 8) & 0xff;
    packet[15]= packetData.ball.angle & 0xff;
    packet[16]= (packetData.ball.angle >> 8) & 0xff;

    packet[17]= packetData.myGoal.leftPost.range & 0xff;
    packet[18]= (packetData.myGoal.leftPost.range >> 8) & 0xff;
    packet[19]= packetData.myGoal.leftPost.angle & 0xff;
    packet[20]= (packetData.myGoal.leftPost.angle >> 8) & 0xff;
    packet[21]= packetData.myGoal.rightPost.range & 0xff;
    packet[22]= (packetData.myGoal.rightPost.range >> 8) & 0xff;
    packet[23]= packetData.myGoal.rightPost.angle & 0xff;
    packet[24]= (packetData.myGoal.rightPost.angle >> 8) & 0xff;

    packet[25]= packetData.theirGoal.leftPost.range & 0xff;
    packet[26]= (packetData.theirGoal.leftPost.range >> 8) & 0xff;
    packet[27]= packetData.theirGoal.leftPost.angle & 0xff;
    packet[28]= (packetData.theirGoal.leftPost.angle >> 8) & 0xff;
    packet[29]= packetData.theirGoal.rightPost.range & 0xff;
    packet[30]= (packetData.theirGoal.rightPost.range >> 8) & 0xff;
    packet[31]= packetData.theirGoal.rightPost.angle & 0xff;
    packet[32]= (packetData.theirGoal.rightPost.angle >> 8) & 0xff;

    packet[33]= packetData.myTeam[0].range & 0xff;
    packet[34]= (packetData.myTeam[0].range >> 8) & 0xff;
    packet[35]= packetData.myTeam[0].angle & 0xff;
    packet[36]= (packetData.myTeam[0].angle >> 8) & 0xff;
    packet[37]= packetData.myTeam[1].range & 0xff;
    packet[38]= (packetData.myTeam[1].range >> 8) & 0xff;
    packet[39]= packetData.myTeam[1].angle & 0xff;
    packet[40]= (packetData.myTeam[1].angle >> 8) & 0xff;

    packet[41]= packetData.theirTeam[0].range & 0xff;
    packet[42]= (packetData.theirTeam[0].range >> 8) & 0xff;
    packet[43]= packetData.theirTeam[0].angle & 0xff;
    packet[44]= (packetData.theirTeam[0].angle >> 8) & 0xff;
    packet[45]= packetData.theirTeam[1].range & 0xff;
    packet[46]= (packetData.theirTeam[1].range >> 8) & 0xff;
    packet[47]= packetData.theirTeam[1].angle & 0xff;
    packet[48]= (packetData.theirTeam[1].angle >> 8) & 0xff;
    packet[49]= packetData.theirTeam[2].range & 0xff;
    packet[50]= (packetData.theirTeam[2].range >> 8) & 0xff;
    packet[51]= packetData.theirTeam[2].angle & 0xff;
    packet[52]= (packetData.theirTeam[2].angle >> 8) & 0xff;

    packet[53]= packetData.myState;
    packet[54]= packetData.playSubstate;
    packet[55]= packetData.gameState;

    for(int i=0; i<UOFM_NUM_BYTES-1; i++)
        checksum = checksum | packet[i];

    packet[UOFM_NUM_BYTES-1]= checksum;

    // transmit the buffer
    //cout << "[info] Transmitting packet" << endl;
    if(sendto(sendSock, packet, UOFM_NUM_BYTES, 0, (struct sockaddr*)&cli_addr/*(struct sockaddr*)&addr*/, sizeof(cli_addr)) < 0)
    {
        //cout << "[error] Failed to transmit packet" << endl;
    }
    //send(sockfd, packet, UOFM_NUM_BYTES, 0);
}

uint16_t SocketMonitor::Angle2Packet(double angle)
{
    angle = SoccerField::normalizeAngle(angle);
    if(angle < 0)
        angle += 360;   // all angles are positive

    angle *= PACKET_DEGREE_PRECISION;

    int tmp = (int)angle;

    uint16_t result = (uint16_t)tmp;
    //cerr << "Angle: " << angle << " " << tmp << " " << result << endl;

    return result;
}

uint16_t SocketMonitor::Range2Packet(double range)
{
    if(range < 0)
        return 0xffff;
    else
        return (uint16_t)range;
}

double SocketMonitor::Packet2Angle(uint16_t angle)
{
    double result = (double) angle;
    result /= PACKET_DEGREE_PRECISION;

    result = SoccerField::normalizeAngle(result);
    return result;
}

double SocketMonitor::Packet2Range(uint16_t range)
{
    if(range == 0xffff)
        return -1;
    else
        return (double)range;
}
