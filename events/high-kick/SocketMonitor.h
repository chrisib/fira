#ifndef SOCKETMONITOR_H
#define SOCKETMONITOR_H

#include <PacketObject.h>
#include <vector>
#include <pthread.h>
#include <PlayerPacket.h>
#include <pthread.h>
#include <stdint.h>

//#define USE_Q_SOCKET
#ifdef USE_Q_SOCKET
#    include <QUdpSocket>
#else
#    include <sys/types.h>
#    include <sys/socket.h>
#    include <netinet/in.h>
#    include <netdb.h>
#endif

//#define SOCKET_DEBUG

class SocketMonitor
{
public:
    SocketMonitor();
    SocketMonitor(int portNo);
    ~SocketMonitor();

    void setPort(int portNo);

    /***************************************************************************************************************/
    // copied from GameController

    static const int MAX_NUM_PLAYERS = 11;
    static const int DEFAULT_PORT = 3838;

    // headers so we can identify packets
    static const char *REFEREE_HEADER;
    static const char *UOFM_HEADER;

    static const int PROTOCOL_VERSION = 7;

    struct RobotInfo {
        uint16_t penalty;             // penalty state of the player
        uint16_t secsTillUnpenalised; // estimate of time till unpenalised
    };

    struct TeamInfo {
        uint8_t teamNumber;          // unique team number
        uint8_t teamColour;          // colour of the team
        uint8_t goalColour;          // colour of the goal
        uint8_t score;               // team's score
        RobotInfo players[MAX_NUM_PLAYERS];       // the team's players
    };

    struct RoboCupGameControlData {
        char   header[4];           // header to identify the structure
        uint32_t version;             // version of the data structure
        uint8_t playersPerTeam;       // The number of players on a team
        uint8_t state;                // state of the game (STATE_READY, STATE_PLAYING, etc)
        uint8_t firstHalf;            // 1 = game in first half, 0 otherwise
        uint8_t kickOffTeam;          // the next team to kick off
        uint8_t secondaryState;       // Extra state information - (STATE2_NORMAL, STATE2_PENALTYSHOOT, etc)
        uint8_t dropInTeam;           // team that caused last drop in
        uint16_t dropInTime;          // number of seconds passed since the last drop in.  -1 before first dropin
        uint32_t secsRemaining;       // estimate of number of seconds remaining in the half
        TeamInfo teams[2];
    };

    enum {
        STATE_INITIAL   = 0,
        STATE_READY     = 1,
        STATE_SET       = 2,
        STATE_PLAY      = 3,
        STATE_FINISHED  = 4

    };

    enum {
        NO_PENALTY                              = 0,        // CIB (non-standard, but pretty intuitive)
        PENALTY_BALL_MANIPULATION               = 1,
        PENALTY_PHYSICAL_CONTACT                = 2,
        PENALTY_ILLEGAL_ATTACK                  = 3,
        PENALTY_ILLEGAL_DEFENSE                 = 4,
        PENALTY_REQUEST_FOR_PICKUP              = 4,
        PENALTY_REQUEST_FOR_SERVICE             = 5,
        PENALTY_REQUEST_FOR_PICKUP_2_SERVICE    = 7
    };
    // copied from GameController
    /***************************************************************************************************************/

    static const int PLAYER_INFO_LENGTH = 4;
    static const int TEAM_INFO_LENGTH = 4 + MAX_NUM_PLAYERS * PLAYER_INFO_LENGTH;
    static const int REFEREE_PACKET_LENGTH = 4 +                            // header
                                            16 +                            // GameControlData
                                            2* TEAM_INFO_LENGTH;            // 2x teams

    static const int PACKET_DEGREE_PRECISION = 100.0;

    static uint16_t Angle2Packet(double angle);
    static uint16_t Range2Packet(double range);
    static double Packet2Angle(uint16_t angle);
    static double Packet2Range(uint16_t range);

    struct RangeData
    {
        uint16_t range;     // mm
        uint16_t angle;     // deg * 100
    };

    struct GoalData
    {
        struct RangeData leftPost;
        struct RangeData rightPost;
    };

    struct PositionData
    {
        uint16_t x;     // mm
        uint16_t y;     // mm
        uint16_t a;     // deg * 100
    };

    struct RobotStateInformation
    {
        char header[4];
        uint16_t sequenceNum;
        uint8_t myId;

        struct PositionData myPosition;

        struct RangeData ball;
        struct GoalData myGoal;
        struct GoalData theirGoal;

        // positions of opposing robots
        struct RangeData myTeam[2];
        struct RangeData theirTeam[3];

        uint8_t myState;
        uint8_t playSubstate;
        uint8_t gameState;

        uint8_t checksum;
    };
    static const int UOFM_NUM_BYTES = 57;

    void transmitPacket(struct RobotStateInformation &packetData);

    // can a given robot play? (i.e. will the referee allow it?)
    bool canRobotPlay(int team, int id);
    bool isRobotOnPenalty(int team, int id);

    // print the last packet we received
    void printLastPacket();

    int getGameState() { return gameControlData.state; }
    int getTimeRemaining() { return gameControlData.secsRemaining; }
    int getKickoffTeam() { return gameControlData.kickOffTeam; }
    bool isFirstHalf() { return gameControlData.firstHalf; }
    bool isSecondtHalf() { return !gameControlData.firstHalf; }

    struct RoboCupGameControlData *getGameControlData() { return &gameControlData; }
    struct RobotStateInformation *getTeamData(int id) { return &(teamData[id-1]); }

    void processUdpDatagram(char *data, int numBytes);
    void updateControlData(char *packetData, int numBytes);
    void parseUofMPacket(char *data, int numBytes);

private:

    struct RoboCupGameControlData gameControlData;
    struct RobotStateInformation teamData[3];   // packet data from each robot on my team (including me)

    int portNo;
#ifndef USE_Q_SOCKET
    int recvSock;
    int sendSock;

    char buffer[256];

    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr;
#else
    QUdpSocket qSocket;
#endif
    pthread_t monitorThreadId;

    static const int BUFFER_LENGTH = 65536;
    char sockBuffer[BUFFER_LENGTH];
    int bufferLength;
    pthread_mutex_t socketBufferLock;

    // threaded functions for reading & validating incoming packets
    static void *listenThread(void* arg);
};

#endif // SOCKETMONITOR_H
