#include "PlayerPacket.h"
#include <iostream>
#include <SocketMonitor.h>
#include <cstdio>

using namespace std;

PlayerPacket::PlayerPacket()
{
    this->id = -1;
    this->lastSequenceNum = -1;
    pthread_mutex_init(&objectLock,NULL);
}

PlayerPacket::PlayerPacket(int id)
{
    this->id = id;
    this->lastSequenceNum = -1;
    pthread_mutex_init(&objectLock,NULL);
}

PlayerPacket::~PlayerPacket()
{
	vector<UnitedSoccerObject*>::iterator it;
	for( it = objects.begin(); it != objects.end(); it++ ) {
		delete *it;
	}
    objects.clear();
}

void PlayerPacket::parsePacket(int packetLength, char *packetData)
{
    int robotId;
    int sequenceNumber;
    int numObjects;
    int type;
    int field1_16;
    int field2_16;
    int field3_16;
    int field4_8;

    pthread_mutex_lock(&objectLock);

    UnitedSoccerObject *newObject;

    // kick out if the ID is invalid
    robotId = (int)packetData[4];
    if(robotId != this->id)
    {
#ifdef SOCKET_DEBUG
        cerr << "Invalid ID: found " << robotId << " expected " << this->id << endl;
#endif
        pthread_mutex_unlock(&objectLock);
        return;
    }

    // kick out if this sequence number is smaller or equal to the last one we recieved
    sequenceNumber = packetData[5];
    sequenceNumber = (sequenceNumber << 8) | packetData[6];

    if(sequenceNumber < this->lastSequenceNum)
    {
#ifdef SOCKET_DEBUG
        cerr << "Ignoring out-of-sequence packet: received " << sequenceNumber << " expected >" << this->lastSequenceNum << endl;
#endif
        pthread_mutex_unlock(&objectLock);
        return;
    }
    else
        this->lastSequenceNum = sequenceNumber;


    // otherwise parse the list of objects this robot found
    //while(objectsLocked)
    //    usleep(1000);
    objects.clear();

    numObjects = (int)packetData[7];
#ifdef SOCKET_DEBUG
    cout << "Packet contains " << numObjects << " objects" << endl;
#endif

    for(int i=0; i<numObjects && i < UnitedSoccerObject::MAX_NUM_OBJECTS && i * UnitedSoccerObject::BYTES_PER_OBJECT < packetLength; i++)
    {
        type = (int)packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+0];

        field1_16 = packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+1];
        field1_16 = (field1_16 << 8) | packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+2];

        field2_16 = packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+3];
        field2_16 = (field2_16 << 8) | packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+4];

        field3_16 = packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+5];
        field3_16 = (field3_16 << 8) | packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+6];

        field4_8 = (int)packetData[8+i*UnitedSoccerObject::BYTES_PER_OBJECT+7];
#ifdef SOCKET_DEBUG
        cout << "\tFound object of type " << type << " [" <<
                field1_16 << " " <<
                field2_16 << " " <<
                field3_16 << " " <<
                field4_8 << "] ";
#endif


        if(type <= UnitedSoccerObject::STATE_TYPE)
        {
            newObject = new StateObject(type, field1_16, field2_16, field3_16, field4_8);
#ifdef SOCKET_DEBUG
            cout << "(StateObject)" << endl;
#endif
        }
        else if(type <= UnitedSoccerObject::PERCEPTION_TYPE)
        {
            newObject = new PerceptionObject(type, field1_16, field2_16, field3_16, field4_8);
#ifdef SOCKET_DEBUG
            cout << "(PerceptionObject)" << endl;
#endif
        }
        else if(type <= UnitedSoccerObject::INTENT_TYPE)
        {
            newObject = new IntentObject(type, field1_16, field2_16, field3_16, field4_8);
#ifdef SOCKET_DEBUG
            cout << "(IntentObject)" << endl;
#endif
        }
        else if(type <= UnitedSoccerObject::REFEREE_TYPE)
        {
            newObject = new RefereeObject(type, field1_16, field2_16, field3_16, field4_8);
#ifdef SOCKET_DEBUG
            cout << "(RefereeObject)" << endl;
#endif
        }
        else
        {
            newObject = new ExtensionObject(type, field1_16, field2_16, field3_16, field4_8);
#ifdef SOCKET_DEBUG
            cout << "(ExtensionObject)" << endl;
#endif
        }

        objects.push_back(newObject);
    }

    pthread_mutex_unlock(&objectLock);
}
