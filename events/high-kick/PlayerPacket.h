#ifndef UNITEDSOCCERPLAYER_H
#define UNITEDSOCCERPLAYER_H

#include "PacketObject.h"
#include <vector>
#include <pthread.h>

class PlayerPacket
{
public:
    PlayerPacket();
    explicit PlayerPacket(int id);
    virtual ~PlayerPacket();

    pthread_mutex_t objectLock;

    std::vector<UnitedSoccerObject*> objects;
    int lastSequenceNum;

    virtual void parsePacket(int packetLength, char* packetData);

    static const int REF_ID = 32;

private:
	PlayerPacket(PlayerPacket&);
	PlayerPacket& operator=(PlayerPacket&);

    int id;
};

#endif // UNITEDSOCCERPLAYER_H
