#include "localizationhint.h"
#include "util.h"

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <vector>
#include <cmath>

using namespace cv;
using namespace std;

// names of the windows for training/tracking
const char *LocalizationHint::MAIN_WINDOW = "LH Main";
const char *LocalizationHint::THRESHOLD_WINDOW = "LH Threshold";

// singleton instance of the goal post tracker
LocalizationHint *LocalizationHint::trackerInstance = NULL;

// private, forcing us to use a singleton approach
LocalizationHint::LocalizationHint()
{

	fieldThreshold = 155;
	houghMinVotes = 35;//50;//90;
	houghMinLength = 50;//150;//230;//64;//70;
	houghMaxGap = 45;//170;//100;//50;

}

// destructor
LocalizationHint::~LocalizationHint()
{
	cout << "LocalizationHint: Closing Tracking Windows" << endl;
	
	closeTrackingWindows();
	
	trackerInstance = NULL;
}

// factory method that returns our singleton instance
LocalizationHint *LocalizationHint::create()
{
	if(trackerInstance == NULL)
	{
		trackerInstance = new LocalizationHint();
	}
	
	return trackerInstance;
}

// pass in a frame for the high gui windows and the goal post detection
void LocalizationHint::update(Mat &frame)
{
	frame.copyTo(originalFrame);
}

// returns the last frame passed in
void LocalizationHint::getOriginalFrame(Mat &frame)
{
	originalFrame.copyTo(frame);
}

int LocalizationHint::countParallelFieldLines()
{

	const Scalar MERGED_LINE_COLOR(0, 0, 255);	// red
	const Scalar LINE_COLOR(0, 255, 255);	    // yellow

	const int CANNY_THRESHOLD1 = 160;
	const int CANNY_THRESHOLD2 = 200;
	
	const float MAX_SLOPE_DIFFERENCE = 0.1;		// maximum allowed difference between two lines'
												// slopes above which they are not considered parallel
	
	const int FIELD_THRESHOLD = 155;
	const int MIN_HOUGH_VOTES = 35;				// higher votes means less lines
	const int MIN_LINE_LENGTH = 170;			// minimum allowed line length
	const int MAX_LINE_GAP = 60;				// maximum allowed space between pixels
												// which could form a potential line

	vector<Vec4i> lines;						// initial group of lines found by Hough line detector
	vector<Vec4i> merged;						// merged group of lines according to slope
	
	unsigned int i;
	
	// we only need a greyscale image
	cvtColor(originalFrame, thresholdFrame, CV_BGR2GRAY);
	
	// eliminate obvious noise and do a threshold based on our slider
	erode(thresholdFrame, thresholdFrame, Mat(), Point(-1, -1), 2);
	dilate(thresholdFrame, thresholdFrame, Mat(), Point(-1, -1), 2);
	threshold(thresholdFrame, thresholdFrame, FIELD_THRESHOLD, 256, THRESH_BINARY);
	
	// perform canny edge detection on our thresholded image
	Canny(thresholdFrame, thresholdFrame, CANNY_THRESHOLD1, CANNY_THRESHOLD2, 3);
	
	// perform probabilistic hough-space line detection
	HoughLinesP(thresholdFrame, lines, 1, CV_PI/180, MIN_HOUGH_VOTES, MIN_LINE_LENGTH, MAX_LINE_GAP);
	
	//merged.clear();
	//mergeLines(lines, merged, 20);
	
	// show debugging output?
	if(windowsOpen)
	{
	
		// draw the detected lines
		for(i = 0; i < lines.size(); i++ )
		{
			line(originalFrame, Point(lines[i][0], lines[i][1]),
				 Point(lines[i][2], lines[i][3]), LINE_COLOR, 3, CV_AA);
		}
		
		// update window contents
		imshow(MAIN_WINDOW, originalFrame);
		imshow(THRESHOLD_WINDOW, thresholdFrame);
	
	}
	
	return countParallelLines(lines, MAX_SLOPE_DIFFERENCE);

}

void LocalizationHint::findFieldLineCorners(vector<Vec2i> &corners)
{

	const Scalar CORNER_COLOR(0, 0, 255);	// red
	const Scalar LINE_COLOR(0, 255, 255);	// yellow

	const int CANNY_THRESHOLD1 = 160;
	const int CANNY_THRESHOLD2 = 200;

	const int FIELD_THRESHOLD = 155;
	const int MIN_HOUGH_VOTES = 35;			// higher votes means less lines
	const int MIN_LINE_LENGTH = 50;			// minimum allowed line length
	const int MAX_LINE_GAP = 45;			// maximum allowed space between pixels
											// which could form a potential line

	vector<Vec4i> lines;				// initial group of lines found by Hough line detector
	
	unsigned int i;
	
	// we only need a greyscale image
	cvtColor(originalFrame, thresholdFrame, CV_BGR2GRAY);
	
	// eliminate obvious noise and do a threshold based on our slider
	erode(thresholdFrame, thresholdFrame, Mat(), Point(-1, -1), 2);
	dilate(thresholdFrame, thresholdFrame, Mat(), Point(-1, -1), 2);
	threshold(thresholdFrame, thresholdFrame, FIELD_THRESHOLD, 256, THRESH_BINARY);
	
	// perform canny edge detection on our thresholded image
	Canny(thresholdFrame, thresholdFrame, CANNY_THRESHOLD1, CANNY_THRESHOLD2, 3);
	
	// perform probabilistic hough-space line detection
	HoughLinesP(thresholdFrame, lines, 1, CV_PI/180, MIN_HOUGH_VOTES, MIN_LINE_LENGTH, MAX_LINE_GAP);
	
	// try to join these lines and find their corners according to their slopes
	findCorners(lines, corners);
	
	// show debugging output?
	if(windowsOpen)
	{
	
		// draw the detected lines
		for(i = 0; i < lines.size(); i++ )
		{
			line(originalFrame, Point(lines[i][0], lines[i][1]),
				 Point(lines[i][2], lines[i][3]), LINE_COLOR, 3, CV_AA);
		}
	
		// draw the detected corners
		for(i = 0; i < corners.size(); i ++)
		{
			circle(originalFrame, Point(corners[i][0], corners[i][1]), 5, CORNER_COLOR, 2);
		}
		
		// update window contents
		imshow(MAIN_WINDOW, originalFrame);
		imshow(THRESHOLD_WINDOW, thresholdFrame);
	
	}

}

// of the list of lines passed in, return the number of lines that are parallel (or close to parallel)
int LocalizationHint::countParallelLines(vector<Vec4i> &lines, float maxSlopeDifference)
{

	vector<Vec4i>::iterator i, j;
	float slope1, slope2;
	int count = 0;
	
	for(i = lines.begin(); i != lines.end(); i ++)
	{
	
		slope1 = Util::computeSlope(*i);
	
		for(j = i + 1; j != lines.end(); j ++)
		{
			
			slope2 = Util::computeSlope(*j);
			if(fabs(slope1 - slope2) < maxSlopeDifference)
			{
				count ++;
			}
			
		}
	
	}
	
	return count;

}

// returns the list of corners detected (by slope and endpoint distance) of the given list of lines
void LocalizationHint::findCorners(vector<Vec4i> &lines, vector<Vec2i> &corners)
{

	const int MIN_END_POINT_DIST = 15;	// max distance between two line endpoints
										// for them to be considered possibly joined

	vector<Vec4i>::iterator i, j;
	float k;							// arbitrary slope center for comparing two lines
	
	Vec4i line1, line2;					// current pair of lines we're considering
	float slope1, slope2;				// slope of above lines
	float slopeDiff1, slopeDiff2;		// difference in slope from an arbitrary slope value
	Vec2i connect;						// detected corner of two joined lines
	
	bool added;
	bool slopesApart;
	bool slopesOpposite;
	bool slopesDifferent;
	
	corners.clear();					// passed by ref, so we clear it first in case caller didn't
	
	for(i = lines.begin(); i != lines.end(); i ++)
	{
	
		for(j = i + 1; j != lines.end(); j ++)
		{
		
			line1 = *i;
			line2 = *j;
		
			// determine if the lines are connected, at least
			if(Util::endPointDistance(line1, line2, connect) < MIN_END_POINT_DIST)
			{
			
				k = 0.81;
				added = false;
				while(!added && k >= 0.09)
				{
					
					// compute the slope of each line using our utilities
					slope1 = Util::computeSlope(line1);
					slope2 = Util::computeSlope(line2);
					
					// get the slope distance from our arbitrary slope value
					slopeDiff1 = fabs(k - slope1);
					slopeDiff2 = fabs(k - slope2);
					
					// ensure that the lines are about the same distance away from k
					slopesApart = (fabs(slopeDiff1 - slopeDiff2) < 0.075);
					
					// ensure that the lines' slopes are on either side of k, our
					// arbitrary slope
					slopesOpposite = (slope1 < k && slope2 > k) ||
									 (slope1 > k && slope2 < k);
					
					// ensure that we don't have two mostly-overlapping lines
					slopesDifferent = (fabs(slope1 - slope2) > 0.05);
					
					// if all these conditions are met, we've likely found a corner
					if(slopesApart && slopesOpposite && slopesDifferent)
					{
						corners.push_back(connect);
						added = true;					// stop comparing these two lines
					}
					
					k -= 0.07;
					
				}
			
			}
		
		}
	
	}

}

// opens training windows, and a window that labels the locations of the left and right goal posts
void LocalizationHint::openTrackingWindows()
{

	cout << "LocalizationHint: Opening Tracking Windows" << endl;

	namedWindow(MAIN_WINDOW, 1);
	namedWindow(THRESHOLD_WINDOW, 1);

	// only the first window is responsible for training
	cvCreateTrackbar("threshold", MAIN_WINDOW, &fieldThreshold, 254, NULL);
	cvCreateTrackbar("min votes", MAIN_WINDOW, &houghMinVotes, 250, NULL);
	cvCreateTrackbar("min length", MAIN_WINDOW, &houghMinLength, 250, NULL);
	cvCreateTrackbar("max gap", MAIN_WINDOW, &houghMaxGap, 250, NULL);

	windowsOpen = true;

}

// closes the training windows
void LocalizationHint::closeTrackingWindows()
{
	destroyWindow(MAIN_WINDOW);
	destroyWindow(THRESHOLD_WINDOW);
}

// maxDistance is the maxmimum distance of two lines' endpoints at which
// the lines will be merged into one; minVotes is the minimum number of lines
// that must be nearby for them to be considered a merged line and not discarded
void LocalizationHint::mergeLines(vector<Vec4i> &lines, vector<Vec4i> &merged, int maxDistance)
{

	vector<Vec4i>::iterator i;
	vector<Vec4i>::iterator j;
	vector<int>::iterator m;
	vector<int>::iterator n;
	vector<int> votes;
	
	int voteCount;
	unsigned int k;
	
	Vec4i curr1;
	Vec4i curr2;
	float slope1, slope2;
	float slopeDiff;
	
	// each line has an initial merge vote of zero
	for(k = 0; k < lines.size(); k ++)
	{
		votes.push_back(0);
	}
	
	// begin traversing the lines and the vote count at the beginning
	i = lines.begin();
	m = votes.begin();
	
	while(i != lines.end())
	{
	
		// start at the line after this one, so we only consider line-line pairs once,
		// and also avoid comparing the same line
		j = i + 1;
		n = m + 1;
	
		while(j != lines.end())
		{
			
			curr1 = *i;
			curr2 = *j;
			
			// reorder the points of the two lines such that
			// their end points are opposite
			Util::orderLineLeftToRight(curr1);
			Util::orderLineLeftToRight(curr2);
			
			// get the slope of the first line
			slope1 = Util::computeSlope(curr1);
			
			// get the slope of the would-be resulting merged line
			Vec4i connect(curr1[2], curr1[3], curr2[0], curr2[1]);
			slope2 = Util::computeSlope(Vec4i(curr1[0], curr1[1], curr2[2], curr2[3]));
			
			// get the difference in slope between the first line and the possible resulting one
			slopeDiff = fabs(slope1 - slope2);
			
			// if the two lines' endpoints are sufficiently close,
			// merge the two lines into one
			if(Util::dist(Vec4i(curr1[0], curr1[1], curr2[2], curr2[3])) < maxDistance && slopeDiff < 0.015)
			{
			
				// remove the lines that were used to make the merge;
				// this needs to occur before we add the merged lines
				// so that we don't invalidate the iterators by
				// reallocating space for the vector by adding first
				lines.erase(j);
				i = lines.erase(i);
				j = i + 1;
				
				// same as above, only with the votes; ensure that
				// we sum the votes for both lines, and add those
				// votes to the newly created merged line
				voteCount = (*m) + (*n);
				votes.erase(n);
				m = votes.erase(m);
				n = m + 1;
			
				// merge the two lines and increase the vote count of the merged line
				lines.push_back(Vec4i(curr2[0], curr2[1], curr1[2], curr1[3]));//Util::averageLines(curr1, curr2));
				votes.push_back(voteCount + 1);
				
			}
			else if(Util::dist(Vec4i(curr1[2], curr1[3], curr2[0], curr2[1])) < maxDistance && slopeDiff < 0.015)
			{
			
				// remove the lines that were used to make the merge;
				// this needs to occur before we add the merged lines
				// so that we don't invalidate the iterators by
				// reallocating space for the vector by adding first
				lines.erase(j);
				i = lines.erase(i);
				j = i + 1;
				
				// same as above, only with the votes; ensure that
				// we sum the votes for both lines, and add those
				// votes to the newly created merged line
				voteCount = (*m) + (*n);
				votes.erase(n);
				m = votes.erase(m);
				n = m + 1;
			
				// merge the two lines and increase the vote count of the merged line
				lines.push_back(Vec4i(curr1[0], curr1[1], curr2[2], curr2[3]));//Util::averageLines(curr1, curr2));
				votes.push_back(voteCount + 1);
				
			}
			else
			{
				// we only increment the inner counter if we don't merge anything,
				// so that we don't skip over elements when we delete lines
				j ++;
				n ++;
			}
		
		}
		
		i ++;
		m ++;
	
	}
	
	// now, add the merged lines that have a sufficiently high merge count
	// to the final merge line list
	i = lines.begin();
	m = votes.begin();
	
	while(i != lines.end())
	{
		//merged.push_back(*i);
	
		if(*m >= 1)
		{
			merged.push_back(*i);
		}
		
		i ++;
		m ++;
	}

}
