#pragma once

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include <vector>

using namespace cv;
using namespace std;

class GoalieFinder
{

	private:

		// names of the windows for training/tracking
		static const char *MAIN_WINDOW;
		static const char *THRESHOLD_WINDOW;

		// we use this so we can refer to the GoalieFinder from a static context
		static GoalieFinder *trackerInstance;

		// OpenCV mat frames
		Mat originalFrame;
		Mat thresholdFrame;

		// threshold for tracking goal line and penalty marker
		int fieldThreshold;

		// angle of detected goal line
		int lastGoalLineAngle;
		Vec4i lastGoalLine;

		// starting values for the Hough-space line detector
		int houghMinVotes;
		int houghMinLength;
		int houghMaxGap;

		// indicates if the training and tracking windows are open
		bool windowsOpen;

		// maxDistance is the maxmimum distance of two lines' endpoints at which
		// the lines will be merged into one; minVotes is the minimum number of lines
		// that must be nearby for them to be considered a merged line and not discarded
		void mergeLines(vector<Vec4i> &lines, vector<Vec4i> &merged, int maxDistance, int minVotes);

		// sorts a series of lines by non-ascending order of length
		void sortLines(Vec4i *arr, int length);

		// private, forcing us to use a singleton approach
		GoalieFinder();

	public:

		// destructor
		~GoalieFinder();

		// factory method that returns our singleton instance
		static GoalieFinder *create();

		// widen the goal post color-tracking based on the passed color sample
		void train(Vec3i color);

		// remove the color-tracking data for the goal post
		void unTrain();

		// pass in a frame for the high gui windows and the goal post detection
		void update(Mat &frame);

		// returns the last frame passed in
		void getOriginalFrame(Mat &frame);

		// findGoalLine() will attempt to locate the goal line and store its angle,
		// which can then be retrieved by getLastGoalLineAngle() if it returns true
		bool findGoalLine();
		int getLastGoalLineAngle();
		Vec4i getLastGoalLine();

		// opens training windows, and a window that labels the locations of the left and right goal posts
		void openTrackingWindows();

		// closes the training windows, and the window that labels the locations of the left and right goal posts
		void closeTrackingWindows();

};
