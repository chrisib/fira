#ifndef WEIGHTLIFTING_EVENT_H
#define WEIGHTLIFTING_EVENT_H

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include "minIni.h"
#include "Point.h"
#include "Target.h"
#include "SingleBlob.h"
#include "Application.h"
#include <darwin/linux/YuvCamera.h>
#include <pthread.h>
#include <string>
#include <darwin/framework/MultiLine.h>

class WeightLiftingEvent : public Robot::Application
{
	public:
        WeightLiftingEvent();
        virtual ~WeightLiftingEvent();

        virtual void LoadIniSettings();
        virtual void Process(){}
        virtual void Execute();
        virtual bool Initialize();

        static const int MOTION_GRAB_BAR = 201;
        static const int MOTION_PICKUP_BAR_LIGHT = 202;
        static const int MOTION_PICKUP_BAR_HEAVY = 203;
        static const int MOTION_HOLD_BAR_WAIST = 210;
        static const int MOTION_RAISE_BAR_SHOULDER = 211;
        static const int MOTION_BIG_LIFT = 212;
        static const int MOTION_SETTLE_BACK = 213;

		
    private:

        enum STATE_T {
            GRABBING,
            FIRST_WALK,
            LIFTING,
            SECOND_WALK
        };
        int currentState;
		
		// Utility methods
        int ArmDistance(double angle) const;
        int GetLeanAngle(int armDistance) const;
        int BarWeight() const;
        void Stabilize() const;

        // dynamically adjust hip and ankle pitches based on FSR and Gyro data
        double hipPitch;
        bool Balance();
        void ResetBalance();
		
		// Action methods
        void PickUpBar();
        void LiftBar();
        void FirstWalk();
        void SecondWalk();
		
        // command-line options
        std::string iniFilePath;
        std::string motionFilePath;
        bool recordVideo;
        bool enableLog;
        bool skipFirstWalk;

        // Video Loop Function
        static void* videoLoop(void* arg);
		
		// Vision methods
        //Robot::SingleTarget *walkingTarget;
        Robot::MultiLine *walkingTarget;
        cv::Mat rgbFrame, processedFrame;
        cv::VideoWriter rgbStream, debugStream;
        void HandleVideo();
        void WriteVideo(std::string msg);

        // adjust walking trajectory based on visual feedback
        void SteerWithVision();

        // adjust walking trajectory based on gyroscope data (experimental, buggy)
        void SteerWithGyro();
		
        // how far do we need to walk?
		int walkDistance;
		
        // These are in the ini file, [Weight Lifting] section
		int leanOffset;
		int initialStagger;
		int lineWidth;
        int startingPosition;
        int CDs;                // overridable by command-line argument

		// Target variables
        bool alreadyFollowed;
		double previousTurnError;
		double currentTurnError;
		double turnP;
		double turnD;
		double maxTurn;
        double turnOffset;
        double turnGain;
		
		// Balance variables
		int balanceCount;
		int hipOffset;
		
		// Balance variables, from ini file
		int balanceLimit;
		int backLimit;
		int fwdLimit;
		int gyroLimit;

        // configuration/debug parameters
        bool liftOnly;

        std::string diskConfig;

        pthread_mutex_t visionSync;
};

#endif
