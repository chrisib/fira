#include "WeightLiftingEvent.h"
#include "LinuxDARwIn.h"
#include "Kinematics.h"
#include "CvCamera.h"
#include "SingleBlob.h"
#include <darwin/linux/CvCamera.h>
#include "Voice.h"
#include <cstdio>
#include <darwin/framework/MultiLine.h>

#define LIFT_WHILE_STANDING

using namespace std;
using namespace Robot;
using namespace cv;

// End the weight-lifting run early (for testing)
#define END_RUN do { cout << "Done for now" << endl; exit(EXIT_SUCCESS); } while(0)

// Approximate weight and length constants for the event
const double torsoWeight = 2460;
const int torsoLength = 120;
const double barBaseWeight = 50;
const double CD_WEIGHT = 16.4;
const int LEAN_LOAD = 25;

// Starting position distance information, everything in mm
const int LINE_DISTANCE = 500; // Distance between lines
const int FOOT_OFFSET = ((int) Kinematics::FOOT_LENGTH)/2; // Get middle of foot onto line

// vision
const int imgCenter = Camera::WIDTH/2;

minIni *ini = new minIni("config.ini");

WeightLiftingEvent::WeightLiftingEvent()
{
    this->RegisterArgument("--discs",&(this->CDs),-1,"Set the number of CDs to carry (overrides value in config.ini)");
    this->RegisterSwitch("--lift-only",&liftOnly, false,"Do not walk; just grab the bar, stand, and lift");
    this->RegisterArgument("--config",&iniFilePath,"config.ini","Set the config file path (default: config.ini)");
    this->RegisterSwitch("--record",&recordVideo,false,"Record video data to files");
    this->RegisterSwitch("--log",&enableLog,false,"Enable MotionManager logging");
    this->RegisterSwitch("--skip-first",&skipFirstWalk,false,"Skip the first walk and go straight to the lift + second walk");

    this->showVideo = false;
    balanceCount = 0;
    hipOffset = 0;
    previousTurnError = 0.0;
    currentTurnError = 0.0;
    walkDistance = 0;
    startingPosition = -90;
    walkingTarget = new MultiLine();

    rgbFrame = Mat(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    processedFrame = Mat(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);

    pthread_mutex_init(&visionSync,NULL);
    pthread_mutex_lock(&visionSync);
}

WeightLiftingEvent::~WeightLiftingEvent()
{
}

bool WeightLiftingEvent::Initialize()
{
    // create CV windows if necessary
    if(showVideo)
    {
        cv::namedWindow("Webcam");
        cv::namedWindow("Target");
    }

    if(!Application::Initialize())
        exit(1);

    if(!visionOnly)
    {
        cout << "Initializing other motion modules..." << endl;
        Walking::GetInstance()->m_Joint.SetEnableBody(false);
        RightArm::GetInstance()->m_Joint.SetEnableBody(false);
        RightArm::GetInstance()->m_Joint.SetEnableRightHandOnly(true,true);
        LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
        LeftArm::GetInstance()->m_Joint.SetEnableLeftHandOnly(true,true);
        Head::GetInstance()->m_Joint.SetEnableBody(false);
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);

        MotionManager::GetInstance()->AddModule(Head::GetInstance());
        MotionManager::GetInstance()->AddModule(Walking::GetInstance());
        MotionManager::GetInstance()->AddModule(RightArm::GetInstance());
        MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
        cout << "Done" << endl;
    }

    if(CDs <= 15)
        diskConfig = "10 Disks";
    else if(CDs <= 25)
        diskConfig = "20 Disks";
    else if(CDs <= 35)
        diskConfig = "30 Disks";
    else if(CDs <= 45)
        diskConfig = "40 Disks";
    else if(CDs <= 55)
        diskConfig = "50 Disks";
    else
        diskConfig = "60 Disks";

    return true;
}

void WeightLiftingEvent::LoadIniSettings()
{
    const int NO_VALUE = -500;
    double value;

    walkingTarget->LoadIniSettings(*ini,ini->gets("Weight Lifting","LineColour","Red"));

    if((value = ini->getd("Weight Lifting", "leanOffset")) != NO_VALUE)
        leanOffset = value;

    if((value = ini->getd("Weight Lifting", "initialStagger")) != NO_VALUE)
        initialStagger = value;

    if((value = ini->getd("Weight Lifting", "lineWidth")) != NO_VALUE)
        lineWidth = value;

    if((value = ini->getd("Weight Lifting", "startingPosition")) != NO_VALUE)
        startingPosition = (int)value;

    if((value = ini->getd("Weight Lifting", "CDs")) != NO_VALUE && CDs <= 0)
        CDs = (int)value;

    if((value = ini->getd("Weight Lifting", "turnP")) != NO_VALUE)
        turnP = value;

    if((value = ini->getd("Weight Lifting", "turnD")) != NO_VALUE)
        turnD = value;

    if((value = ini->getd("Weight Lifting", "maxTurn")) != NO_VALUE)
        maxTurn = value;

    if((value = ini->getd("Balance", "balanceLimit")) != NO_VALUE)
        balanceLimit = value;

    if((value = ini->getd("Balance", "backLimit")) != NO_VALUE)
        backLimit = value;

    if((value = ini->getd("Balance", "fwdLimit")) != NO_VALUE)
        fwdLimit = value;

    if((value = ini->getd("Balance", "gyroLimit")) != NO_VALUE)
        gyroLimit = value;

    //walkingTarget->LoadIniSettings(*ini, ini->gets("Target","colour","Yellow"));

    initialStagger = ini->getd("Weight Lifting","initialStagger",0.0);
    leanOffset = ini->getd("Weight Lifting","leanOffset",0.0);
    lineWidth = ini->getd("Weight Lifting","lineWidth",0.0);
    startingPosition = ini->getd("Weight Lifting","startingPosition",0.0);
    if(CDs == -1)
        CDs = ini->geti("Weight Lifting","CDs",CDs);
    maxTurn = ini->getd("Weight Lifting","maxTurn",0.0);
    turnP = ini->getd("Weight Lifting","turnP",0.0);
    turnD = ini->getd("Weight Lifting","turnD",0.0);
    turnGain = ini->getd("Weight Lifting","turnGain",0.0);

    motionFilePath = ini->gets("Files","motionFile","motion.bin");

    walkDistance = LINE_DISTANCE + FOOT_OFFSET + lineWidth * 2 + startingPosition; // = 682

    if(recordVideo)
    {
        string filename = ini->gets("Files","rgbStream","rgb.avi");
        rgbStream.open(filename,CV_FOURCC('D','I','V','X'),15,cvSize(Camera::WIDTH,Camera::HEIGHT),true);

        filename = ini->gets("Files","debugStream","debug.avi");
        debugStream.open(filename,CV_FOURCC('D','I','V','X'),15,cvSize(Camera::WIDTH,Camera::HEIGHT),true);
    }

    Walking::GetInstance()->LoadINISettings(ini);

}

int WeightLiftingEvent::ArmDistance(double angle) const
{
    angle = -angle;
    Point3D point;
    int hits = 0;
    double result = 0;
    double offset = 0;

    point = LeftArm::GetInstance()->GetPosition();
    result += point.X;
    offset += point.Y;
    hits++;

    point = RightArm::GetInstance()->GetPosition();
    result += point.X;
    offset += point.Y;
    hits++;

    //const int LENGTH = 90;
    result = (result) * (cos(deg2rad(angle))) / hits;
    offset = 0; // (offset + LENGTH) * sin(deg2rad(angle)) / hits;

    return static_cast<int>(result - offset);
}

int WeightLiftingEvent::GetLeanAngle(int armDistance) const
{
    int weight = BarWeight();
    double leanBack = weight * armDistance / torsoWeight;

    return leanOffset - rad2deg( asin(leanBack/torsoLength) );
}

int WeightLiftingEvent::BarWeight() const
{
    return static_cast<int>(barBaseWeight + CDs * CD_WEIGHT);
}

bool WeightLiftingEvent::Balance()
{
    int lfsr = MotionStatus::L_FSR_X;
    int rfsr = MotionStatus::R_FSR_X;
    int gyro = MotionStatus::FB_GYRO;

    // use fsr data from whatever foot is on the ground (or average if both are on the ground)
    int fsr;
    if(lfsr<0)
        fsr = rfsr;
    else if(rfsr<0)
        fsr = lfsr;
    else
        fsr = (lfsr+rfsr)/2;

    cout << "[balance] FSR: " << fsr << " (" << lfsr << ", " << rfsr << ") Gyro: " << gyro << endl;

    bool result = false;
    if((fsr >= 0 && fsr >= backLimit) || gyro > gyroLimit)
    {
        balanceCount++;
        cout << "[balance] leaning too far back (" << balanceCount << ")" << endl;
        if (balanceCount > balanceLimit)
        {
            hipOffset++;
            balanceCount = 0;
        }
    }
    else if ((fsr >= 0 && fsr <= fwdLimit) || gyro < -gyroLimit)
    {
        balanceCount--;
        cout << "[balance] leaning too far forward (" << balanceCount << ")" << endl;
        if (balanceCount < -balanceLimit)
        {
            hipOffset--;
            balanceCount = 0;
        }
    }
    else
    {
        result = true;
    }

    return result;
}

// Take a couple seconds to settle
void WeightLiftingEvent::Stabilize() const
{
    MotionManager::msleep(3000);
}

// Call this between balancing loops to make things more consistent
void WeightLiftingEvent::ResetBalance()
{
    balanceCount = 0;
    hipOffset = 0;
}

// TODO: Make snaps more dynamic
void WeightLiftingEvent::PickUpBar()
{
    Action::GetInstance()->m_Joint.SetEnableBody(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
    Head::GetInstance()->MoveByAngle(ini->geti("Weight Lifting","pan",0),ini->geti("Weight Lifting","tilt",0));

    RightArm::GetInstance()->OpenHand(1);
    LeftArm::GetInstance()->OpenHand(1);
    RightArm::GetInstance()->Finish();
    LeftArm::GetInstance()->Finish();

    Action::GetInstance()->Start(MOTION_GRAB_BAR);
    Action::GetInstance()->Finish();

    //Close the hands holding the bar without overtorquing
    RightArm::GetInstance()->MoveHand(-22, 1);
    LeftArm::GetInstance()->MoveHand(-22, 1);

    RightArm::GetInstance()->Finish();
    LeftArm::GetInstance()->Finish();

    if(CDs <= 35)
        Action::GetInstance()->Start(MOTION_PICKUP_BAR_LIGHT);
    else
        Action::GetInstance()->Start(MOTION_PICKUP_BAR_HEAVY);
    Action::GetInstance()->Finish();

    MotionManager::msleep(400); // Stabilize

    // Lean back for more weight
    /*if(CDs > LEAN_LOAD) {
        Action::GetInstance()->Start(203);
        Action::GetInstance()->Finish();
    }*/

    Action::GetInstance()->SetEnable(true, true);

    Action::GetInstance()->Start(MOTION_HOLD_BAR_WAIST);

    /*while(Action::GetInstance()->IsRunning())
    {
        cout<<"ANGLE: "<<Action::GetInstance()->m_Joint.GetAngle(Action::GetInstance()->m_Joint.ID_L_HIP_PITCH)<<endl;
    }*/

    // Snap
    /*if(CDs > LEAN_LOAD) {
        MotionManager::msleep(250);
        Walking::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HIP_PITCH, true, true);
        Walking::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HIP_PITCH, true, true);
        Walking::GetInstance()->HIP_PITCH_OFFSET = leanOffset;
        Walking::GetInstance()->EnterReadyPosition();
        MotionManager::Sync();
    }*/
    Action::GetInstance()->Finish();
}

void WeightLiftingEvent::LiftBar()
{
#ifdef LIFT_WHILE_STANDING
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    Walking::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HIP_PITCH, true, true);
    Walking::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HIP_PITCH, true, true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

    Action::GetInstance()->Start(MOTION_RAISE_BAR_SHOULDER);
    Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd(diskConfig, "pickUpHipPitch");
    LeftArm::GetInstance()->RelaxHand();
    RightArm::GetInstance()->RelaxHand();

    RightArm::GetInstance()->Finish();
    LeftArm::GetInstance()->Finish();
    Action::GetInstance()->Finish();

    MotionManager::msleep(1000);

    //Walking::GetInstance()->HIP_PITCH_OFFSET = 5;
    Action::GetInstance()->Start(MOTION_BIG_LIFT);
    Action::GetInstance()->Finish();

    Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd(diskConfig, "SettleBackHipPitch");
    Action::GetInstance()->Start(MOTION_SETTLE_BACK);

    while(Action::GetInstance()->IsRunning()) {
        //Balance();

        //int ry_fsr = MotionStatus::R_FSR_Y;
        //int ly_fsr = MotionStatus::L_FSR_Y;

        /*int angle = Walking::GetInstance()->HIP_PITCH_OFFSET;
        int distance = ArmDistance(angle);
        angle = GetLeanAngle(distance);
        Walking::GetInstance()->HIP_PITCH_OFFSET = angle; // + hipOffset;

        printf("\rW: %3d D: %3d A: %3d H: %03.2f",
            BarWeight(), distance, angle, Walking::GetInstance()->HIP_PITCH_OFFSET);

        //Walking::GetInstance()->EnterReadyPosition();
        //balance();*/
        MotionManager::Sync();
    }

    RightArm::GetInstance()->CloseHand();
    LeftArm::GetInstance()->CloseHand();

    RightArm::GetInstance()->Finish();
    LeftArm::GetInstance()->Finish();
    Action::GetInstance()->Finish();

#else
    Action::GetInstance()->m_Joint.SetEnableBody(false);
    Action::GetInstance()->m_Joint.SetEnableLowerBody(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);
    LeftArm::GetInstance()->RelaxHand();
    RightArm::GetInstance()->RelaxHand();

    // kneel down and then lock the legs in that position
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_SIT_DOWN);
    Action::GetInstance()->Finish();
    Action::GetInstance()->m_Joint.SetEnableBody(false);
    Action::GetInstance()->Start(MOTION_HOLD_BAR_WAIST);
    Action::GetInstance()->Finish();
    Action::GetInstance()->m_Joint.SetEnableUpperBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HAND,true,true);
    RightArm::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HAND,true,true);

    // lift the bar over the head
    Action::GetInstance()->Start(MOTION_RAISE_BAR_SHOULDER);
    Action::GetInstance()->Finish();
    Action::GetInstance()->Start(MOTION_BIG_LIFT);
    Action::GetInstance()->Finish();

    // stand back up again
    Action::GetInstance()->m_Joint.SetEnableBody(false);
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_SIT_DOWN);
    Action::GetInstance()->Finish();
    Action::GetInstance()->m_Joint.SetEnableLowerBody(true,true);
    Action::GetInstance()->Start(MOTION_BIG_LIFT);
    Action::GetInstance()->Finish();

    // give walking control of the hips so we can balance dynamically
    Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true,true);
    LeftArm::GetInstance()->m_Joint.SetEnableLeftHandOnly(true,true);
    RightArm::GetInstance()->m_Joint.SetEnableRightHandOnly(true,true);
    Walking::GetInstance()->m_Joint.SetEnable(JointData::ID_R_HIP_PITCH, true, true);
    Walking::GetInstance()->m_Joint.SetEnable(JointData::ID_L_HIP_PITCH, true, true);
    Walking::GetInstance()->HIP_PITCH_OFFSET = ini->getd(diskConfig, "SettleBackHipPitch");
    Action::GetInstance()->Start(MOTION_SETTLE_BACK);
    Action::GetInstance()->Finish(MotionManager::Sync);

    RightArm::GetInstance()->CloseHand();
    LeftArm::GetInstance()->CloseHand();

    RightArm::GetInstance()->Finish();
    LeftArm::GetInstance()->Finish();
    Action::GetInstance()->Finish();
#endif

    //Walking::GetInstance()->HIP_PITCH_OFFSET = snapBack;
    Walking::GetInstance()->EnterReadyPosition();
    ResetBalance();

}

void WeightLiftingEvent::FirstWalk()
{
    Walking::GetInstance()->LoadINISettings(ini, "First Leg");
    Walking::GetInstance()->X_MOVE_AMPLITUDE = ini->getd("First Leg", "x_amplitude");
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = ini->getd("First Leg", "y_amplitude");
    Walking::GetInstance()->Z_MOVE_AMPLITUDE = ini->getd("First Leg", "z_amplitude");
    Walking::GetInstance()->A_MOVE_AMPLITUDE = ini->getd("First Leg", "a_amplitude");
    turnOffset = Walking::GetInstance()->A_MOVE_AMPLITUDE;

    cout << "First Leg Walking Parameters: " << endl <<
            "\tX: " << Walking::GetInstance()->X_MOVE_AMPLITUDE << endl <<
            "\tY: " << Walking::GetInstance()->Y_MOVE_AMPLITUDE << endl <<
            "\tZ: " << Walking::GetInstance()->Z_MOVE_AMPLITUDE << endl <<
            "\tA: " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl <<
            "Distance: " << walkDistance << endl <<
            "Stagger: " << initialStagger << endl <<
            endl;

    Walking::GetInstance()->SetEnable(true, true);

    int numSteps = ini->getd(diskConfig, "numSteps");//Walking::GetSteps(walkDistance + initialStagger, Walking::GetInstance()->X_MOVE_AMPLITUDE) - 2;
    if (numSteps % 2 == 1) numSteps++; // Keep looking straight
    cout << "Taking " << numSteps << " steps" << endl;

    Walking::GetInstance()->Start();

    int startCount = Walking::GetInstance()->GetStepCount();
    int stepsRemaining = numSteps;
    bool keepWalking = true;
    while(keepWalking)
    {
        SteerWithVision();
        stepsRemaining = numSteps - (Walking::GetInstance()->GetStepCount()-startCount);
        cout << "Steps remaining in first walk: " << stepsRemaining << endl;

        if(stepsRemaining <= 0)
        {
            Walking::GetInstance()->Stop();
            keepWalking = false;
        }
    }
    Walking::GetInstance()->Finish();
}

inline void WeightLiftingEvent::SteerWithVision()
{
    pthread_mutex_lock(&visionSync);

    double turnAmount;
    vector<Line2D*> *lines = walkingTarget->GetAllLines();
    if(lines->size() > 0)
    {
        Line2D *line= lines->at(0);
        double slope = (line->start.Y - line->end.Y)/(line->start.X - line->end.X);

        turnAmount = maxTurn * slope * turnGain;

        if(turnAmount < -maxTurn)
            turnAmount = -maxTurn;
        else if(turnAmount > maxTurn)
            turnAmount = maxTurn;

        turnAmount += turnOffset;

        cout << "[info] Turning by: " << turnAmount << endl;

        Walking::GetInstance()->A_MOVE_AMPLITUDE = turnAmount;
    }
}

void WeightLiftingEvent::SecondWalk()
{
    //int prevZOffset = Walking::GetInstance()->Z_OFFSET;
    Walking::GetInstance()->LoadINISettings(ini, "Second Leg");
    Walking::GetInstance()->X_MOVE_AMPLITUDE = ini->getd("Second Leg", "x_amplitude");
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = ini->getd("Second Leg", "y_amplitude");
    Walking::GetInstance()->Z_MOVE_AMPLITUDE = ini->getd("Second Leg", "z_amplitude");
    Walking::GetInstance()->A_MOVE_AMPLITUDE = ini->getd("Second Leg", "a_amplitude");
    turnOffset = Walking::GetInstance()->A_MOVE_AMPLITUDE;
    const double leanOffset = ini->getd("Second Leg", "leanForwardSpeed");

    cout << "Second Leg Walking Parameters: " << endl <<
            "\tX: " << Walking::GetInstance()->X_MOVE_AMPLITUDE << endl <<
            "\tY: " << Walking::GetInstance()->Y_MOVE_AMPLITUDE << endl <<
            "\tZ: " << Walking::GetInstance()->Z_MOVE_AMPLITUDE << endl <<
            "\tA: " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl <<
            "Lean Offset: " << leanOffset << endl <<
            endl;

    //int targetZOffset = Walking::GetInstance()->Z_OFFSET;
    //Walking::GetInstance()->Z_OFFSET = prevZOffset;

    Walking::GetInstance()->SetEnable(true, true);
    double initialHipPitch = ini->getd(diskConfig,"2ndWalkInitHipPitch",-1);
    if(initialHipPitch < 0)
        initialHipPitch = ini->getd("Second Leg", "initialHipPitch");

    hipPitch = ini->getd(diskConfig,"2ndWalkTargetHipPitch",-1);
    if(hipPitch < 0)
        hipPitch = ini->getd("Second Leg", "targetHipPitch");

    Walking::GetInstance()->HIP_PITCH_OFFSET = initialHipPitch;

    if(Walking::GetInstance()->HIP_PITCH_OFFSET == hipPitch) { cout << "Are you sure this is right!?" << endl; }

    //cout << "DEBUG: " << Walking::GetInstance()->Z_OFFSET << " " << targetZOffset << endl;

    //Walking::GetInstance()->Z_OFFSET = targetZOffset;
    Walking::GetInstance()->EnterReadyPosition();

    MotionManager::msleep(1000);

    Walking::GetInstance()->Start();
    while(Walking::GetInstance()->IsRunning())
    {
        // adjust the goal hip pitch based on gyroscope and FSR data
        Balance();

        // adjust the actual hip pitch offset toward our target
        if(Walking::GetInstance()->HIP_PITCH_OFFSET > hipPitch)
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET -= leanOffset;
            if(Walking::GetInstance()->HIP_PITCH_OFFSET < hipPitch)
                Walking::GetInstance()->HIP_PITCH_OFFSET = hipPitch;
        }
        else if(Walking::GetInstance()->HIP_PITCH_OFFSET < hipPitch)
        {
            Walking::GetInstance()->HIP_PITCH_OFFSET += leanOffset;
            if(Walking::GetInstance()->HIP_PITCH_OFFSET > hipPitch)
                Walking::GetInstance()->HIP_PITCH_OFFSET = hipPitch;
        }


        // steer with vision
        SteerWithVision();


        //balance(); //here
        Walking::GetInstance()->HIP_PITCH_OFFSET = hipPitch + hipOffset; //here
    }
}

void WeightLiftingEvent::SteerWithGyro()
{
    // TODO
}

void WeightLiftingEvent::Execute()
{
    if(!visionOnly)
    {
        Voice::Speak("Press the middle button");
        cout << "Ready when you are" << endl;
        WaitButton(CM730::MIDDLE_BUTTON);
        cout << "Button pressed!  Starting the event" << endl;
        if(enableLog)
            MotionManager::GetInstance()->StartLogging();

        cout << "Picking up bar..." << endl;
        PickUpBar();
        cout << "done" << endl;

        if(!liftOnly && !skipFirstWalk)
        {
            cout << "Walking first leg..." << endl;
            Stabilize();
            FirstWalk();
            cout << "done" << endl;
        }

        cout << "Lifting bar over head..." << endl;
        Stabilize();
        LiftBar();
        cout << "done" << endl;

        if(!liftOnly)
        {
            cout << "Walking second leg..." << endl;
            Stabilize();
            SecondWalk();
            cout << "done" << endl;
        }
    }
    else
    {
        for(;;)
            pthread_mutex_lock(&visionSync);
    }
}

void WeightLiftingEvent::HandleVideo()
{
    CvCamera::GetInstance()->RGB_ENABLE = showVideo || recordVideo;


    CvCamera::GetInstance()->CaptureFrame();

    walkingTarget->FindInFrame(CvCamera::GetInstance()->yuvFrame, &processedFrame);
    pthread_mutex_unlock(&visionSync);

    if(showVideo || recordVideo)
    {
        CvCamera::GetInstance()->rgbFrame.copyTo(rgbFrame,Mat());

        walkingTarget->Draw(rgbFrame);
        walkingTarget->Draw(processedFrame);

        char buffer[255];
        cv::Point pt;
        vector<Line2D*> *lines = walkingTarget->GetAllLines();
        if(lines->size() > 0)
        {
            Line2D *line = lines->at(0);
            double slope = (line->start.Y - line->end.Y) / (line->start.X - line->end.X);
            double turnAmount = slope * maxTurn * turnGain + turnOffset;

            sprintf(buffer,"Turn: %0.2f",turnAmount);
            pt = cv::Point(0,24);
            cv::putText(rgbFrame,buffer,pt,CV_FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255));
            cv::putText(processedFrame,buffer,pt,CV_FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255));
        }

        sprintf(buffer,"FSR: (%d,%d) (%d,%d)",MotionStatus::L_FSR_X,MotionStatus::L_FSR_Y,MotionStatus::R_FSR_X,MotionStatus::R_FSR_Y);
        pt = cv::Point(0,Camera::HEIGHT-12);
        cv::putText(rgbFrame,buffer,pt,CV_FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255));
        cv::putText(processedFrame,buffer,pt,CV_FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255));

        sprintf(buffer,"Gyro: (%d,%d,%d)",MotionStatus::FB_GYRO, MotionStatus::RL_GYRO, MotionStatus::Z_GYRO);
        pt = cv::Point(0,Camera::HEIGHT-24);
        cv::putText(rgbFrame,buffer,pt,CV_FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255));
        cv::putText(processedFrame,buffer,pt,CV_FONT_HERSHEY_PLAIN,1.0,Scalar(255,255,255));

        if(showVideo)
        {
            imshow("Webcam", rgbFrame);
            imshow("Target", processedFrame);

            cvMoveWindow("Webcam",0,0);
            cvMoveWindow("Target",0,300);
            waitKey(1);
        }

        if(recordVideo)
        {
            rgbStream << rgbFrame;
            debugStream << processedFrame;
        }
    }
    /*
    stringstream ss;

    // Push data to ss
    WriteVideo(ss.str());

    // TODO: Add recordVideo option
    if(recordVideo)
    {
        webcamStream << rgbImage;
        debugStream << dbgImage;
    }
    instance->alreadyFollowed = false;*/
}

void* WeightLiftingEvent::videoLoop(void* arg)
{
    WeightLiftingEvent *instance = (WeightLiftingEvent *) arg;
    for(;;)
    {
        instance -> HandleVideo();

        // sleep 5 ms
        //MotionManager::GetInstance()->msleep(5,pauseThread);
    }

    pthread_exit(NULL);
    return NULL; // Unreachable
}



void pauseThread() { pthread_yield(); }

void WeightLiftingEvent::WriteVideo(string msg)
{
    if (msg.size() > 0) {
        cv::Mat& img = CvCamera::GetInstance()->yuvFrame;
        cv::Point pt = cv::Point(0, Camera::HEIGHT-5);
        int font = cv::FONT_HERSHEY_PLAIN;
        int fontScale = 1;
        cv::Scalar colour = cv::Scalar(0, 0, 0);
        cv::putText(img, msg, pt, font, fontScale, colour);
    }
 }
