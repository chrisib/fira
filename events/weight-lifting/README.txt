WEIGHT-LIFTING
Author (of this readme): Josh Jung
Primary Coder for 2013: Tiago Martins Araujo

The bulk of this code was written for FIRA 2012.  At that time, walking was performed with a step counter and dead reckoning.

For FIRA 2013, vision was incorporated to keep the walking straight.  Since small visual markers were disallowed, an on-site hack involving the LadderTarget class was used to guide the robot based on the slope of the field lines.

Also, the config file was set to accomodate several different numbers of discs specified by the --discs command line argument.  It will round to the nearest value that it has a configuration for.

The SNOBOTS placed first in weight-lifting in 2013 with lifts of 42 and 50 disks, scoring 18 points for the all-around.

Vision is handled continuously in a thread separate from the main state machine.

For a competitive run, the command line arguments --discs INTEGER_NUMBER_OF_DISCS should be used.

The program is started when the middle button is pressed.  It's important that the bar be positioned very near the robot's knees and offset a little to the right.

There are some important things to calibrate before running:
Vision - Standard calibration of the field lines (colour set in config.ini) using Darwinviewer
For each weight of disks, config.ini contains a section that sets parameters at various stages of the run:
    pickUpHipPitch - hip pitch angle used when the bar is lifted over the head
    settleBackHipPitch - hip pitch angle used when the bar is brought behind the head in preparation for walking
    numSteps - number of steps to the center (lift) line
    2ndWalkInitHipPitch - initial hip pitch angle used when starting to walk with the bar above the head
    2ndWalkTargetHipPitch - hip pitch eventually reached during the walk with the bar over the head

Unlike other events, weight-lifting does not have a well defined state machine. It does, however, call a few important functions:



Notes from 2012:

Weight-Lifting Event
Simon B-D

----------------------------------------------------------------------------------
Compilation:
Run make command using the included Makefile.  There are some pre-processor
definitions inside the Makefile that can be enabled/disabled to change
the program slightly.  Consult the Makefile for details.

---------------------------------------------------------------------------------
Execution:
To execute weight-lifting run:
    $ ./weight-lifting [--show-video]

--show-video will display the video stream in an x-window

------------------------------------------------------------------------------
How to Configure Weight-lifting:

Before each run make sure CDs is the correct number.
The leanOffset will be need to be tweaked for different weights and gaits.

You shouldn't have to touch snapBack, which is only need for heavier weights
(The code sets it to zero if CDs < 25 or so)
If the robot is falling over while standing up with the weights, use this to fix that

If the robot is having trouble starting the first leg, the options at the end of Second Lef can help

-------------------------------------------------------------------------------
Notes:
The program can use vision and the gyros to steer. It currently doesn't.
