#include "Util.h"

#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>

#include <cmath>
#include <climits>

using namespace cv;

// returns the average of 2 points in space
Point2f Util::average(Point2f a, Point2f b)
{

    Point2f ab = a + b;
    ab.x /= 2.0;
    ab.y /= 2.0;

    return ab;

}

// returns the average of 3 points in space
Point2f Util::average(Point2f a, Point2f b, Point2f c)
{

    Point2f ab = average(a, b);
    Point2f ac = average(a, c);
    Point2f bc = average(b, c);
    Point2f abc;

    abc = ab + ac + bc;
    abc.x /= 3.0;
    abc.y /= 3.0;

    return abc;

}

// returns the average of a vector of points in space
Point2f Util::average(const vector<Point2f>& points)
{

    Point2f result(0.0, 0.0);
    vector<Point2f>::const_iterator i;

    for(i = points.begin(); i != points.end(); i ++)
    {
        result = result + *i;
    }

    result.x /= (float)points.size();
    result.y /= (float)points.size();

    return result;

}

// returns the length of a given vector
float Util::length(Point2f v)
{
    return sqrt((v.x * v.x) + (v.y * v.y));
}

// returns the length of a given line
float Util::length(Vec4i line)
{
    return length(Point2f(line[2] - line[0], line[3] - line[1]));
}

// returns the distance between two points in space
float Util::dist(Point2f a, Point2f b)
{
    return length(a - b);
}

// returns the distance of a line
float Util::dist(Vec4i line)
{
    return dist(Point2f(line[0], line[1]), Point2f(line[2], line[3]));
}

// returns the angle of a given vector, from -180 to 180
float Util::angle(Point2f v)
{
    return 180.0 - radToDeg(atan2(v.x, v.y));
}

// normalizes the given vector
Point2f Util::normalize(Point2f v)
{

    const float LENGTH = length(v);
    Point2f result = v;

    result.x /= LENGTH;
    result.y /= LENGTH;

    return result;

}

Point2f Util::vecAtAngle(float angle)
{
    return Point2f(sinf(degToRad(angle)), cosf(degToRad(angle)));
}

// this method reorders the points of the two lines such that
// the ends of each line are closest to each other
void Util::orderLinePointsTogether(Vec4i &line1, Vec4i &line2)
{

    int swap1;
    int swap2;

    if(dist(Point2f(line1[0], line1[1]), Point2f(line2[0], line2[1])) >
       dist(Point2f(line1[0], line1[1]), Point2f(line2[2], line2[3])))
    {
        swap1 = line1[0];
        swap2 = line1[1];
        line1[0] = line1[2];
        line1[1] = line1[3];
        line1[2] = swap1;
        line1[3] = swap2;
    }

}

void Util::orderLineLeftToRight(Vec4i &line)
{

    int swap1;
    int swap2;

    if(line[0] > line[2])
    {
        swap1 = line[0];
        swap2 = line[1];
        line[0] = line[2];
        line[1] = line[3];
        line[2] = swap1;
        line[3] = swap2;
    }

}

void Util::orderLineTopToBottom(Vec4i &line)
{

    int swap1;
    int swap2;

    if(line[1] > line[3])
    {
        swap1 = line[0];
        swap2 = line[1];
        line[0] = line[2];
        line[1] = line[3];
        line[2] = swap1;
        line[3] = swap2;
    }

}

// this method assumes that Util::orderLinePoints() has been
// called on these two parameters; maxDistance is the maximum allowed
// distance that line ends can be apart from each other in order
// to be considered 'nearby'
bool Util::linePointsNearby(Vec4i line1, Vec4i line2, int maxDistance)
{
    return dist(Point2f(line1[0], line1[1]), Point2f(line2[0], line1[1])) < maxDistance &&
           dist(Point2f(line1[2], line1[3]), Point2f(line2[2], line2[3])) < maxDistance;
}

Vec4i Util::verticalMerge(Vec4i line1, Vec4i line2)
{

    int highest;
    int lowest;

    if(line1[1] < line2[1])
    {
        highest = line1[1];
    }
    else
    {
        highest = line2[1];
    }

    if(line1[3] > line2[3])
    {
        lowest = line1[3];
    }
    else
    {
        lowest = line2[3];
    }

    return Vec4i((line1[0] + line2[0]) / 2, highest,
                 (line1[2] + line2[2]) / 2, lowest);

}

// this method assumes that Util::orderLinePoints() has been
// called on these two parameters
Vec4i Util::averageLines(Vec4i line1, Vec4i line2)
{
    return Vec4i((line1[0] + line2[0]) / 2, (line1[1] + line2[1]) / 2,
                 (line1[2] + line2[2]) / 2, (line1[3] + line1[3]) / 2);
}

Vec4i Util::averageLinesToLongestVertical(Vec4i line1, Vec4i line2)
{

    int lowestY = INT_MAX;			// lowest-valued, not lowest-positioned
    int highestY = INT_MIN;			// highest-valued, not lowest-positioned
    int y;

    y = line1[1];
    if(y < lowestY)
    {
        lowestY = y;
    }

    y = line2[1];
    if(y < lowestY)
    {
        lowestY = y;
    }

    y = line1[3];
    if(y > highestY)
    {
        highestY = y;
    }

    y = line2[3];
    if(y > highestY)
    {
        highestY = y;
    }

    return Vec4i((line1[0] + line2[0]) / 2, lowestY, (line1[2] + line2[2]) / 2, highestY);

}

Vec4i Util::averageLinesToLongestVertical(vector<Vec4i> &lines)
{

    unsigned int i;
    int lowestY = INT_MAX;			// lowest-valued, not lowest-positioned
    int highestY = INT_MIN;			// highest-valued, not lowest-positioned
    int y;

    Vec4i line1, line2, newLine;

    for(i = 0; i < lines.size() - 1; i ++)
    {

        line1 = lines[i];
        line2 = lines[i + 1];

        y = line1[1];
        if(y < lowestY)
        {
            lowestY = y;
        }

        y = line2[1];
        if(y < lowestY)
        {
            lowestY = y;
        }

        y = line1[3];
        if(y > highestY)
        {
            highestY = y;
        }

        y = line2[3];
        if(y > highestY)
        {
            highestY = y;
        }

        newLine = Vec4i((line1[0] + line2[0]) / 2, lowestY, (line1[2] + line2[2]) / 2, highestY);

        lines.erase(lines.begin() + i);
        lines.erase(lines.begin() + i);
        lines.push_back(newLine);

    }

    return lines[0];

}

// this method computes the slope of a line (rise over run)
float Util::computeSlope(Vec4i line)
{

    int num = fabs(line[3] - line[1]);
    int den = fabs(line[2] - line[0]);
    float result = 1.0;

    if(den != 0)
    {
        result = (float)num / ((float)num + (float)den);
    }

    return result;

}

// this method returns the line with the longest length
Vec4i Util::getClosestLongestLine(const vector<Vec4i>& lines)
{

    vector<Vec4i>::const_iterator i;
    Vec4i curr;
    Vec4i longestSoFar;

    int currLength;
    double currY;
    int longestLengthSoFar = 0;
    double closestYSoFar = 0;
    double percentTolerance = 0.95;

    for(i = lines.begin(); i != lines.end(); i ++)
    {

        curr = *i;
        currLength = length(curr);
        currY = (double)(curr[1] + curr[3])/2.0;

        if(currLength > percentTolerance*longestLengthSoFar && currY > percentTolerance*closestYSoFar)
        {
            longestLengthSoFar = currLength;
            closestYSoFar = currY;
            longestSoFar = curr;
        }

    }

    return longestSoFar;

}

// this method returns the line with the longest length
Vec4i Util::getLongestLine(const vector<Vec4i>& lines)
{

    vector<Vec4i>::const_iterator i;
    Vec4i curr;
    Vec4i longestSoFar;

    int currLength;
    int longestLengthSoFar = 0;

    for(i = lines.begin(); i != lines.end(); i ++)
    {

        curr = *i;
        currLength = length(curr);

        if(currLength >longestLengthSoFar)
        {
            longestLengthSoFar = currLength;
            longestSoFar = curr;
        }

    }

    return longestSoFar;

}

// this method determines the closest distance between either two end points of a line,
// and returns the closest distance and the connecting point between them
int Util::endPointDistance(Vec4i line1, Vec4i line2, Vec2i &connect)
{

    int closestSoFar = INT_MAX;
    int dist = INT_MAX;

    Point2f point1;
    Point2f point2;
    int i, j;

    for(i = 0; i < 3; i += 2)
    {

        point1 = Point2f(line1[i], line1[i + 1]);

        for(j = 0; j < 3; j += 2)
        {

            point2 = Point2f(line2[j], line2[j + 1]);

            dist = Util::dist(point1, point2);
            if(dist < closestSoFar)
            {
                closestSoFar = dist;
                connect = point2fToVec2i(average(point1, point2));
            }

        }

    }

    return closestSoFar;
}

// converts radians to degrees
float Util::radToDeg(float rads)
{
    return rads * 180.0f / (atanf(1.0f) * 4.0f);
}

// converts degrees to radius
float Util::degToRad(float degrees)
{
    return degrees * ((atanf(1.0f) * 4.0f) / 180.0f);
}

// wraps the given angle between 0 and 360
float Util::wrapvalue(float val)
{

    float result = val;

    if(val >= 360.0)
        result = val - 360.0;
    else if(val < 0.0)
        result = 360.0 + val;

    return result;

}

Vec3b Util::matToVec3b(Mat m)
{
    return Vec3b(m.at<float>(0, 0), m.at<float>(0, 1), m.at<float>(0, 2));
}

Mat Util::vec3bToMat(Vec3b v)
{
    return (Mat_<float>(3,1) << v[0], v[1], v[2]);
}

Point2f Util::keyPointToPoint(KeyPoint keyPoint)
{
    return keyPoint.pt;
}

Vec2i Util::point2fToVec2i(Point2f point)
{
    return Vec2i(point.x, point.y);
}

Point2f Util::vec2iToPoint2f(Vec2i vec)
{
    return Point2f(vec[0], vec[1]);
}

int Util::getFurthestFromAverage(const vector<Point2f>& points)
{

    Point2f average = Util::average(points);

    int i;
    int furthest = 0;
    int furthestDist = 0;
    int dist;

    for(i = 0; i < (int)points.size(); i ++)
    {

        dist = length(points[i] - average);
        if(dist > furthestDist)
        {
            furthest = i;
            furthestDist = dist;
        }

    }

    return furthest;

}

float Util::areaToRadius(float a)
{
    return sqrt(a / 3.14159);
}

void Util::sortKeyPointsDownBySize(vector<KeyPoint>& keypoints)
{

    int i, j;
    KeyPoint curr;

    for(i = 1; i < (int)keypoints.size(); i ++)
    {

        curr = keypoints[i];

        for(j = i; j > 0 && keypoints[j - 1].size < curr.size; j --)
        {
            keypoints[j] = keypoints[j - 1];
        }

        keypoints[j] = curr;

    }

}
