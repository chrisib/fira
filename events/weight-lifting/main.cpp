#include "WeightLiftingEvent.h"
#include "MotionManager.h"
#include "MotionStatus.h"
#include <cstdlib>
#include <libgen.h>

using namespace std;
using namespace Robot;

bool changeToExeDir();

int main(int argc, char *argv[]) {
	if( !changeToExeDir() ) {
		cerr << "[ERROR] Could not change current dir to executable's dir" << endl;
		return EXIT_FAILURE;
    }
	
    WeightLiftingEvent* event = new WeightLiftingEvent();
    event->ParseArguments(argc, argv);
    event->Initialize();
	
    event->Execute();

	return EXIT_SUCCESS;
}

//------------------------------------------------------------
// changeToExeDir
//
// Change the program's current directory to the directory
// of this executable.
//
// Return: bool, true if successful.
//------------------------------------------------------------
bool changeToExeDir()
{
    char path[1024] = {0};

    if (-1 != readlink("/proc/self/exe", path, sizeof(path)))
    {
        if (-1 != chdir(dirname(path)))
        {
            return true;
        }
    }

    return false;
}
