##################################################
# PROJECT: DARwIn-OP Sample Makefile
# AUTHOR : Stela H. Seo <shb8775@hotmail.com>
#          Chris Iverach-Brereton <ve4cib@gmail.com>
##################################################

#---------------------------------------------------------------------
# Makefile template for Darwin projects
#
# This makefile is intended to be a blank template used for all AA Lab
# projects done using the Darwin robots.
#
# Please make sure to follow these instructions when setting up your
# own copy of this file:
#
#   1- Enter the name of the target (the TARGET variable)
#   2- Add additional source files to the SOURCES variable
#   3- Add additional static library objects to the OBJECTS variable
#      if necessary
#   4- Ensure that compiler flags, INCLUDES, and LIBRARIES are
#      appropriate to your needs
#
# note: libdarwin and libmlibrary must be compiled & installed
# libdarwin is included in this git project
# libmlibrary is part of MotionController_v2.0, also available on
# scm@sculpin.cs.umanitoba.ca
#
#
# (2012.JAN.19: libmlibrary is not presently used in libdarwin, but
# may be re-added at a later date; references to the motion library
# have been commented out below)
#
# To ensure compatibility with the latest version of GCC please make
# sure that any -l directives come *AFTER* any object files when
# compiling and linking.
#
# This makefile will link against several libraries, not all of which
# are necessarily needed for your project.  Please feel free to
# remove libaries you do not need.
#---------------------------------------------------------------------

# *** ENTER THE TARGET NAME HERE ***
TARGET      = TODO_TARGET_NAME

# important directories used by assorted rules and other variables
DIR_DARWIN  = /usr/local
DIR_OBJS    = .objects
DIR_QT      = /usr/include/qt4

# compiler options
CC          = gcc
CX          = g++
CCFLAGS     = -O2 -O3 -DLINUX -D_GNU_SOURCE -Wall $(INCLUDES) -g
CXFLAGS     = -O2 -O3 -DLINUX -D_GNU_SOURCE -Wall $(INCLUDES) -g
LNKCC       = $(CX)
LNKFLAGS    = $(CXFLAGS) #-Wl,-rpath,$(DIR_DARWIN)/lib

#---------------------------------------------------------------------
# Core components (all of these are likely going to be needed)
#---------------------------------------------------------------------
INCLUDES   += -I$(DIR_DARWIN)/include/darwin/framework 
INCLUDES   += -I$(DIR_DARWIN)/include/darwin/linux 
#INCLUDES   += -I$(DIR_DARWIN)/include/motionlibrary

LIBRARIES  += -ldarwin
LIBRARIES  += -ljpeg
LIBRARIES  += -lpthread 
LIBRARIES  += -ldl 
LIBRARIES  += -lespeak  
LIBRARIES  += -lopencv_core
LIBRARIES  += -lopencv_highgui
LIBRARIES  += -lopencv_imgproc 
#LIBRARIES  += -lmlibrary

#---------------------------------------------------------------------
# Qt components (not always needed)
#---------------------------------------------------------------------
INCLUDES   += -I$(DIR_QT)/QtCore 
INCLUDES   += -I$(DIR_QT)/QtGui 
INCLUDES   += -I$(DIR_QT)/QtXml 
INCLUDES   += -I$(DIR_QT)

LIBRARIES  += -lQtXml 
LIBRARIES  += -lQtGui 
LIBRARIES  += -lQtCore

#---------------------------------------------------------------------
# Files
#---------------------------------------------------------------------
SOURCES = main.cpp \
	# *** OTHER SOURCES GO HERE ***

OBJECTS  = $(addsuffix .o,$(addprefix $(DIR_OBJS)/,$(basename $(notdir $(SOURCES)))))
#OBJETCS += *** ADDITIONAL STATIC LIBRARIES GO HERE ***

#---------------------------------------------------------------------
# Compiling Rules
#---------------------------------------------------------------------
$(TARGET): make_directory $(OBJECTS)
	$(LNKCC) $(LNKFLAGS) $(OBJECTS) $(LIBRARIES) -o $(TARGET)

all: $(TARGET)

clean:
	rm -rf $(TARGET) $(DIR_OBJS) core *~ *.a *.so *.lo

make_directory:
	mkdir -p $(DIR_OBJS)/

$(DIR_OBJS)/%.o: %.c
	$(CC) $(CCFLAGS) -c $? -o $@

$(DIR_OBJS)/%.o: %.cpp
	$(CX) $(CXFLAGS) -c $? -o $@

# useful to make a backup "make tgz"
tgz: clean
	mkdir -p backups
	tar czvf ./backups/DARwIn_demo_`date +"%Y_%m_%d_%H.%M.%S"`.tgz --exclude backups *

archive: tgz

#---------------------------------------------------------------------
# End of Makefile
#---------------------------------------------------------------------

